<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Testshihan */

$this->title = 'Create Testshihan';
$this->params['breadcrumbs'][] = ['label' => 'Testshihans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="testshihan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
