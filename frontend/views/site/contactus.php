<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact Us';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-md-6">
    <div class="box box-default">
        <div class="box-header with-border">
            <i class="fa fa-life-ring"></i>
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            <h4>For further enquiries, please contact us at :</h4>
            <p><label>Call us :</label> 1-800-88-9338 (Mon – Fri, 9am-5pm)</p>
            <p><label>Email us :</label> customercare.my@akzonobel.com</p>

        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>

