<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\PointOrder;
use common\models\Redemption;

$this->title = Yii::t('app', "Redeem"); 
$this->params['breadcrumbs'][] = $this->title;


$trTotal = (new \yii\db\Query())->from('point_order po')
        ->join('JOIN', 'point_order_item poi', 'po.order_id = poi.point_order_id')
        ->where(['po.painter_login_id' => Yii::$app->user->getId(), 'po.order_status' => '17', 'poi.Item_status' => 'G'])
        //->count();
        ->sum('poi.total_qty_point');
if($trTotal > 0){
   $pointawerd = $trTotal;
}else{
   $pointawerd = '0'; 
}
$points_redemption = '0';
$payout = '0.00';
$points_redemption = \common\models\Redemption::find()->where(['painterID' => Yii::$app->user->getId(),'redemption_status' => '19'])->sum('req_points');
$payout = \common\models\Redemption::find()->where(['painterID' => Yii::$app->user->getId(),'redemption_status' => '19'])->sum('req_amount');
$points_balance = $pointawerd - $points_redemption;

//$allorder =
$allorder = PointOrder::find()->where(['painter_login_id' => Yii::$app->user->getId()])->count();
$opending = PointOrder::find()->where(['order_status' => '1', 'painter_login_id' => Yii::$app->user->getId()])->count();
$ocancel = PointOrder::find()->where(['order_status' => '7', 'painter_login_id' => Yii::$app->user->getId()])->count();
$oapprove = PointOrder::find()->where(['order_status' => '17', 'painter_login_id' => Yii::$app->user->getId()])->count();

//Redemption
$allredemption = Redemption::find()->where(['painterID' => Yii::$app->user->getId()])->count();
$rpending = Redemption::find()->where(['redemption_status' => '1', 'painterID' => Yii::$app->user->getId()])->count();
$rcancel = Redemption::find()->where(['redemption_status' => '7', 'painterID' => Yii::$app->user->getId()])->count();
$rapprove = Redemption::find()->where(['redemption_status' => '17', 'painterID' => Yii::$app->user->getId()])->count();

//Payout
$allpayout = Redemption::find()->where(['redemption_status' => '17', 'painterID' => Yii::$app->user->getId()])->count();
$p_processing = Redemption::find()->where(['redemption_status_ray' => '2', 'painterID' => Yii::$app->user->getId()])->count();
$p_paid = Redemption::find()->where(['redemption_status_ray' => '19', 'painterID' => Yii::$app->user->getId()])->count();
$p_failed = Redemption::find()->where(['redemption_status_ray' => '10', 'painterID' => Yii::$app->user->getId()])->count();

$po_total = Redemption::find()->where(['redemption_status_ray' => '19', 'painterID' => Yii::$app->user->getId()])->count();
$po_rm = Redemption::find()->where(['redemption_status_ray' => '19', 'painterID' => Yii::$app->user->getId()])->sum('req_amount');
$po_point = Redemption::find()->where(['redemption_status_ray' => '19', 'painterID' => Yii::$app->user->getId()])->sum('req_points');

$totalawpoint = PointOrder::find()->where(['order_status' => '17', 'painter_login_id' => Yii::$app->user->getId()])->sum('order_total_point');
$totalawrm = PointOrder::find()->where(['order_status' => '17', 'painter_login_id' => Yii::$app->user->getId()])->sum('order_total_amount');

$balancerm = $totalawrm - $po_rm;
$balancepoint = $totalawpoint - $po_point;

?>
<div class="col-xs-12">
  <div class="col-lg-4 col-sm-6 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-edit"></i> <?= Html::encode($this->title); ?></h3></div>
</div>

<div class="col-xs-6 col-lg-6">
  <div class="box-info box view-item col-xs-12 col-lg-12">
    <div class="form">
	<?php
        
        $model->point = $balancepoint;
        
        $form = ActiveForm::begin([
			'id' => 'change-password-form',
			'fieldConfig' => [
			    'template' => "{label}{input}{error}",
			],
    ]); ?>

	<?= $form->field($model, 'point')->textInput(['maxlength' => 1, 'placeholder' => $model->getAttributeLabel('point')]) ?>

	<?= $form->field($model, 'amount')->textInput(['maxlength' => 1, 'placeholder' => $model->getAttributeLabel('amount')]) ?>
        <?=
                                $form->field($model, 'type')->dropDownList(['S' => 'SandBox', 'L' => 'Live'], ['prompt' => '-- Select --']
                                )
                                ?> 
   <div class="form-group col-xs-12 col-sm-6 col-lg-4 no-padding">
            <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
	<?= Html::a(Yii::t('app', 'Cancel'), ['/site/index'], ['class' => 'btn btn-default']) ?>
     </div>
 <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>