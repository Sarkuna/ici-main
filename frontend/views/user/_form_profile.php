<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\date\DatePicker;

use common\models\TitleOptions;
use common\models\Race;
use common\models\Banks;
use common\models\Country;
use common\models\DealerList;
use common\models\PaintingJobs;
use common\models\Region;
use common\models\State;
/* @var $this yii\web\View */
/* @var $model app\modules\painter\models\PainterProfile */
/* @var $form yii\widgets\ActiveForm */

?>


<div class="col-xs-12 col-lg-12">
    <div class="painter-profile-form">
      <?php $form = ActiveForm::begin(); ?>

        <div class="box-success box view-item col-xs-12 col-lg-12">            
            <div class="box box-solid box-info col-xs-12 col-lg-12 no-padding">
                <div class="box-header with-border">
                    <h4 class="box-title"><i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Painter Profile Information'); ?></h4>
                </div>
                
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-lg-7 no-padding">
                        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                            <div class="col-xs-12 col-sm-4 col-lg-4">
                                <?=
                                $form->field($model, 'profile_title')->dropDownList(
                                        ArrayHelper::map(TitleOptions::find()->all(), 'id', 'title'), ['prompt' => '-- Select --']
                                )
                                ?>                
                            </div>
                            <div class="col-xs-12 col-sm-4 col-lg-8">
                                <?= $form->field($model, 'profile_full_name')->textInput(['maxlength' => true]) ?>               
                            </div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">                        

                            <div class="col-xs-12 col-sm-4 col-lg-5">
                                <?= $form->field($model, 'profile_mobile')->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
                                    //'mask' => '9999999999999',
                                    'mask' => '9',
                                    'clientOptions' => ['repeat' => 12, 'greedy' => false],
                                    'options' => ['placeholder' => '60XXXXXXXXX', 'class' => 'form-control',],
                                ]) ?> 
                                
                            </div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-lg-12">
                            <?= $form->field($model, 'profile_address')->textarea(['rows' => 5, 'cols' => 5]); ?>
                        </div>  
                        
                        
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-lg-5 no-padding">
                        <div class="col-xs-12 col-sm-3 col-lg-6">
                            <?= $form->field($model, 'profile_ic_no')->textInput(['maxlength' => true]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-3 col-lg-6">
                            <?=
                            $form->field($model, 'profile_race')->dropDownList(
                                    ArrayHelper::map(Race::find()->all(), 'id', 'title'), ['prompt' => '-- Select --']
                            )
                            ?> 
                        </div>

                        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                        <div class="col-xs-12 col-sm-3 col-lg-6">
                            <?=
                            $form->field($model, 'profile_nationality')->radioList(['L' => 'Malaysian', 'F' => 'Non Malaysian'], [
                                'prompt' => '-- Select --',
                                'onchange' => '
                                            var ck = $("input:radio[name=\'PainterProfile[nationality]\']:checked").val()
                                            $.get( "' . Url::toRoute('/painter/painterprofile/pickcountry') . '", {id: ck } )
                                                .done(function( data ) {
                                                    $("select#painterprofile-country").html(data);    
                                                }
                                            );'
                                    ]
                            )
                            ?>

                        </div>
                        <div class="col-xs-12 col-sm-3 col-lg-6">
                            <?=
                            $form->field($model, 'profile_country')->dropDownList(
                                    ArrayHelper::map(Country::find()
                                                    ->orderBy('name ASC')
                                                    ->all(), 'id', 'name'), ['prompt' => '-- Select --']
                            )
                            ?>
                        </div>
                        </div>
                    </div>
                </div> <!-- End personal Div -->
            </div>               
          
            
            
            
            


            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div> 
        </div>
        
      <?php ActiveForm::end(); ?>  
    </div>    
</div>

