<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\PointOrder;
use common\models\Redemption;

$this->title = Yii::t('app', "Payout Summery"); 
$this->params['breadcrumbs'][] = $this->title;

if (isset($response['failureReason'])) {
    $tr = '<tr><th>Reason</th><td>'.$response['failureReason'].'</td></tr>';
}else {
    $tr = '';
}

if($response['responseMessage'] == 'FAILURE') {
    $colur = 'danger';
}else {
    $colur = 'success';
}
?>


<div class="col-xs-6 col-lg-6 col-lg-offset-3">
  <div class="box-<?=$colur?> box view-item col-xs-12 col-lg-12">
      <h2 class="text-<?=$colur?> text-center"><?= $response['responseMessage'] ?> </h2>
    <div class="table-responsive">
        <table class="table">
            <tbody><tr>
                    <th style="width:50%">Code:</th>
                    <td><?= $response['responseCode'] ?></td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td><?= $response['responseMessage'] ?></td>
                </tr>
                <?= $tr ?>
                <tr>
                    <th>Description</th>
                    <td><?= $response['responseDescription'] ?></td>
                </tr>
            </tbody></table>
    </div>
  </div>
</div>