<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sms_queue".
 *
 * @property integer $sms_queue_id
 * @property integer $painterID
 * @property string $name
 * @property string $mobile
 * @property string $data
 * @property integer $email_template_id
 * @property string $date_to_send
 * @property string $created_datetime
 * @property string $status
 */
class SMSQueue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sms_queue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['painterID', 'name', 'mobile', 'data', 'email_template_id', 'date_to_send'], 'required'],
            [['painterID', 'email_template_id'], 'integer'],
            [['data'], 'string'],
            [['date_to_send', 'created_datetime'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['mobile'], 'string', 'max' => 20],
            [['status'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sms_queue_id' => 'Sms Queue ID',
            'painterID' => 'Painter ID',
            'name' => 'Name',
            'mobile' => 'Mobile',
            'data' => 'Data',
            'email_template_id' => 'Email Template ID',
            'date_to_send' => 'Date To Send',
            'created_datetime' => 'Created Datetime',
            'status' => 'Status',
        ];
    }
}
