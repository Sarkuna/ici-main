<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sms_setting".
 *
 * @property int $id
 * @property string $name
 * @property int $del
 */
class SMSSetting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_setting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'del'], 'required'],
            [['del'], 'integer'],
            [['name'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'del' => 'Del',
        ];
    }
}
