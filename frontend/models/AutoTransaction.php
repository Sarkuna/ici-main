<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auto_transaction".
 *
 * @property int $id
 * @property string $upload_date
 * @property string $desceription
 * @property int $upload_by
 */
class AutoTransaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auto_transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['upload_date', 'desceription', 'upload_by'], 'required'],
            [['upload_date'], 'safe'],
            [['desceription'], 'string'],
            [['upload_by'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'upload_date' => 'Upload Date',
            'desceription' => 'Desceription',
            'upload_by' => 'Upload By',
        ];
    }
}
