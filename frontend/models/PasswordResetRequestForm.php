<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use common\models\PainterProfile;
use common\models\ProfileUser;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }
        
        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->updated_by = $user->id;
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }
        $email = $this->email;
        $username = $user->username;
       
        //$returnedValue = 
        if ($user) {
            $user->updated_by = $user->id;
                    $user->save();
            $userid = $user->id;
            $user_type = $user->user_type;
            if($user_type == 'P'){
                $profile = PainterProfile::findOne([
                    'user_id' => $userid,
                ]);
                $username = $profile->full_name;
            }else{
                $profile = ProfileUser::findOne([
                    'user_id' => $userid,
                ]);
                $username = $profile->profile_full_name;
                
            }
            //$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
            $resetLink = Yii::$app->request->hostInfo.'/site/reset-password?token='.$user->password_reset_token;
            Yii::$app->ici->passwordResetEmail($email,$username,$resetLink);
            return true;
        }else{
           return false; 
        }
        /*return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Password reset for ' . Yii::$app->name)
            ->send();*/
    }
}
