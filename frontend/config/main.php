<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name'=>'ICIPaint',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
        
    'components' => [
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'jquery.js' : 'jquery.min.js'
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [
                        YII_ENV_DEV ? 'css/bootstrap.css' : 'css/bootstrap.min.css',
                    ]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'js/bootstrap.js' : 'js/bootstrap.min.js',
                    ]
                ]
            ],
        ],
        /*'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
        ],*/
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        /*'request' => [
            'csrfParam' => '_csrf-frontend',
        ],*/
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            //'authTimeout' => 86400,
            //'loginUrl' => ['site/login'],
            //'authTimeout' => 86400,
            //'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'ici' => [
            'class' => 'app\components\ICIComponent',
        ],
        /*'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],*/
        'view' => [
            'theme' => [
                'pathMap' => ['@app/views' => '@app/themes/AdminLTE'],
                'baseUrl' => '@app/themes/AdminLTE',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<alias:index|logout|login|request-password-reset|reset-password>' => 'site/<alias>', 
            ],
        ],
        'as beforeRequest' => [
            'class' => 'yii\filters\AccessControl',
            'rules' => [
                [
                    'actions' => ['login', 'error'],
                    'allow' => true,
                ],
                [

                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
        'formatter' => [
            'dateFormat' => 'd-m-Y',
            'datetimeFormat' => 'd-m-Y h.i A',
            'timeFormat' => 'h.i A',
            'defaultTimeZone' => 'Asia/Kuala_Lumpur', // time zone
            'thousandSeparator' => ',',
            'currencyCode' => 'MYR',
       ],
        
        
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Admin',
        ],
        'management' => [
            'class' => 'app\modules\management\Management',
        ],
        'support' => [
            'class' => 'app\modules\support\Support',
        ],
        'painter' => [
            'class' => 'app\modules\painter\Painter',
        ],
        'rights' => [
            'class' => 'mdm\admin\Module',
            'layout' => 'left-menu', // it can be '@path/to/your/layout'.
	    'controllerMap' => [
                 'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    'userClassName' => 'common\models\User',
                    'idField' => 'id', // id field of model User
		    'usernameField' => 'username', // username field of model User
                ],
            ],
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to  
            // use your own export download action or custom translation 
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],        
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'site/*',
            'admin/*',
            'gii/*',
            //'some-controller/some-action',
            // The actions listed here will be allowed to everyone including guests.
            // So, 'admin/*' should not appear here in the production, of course.
            // But in the earlier stages of your development, you may probably want to
            // add a lot of actions here until you finally completed setting up rbac,
            // otherwise you may not even take a first step.
        ]
    ],
    
    'params' => $params,
];
