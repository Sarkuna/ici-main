<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\filters\AccessControl;

use common\models\EmailTemplate;
use common\models\EmailTemplateSearch;

/**
 * EmailtemplateController implements the CRUD actions for EmailTemplate model.
 */
class EmailtemplateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        //'actions' => ['login','index', 'create', 'update', 'view'],
                        'actions' => ['login', 'error', 'request-password-reset', 'reset-password','about'],
                        'allow' => true,
                    ],
                    [
                        //'actions' => ['login','index', 'create', 'update', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all EmailTemplate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmailTemplateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EmailTemplate model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EmailTemplate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EmailTemplate();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            //Yii::$app->session->setFlash('success', 'New email template successfully created.');
            \Yii::$app->getSession()->setFlash('success',['title' => 'Template Created', 'text' => 'New email template successfully created.']);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing EmailTemplate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);            
            \Yii::$app->getSession()->setFlash('success',['title' => 'Template Updated', 'text' => 'New email template successfully updated.']);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionTestsh()
    {
        foreach (\common\models\EmailQueue::find()->where("status = 'P' AND email_template_global_id = '5' AND (email IS NOT NULL) AND (date_to_send IS NULL OR date_to_send <= CURDATE())")->limit(20)->all() as $emailQueue) {
            echo '<pre>';
            print_r($emailQueue).die;
        }
        foreach (\common\models\EmailQueue::find()->where("sms_status = 'P' AND email_template_global_id = '5' AND (mobile IS NOT NULL)")->limit(20)->all() as $emailQueue) {
            echo '<pre>'.print_r($emailQueue).die;
        }

        
    }
    /**
     * Deletes an existing EmailTemplate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EmailTemplate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmailTemplate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmailTemplate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
