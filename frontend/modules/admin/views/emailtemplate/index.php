<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\EmailTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Email Templates';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-lg-12">
    <div class="box email-template-index">
        <div class="box-header with-border">
            <div class="col-lg-8 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?></h3></div>
            <div class="col-lg-4 col-sm-2 col-xs-12 text-right no-padding">
                <p class="hide">
                    <?= Html::a('Create Email Template', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
            </div>
        </div>
        <div class="box-body table-responsive">

            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'id',
                    'code',
                    'name',
                    'subject',
                    [
                        'attribute' => 'email',
                        'label' => 'Email',
                        'format' => 'html',
                        'value' => function ($model) {
                            if($model->email == 1) {
                               return 'Yes'; 
                            }else {
                               return 'No'; 
                            }
                            
                        },
                    ],
                    [
                        'attribute' => 'sms',
                        'label' => 'SMS',
                        'format' => 'html',
                        'value' => function ($model) {
                            if($model->sms == 1) {
                               return 'Yes'; 
                            }else {
                               return 'No'; 
                            }
                            
                        },
                    ],           
                    //'template:ntext',
                    // 'created_datetime',
                    // 'updated_datetime',
                    // 'created_by',
                    // 'updated_by',
                    //['class' => 'yii\grid\ActionColumn'],
                    [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update}', //{view} {delete}
                            'buttons' => [
                                'update' => function ($url, $model) {
                                    return (Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('app', 'Edit'),]));
                                },/* ,
                                      'view' => function ($url, $model) {
                                      return (Html::a('<span class="glyphicon glyphicon-search"></span>', $url, ['title' => Yii::t('app', 'Update'),]));
                                      },
                                      'delete' => function ($url, $model) {
                                      return (Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('app', 'Delete'), 'data' => ['confirm' => 'Are you sure you want to delete this item?', 'method' => 'post'],]));
                                      } */
                                    ],
                                //'visible' => $visible,
                                ],
                ],
            ]);
            ?>
        </div>
    </div>
</div>