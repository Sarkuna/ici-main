<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'SMS Setting';
$listData= ArrayHelper::map(app\models\SMSSetting::find()->orderBy('name ASC')->all(), 'id', 'name');


$formFeatureModel = app\models\SMSSetting::find()->where(['del' => 1])->all();

$checkedFeatureArr = array();
foreach ($formFeatureModel as $data) {
    $checkedFeatureArr[$data->name] = $data->name;
}
$model->name = $formFeatureModel;
?>
<style>
.material-switch > input[type="checkbox"] {
    display: none;   
}

.material-switch > label {
    cursor: pointer;
    height: 0px;
    position: relative; 
    width: 40px;  
}

.material-switch > label::before {
    background: rgb(0, 0, 0);
    box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
    border-radius: 8px;
    content: '';
    height: 16px;
    margin-top: -8px;
    position:absolute;
    opacity: 0.3;
    transition: all 0.4s ease-in-out;
    width: 40px;
}
.material-switch > label::after {
    background: rgb(255, 255, 255);
    border-radius: 16px;
    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
    content: '';
    height: 24px;
    left: -4px;
    margin-top: -8px;
    position: absolute;
    top: -4px;
    transition: all 0.3s ease-in-out;
    width: 24px;
}
.material-switch > input[type="checkbox"]:checked + label::before {
    background: inherit;
    opacity: 0.5;
}
.material-switch > input[type="checkbox"]:checked + label::after {
    background: inherit;
    left: 20px;
}
</style>
<div class="login-box">
    <div class="login-box-body">
        <div class="point-order-form">
            <?php $form = ActiveForm::begin(); ?>
<h3 class="text-info">SMS Setting</h3>

<ul class="list-group">
 <?=

 $form->field($model, 'name')->checkboxList($listData, [

        	'onclick' => "$(this).val( $('input:checkbox:checked').val()); ",//if you use required as a validation rule you will need this for the time being until a fix is in place by yii2

        	'item' => function($index, $label, $name, $checked, $value) {
                if(!empty($checked)){
                    $chek = 'checked';
                }else {
                    $chek = '';
                }

            	return "<li class='list-group-item'>{$label}<div class='material-switch pull-right'><input id='someSwitchOptionDefault$index' $chek type='checkbox' {$checked} name='{$name}' value='{$value}'><label for='someSwitchOptionDefault$index' class='label-primary'></label></div></li>";

        	}

    	])->label(false)

    	?>
            


</ul>

            <button class="btn btn-info btn-flat btn-md" type="submit">Save</button>
        <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<?php
$script = <<< JS

$(document).ready(function () {
    $("#findpainter-type").change(function(){        
        var val = $('#findpainter-type').val();
        if(val == 'mid' ) {
            $('.field-findpainter-membership_id').show();
            $('.field-findpainter-ic_no').hide();
        } else {
            $('.field-findpainter-ic_no').show();
            $('.field-findpainter-membership_id').hide();
        }
    });
});

JS;
$this->registerJs($script);
?>