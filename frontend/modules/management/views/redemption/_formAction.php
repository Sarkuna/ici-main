<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Redemption */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="invoice no-margin">    
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-edit"></i> Action                   
            </h2>
        </div><!-- /.col -->
        <div class="col-xs-12">
        <div class="redemption-form">

            <?php $form = ActiveForm::begin(); ?>

            <?php
            echo $form->field($model, 'redemption_status')->dropDownList(['1' => 'Pending', "7" => "Cancel", '17' => 'Approve']); 
            ?>
            <?= $form->field($newcomment, 'comment')->textarea(['rows' => 6]) ?>

            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
    </div>
    
</section>


