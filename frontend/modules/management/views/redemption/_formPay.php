<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Redemption */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .field-redemptionremark-intake_month{display:none;}
</style>

<section class="invoice no-margin">    
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-edit"></i> Action                  
            </h2>
        </div><!-- /.col -->
        <div class="col-xs-12">
        <div class="redemption-form">

            <?php $form = ActiveForm::begin(); ?>

            <?php
            echo $form->field($model, 'redemption_status_ray')->dropDownList(['2' => 'Processing', '8' => 'Denied', '10' => 'Failed', '19' => 'Paid'],['prompt' => '-- Select --']); 
            ?>
            <?= $form->field($newcomment, 'intake_month')->textInput() ?>
            <?= $form->field($newcomment, 'comment')->textarea(['rows' => 6]) ?>

            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
    </div>
    
</section>

<?php
    $clientScript = '
        $("#redemption-redemption_status_ray").change(function ()
        {
            var myval = $(this).val();
            if (myval == "19"){
                $(".field-redemptionremark-intake_month").show();
            }else {
                $(".field-redemptionremark-intake_month").hide();
            }
        });

    ';
    $this->registerJs($clientScript, \yii\web\View::POS_END, 'booking-period');
    ?>
