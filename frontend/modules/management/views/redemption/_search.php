<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RedemptionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="redemption-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'redemptionID') ?>

    <?= $form->field($model, 'painterID') ?>

    <?= $form->field($model, 'redemption_invoice_no') ?>

    <?= $form->field($model, 'orderID') ?>

    <?= $form->field($model, 'req_points') ?>

    <?php // echo $form->field($model, 'req_amount') ?>

    <?php // echo $form->field($model, 'redemption_status') ?>

    <?php // echo $form->field($model, 'redemption_remarks') ?>

    <?php // echo $form->field($model, 'redemption_IP') ?>

    <?php // echo $form->field($model, 'redemption_created_datetime') ?>

    <?php // echo $form->field($model, 'redemption_created_by') ?>

    <?php // echo $form->field($model, 'redemption_updated_datetime') ?>

    <?php // echo $form->field($model, 'redemption_updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
