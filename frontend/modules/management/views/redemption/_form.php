<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Redemption */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="redemption-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'painterID')->textInput() ?>

    <?= $form->field($model, 'redemption_invoice_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'orderID')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'req_points')->textInput() ?>

    <?= $form->field($model, 'req_amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'redemption_status')->textInput() ?>

    <?= $form->field($model, 'redemption_remarks')->textInput() ?>

    <?= $form->field($model, 'redemption_IP')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
