<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\RedemptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Redemption Payment - Processing';
$this->params['breadcrumbs'][] = $this->title;
if ($dataProvider->totalCount > 0) {
    $disable = false;
} else {
    $disable = true;
}
$my_array1 = array('0' => 'All');
$payOutSummary = \common\models\PayOutSummary::find()->orderBy([
            'pay_out_summary_id' => SORT_ASC,
        ])->all();
$payOutSummarys = $my_array1 + ArrayHelper::map($payOutSummary, 'pay_out_summary_id', 'description');

//$payOutSummarys = arrayjoin($payOutSummarys);
//$res[] = array_unshift($my_array1,$payOutSummarys);
?>
<style>
    .actionbulk{display: none;}
    .hideme{display: none;}
</style>
<div class="col-lg-12">
<div class="box">
    <div class="box-header with-border">
        <div class="col-lg-8 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?></h3></div>
        <div class="col-lg-4 col-sm-2 col-xs-12 text-right no-padding">

        </div>
    </div>
    <div class="box-body table-responsive">
        <div class="redemption-index">
            <div class="row">
            <div class="col-lg-3 bulkact no-padding"> 
                <button class="btn btn-primary" id="btntest">Bulk Action</button>
                <?= Html::a('Excel Import Bulk', ['import-excel-redemption'], ['class' => 'btn btn-info']) ?>
            </div>
            <div class="col-lg-3">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?= $total_transactions ?></h3>

                        <p>Total Transactions</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                </div>
            </div>    
            <div class="col-lg-3">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?= $total_amount ?></h3>

                        <p>Total RM Amount</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                </div>
            </div> 
            <div class="col-lg-3">
                <?= Html::dropDownList('payout', [Yii::$app->request->get('id')], $payOutSummarys,['prompt' => '-- Select --','class' => 'form-control','id' => 'payout']) ?>
            </div>
            </div>

            <?=Html::beginForm(['/management/redemption/processing'],'post');?>
                <div class="col-lg-12 actionbulk no-padding">
                    <div class="col-lg-2 no-padding">
                        <?php
                        //echo $form->field($bulkredem, 'redemption_status_ray')->dropDownList(['8' => 'Denied', '10' => 'Failed', '19' => 'Paid'], ['prompt' => '-- Select --']);
                        ?>

                        <?= Html::dropDownList('redemption_status_ray', [1, 3, 5], ['8' => 'Denied', '10' => 'Failed', '19' => 'Paid'], ['prompt' => '-- Select --','class' => 'form-control','id' => 'status']) ?>
                        <?= Html::textInput('intake_month', '', ['class' => 'form-control hideme', 'placeholder' => 'Intake Month']) ?>
                    </div>
                    <div class="col-lg-6">
                       
                        <?= Html::textarea('comment', '', ['class' => 'form-control','rows' => 6]) ?>
                    </div>
                    <div class="col-lg-4 no-padding">
                        <label class="control-label" for="redemptionbulk-comment"></label>
                        <div class="form-group">
                            <?=
                            Html::submitButton('Submit', ['class' => 'btn btn-success', 'id' => 'btnsubmit',])
                            ?>
                            <?= Html::a('Cancel', ['/management/redemption/processing'], ['class'=>'btn btn-default']) ?>
                            
                        </div>
                    </div>                      
                </div>  

            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['class' => 'yii\grid\CheckboxColumn','headerOptions' => ['width' => '10'],],
                    //'redemptionID',
                    'redemption_invoice_no',
                    //'profile.card_id',
                    
                    //'painterID',
                    [
                        'attribute' => 'card_id',
                        'label' => 'Membership No',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '95'],
                        'value' => function ($model) {
                            return $model->profile->card_id;
                        },
                    ],
                    [
                        'attribute' => 'full_name',
                        'label' => 'Painter Name',
                        'format' => 'html',
                        'headerOptions' => ['width' => '195'],
                        'value' => function ($model) {
                            return $model->profile->full_name;
                        },
                    ],
                    [
                        'attribute' => 'ic_no',
                        'label' => 'NRIC/Passport Number',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->profile->ic_no;
                        },
                    ],
                    /*[
                        'attribute' => 'account_no_verification',
                        'label' => 'Bank # Verification',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->bank->account_no_verification;
                        },
                    ],*/            
                    //'profile.ic_no',            
                    /*[
                        'attribute' => 'painterID',
                        'label' => 'NRIC/PP',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '95'],
                        'value' => function ($model) {
                            return $model->profile->ic_no;
                        },
                    ], */           
                    //'orderID:ntext',
                    //'req_points',
                    [
                        'attribute' => 'total_transactions',
                        'label' => 'Total Transactions',
                        'format' => 'html',
                        'headerOptions' => ['width' => '50', 'class' => 'text-center'],
                        'contentOptions' =>['class' => 'text-right'],
                        'value' => function ($model) {
                            return $model->getTotalTransactions();
                        },
                    ],            
                    [
                        'attribute' => 'req_points',
                        'label' => 'Total Points',
                        'format' => 'html',
                        'headerOptions' => ['width' => '60', 'class' => 'text-center'],
                        'contentOptions' =>['class' => 'text-right'],
                        'value' => function ($model) {
                            return $model->req_points;
                        },
                    ],            
                    
                    [
                        'attribute' => 'req_amount',
                        'label' => 'Total RM Amount',
                        'format' => 'html',
                        'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                        'contentOptions' =>['class' => 'text-right'],
                        'value' => function ($model) {
                            return $model->req_amount;
                        },
                    ],
                    /*[
                        'attribute' => 'redemption_status_ray',
                        'label' => 'Status',
                        'format' => 'html',
                        'headerOptions' => ['width' => '95'],
                        'value' => function ($model) {
                            //$ord =
                            return $model->orderStatus2->name;
                        },
                        'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                        'filter' => ["2" => "Processing", "8" => "Denied", "10" => "Failed", "19" => "Paid"],        
                    ], */            
                    //'req_amount',
                    //'redemption_status',
                    // 'redemption_remarks',
                    // 'redemption_IP',
                    //'redemption_created_datetime',
                    /*[
                        'attribute' => 'redemption_created_datetime',
                        //'format' => ['raw', 'Y-m-d H:i:s'],
                        'format' =>  ['date', 'php:d-m-Y h.i A'],
                        'options' => ['width' => '200']
                    ],*/
                    
                    [
                        'attribute' => 'redemption_created_datetime',
                        //'value' => 'redemption_created_datetime',
                        'value' => function ($model) {
                            return date('d-m-Y', strtotime($model->redemption_created_datetime));
                        },
                        //'format' => ['php:D, d-M-Y H:i:s A'],
                        //'format' => 'datetime',
                        //'format' =>  ['date', 'php:d-m-Y h.i A'],
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'redemption_created_datetime',
                            'convertFormat' => true,
                            'removeButton' => false,
                            //'displayFormat' => 'php:D, d-M-Y H:i:s A',
                            'pluginOptions' => [
                                'format' => 'yyyy-M-dd'
                                //'format' => 'dd-M-yyyy'
                            ],
                        ]),
                        'options' => ['width' => '150']
                    ],             
                    // 'redemption_created_by',
                    // 'redemption_updated_datetime',
                    // 'redemption_updated_by',
                    //['class' => 'yii\grid\ActionColumn'],
                    [
                        'attribute' => 'action',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->getActions2();
                        },
                        'options' => ['width' => '60'],
                    ],            
                ],
            ]);
            ?>
            <?= Html::endForm();?> 
            </div>
        </div>
    </div>
</div>

<?php
    $clientScript = '
        $("#w0 table tbody tr").click(function()
        {
            recalculate();
        });
        $("#btntest").click(function()
        {
            $(".actionbulk").slideToggle( "slow" );
            $(".bulkact").hide();
            
            
        });
        $("#btnsubmit").click(function()
        {
            var keys = $("#w1").yiiGridView("getSelectedRows");
            if(keys > "0"){
                return true;
            }else{
                alert("Please check at least one checkbox");
                return false;
            }
            
        });
        $(".select-on-check-all").change(function ()
        {
            recalculate();
        });
        
        $("#status").change(function ()
        {
            var myval = $(this).val();
            if (myval == "19"){
                $(".hideme").show();
            }else {
                $(".hideme").hide();
            }
        });
        
$(function() {
      $("#payout").on("change",function() {
        if($(this).val() != 0) {
            window.location.href = "processing?id="+$(this).val();
        }else {
            window.location.href = "processing";
        } 
      });
});


    ';
    $this->registerJs($clientScript, \yii\web\View::POS_END, 'booking-period');
    ?>




