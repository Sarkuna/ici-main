
<!DOCTYPE html>
<html lang="en">
  <body>
    <header class="clearfix">
      <div id="logo">
          <img src="http://dev.agnichakra.com/images/club_logo.png" width="100">
      </div>
      <h1>Redemption Statement</h1>
      <table>
          <tr>
              <td style="text-align: left;"><div id="project">
                      <?php
            echo 'Redemption #: '.$model->redemption_invoice_no;       
            echo '<br><br>Full Name : '.$model->profile->full_name;
            echo '<br>Membership ID# : '.$model->profile->card_id;
            echo '<br>NRIC/PP : '.$model->profile->ic_no;
            echo '<br>Mobile # : '.$model->profile->mobile;
            echo '<br>Email ID: '.$model->user->email;
            ?>
      </div></td>
      <td>
          <?php
              $banks = common\models\Banks::find()->where(['id' => $model->bank->bank_name])->one();
              $bankname = $banks->bank_name;
              ?>
              <b>Bank Name:</b> <?= $bankname ?><br/>
              <b>Account Name:</b> <?= $model->bank->account_name ?><br/>
              <b>Account #:</b> <?= $model->bank->account_number ?>
      </td>
          </tr>
      </table>
      
      
    </header>
    <main>
      <table>
        <thead>
          <tr>
            <th>#</th>  
            <th class="service">Transaction#</th>
            <th class="desc">Dealer Name</th>
            <th>Area</th>
            <th>Dealer Invoice</th>
            <th>Items</th>
            <th>Points Awarded</th>
            <th>Value (RM)</th>
          </tr>
        </thead>
        <tbody>
          <?php
                    $redemptionitems = \common\models\RedemptionItems::find()->where(['redemptionID' => $model->redemptionID])->all();
                    $n = 1;
                    foreach($redemptionitems as $redemptionitem){
                        $order_id = $redemptionitem->order_id;
                        $order = \common\models\PointOrder::find()->where(['order_id' => $order_id])->one();
                        $orderitem_tot_point = \common\models\PointOrderItem::find()->where(['point_order_id' => $order_id, 'Item_status' => 'G'])->sum('total_qty_point');
                        $orderitem_tot_amt = \common\models\PointOrderItem::find()->where(['point_order_id' => $order_id, 'Item_status' => 'G'])->sum('total_qty_value');
                        $orderitem_qty = \common\models\PointOrderItem::find()->where(['point_order_id' => $order_id, 'Item_status' => 'G'])->count();
                        $Dealerid = $order->order_dealer_id;
                        $dealername = \common\models\DealerList::find()->where(['id' => $Dealerid])->one();
                        echo '<tr>
                            <td>'.$n.'</td>
                            <td>'.$order->order_num.'</td>
                            <td style="text-align: left;">'.$dealername->customer_name.'</td>
                            <td style="text-align: left;">'.$dealername->area.'</td>
                            <td style="text-align: left;">'.$order->dealer_invoice_no.'</td>
                            <td class="text-center">'.$orderitem_qty.'</td>       
                            <td class="text-center">'.$orderitem_tot_point.'</td>
                            <td class="text-center">'.$orderitem_tot_amt.'</td>
                            <td><a href="" onclick="vieworderitem('.$order_id.');return false;"><i class="fa fa-search fa-rotate-360"></i></a></td>    
                          </tr>';
                        $n++;
                    }
                    ?>

            <tr style="font-weight: bold;">
            <td colspan="6">Total :</td>
            <td class="total"><?= $model->req_points ?></td>
            <td class="total"><?= $model->req_amount ?></td>
          </tr>

        </tbody>
      </table>
    </main>

  </body>
</html>