<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    input#findpainter-ic_no {font-size: 35px;}
    .field-findpainter-membership_id {display: none;}
</style>
<div class="login-box">
    <div class="login-box-body">
        <div class="point-order-form">
            
            <?php $form = ActiveForm::begin(); ?>
            <?=
        $form->field($model, 'date_month')->widget(DatePicker::classname(), [
            'options' => ['readOnly' => true, 'placeholder' => 'From date ...'],
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose' => true,
                'todayHighlight' => false,
                'todayBtn' => false,
                'format' => 'yyyy-mm-dd',
                //'viewMode' => "months", 
                'minViewMode' => "months",
                //'minDate' => 'tenancy_period_from',
                //'endDate' => '+0d', 
                //'startDate' => date('#newtenantform-tenancy_period_from'),
            ]
        ]);
        ?>
       <?= $form->field($model, 'description')->textInput() ?>    

        <button class="btn btn-info btn-flat btn-lg" type="submit">Go!</button>
        <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

