<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\RedemptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Points Redemption Management - Approved';

$this->params['breadcrumbs'][] = $this->title;
if ($dataProvider->totalCount > 0) {
    $disable = false;
} else {
    $disable = true;
}
?>
<style>
    .actionbulk{display: none;}
</style>
<div class="col-lg-12">
<div class="box">
    <div class="box-header with-border">
        <div class="col-lg-8 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?> (<small style="color: black !important;font-size: 14px;">12 Month Ago From Today</small>)</h3></div>
        <div class="col-lg-4 col-sm-2 col-xs-12 text-right no-padding">

        </div>
    </div>
    <div class="box-body table-responsive">
        <div class="redemption-index">
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //['class' => 'yii\grid\CheckboxColumn','headerOptions' => ['width' => '10'],],
                    //'redemptionID',
                    'redemption_invoice_no',
                    //'profile.card_id',
                    
                    //'painterID',
                    [
                        'attribute' => 'card_id1',
                        'label' => 'Membership No',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '95'],
                        'value' => function ($model) {
                            return $model->profile->card_id;
                        },
                    ],
                    [
                        'attribute' => 'full_name1',
                        'label' => 'Painter Name',
                        'format' => 'html',
                        'headerOptions' => ['width' => '195'],
                        'value' => function ($model) {
                            return $model->profile->full_name;
                        },
                    ],
                    [
                        'attribute' => 'ic_no1',
                        'label' => 'NRIC/Passport Number',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->profile->ic_no;
                        },
                    ],            
                    [
                        'attribute' => 'total_transactions',
                        'label' => 'Total Transactions',
                        'format' => 'html',
                        'headerOptions' => ['width' => '50', 'class' => 'text-center'],
                        'contentOptions' =>['class' => 'text-right'],
                        'value' => function ($model) {
                            return $model->getTotalTransactions();
                        },
                    ],            
                    [
                        'attribute' => 'req_points',
                        'label' => 'Total Points',
                        'format' => 'html',
                        'headerOptions' => ['width' => '60', 'class' => 'text-center'],
                        'contentOptions' =>['class' => 'text-right'],
                        'value' => function ($model) {
                            return $model->req_points;
                        },
                    ],            
                    
                    [
                        'attribute' => 'req_amount',
                        'label' => 'Total RM Amount',
                        'format' => 'html',
                        'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                        'contentOptions' =>['class' => 'text-right'],
                        'value' => function ($model) {
                            return $model->req_amount;
                        },
                    ],
                    [
                        'attribute' => 'redemption_created_datetime',
                        'value' => 'redemption_created_datetime',
                        //'format' => ['php:D, d-M-Y H:i:s A'],
                        //'format' => 'datetime',
                        'format' =>  ['date', 'php:d-m-Y h.i A'],
                        
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'redemption_created_datetime',
                            'convertFormat' => true,
                            'readonly' => true,
                            //'type' => DatePicker::TYPE_BUTTON,
                            //'displayFormat' => 'php:D, d-M-Y H:i:s A',
                            'pluginOptions' => [
                                'format' => 'yyyy-M-dd'
                                //'format' => 'dd-M-yyyy'
                            ],
                        ]),
                        'options' => ['width' => '150']
                    ],             
                    // 'redemption_created_by',
                    // 'redemption_updated_datetime',
                    // 'redemption_updated_by',
                    //['class' => 'yii\grid\ActionColumn'],
                    [
                        'attribute' => 'action',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->getActions();
                        },
                        'options' => ['width' => '60'],
                    ],            
                ],
            ]);
            ?>
        </div>
    </div>
</div>
</div>

<?php
    $clientScript = '
        $("#w0 table tbody tr").click(function()
        {
            recalculate();
        });
        $("#btntest").click(function()
        {
            $(".actionbulk").slideToggle( "slow" );
            $(".bulkact").hide();
            
            
        });
        $("#btnsubmit").click(function()
        {
            var keys = $("#w1").yiiGridView("getSelectedRows");
            if(keys > "0"){
                return true;
            }else{
                alert("Please check at least one checkbox");
                return false;
            }
            
        });
        $(".select-on-check-all").change(function ()
        {
            recalculate();
        });

    ';
    $this->registerJs($clientScript, \yii\web\View::POS_END, 'booking-period');
    ?>

<script language="javascript">  
    function recalculate() {
        var sum = 0;
        var sumrm = 0.00;
        var sumtot = 0.00;
        var a = "";
        $("input[name='selection[]']:checked").each(function () {
            $this=$(this);
            var amount = $(this).closest("tr").find("td:nth-child(6)").text();
            var amountrm = $(this).closest("tr").find("td:nth-child(7)").text();
            //alert(amount);
            
            if (this.checked) sum = sum + parseFloat(amount);
            if (this.checked) sumrm = sumrm + parseFloat(amountrm);
            if (this.checked) sumtot = sumtot + parseFloat($(this).attr('rel'));
            if (this.checked) {
                if (a.length == 0) {
                    a = $(this).closest("tr").find("td:first").text();
                } else {
                    a = a + "," + $(this).closest("tr").find("td:first").text();
                }
            } else {
                if (a.length == 1) {
                    a = a.slice(0, 1);
                } else {
                    a = a.replace(("," + $(this).closest("tr").find("td:first").text()), "");
                }
            }
        });

        $("#redemption-orderid").val(a); 
        $("#output").text(sum);
        $("#outputrm").text(sumrm.toFixed(2));
        $("#redemption-req_points").val(sum);
        $("#redemption-req_amount").val(sumtot.toFixed(2));
        $("#multiamt").val(sum);
        
    }
    function checkempty() {
        var minamt = $("#csvsendpayment-csv_amount").val(); 
        if(minamt == '0.00' || minamt == '0' || minamt == ''){
            alert('Minimum Ammount Required above 0.00');
            return false;
        }else{
            return true;
        }
        
    }
//});
    </script> 