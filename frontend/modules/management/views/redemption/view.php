<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\db\Expression;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\Redemption */

$this->title = $model->redemption_invoice_no;

$this->params['breadcrumbs'][] = ['label' => 'Redemptions', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
?>
<section class="invoice">    
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-globe"></i> Redemption # <?= $model->redemption_invoice_no ?>
                <?php
                if(!empty($model->redemption_status_date)){
                    echo '<small class="pull-right">Approved Date: '.date('d-m-Y', strtotime($model->redemption_status_date)).'</small>&nbsp;';
                }
                ?>
                
                <small class="pull-right" style="margin-right: 15px;">Created Date: <?php echo date('d-m-Y', strtotime($model->redemption_created_datetime)) ?></small>
                
            </h2>
        </div><!-- /.col -->
    </div>
    <div class="row invoice-info">
        <div class="col-sm-5 invoice-col">
            <h4>Painter Profile</h4>
            <address>
            <?php
            echo 'Full Name : '.$model->profile->full_name;
            echo '<br>Membership ID# : '.$model->profile->card_id;
            echo '<br>NRIC/PP : '.$model->profile->ic_no;
            echo '<br>Mobile # : '.$model->profile->mobile;
            echo '<br>Email ID: '.$model->user->email;
            ?>
            </address>
        </div>
        <div class="col-sm-5 invoice-col">
            <h4>Bank Account Info</h4>
            <address>
                <?php
              $banks = common\models\Banks::find()->where(['id' => $model->bank->bank_name])->one();
              $bankname = $banks->bank_name;
              ?>
              <b>Bank Name:</b> <?= $bankname ?><br/>
              <b>Account Name:</b> <?= $model->bank->account_name ?><br/>
              <b>Account #:</b> <?= $model->bank->account_number ?>
            </address>
        </div><!-- /.col -->
        

        <div class="col-sm-2 invoice-col">
                <?php
                if(!empty($model->redemption_status)){
                    if($model->redemption_status == 1){
                        $sts = '<span class="label label-warning">Pending</span>';
                    }else if($model->redemption_status == 7){
                        $sts = '<span class="label label-danger">Canceled</span>';
                    }else if($model->redemption_status == 17){
                        //"2" => "Processing", "8" => "Denied", "10" => "Failed", "19" => "Paid"
                        if($model->redemption_status_ray == 2){
                            $sts = '<span class="label label-info">Processing</span>';  
                        }else if($model->redemption_status_ray == 8){
                            $sts = '<span class="label label-warning">Denied</span>';  
                        }else if($model->redemption_status_ray == 10){
                            $sts = '<span class="label label-danger">Failed</span>';  
                        }else if($model->redemption_status_ray == 19){
                            $sts = '<span class="label label-primary">Paid</span>';  
                        }else{
                            $sts = '<span class="label label-success">Approved</span>';
                        }
                    }else{
                       $orderaction = common\models\OrderStatus::find()->where(['order_status_id' => $model->redemption_status])->one();
                       $sts = $orderaction->name;
                    }
                    echo '<h3 style="margin-top: 0px;" class="pull-right">'.$sts.'</h3>';
                }
                ?>
              <br/>
              
            </div><!-- /.col -->
    </div>
    
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Transaction#</th>
                    <th class="text-left">Dealer Name</th> 
                    <th class="text-center">Items</th>                                       
                    <th class="text-center">Points Awarded</th>
                    <th class="text-center">Value (RM)</th>
                    <th class="text-center"></th>
                  </tr>
                </thead>
                <tbody>
                    <?php
                    $redemptionitems = \common\models\RedemptionItems::find()->where(['redemptionID' => $model->redemptionID])->all();
                    $n = 1;
                    foreach($redemptionitems as $redemptionitem){
                        $order_id = $redemptionitem->order_id;
                        $order = \common\models\PointOrder::find()->where(['order_id' => $order_id])->one();
                        $orderitem_tot_point = \common\models\PointOrderItem::find()->where(['point_order_id' => $order_id, 'Item_status' => 'G'])->sum('total_qty_point');
                        $orderitem_tot_amt = \common\models\PointOrderItem::find()->where(['point_order_id' => $order_id, 'Item_status' => 'G'])->sum('total_qty_value');
                        $orderitem_qty = \common\models\PointOrderItem::find()->where(['point_order_id' => $order_id, 'Item_status' => 'G'])->count();
                        $Dealerid = $order->order_dealer_id;
                        $dealername = \common\models\DealerList::find()->where(['id' => $Dealerid])->one();
                        echo '<tr>
                            <td>'.$n.'</td>
                            <td>'.$order->order_num.'</td>
                            <td class="text-left">'.$dealername->customer_name.'</td>    
                            <td class="text-center">'.$orderitem_qty.'</td>       
                            <td class="text-center">'.$orderitem_tot_point.'</td>
                            <td class="text-center">'.$orderitem_tot_amt.'</td>
                            <td><a href="" onclick="vieworderitem('.$order_id.');return false;"><i class="fa fa-search fa-rotate-360"></i></a></td>    
                          </tr>';
                        $n++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
              <p class="lead">History:</p>
              <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                <?php
                $comments = common\models\RedemptionHistory::find()->where(['redemptionID' => $model->redemptionID])->all();
                if(count($comments) > 0){
                    foreach($comments as $comment){
                        if(!empty($comment->comment)){
                            $redemptionstatus = common\models\OrderStatus::find()->where(['order_status_id' => $comment->redemption_status])->one();
                            echo '<b>'.$redemptionstatus->name.'</b> - <em>'.date('d-m-Y', strtotime($comment->date_added)).'</em><br>';
                            echo $comment->comment.'<br><br>';
                        }
                    }
                }else{
                    echo 'No Comments';
                }
                ?>
              </p>
            </div><!-- /.col -->
            <div class="col-xs-6">
              <p class="lead">Awarded </p>
              <div class="table-responsive">
                <table class="table align-right">
                  <tr>
                    <th>Total Points:</th>
                    <td><b><?= $model->req_points ?></b></td>
                  </tr>
                  <tr>
                    <th>Total RM:</th>
                    <td><b><?= $model->req_amount ?></b></td>
                  </tr>
                </table>
              </div>
            </div><!-- /.col -->
          </div><!-- /.row -->
    </div>
    <div class="row no-print">
        <div class="col-xs-12">
            <a href="invoice-print.html" target="_blank" class="hide btn btn-default"><i class="fa fa-print"></i> Print</a>            
            
            <?php
            if (($session['currentRole'] == Yii::$app->params['role.type.administrator'])) {
                $back = Yii::$app->request->referrer;
            }else{
                $back = 'index';
            }
            ?>
            <a href="javascript: history.back()" class="btn btn-default">Back</a>
            <a href="javascript: history.back()" class="btn btn-primary pull-right">Cancel</a>
            <button class="hide btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
        </div>
    </div>
    
    
</section>

<?php
	yii\bootstrap\Modal::begin([
		'id' => 'vieworderitemModal',
		'header' => "<h4 class='modal-title'><i class='fa fa-edit'></i> View Transaction Details </h4>",
                'size' => "modal-lg",
	]);
 	yii\bootstrap\Modal::end(); 
?>

<script>
/***
  * Start Update Gardian Jquery
***/
function vieworderitem(id) {
	$.ajax({
	  type:'GET',
	  url:'<?= Url::toRoute(["/management/pointorder/viewviainvoice"]) ?>',
	  data: { id : id},
	  success: function(data)
		   {
		       $(".modal-content").addClass("row");
		       $('.modal-body').html(data);
		       $('#vieworderitemModal').modal();

		   }
	});
}
</script>   


