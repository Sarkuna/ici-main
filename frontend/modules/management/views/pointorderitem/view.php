<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrderItem */

$this->title = $model->item_id;
$this->params['breadcrumbs'][] = ['label' => 'Point Order Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="point-order-item-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->item_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->item_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'item_id',
            'point_order_id',
            'painter_login_id',
            'item_bar_code',
            'item_bar_point',
            'item_bar_total_point',
            'item_bar_total_value',
            'Item_status',
            'Item_IP',
            'created_datetime',
            'created_by',
            'updated_datetime',
            'updated_by',
        ],
    ]) ?>

</div>
