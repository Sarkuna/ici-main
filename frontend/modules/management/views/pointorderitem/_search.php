<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrderItemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="point-order-item-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'item_id') ?>

    <?= $form->field($model, 'point_order_id') ?>

    <?= $form->field($model, 'painter_login_id') ?>

    <?= $form->field($model, 'item_bar_code') ?>

    <?= $form->field($model, 'item_bar_point') ?>

    <?php // echo $form->field($model, 'item_bar_total_point') ?>

    <?php // echo $form->field($model, 'item_bar_total_value') ?>

    <?php // echo $form->field($model, 'Item_status') ?>

    <?php // echo $form->field($model, 'Item_IP') ?>

    <?php // echo $form->field($model, 'created_datetime') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_datetime') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
