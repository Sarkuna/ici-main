<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
//use kartik\grid\GridView;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\PayOutSummary */
$this->title = 'Batch '.$model->batch.' ('.$model->getStartredemption().'-'.$model->getEndredemption().')';;
$this->params['breadcrumbs'][] = ['label' => 'Pay Out Summaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$daterange = date('d-M-Y', strtotime($model->start_date)).' '.date('d-M-Y', strtotime($model->end_date));


            
?>

<div class="col-lg-12">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-8 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?> - Mobi Balance RM <span class="text-bold text-green"><?= number_format(Yii::$app->ici->mobiBalance(),2); ?></span></h3></div>
            <div class="col-lg-4 col-sm-2 col-xs-12 text-right no-padding">
                <?= Html::a('Bulk Payment', '/management/bar-code/bulk-payment?id='.$model->id, ['title' => Yii::t('app', 'Bulk Payment'), 'class'=> 'btn btn-success']) ?>
                <?= $daterange ?>
            </div>
        </div>
        <div class="box-body">
            <div class="col-lg-6">
                <table class="table table-bordered">
                    <tr>
                        <th>Status</th>
                        <th>Number of Painters</th>
                        <th>Total Amount, RM</th>
                    </tr>
                    <?php
                        $internel_statuss = array("crm_id_not_found","processing", "review", "cancelled", "not_verified", "paid", "refunded", "rejected", "manual_payment", "invalid_acc", "declined", "unsuccessful", "on_hold", "re_verified");
                        $status_text = '';
                        $redemptioncountsum = 0; $redemptionamountsum = 0.00;
                        foreach ($internel_statuss as $internel_status) {
                            
                            if($internel_status == 'paid') {
                                $redemptioncount = \common\models\BCRedemption::find()->where(['internel_status' => 'paid', 'pay_out_summary_id' => $model->id])->count();
                                $redemptionamount = \common\models\BCRedemption::find()->where(['internel_status' => 'paid', 'pay_out_summary_id' => $model->id])->sum('total_rm');
                            }else if($internel_status == 'not_verified') {                                
                                $redemptioncount = \common\models\BCRedemption::find()->where(['internel_status' => 'not_verified', 'pay_out_summary_id' => $model->id])->count();
                                $redemptionamount = \common\models\BCRedemption::find()->where(['internel_status' => 'not_verified', 'pay_out_summary_id' => $model->id])->sum('total_rm');
                                
                            }else if($internel_status == 'processing') {
                                $redemptioncount = \common\models\BCRedemption::find()
                                ->where(['internel_status' => $internel_status, 'pay_out_summary_id' => $model->id])
                                //->andWhere(['=','banking_information.account_no_verification','Y'])
                                ->count();
                                
                                $redemptionamount = \common\models\BCRedemption::find()       
                                ->where(['internel_status' => $internel_status, 'pay_out_summary_id' => $model->id])
                                //->andWhere(['=','banking_information.account_no_verification','Y'])
                                ->sum('total_rm');  

                            }else {
                                $redemptioncount = \common\models\BCRedemption::find()->where(['internel_status' => $internel_status, 'pay_out_summary_id' => $model->id])->count();
                                $redemptionamount = \common\models\BCRedemption::find()->where(['internel_status' => $internel_status, 'pay_out_summary_id' => $model->id])->sum('total_rm');
                            }
                            $status_text = str_replace('_', ' ', $internel_status);
                            echo '<tr>
                                <td>'.ucwords($status_text).'</td>
                                <td class="text-right">'.$redemptioncount.'</td>
                                <td class="text-right">';
                                if(!empty($redemptionamount)){
                                    echo Yii::$app->formatter->asDecimal($redemptionamount,2);
                                }else {
                                    echo '-';
                                }
                                echo '</td>
                            </tr>';
                            $redemptioncountsum += $redemptioncount;
                            $redemptionamountsum += $redemptionamount;
                        }
                        
                        echo '<tr class="text-bold">
                                <td>Total</td>
                                <td class="text-right">'.$redemptioncountsum.'</td>
                                <td class="text-right">'.Yii::$app->formatter->asDecimal($redemptionamountsum,2).'</td>
                            </tr>';
                    ?>
                    
                    

                </table>
            </div>
            <div class="col-lg-6">
                <table class="table table-bordered">
                    <tr>
                        <th>Paid Date</th>
                        <th>Number of Painters</th>
                        <th>Total Amount, RM</th>
                    </tr>
                    <?php
                        $redemptionpaids = \common\models\BCRedemption::find()->where(['pay_out_summary_id' => $model->id])->andWhere(['is not','paid_date', null])->groupBy('paid_date')->all();
                        if(!empty($redemptionpaids)) {
                            $paidcountsum=0; $paidsum=0.00;
                            foreach ($redemptionpaids as $redemptionpaid) {
                                $nopainter = \common\models\BCRedemption::find()->where(['pay_out_summary_id' => $model->id])->andWhere(['paid_date' => $redemptionpaid->paid_date])->groupBy('painterID')->count();
                                $paidamount = \common\models\BCRedemption::find()->where(['pay_out_summary_id' => $model->id])->andWhere(['paid_date' => $redemptionpaid->paid_date])->sum('total_rm');
                                echo '<tr>
                                    <td>'.$redemptionpaid->paid_date.'</td>
                                    <td class="text-right">'.$nopainter.'</td>
                                    <td class="text-right">'.Yii::$app->formatter->asDecimal($paidamount,2).'</td>
                                </tr>';
                                
                                $paidcountsum += $nopainter;
                                $paidsum += $paidamount;
                            }
                            $final_painter = $redemptioncountsum - $paidcountsum;
                            $balnce = $redemptionamountsum - $paidsum;
                            echo '<tr class="text-bold">
                                <td>Total</td>
                                <td class="text-right">'.$paidcountsum.'</td>
                                <td class="text-right">'.Yii::$app->formatter->asDecimal($paidsum,2).'</td>
                            </tr>
                            <tr class="text-bold">
                                <td>Balance</td>
                                <td class="text-right">'.$final_painter.'</td>
                                <td class="text-right">'.Yii::$app->formatter->asDecimal($balnce,2).'</td>
                            </tr>';
                        }else {
                            echo '<tr>
                                <td colspan = "3" class="text-center">No payment records!</td>
                            </tr>';
                        }
                    ?>
                    
                    
                    

                </table>
            </div>
            
            <div class="col-lg-12 pay-out-summary-index table-responsive">
                 <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'crm_id',
                            'label' => 'CRM ID',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                return $model->crm_id;
                                
                            },
                        ],
                        [
                            'attribute' => 'card_id',
                            'label' => 'Membership #',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                if(!empty($model->painterID)) {
                                    return $model->profile->card_id;
                                }else {
                                    return 'N/A';
                                }
                                
                            },
                        ],
                        [
                            'attribute' => 'account_name',
                            'label' => 'Full Name',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '50'],
                            'value' => function ($model) {
                                if(!empty($model->painterID)) {
                                    return (Html::a($model->bank->account_name, ['/painter/painterprofile/view', 'id' => $model->profile->id, '#' => 'bank'], ['title' => Yii::t('app', $model->bank->account_name), 'target' => '_blank']));
                                }else {
                                    return 'N/A';
                                }
                            },
                        ],
                        [
                            'attribute' => 'bank_name',
                            'label' => 'Bank Name',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '380'],
                            'value' => function ($model) {
                                if(!empty($model->painterID)) {
                                    $banks = common\models\Banks::find()->where(['id' => $model->bank->bank_name])->one();
                                    $bankname = $banks->bank_name;
                                }else {
                                    $bankname = 'N/A';
                                }
                                return $bankname;
                            },
                            //'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            //'filter'=>ArrayHelper::map(common\models\Banks::find()->where(['IN', 'id' ,$bank_names])->asArray()->all(), 'id', 'bank_name'),        
                        ],

                        [
                            'attribute' => 'account_number',
                            'label' => 'Bank Acc #',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                if(!empty($model->painterID)) {
                                    return $model->bank->account_number;
                                }else {
                                    return 'N/A';
                                }
                            },
                        ],
                        [
                            'attribute' => 'redemption_invoice_no',
                            'label' => 'Redemption',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                return $model->redemption_invoice_no;
                            },
                        ],
                        [
                            'attribute' => 'total_rm',
                            'label' => 'Total',
                            'format' => ['decimal', 2],
                            'value' => function ($model) {
                                return $model->total_rm;
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                        ],
                        [
                            'attribute' => 'mobile',
                            'label' => 'Mobile',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                if(!empty($model->painterID)) {
                                    return $model->profile->mobile;
                                }else {
                                    return 'N/A';
                                }
                            },
                        ],
                        [
                            'attribute' => 'account_no_verification',
                            'label' => 'Account Verified',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                if(!empty($model->painterID)) {
                                    if($model->bank->account_no_verification == 'Y') {
                                        return 'Yes';
                                    }else {
                                        return 'No';
                                    }                                    
                                }else {
                                    return 'N/A';
                                }
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            'filter' => ["Y" => "Yes", "N" => "No"],        
                        ],
                        [
                            'attribute' => 'internel_status',
                            'label' => 'Status',
                            'format' => 'html',
                            'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                //$ord =
                                if(!empty($model->internel_status)) {
                                    $sttext = str_replace('_', ' ', $model->internel_status);                                            
                                    $instatus = ucwords($sttext);
                                }else {
                                    $instatus = 'Processing';
                                }
                                return $instatus;
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            'filter' => ['crm_id_not_found' => 'Crm Id Not Found', 'not_verified' => 'Not Verified', 're_verified' => 'Re Verified', 'processing' => 'Processing', 'review' => 'Review', 'cancelled' => 'Cancelled', 'refunded' => 'Refunded', 'rejected' => 'Rejected', 'manual_payment' => 'Manual Payment', 'invalid_acc' => 'Invalid Acc', 'declined' => 'Declined', 'unsuccessful' => 'Unsuccessful', 'on_hold' => 'On Hold', 'paid' => 'Paid'],         
                        ],
                        [
                            'attribute' => 'redemption_status_ray1',
                            'label' => 'Paid Date',
                            'format' => 'html',
                            'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                //$ord =
                                if(!empty($model->paid_date)) {
                                    return date('d-m-y', strtotime($model->paid_date));
                                }else {
                                    return 'N/A';
                                }
                                
                            },      
                        ],             

                        [
                            'attribute' => 'redemption_created_datetime1',
                            'label' => 'Request Date',
                            'format'=>['date', 'php:d-m-y'],//h:i:s
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->created_datetime;
                            },   
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'headerOptions' => ['width' => '60'],
                            'template' => '{update} {pay}', //{view} {delete}
                            'buttons' => [
                                'update' => function ($url, $model) {
                                    $url = 'update-bank?id=' . $model->redemptionID;
                                    if(!empty($model->painterID)) {                                   
                                        if($model->bank->account_no_verification != 'Y') {                                            
                                           return (Html::a('<i class="glyphicon glyphicon-question-sign"></i>', $url, ['title' => Yii::t('app', 'Update'), 'onclick' => "updateBank(" . $model->redemptionID . ");return false;"]));
                                        }else {
                                           return (Html::a('<i class="glyphicon glyphicon-question-sign"></i>', $url, ['title' => Yii::t('app', 'Update'), 'onclick' => "updatePayment(" . $model->redemptionID . ");return false;"]));                                        
                                        }
                                    }
                                },
                                'pay' => function ($url, $model) {
                                    $url = 'mobi?id=' . $model->redemptionID;

                                    if(!empty($model->painterID)) {                                   
                                        if($model->bank->account_no_verification == 'Y' && $model->total_rm > 100 && $model->internel_status != 'paid') {                                            
                                           return (Html::a('<i class="fa fa-money-bill-alt"></i>', $url, ['title' => Yii::t('app', 'Mobi Payment')]));
                                        }
                                    }
                                }        
                            ],
                        ],            
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<?php
	yii\bootstrap\Modal::begin([
		'id' => 'guardModal',
		'header' => "<h4 class='modal-title'><i class='fa fa-edit'></i> Bank Account Number Verification</h4>",
	]);
 	yii\bootstrap\Modal::end(); 
?>

<?php
	yii\bootstrap\Modal::begin([
		'id' => 'guardModal1',
		'header' => "<h4 class='modal-title'><i class='fa fa-edit'></i> Payment Status </h4>",
	]);
 	yii\bootstrap\Modal::end(); 
?>

<script>
/***
  * Start Update Gardian Jquery
***/
function updateBank(id) {
	$.ajax({
	  type:'GET',
	  url:'<?= Url::toRoute(["bar-code/update-bank"]) ?>',
	  data: { id : id},
	  success: function(data)
		   {
		       $(".modal-content").addClass("row");
		       $('.modal-body').html(data);
		       $('#guardModal').modal();

		   }
	});
}
function updatePayment(id) {
	$.ajax({
	  type:'GET',
	  url:'<?= Url::toRoute(["bar-code/update-payment"]) ?>',
	  data: { id : id},
	  success: function(data)
		   {
		       $(".modal-content").addClass("row");
		       $('.modal-body').html(data);
		       $('#guardModal1').modal();

		   }
	});
}
</script>