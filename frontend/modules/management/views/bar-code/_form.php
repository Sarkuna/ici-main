<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\BCPayOutSummary */
/* @var $form yii\widgets\ActiveForm */


?>


<div class="bcpay-out-summary-form">
<div class="box-body">
            <div class="col-md-6 no-padding">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?=
        $form->field($model, 'start_date')->widget(DatePicker::classname(), [
            'options' => ['readOnly' => true, 'placeholder' => 'Start date ...'],
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose' => true,
                'todayHighlight' => false,
                'todayBtn' => false,
                'format' => 'yyyy-mm-dd',
                //'viewMode' => "months", 
                //'minViewMode' => "months",
                //'minDate' => 'tenancy_period_from',
                'endDate' => '+0d', 
                //'startDate' => date('#bcpayoutsummary-start_date'),
            ],
            'pluginEvents' => [
                'changeDate' => 'function(ev) {
                    var sdat = $("#bcpayoutsummary-start_date").val();                    
                    var bDate = sdat.toString("dd-MM-yy");
                    $("#bcpayoutsummary-end_date-kvdate").kvDatepicker("setStartDate", bDate);
                }'
            ]
        ]);
        ?>           
     
    <?=
        $form->field($model, 'end_date')->widget(DatePicker::classname(), [
            'options' => ['readOnly' => true, 'placeholder' => 'End date ...'],
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose' => true,
                'todayHighlight' => false,
                'todayBtn' => false,
                'format' => 'yyyy-mm-dd',
                //'viewMode' => "months", 
                //'minViewMode' => "months",
                //'minDate' => 'bcpayoutsummary-start_date',
                'endDate' => '+0d', 
                //'startDate' => date('#bcpayoutsummary-start_date'),
            ]
        ]);
        ?>            


    <?= $form->field($model, 'rm_value')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'remark')->textarea(['rows' => 6]) ?>


                

    
    <?= $form->field($model, 'excel')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Upload', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
            </div>
</div>
</div>
