<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ExcelTransaction */
$batch = \common\models\BCPayOutSummary::find()->count();
$batch = $batch + 1;
$this->title = 'Upload Excel - Batch '.$batch;
$this->params['breadcrumbs'][] = ['label' => 'Excel Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="col-md-12">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div><!-- /.box-header -->
        <div class="excel-transaction-create">
            <?=
            $this->render('_form', [
                'model' => $model,
            ])
            ?>
        </div>
    </div>
</div>

