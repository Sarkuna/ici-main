
<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PayOutSummarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = 'Bar Code Pay Out Summaries';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="col-lg-12">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-12 col-sm-12 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?></h3></div>

        </div>
        <div class="box-body table-responsive">
            <div class="bcpay-out-summary-index">
                <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'batch1',
                'label' => 'Description',
                'format' => 'html',
                //'headerOptions' => ['width' => '95'],
                'value' => function ($model) {
                    $string = floatval($model->rm_value);
                    return 'Batch '.$model->batch.'<br>('.$model->getStartredemption().'-'.$model->getEndredemption().')'.'<br>'.number_format($string, 2, '.', '').'<br>'.$model->getCountofSk();
                },
            ],
            [
                'attribute' => 'date_created1',
                'label' => 'Date',
                'format' => 'html',
                'value' => function ($model) {
                    return date('M-Y', strtotime($model->start_date));
                },
            ],            
            //'start_date',
            //'end_date',
            //'rm_value',
            [
                'attribute' => 'date_created1',
                'label' => 'Total Users',
                'format' => 'html',
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    return $model->getTotalUsers();
                },
            ],            
            [
                'attribute' => 'date_created1',
                'label' => 'Total RM',
                'format' => 'html',
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    $string = floatval($model->getTotalAmount());
                    return number_format($string, 2, '.', '');
                },
            ],
            [
                'attribute' => 'date_created1',
                'label' => 'Total Verified',
                'format' => 'html',
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    //return Yii::$app->formatter->asDecimal($model->getSumofPoints(),2);
                    return $model->getBankAccVerified();
                },
            ],
            [
                'attribute' => 'date_created1',
                'label' => 'Total Unverified',
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    //return Yii::$app->formatter->asDecimal($model->getTotalAmount(),2);
                    return $model->getBankAccUnverified();
                },
            ],
            [
                'attribute' => 'date_created1',
                'label' => 'Re Verified',
                'format' => 'html',
                //'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    return $model->getReVerified();
                },
            ],            
            [
                'attribute' => 'date_created1',
                'label' => 'Paid Acc',
                'format' => 'html',
                //'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    return $model->getPaidAcc();
                },
            ],
            //				
            [
                'attribute' => 'date_created1',
                'label' => 'Unpaid Acc',
                'format' => 'html',
                //'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    return $model->getUnpaidAcc();
                },
            ],

            [
                'attribute' => 'date_created1',
                'label' => 'Unpaid RM',
                'format' => 'html',
                //'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                'contentOptions' =>['class' => 'text-right'],
                'value' => function ($model) {
                    //return $model->getUnpaidAccRm();
                    return Yii::$app->formatter->asDecimal($model->getUnpaidAccRm(),2);
                },
            ],
            [
                            'class' => 'yii\grid\ActionColumn',
                            'headerOptions' => ['width' => '90', 'class' => 'text-center'],
                            'template' => '{summery} {view} {download} {upload}', //{update} {delete}
                            'buttons' => [
                                'summery' => function ($url, $model) {
                                    $url = '/management/report/bar-code-redemption-payment?id='.$model->id;
                                    return (Html::a('<span class="glyphicon glyphicon-download icon-4x text-danger"></span>', $url, ['title' => Yii::t('app', 'Summery'),]));
                                },
                                'view' => function ($url, $model) {
                                    return (Html::a('<span class="glyphicon glyphicon-eye-open text-warning"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                                },                                
                                'download' => function ($url, $model) {
                                    $url = '/management/report/bar-code-excel-summery?id='.$model->id;
                                    return (Html::a('<span class="glyphicon glyphicon-download text-success"></span>', $url, ['title' => Yii::t('app', 'Report Excel'),]));
                                },
                                'upload' => function ($url, $model) {
                                    $checklastid = \common\models\BCPayOutSummary::find()->orderBy(['id' => SORT_DESC])->one();
                                    if($checklastid->id == $model->id){
                                        $url = 'update?id='.$model->id;
                                        return (Html::a('<span class="glyphicon glyphicon-upload text-primary"></span>', $url, ['title' => Yii::t('app', $checklastid->id.'Upload Excel'.$model->id),]));
                                    }
                                },        
                            ],
                        ],            
        ],
    ]); ?>

            </div>
        </div>
    </div>
</div>

