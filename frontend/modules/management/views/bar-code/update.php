<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\BCPayOutSummary */

$this->title = 'Update Bc Pay Out Summary: ';
$this->params['breadcrumbs'][] = ['label' => 'Bc Pay Out Summaries', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="col-md-12 bcpay-out-summary-update">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div><!-- /.box-header -->
        <div class="excel-transaction-create">
            <div class="box-body">
                <div class="col-md-6 no-padding">
                    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

                    
                    <?= $form->field($model, 'excel')->fileInput() ?>

                    <div class="form-group">
                        <?= Html::submitButton('Upload', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
