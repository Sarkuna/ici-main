<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
//use kartik\grid\GridView;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\PayOutSummary */
$this->title = 'Batch '.$model->batch.' ('.$model->getStartredemption().'-'.$model->getEndredemption().')';;
$this->params['breadcrumbs'][] = ['label' => 'Pay Out Summaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$daterange = date('d-M-Y', strtotime($model->start_date)).' '.date('d-M-Y', strtotime($model->end_date));


            
?>
<style>
    .ui-pnotify{display: none;}
</style>
<div class="col-lg-12">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-8 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?> - Mobi Balance RM <span class="text-bold text-green"><?= number_format(Yii::$app->ici->mobiBalance(),2); ?></span> - <span class="text-bold text-red redemption-req_amount"></span></h3></div>
            <div class="col-lg-4 col-sm-2 col-xs-12 text-right no-padding">
                <?= $daterange ?>
            </div>
        </div>
        <div class="box-body">
            <?=Html::beginForm(['/management/bar-code/bulk-payment?id='.$model->id],'post');?>
            <input id="mobibalance" type="hidden" value="<?= Yii::$app->ici->mobiBalance(); ?>" />
            <input id="selectamt" type="hidden" value="" />
            <div class="col-lg-12 actionbulk no-padding">

                    <div class="col-lg-4 no-padding">
                        <label class="control-label" for="redemptionbulk-comment"></label>
                        <div class="form-group">
                            <?=
                            Html::submitButton('Submit', ['class' => 'btn btn-success', 'id' => 'btnsubmit',])
                            ?>
                            <?= Html::a('Cancel', ['/management/bar-code/view?id='.$model->id], ['class'=>'btn btn-default']) ?>
                            
                        </div>
                    </div>                      
                </div>
            <div class="col-lg-12 pay-out-summary-index table-responsive">
                 <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        ['class' => 'yii\grid\CheckboxColumn','headerOptions' => ['width' => '10'],],
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'crm_id',
                            'label' => 'CRM ID',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                return $model->crm_id;
                                
                            },
                        ],
                        [
                            'attribute' => 'card_id',
                            'label' => 'Membership #',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                if(!empty($model->painterID)) {
                                    return $model->profile->card_id;
                                }else {
                                    return 'N/A';
                                }
                                
                            },
                        ],
                        [
                            'attribute' => 'account_name',
                            'label' => 'Full Name',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '50'],
                            'value' => function ($model) {
                                if(!empty($model->painterID)) {
                                    return (Html::a($model->bank->account_name, ['/painter/painterprofile/view', 'id' => $model->profile->id, '#' => 'bank'], ['title' => Yii::t('app', $model->bank->account_name), 'target' => '_blank']));
                                }else {
                                    return 'N/A';
                                }
                            },
                        ],
                        [
                            'attribute' => 'bank_name',
                            'label' => 'Bank Name',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '380'],
                            'value' => function ($model) {
                                if(!empty($model->painterID)) {
                                    $banks = common\models\Banks::find()->where(['id' => $model->bank->bank_name])->one();
                                    $bankname = $banks->bank_name;
                                }else {
                                    $bankname = 'N/A';
                                }
                                return $bankname;
                            },
                            //'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            //'filter'=>ArrayHelper::map(common\models\Banks::find()->where(['IN', 'id' ,$bank_names])->asArray()->all(), 'id', 'bank_name'),        
                        ],

                        [
                            'attribute' => 'account_number',
                            'label' => 'Bank Acc #',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                if(!empty($model->painterID)) {
                                    return $model->bank->account_number;
                                }else {
                                    return 'N/A';
                                }
                            },
                        ],
                        [
                            'attribute' => 'redemption_invoice_no',
                            'label' => 'Redemption',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                return $model->redemption_invoice_no;
                            },
                        ],
                        [
                            'attribute' => 'total_rm',
                            'label' => 'Total',
                            //'format' => ['decimal', 2],
                            'value' => function ($model) {
                                return $model->total_rm;
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                        ],
                        [
                            'attribute' => 'mobile',
                            'label' => 'Mobile',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                if(!empty($model->painterID)) {
                                    return $model->profile->mobile;
                                }else {
                                    return 'N/A';
                                }
                            },
                        ],
                        [
                            'attribute' => 'account_no_verification',
                            'label' => 'Account Verified',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                if(!empty($model->painterID)) {
                                    if($model->bank->account_no_verification == 'Y') {
                                        return 'Yes';
                                    }else {
                                        return 'No';
                                    }                                    
                                }else {
                                    return 'N/A';
                                }
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            'filter' => ["Y" => "Yes", "N" => "No"],        
                        ],
                        [
                            'attribute' => 'internel_status',
                            'label' => 'Status',
                            'format' => 'html',
                            'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                //$ord =
                                if(!empty($model->internel_status)) {
                                    $sttext = str_replace('_', ' ', $model->internel_status);                                            
                                    $instatus = ucwords($sttext);
                                }else {
                                    $instatus = 'Processing';
                                }
                                return $instatus;
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            'filter' => ['crm_id_not_found' => 'Crm Id Not Found', 'not_verified' => 'Not Verified', 're_verified' => 'Re Verified', 'processing' => 'Processing', 'review' => 'Review', 'cancelled' => 'Cancelled', 'refunded' => 'Refunded', 'rejected' => 'Rejected', 'manual_payment' => 'Manual Payment', 'invalid_acc' => 'Invalid Acc', 'declined' => 'Declined', 'unsuccessful' => 'Unsuccessful', 'on_hold' => 'On Hold', 'paid' => 'Paid'],         
                        ],
                        [
                            'attribute' => 'redemption_status_ray1',
                            'label' => 'Paid Date',
                            'format' => 'html',
                            'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                //$ord =
                                if(!empty($model->paid_date)) {
                                    return date('d-m-y', strtotime($model->paid_date));
                                }else {
                                    return 'N/A';
                                }
                                
                            },      
                        ],             

                        [
                            'attribute' => 'redemption_created_datetime1',
                            'label' => 'Request Date',
                            'format'=>['date', 'php:d-m-y'],//h:i:s
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->created_datetime;
                            },   
                        ],
                                   
                    ],
                ]);
                ?>
                <?= Html::endForm();?> 
            </div>
        </div>
    </div>
</div>

<?php
    $clientScript = '
        $("#w0 table tbody tr").click(function()
        {
            recalculate();
            checkbal();
            $(".ui-pnotify").show();
        });

        $(".select-on-check-all").change(function ()
        {
            recalculate();
            checkbal();
            $(".ui-pnotify").show();
        });

    ';
    $this->registerJs($clientScript, \yii\web\View::POS_END, 'booking-period');
    

    ?>

<script language="javascript">  
    function recalculate() {
        //var sum = 0;
        var sumrm = 0.00;
        //var sumtot = 0.00;
        var a = "";
        $("input[name='selection[]']:checked").each(function () {
            $this=$(this);
            //var amount = $(this).closest("tr").find("td:nth-child(6)").text();
            var amountrm = $(this).closest("tr").find("td:nth-child(10)").text();

            
            //if (this.checked) sum = sum + parseFloat(amount);
            if (this.checked) sumrm = sumrm + parseFloat(amountrm);
            //if (this.checked) sumtot = sumtot + parseFloat($(this).attr('rel'));
            if (this.checked) {
                if (a.length == 0) {
                    a = $(this).closest("tr").find("td:first").text();
                } else {
                    a = a + "," + $(this).closest("tr").find("td:first").text();
                }
            } else {
                if (a.length == 1) {
                    a = a.slice(0, 1);
                } else {
                    a = a.replace(("," + $(this).closest("tr").find("td:first").text()), "");
                }
            }
        });

        
        $(".redemption-req_amount").html(sumrm.toFixed(2));
        $("#selectamt").val(sumrm.toFixed(2));

        
        
    }
    
    function checkbal() {
        var selectedValue = parseInt($("#selectamt").val());
        var totalValue = parseInt($("#mobibalance").val());

        if (selectedValue <= totalValue) {
            $("#btnsubmit").prop('disabled', false);
        }else {
            $("#btnsubmit").prop('disabled', true);
        }
    }
    
    function checkempty() {
        var minamt = $("#csvsendpayment-csv_amount").val(); 
        if(minamt == '0.00' || minamt == '0' || minamt == ''){
            alert('Minimum Ammount Required above 0.00');
            return false;
        }else{
            return true;
        }
        
    }
//});
    </script> 
    <link href="/assets/6634fb90/css/pnotify.custom.css" rel="stylesheet">
    <div class="ui-pnotify " style="position: fixed; width: 300px; opacity: 1; overflow: visible; right: 25px; top: 25px;">
        <div class="alert ui-pnotify-container alert-info ui-pnotify-shadow" style="min-height: 16px; overflow: hidden;">
            <div class="ui-pnotify-icon">
                <span class="glyphicon glyphicon-exclamation-sign"></span>
            </div>
            <h4 class="ui-pnotify-title">Info</h4>
            <div class="ui-pnotify-text">
                <h4>
                Available Balance RM: <?= number_format(Yii::$app->ici->mobiBalance(),2); ?><br>
                Total Select RM: <span class="text-bold text-red redemption-req_amount">0</span>
                </h4>
            </div>
        </div>
    </div>