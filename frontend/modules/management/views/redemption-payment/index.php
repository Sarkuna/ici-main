<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PayOutSummarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = 'Pay Out Summaries';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-lg-12">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-8 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?></h3></div>
            <div class="col-lg-4 col-sm-2 col-xs-12 text-right no-padding">
                <?= Html::a('Create', ['create'], ['class'=>'btn btn-success']) ?>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="pay-out-summary-index">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'pay_out_summary_id',
                        [
                            'attribute' => 'date_created',
                            'label' => 'Date Created',
                            'format' => 'html',
                            'headerOptions' => ['width' => '100'],
                            //'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return date('d-m-Y', strtotime($model->date_created));
                            },
                        ],
                        //'date_created',
                        [
                            'attribute' => 'description1',
                            'label' => 'Description',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '100'],
                            //'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->description.'<br>('.$model->getStartredemption().'-'.$model->getEndredemption().')';
                            },
                        ],            
                        //'description:ntext',
                        
                        [
                            'attribute' => 'date_created1',
                            'label' => 'Total Users',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                            //'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->getTotalUsers();
                            },
                        ],
                        [
                            'attribute' => 'date_created1',
                            'label' => 'Total RM',
                            //'format' => ['decimal',2],
                            //'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                            //'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDecimal($model->getTotalAmount(),2);
                            },
                        ],             
                        [
                            'attribute' => 'date_created1',
                            'label' => 'Total Verified',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                            //'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->getBankAccVerified();
                            },
                        ],
                        //				
                        [
                            'attribute' => 'date_created1',
                            'label' => 'Total Unverified',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                            //'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->getBankAccUnverified();
                            },
                        ],
                        [
                            'attribute' => 'date_created1',
                            'label' => 'Re Verified',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                            'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->getReVerified();
                            },
                        ],             
                        [
                            'attribute' => 'date_created1',
                            'label' => 'Paid Acc',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                            //'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->getPaidAcc();
                            },
                        ],
                        [
                            'attribute' => 'date_created1',
                            'label' => 'Unpaid Acc',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                            //'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->getUnpaidAcc();
                            },
                        ],
                        [
                            'attribute' => 'date_created1',
                            'label' => 'Unpaid RM',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                            //'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->getUnpaidAccRm();
                            },
                        ],            
                                    
                        //['class' => 'yii\grid\ActionColumn'],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'headerOptions' => ['width' => '50', 'class' => 'text-center'],
                            'template' => '{view} {download}', //{update} {delete}
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                                },
                                'download' => function ($url, $model) {
                                    $url = '/management/report/redemption-payment?id='.$model->pay_out_summary_id;
                                    return (Html::a('<span class="glyphicon glyphicon-download"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                                },
                            ],
                        ],            
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>
