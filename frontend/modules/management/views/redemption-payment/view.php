<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
//use kartik\grid\GridView;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\PayOutSummary */

$this->title = $model->description;
$this->params['breadcrumbs'][] = ['label' => 'Pay Out Summaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$bank_names = common\models\Redemption::find()->joinWith([           
            'bank',
            ])
            ->where(['pay_out_summary_id' => $model->pay_out_summary_id])
            ->select('banking_information.bank_name')
            ->groupBy(['painterID','bank_name'])
            ->column();

if(!empty($model->start_date)){
    $daterange = date('d/m/Y', strtotime($model->start_date)).' - '.date('d/m/Y', strtotime($model->end_date));
}else {
    $daterange = '';
}
?>



<div class="col-lg-12">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-8 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?> </h3></div>
            <div class="col-lg-4 col-sm-2 col-xs-12 text-right no-padding">
                <?= $daterange ?>
            </div>
        </div>
        <div class="box-body">
            <div class="col-lg-6">
                <table class="table table-bordered">
                    <tr>
                        <th>Status</th>
                        <th>Number of Painters</th>
                        <th>Total Amount, RM</th>
                    </tr>
                    <?php
                        $internel_statuss = array("processing", "review", "cancelled", "not_verified", "paid", "refunded", "rejected", "manual_payment", "invalid_acc", "declined", "unsuccessful", "on_hold", "re_verified");
                        $status_text = '';
                        $redemptioncountsum = 0; $redemptionamountsum = 0.00;
                        foreach ($internel_statuss as $internel_status) {
                            
                            if($internel_status == 'paid') {
                                $redemptioncount = \common\models\Redemption::find()->where(['redemption_status_ray' => 19, 'pay_out_summary_id' => $model->pay_out_summary_id])->count();
                                $redemptionamount = \common\models\Redemption::find()->where(['redemption_status_ray' => 19, 'pay_out_summary_id' => $model->pay_out_summary_id])->sum('req_amount');
                            }else if($internel_status == 'not_verified') {
                                /*$notverifiedcount = \common\models\Redemption::find()->joinWith([           
                                    'bank',
                                ])
                                ->where(['redemption.pay_out_summary_id' => $model->pay_out_summary_id])
                                ->andWhere(['!=','banking_information.account_no_verification','Y'])        
                                ->count();
                                
                                $notverifiedamount = \common\models\Redemption::find()->joinWith([           
                                    'bank',
                                ])
                                ->where(['redemption.pay_out_summary_id' => $model->pay_out_summary_id])
                                ->andWhere(['!=','banking_information.account_no_verification1','Y'])        
                                ->sum('req_amount');
                                
                                $reviewcount = \common\models\Redemption::find()->where(['internel_status' => 'processing', 'pay_out_summary_id' => $model->pay_out_summary_id])->count();
                                $reviewamount = \common\models\Redemption::find()->where(['internel_status' => 'processing', 'pay_out_summary_id' => $model->pay_out_summary_id])->sum('req_amount');
                                
                                $redemptioncount = $notverifiedcount - $reviewcount;
                                $redemptionamount = $notverifiedamount - $reviewamount;*/
                                
                                $redemptioncount = \common\models\Redemption::find()->where(['internel_status' => 'not_verified', 'pay_out_summary_id' => $model->pay_out_summary_id])->count();
                                $redemptionamount = \common\models\Redemption::find()->where(['internel_status' => 'not_verified', 'pay_out_summary_id' => $model->pay_out_summary_id])->sum('req_amount');
                                
                            }else if($internel_status == 'processing') {
                                $redemptioncount = \common\models\Redemption::find()->joinWith(['bank'])
                                ->where(['internel_status' => $internel_status, 'pay_out_summary_id' => $model->pay_out_summary_id])
                                ->andWhere(['=','banking_information.account_no_verification','Y'])->count();
                                
                                $redemptionamount = \common\models\Redemption::find()->joinWith(['bank'])        
                                ->where(['internel_status' => $internel_status, 'pay_out_summary_id' => $model->pay_out_summary_id])
                                ->andWhere(['=','banking_information.account_no_verification','Y'])->sum('req_amount');  

                            }else {
                                $redemptioncount = \common\models\Redemption::find()->where(['internel_status' => $internel_status, 'pay_out_summary_id' => $model->pay_out_summary_id])->count();
                                $redemptionamount = \common\models\Redemption::find()->where(['internel_status' => $internel_status, 'pay_out_summary_id' => $model->pay_out_summary_id])->sum('req_amount');
                            }
                            $status_text = str_replace('_', ' ', $internel_status);
                            echo '<tr>
                                <td>'.ucwords($status_text).'</td>
                                <td class="text-right">'.$redemptioncount.'</td>
                                <td class="text-right">';
                                if(!empty($redemptionamount)){
                                    echo Yii::$app->formatter->asDecimal($redemptionamount,2);
                                }else {
                                    echo '-';
                                }
                                echo '</td>
                            </tr>';
                            $redemptioncountsum += $redemptioncount;
                            $redemptionamountsum += $redemptionamount;
                        }
                        
                        echo '<tr class="text-bold">
                                <td>Total</td>
                                <td class="text-right">'.$redemptioncountsum.'</td>
                                <td class="text-right">'.Yii::$app->formatter->asDecimal($redemptionamountsum,2).'</td>
                            </tr>';
                    ?>
                    
                    

                </table>
            </div>
            <div class="col-lg-6">
                <table class="table table-bordered">
                    <tr>
                        <th>Paid Date</th>
                        <th>Number of Painters</th>
                        <th>Total Amount, RM</th>
                    </tr>
                    <?php
                        $redemptionpaids = \common\models\Redemption::find()->where(['pay_out_summary_id' => $model->pay_out_summary_id])->andWhere(['is not','paid_date', null])->groupBy('paid_date')->all();
                        if(!empty($redemptionpaids)) {
                            $paidcountsum=0; $paidsum=0.00;
                            foreach ($redemptionpaids as $redemptionpaid) {
                                $nopainter = \common\models\Redemption::find()->where(['pay_out_summary_id' => $model->pay_out_summary_id])->andWhere(['paid_date' => $redemptionpaid->paid_date])->groupBy('painterID')->count();
                                $paidamount = \common\models\Redemption::find()->where(['pay_out_summary_id' => $model->pay_out_summary_id])->andWhere(['paid_date' => $redemptionpaid->paid_date])->sum('req_amount');
                                echo '<tr>
                                    <td>'.$redemptionpaid->paid_date.'</td>
                                    <td class="text-right">'.$nopainter.'</td>
                                    <td class="text-right">'.Yii::$app->formatter->asDecimal($paidamount,2).'</td>
                                </tr>';
                                
                                $paidcountsum += $nopainter;
                                $paidsum += $paidamount;
                            }
                            $final_painter = $redemptioncountsum - $paidcountsum;
                            $balnce = $redemptionamountsum - $paidsum;
                            echo '<tr class="text-bold">
                                <td>Total</td>
                                <td class="text-right">'.$paidcountsum.'</td>
                                <td class="text-right">'.Yii::$app->formatter->asDecimal($paidsum,2).'</td>
                            </tr>
                            <tr class="text-bold">
                                <td>Balance</td>
                                <td class="text-right">'.$final_painter.'</td>
                                <td class="text-right">'.Yii::$app->formatter->asDecimal($balnce,2).'</td>
                            </tr>';
                        }else {
                            echo '<tr>
                                <td colspan = "3" class="text-center">No payment records!</td>
                            </tr>';
                        }
                    ?>
                    
                    
                    

                </table>
            </div>
            
            <div class="col-lg-12 pay-out-summary-index table-responsive">
                 <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'card_id',
                            'label' => 'Membership #',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                return $model->profile->card_id;
                            },
                        ],
                        [
                            'attribute' => 'account_name',
                            'label' => 'Full Name',
                            'format' => 'raw',
                            //'headerOptions' => ['width' => '50'],
                            'value' => function ($model) {
                                //return $model->bank->account_name;
                                return (Html::a($model->profile->full_name, ['/painter/painterprofile/view', 'id' => $model->profile->id, '#' => 'bank'], ['title' => Yii::t('app', $model->bank->account_name), 'target' => '_blank']));
                            },
                        ],
                        [
                            'attribute' => 'bank_name',
                            'label' => 'Bank Name',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '380'],
                            'value' => function ($model) {
                                $banks = common\models\Banks::find()->where(['id' => $model->bank->bank_name])->one();
                                $bankname = $banks->bank_name;
                                return $bankname;
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            'filter'=>ArrayHelper::map(common\models\Banks::find()->where(['IN', 'id' ,$bank_names])->asArray()->all(), 'id', 'bank_name'),        
                        ],
                                    [
                            'attribute' => 'account_number',
                            'label' => 'Bank Acc Name',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->bank->account_name;
                            },
                        ],
                        [
                            'attribute' => 'account_number',
                            'label' => 'Bank Acc #',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->bank->account_number;
                            },
                        ],
                        [
                            'attribute' => 'redemption_invoice_no',
                            'label' => 'Redemption',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                return $model->redemption_invoice_no;
                            },
                        ],
                        [
                            'attribute' => 'req_amount',
                            'label' => 'Total',
                            'format' => ['decimal', 2],
                            'value' => function ($model) {
                                return $model->req_amount;
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                        ],
                        [
                            'attribute' => 'mobile',
                            'label' => 'Mobile',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->profile->mobile;
                            },
                        ],
                        [
                            'attribute' => 'account_no_verification',
                            'label' => 'Account Verified',
                            'format' => 'html',
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                //return $model->bank->account_no_verification;
                                if($model->bank->account_no_verification == 'Y') {
                                    return 'Yes';
                                }else {
                                    return 'No';
                                }
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            'filter' => ["Y" => "Yes", "N" => "No"],        
                        ],
                        [
                            'attribute' => 'internel_status',
                            'label' => 'Status',
                            'format' => 'html',
                            'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                //$ord =
                                if($model->redemption_status_ray == '2') {                                    
                                    if($model->bank->account_no_verification != 'Y') {
                                        if($model->internel_status == 'review') {
                                            $instatus = 'Review';
                                        }else {
                                            $instatus = 'Not Verified';
                                        }
                                       
                                    }else {
                                        if(!empty($model->internel_status)) {
                                            $sttext = str_replace('_', ' ', $model->internel_status);                                            
                                            $instatus = ucwords($sttext);
                                        }else {
                                            $instatus = 'Processing';
                                        }                                        
                                    }
                                }else {
                                    $instatus = $model->orderStatus2->name;
                                }
                                return $instatus;
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],        
                            'filter' => ['processing' => 'Processing', 're_verified' => 'Re Verified', 'review' => 'Review', 'cancelled' => 'Cancelled', 'refunded' => 'Refunded', 'rejected' => 'Rejected', 'manual_payment' => 'Manual Payment', 'invalid_acc' => 'Invalid Acc', 'declined' => 'Declined', 'unsuccessful' => 'Unsuccessful', 'on_hold' => 'On Hold', 'paid' => 'Paid'],         
                        ],
                        [
                            'attribute' => 'redemption_status_ray1',
                            'label' => 'Paid Date',
                            'format' => 'html',
                            'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                //$ord =
                                if(!empty($model->paid_date)) {
                                    return date('d-m-y', strtotime($model->paid_date));
                                }else {
                                    return 'N/A';
                                }
                                
                            },      
                        ],             

                        [
                            'attribute' => 'redemption_created_datetime1',
                            'label' => 'Request Date',
                            'format'=>['date', 'php:d-m-y'],//h:i:s
                            //'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->redemption_created_datetime;
                            },   
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update}', //{view} {delete}
                            'buttons' => [
                                'update' => function ($url, $model) {
                                    $url = 'update-bank?id=' . $model->redemptionID;
                                    if($model->redemption_status_ray == '2') {                                    
                                        if($model->bank->account_no_verification != 'Y') {                                            
                                           return (Html::a('<i class="glyphicon glyphicon-question-sign"></i>', $url, ['title' => Yii::t('app', 'Update'), 'onclick' => "updateBank(" . $model->redemptionID . ");return false;"]));
                                        }else {
                                           return (Html::a('<i class="glyphicon glyphicon-question-sign"></i>', $url, ['title' => Yii::t('app', 'Update'), 'onclick' => "updatePayment(" . $model->redemptionID . ");return false;"]));                                        
                                        }
                                    }
                                }
                            ],
                        ],            
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<?php
	yii\bootstrap\Modal::begin([
		'id' => 'guardModal',
		'header' => "<h4 class='modal-title'><i class='fa fa-edit'></i> Bank Account Number Verification</h4>",
	]);
 	yii\bootstrap\Modal::end(); 
?>

<?php
	yii\bootstrap\Modal::begin([
		'id' => 'guardModal1',
		'header' => "<h4 class='modal-title'><i class='fa fa-edit'></i> Payment Status </h4>",
	]);
 	yii\bootstrap\Modal::end(); 
?>

<script>
/***
  * Start Update Gardian Jquery
***/
function updateBank(id) {
	$.ajax({
	  type:'GET',
	  url:'<?= Url::toRoute(["redemption-payment/update-bank"]) ?>',
	  data: { id : id},
	  success: function(data)
		   {
		       $(".modal-content").addClass("row");
		       $('.modal-body').html(data);
		       $('#guardModal').modal();

		   }
	});
}
function updatePayment(id) {
	$.ajax({
	  type:'GET',
	  url:'<?= Url::toRoute(["redemption-payment/update-payment"]) ?>',
	  data: { id : id},
	  success: function(data)
		   {
		       $(".modal-content").addClass("row");
		       $('.modal-body').html(data);
		       $('#guardModal1').modal();

		   }
	});
}
</script>
