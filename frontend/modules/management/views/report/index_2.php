<?php
ini_set("memory_limit","128M");
use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\painter\models\PainterProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Summary View';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$amount = 0;
$totbox6 = 0;
$amount2 = 0;
    /*if (!empty($dataProvider->getModels())) {
        foreach ($dataProvider->getModels() as $key => $val) {
            $amount2 = $val->getTotalrmawarded() - $val->getTotal_rm_paid();
            $amount += $amount2;
            $totbox6 += $val->getTotalrmawarded();
        }
    }*/
    ?>
<div class="col-xs-12">
<!--    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-4 col-sm-4 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i></h3></div>
            <div class="col-xs-6"></div>
            <div class="col-lg-2 col-sm-2 col-xs-12 no-padding">
                <div class="col-xs-12 no-padding"></div>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="painter-profile-index">-->
                
                <?php
                // Header and Footer options for PDF format
        $ourPdfHeader = [
            'L' => [
                'content'   => 'Summary View',
                'font-size' => 8,
                'color'     => '#333333'
            ],
            'C' => [
                'content'   => '',
                'font-size' => 16,
                'color'     => '#333333'
            ],
            'R' => [
                'content'   => 'Generated' . ': ' . date("D, d-M-Y g:i a T"),
                'font-size' => 8,
                'color'     => '#333333'
            ]
        ];
        $ourPdfFooter = [
            'L'    => [
                'content'    => '',
                'font-size'  => 8,
                'font-style' => 'B',
                'color'      => '#999999'
            ],
            'R'    => [
                'content'     => '[ {PAGENO} ]',
                'font-size'   => 10,
                'font-style'  => 'B',
                'font-family' => 'serif',
                'color'       => '#333333'
            ],
            'line' => TRUE,
        ];

        $exportFilename = date("Y-m-d_H-m-s").'_summary_view';

        $exportConfig = [

            GridView::CSV   => [
                'label'           => 'CSV',
                'icon'            => ' fa fa-file-code-o',
                'iconOptions'     => ['class' => 'text-primary'],
                'showHeader'      => TRUE,
                'showPageSummary' => TRUE,
                'showFooter'      => TRUE,
                'showCaption'     => TRUE,
                'filename'        => $exportFilename,
                'alertMsg'        => 'The CSV export file will be generated for download.',
                'options'         => ['title' => 'Comma Separated Values'],
                'mime'            => 'application/csv',
                'config'          => [
                    'colDelimiter' => ",",
                    'rowDelimiter' => "\r\n",
                ]
            ],
            GridView::EXCEL => [
                'label'           => 'Excel',
                'icon'            => ' fa fa-file-excel-o',
                'iconOptions'     => ['class' => 'text-success'],
                'showHeader'      => TRUE,
                'showPageSummary' => TRUE,
                'showFooter'      => TRUE,
                'showCaption'     => TRUE,
                'filename'        => $exportFilename,
                'alertMsg'        => 'The EXCEL export file will be generated for download.',
                'options'         => ['title' => 'Microsoft Excel 95+'],
                'mime'            => 'application/vnd.ms-excel',
                'config'          => [
                    'worksheet' => 'Worksheet',
                    'cssFile'   => ''
                ]
            ],
            GridView::PDF   => [
                'label'           => 'PDF',
                'icon'            => ' fa fa-file-pdf-o',
                'iconOptions'     => ['class' => 'text-danger'],
                'showHeader'      => TRUE,
                'showPageSummary' => TRUE,
                'showFooter'      => TRUE,
                'showCaption'     => TRUE,
                'filename'        => $exportFilename,
                'alertMsg'        => 'The PDF export file will be generated for download.',
                'options'         => ['title' => 'Portable Document Format'],
                'mime'            => 'application/pdf',
                'config'          => [
                    'mode'          => 'c',
                    'format'        => 'A4-L',
                    'destination'   => 'D',
                    'marginTop'     => 20,
                    'marginBottom'  => 20,
                    'cssInline'     => '.kv-wrap{padding:20px;}' .
                        '.kv-align-center{text-align:center;}' .
                        '.kv-align-left{text-align:left;}' .
                        '.kv-align-right{text-align:right;}' .
                        '.kv-align-top{vertical-align:top!important;}' .
                        '.kv-align-bottom{vertical-align:bottom!important;}' .
                        '.kv-align-middle{vertical-align:middle!important;}' .
                        '.kv-page-summary{border-top:4px double #ddd;font-weight: bold;}' .
                        '.kv-table-footer{border-top:4px double #ddd;font-weight: bold;}' .
                        '.kv-table-caption{font-size:1.5em;padding:8px;border:1px solid #ddd;border-bottom:none;}',
                    'methods'       => [
                        'SetHeader' => [
                            ['odd' => $ourPdfHeader, 'even' => $ourPdfHeader]
                        ],
                        'SetFooter' => [
                            ['odd' => $ourPdfFooter, 'even' => $ourPdfFooter]
                        ],
                    ],
                    'options'       => [
                        'title'    => 'Summary View',
                        'subject'  => 'PDF export',
                        'keywords' => 'pdf'
                    ],
                    'contentBefore' => '',
                    'contentAfter'  => ''
                ]
            ]
        ];
                $gridColumns = [
                        //['class' => 'yii\grid\SerialColumn'],
                        //['class' => 'kartik\grid\SerialColumn'],
                        //'full_name',
                        [
                            //'class' => 'kartik\grid\EditableColumn',
                            'attribute' => 'full_name',
                            'options' => ['width' => '200'],
                            'pageSummary' => 'Total Summary',
                            //'vAlign'=>'middle',
                        ],
                        [
                            'attribute' => 'ic_no',
                            'label' => 'NRIC/PP Number',
                            'format' => 'raw',
                            //'format' => ['date', 'medium'],
                            'value' => function ($model) {
                                return $model->ic_no;
                            },
                            //'group'=>true,
                        ],
                        [
                            'attribute' => 'total_no_transactions',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->getTotalnotransactions();
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            'options' => ['width' => '100']
                        ],
                        [
                            'attribute' => 'total_items',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->getTotalitems();
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            //'options' => ['width' => '100']
                        ],
                        [
                            'attribute' => 'total_points_awarded',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->getTotalpointsawarded();
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            'options' => ['width' => '100']
                        ], 
                        [
                            'attribute' => 'total_rm_awarded',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDecimal($model->getTotalrmawarded());
                            },
                            'contentOptions' =>['class' => 'text-right',],        
                            'options' => ['width' => '100'],                             
                            //'pageSummary' => Yii::$app->formatter->asDecimal($totbox6),
                            'pageSummary' => TRUE,
                            //'vAlign'=>'top',
                            'hAlign'=>'right',        
                        ],
                        [
                            'attribute' => 'total_no_of_redemptions',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->getTotal_no_of_redemptions();
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            'options' => ['width' => '100']
                        ],            
                        [
                            'attribute' => 'total_points_redeemed',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->getTotal_points_redeemed();
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            'options' => ['width' => '100']
                        ],            
                        [
                            'attribute' => 'total_rm_paid',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDecimal($model->getTotal_rm_paid());
                            },
                            'contentOptions' =>['class' => 'text-right',],        
                            //'options' => ['width' => '80']
                        ],
                        [
                            'attribute' => 'balance_total_points',
                            'format' => 'raw',
                            'value' => function ($model) {
                                $vbalance_total_points = '0';
                                $vbalance_total_points = $model->getTotalpointsawarded() - $model->getTotal_points_redeemed();
                                return $vbalance_total_points;
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            //'options' => ['width' => '100']
                        ],
                        [
                            'attribute' => 'Balance_rm_to_be_paid',
                            'format' => 'raw',
                            'value' => function ($model) {
                                $bal = '0.00';
                                $bal = $model->getTotalrmawarded() - $model->getTotal_rm_paid();
                                return Yii::$app->formatter->asDecimal($bal);
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            'options' => ['width' => '100'],
                            //'footerOptions'=>['class' => 'text-right','style'=>'font-weight:bold;'],
                            //'footer' => $amount,
                            //'pageSummary' => Yii::$app->formatter->asDecimal($amount),
                            'pageSummary' => TRUE,        
                            'hAlign'=>'right',        
                            //'pageSummaryOptions'=>['class' => 'text-right','style'=>'font-weight:bold;'],        
                                   
                            //'headerOptions'=>['class'=>'text-right text'],        
                            //'contentOptions' =>['class' => 'kv-align-right',],        
                        ],
                        //['class' => 'kartik\grid\CheckboxColumn'],            
                    ];
                echo GridView::widget([
                    'dataProvider'=> $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumns,
                    'pjax' => true,
                    'bordered' => true,
                    'striped' => false,
                    'condensed' => false,
                    'responsive' => true,
                    'hover' => true,
                    //'exportConfig'     => $exportConfig,
                    //'exportConfig'     => false,
                    'export' => false,
                    'toolbar' =>  [
                        ['content'=>
                            //Html::button('<i class="fa fa-file-excel-o"></i> Export to Excel', ['my-action', 'type'=>'button', 'title'=>'hhh', 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . '&nbsp;&nbsp;&nbsp;'.
                            Html::a(Yii::t('app', '<i class="fa fa-file-excel-o"></i> Export to Excel'), ['summary-view-excel'], ['class' => 'btn btn-primary',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to download?'),
                                    'method' => 'post',
                                ],
                            ]).
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/management/report/'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'ggg'])
                        ],
                        //'{export}',
                        //'{toggleData}'
                    ],
                    'floatHeader' => false,
                    //'floatHeaderOptions' => ['scrollingTop' => $scrollingTop],
                    'showPageSummary' => true,
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => '<i class="fa fa-th-list"></i> '.Html::encode($this->title).''
                    ],
                    
                ]);
                ?>
                
<!--            </div>
        </div>
    </div>-->
</div>