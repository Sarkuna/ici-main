<?php
/*ini_set("memory_limit","128M");
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;*/

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\painter\models\PainterProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Summary View';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-12">
<div class="box box-primary">
    <div class="box-header with-border">
        <div class="col-lg-12 text-left" style="padding-left: 0px;">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

        </div>


    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="vipcustomer-index">
            <?php
        
                $gridColumns = [
                        //['class' => 'yii\grid\SerialColumn'],
                        //['class' => 'kartik\grid\SerialColumn'],
                        //'full_name',
                        [
                            //'class' => 'kartik\grid\EditableColumn',
                            'attribute' => 'full_name',
                            'options' => ['width' => '200'],
                            'pageSummary' => 'Total Summary',
                            //'vAlign'=>'middle',
                        ],
                        [
                            'attribute' => 'ic_no',
                            'label' => 'NRIC/PP Number',
                            'format' => 'raw',
                            //'format' => ['date', 'medium'],
                            'value' => function ($model) {
                                return $model->ic_no;
                            },
                            //'group'=>true,
                        ],
                        [
                            'attribute' => 'total_no_transactions',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->getTotalnotransactions();
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            'options' => ['width' => '100']
                        ],
                        [
                            'attribute' => 'total_items',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->getTotalitems();
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            //'options' => ['width' => '100']
                        ],
                        [
                            'attribute' => 'total_points_awarded',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->getTotalpointsawarded();
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            'options' => ['width' => '100']
                        ], 
                        [
                            'attribute' => 'total_rm_awarded',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDecimal($model->getTotalrmawarded());
                            },
                            'contentOptions' =>['class' => 'text-right',],        
                            'options' => ['width' => '100'],                             
                            //'pageSummary' => Yii::$app->formatter->asDecimal($totbox6),
                            'pageSummary' => TRUE,
                            //'vAlign'=>'top',
                            'hAlign'=>'right',        
                        ],
                        [
                            'attribute' => 'total_no_of_redemptions',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->getTotal_no_of_redemptions();
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            'options' => ['width' => '100']
                        ],            
                        [
                            'attribute' => 'total_points_redeemed',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->getTotal_points_redeemed();
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            'options' => ['width' => '100']
                        ],            
                        [
                            'attribute' => 'total_rm_paid',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDecimal($model->getTotal_rm_paid());
                            },
                            'contentOptions' =>['class' => 'text-right',],        
                            //'options' => ['width' => '80']
                        ],
                        [
                            'attribute' => 'balance_total_points',
                            'format' => 'raw',
                            'value' => function ($model) {
                                $vbalance_total_points = '0';
                                $vbalance_total_points = $model->getTotalpointsawarded() - $model->getTotal_points_redeemed();
                                return $vbalance_total_points;
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            //'options' => ['width' => '100']
                        ],
                        [
                            'attribute' => 'Balance_rm_to_be_paid',
                            'format' => 'raw',
                            'value' => function ($model) {
                                $bal = '0.00';
                                $bal = $model->getTotalrmawarded() - $model->getTotal_rm_paid();
                                return Yii::$app->formatter->asDecimal($bal);
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            'options' => ['width' => '100'],
                            //'footerOptions'=>['class' => 'text-right','style'=>'font-weight:bold;'],
                            //'footer' => $amount,
                            //'pageSummary' => Yii::$app->formatter->asDecimal($amount),
                            'pageSummary' => TRUE,        
                            'hAlign'=>'right',        
                            //'pageSummaryOptions'=>['class' => 'text-right','style'=>'font-weight:bold;'],        
                                   
                            //'headerOptions'=>['class'=>'text-right text'],        
                            //'contentOptions' =>['class' => 'kv-align-right',],        
                        ],
                        //['class' => 'kartik\grid\CheckboxColumn'],            
                    ];
                
                ?>
<?=
                ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'exportConfig' => [
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_HTML => false,
                    ],
                    'filename' => Html::encode($this->title).Date('YmdGis'),
                    //'showColumnSelector'=> true,
                    //'fontAwesome' => true,
                    //'batchSize' => 50,
                    //'target' => '_blank',
                    //'folder' => '@webroot/tmp', // this is default save folder on server
                ]);
                            
                            
                echo GridView::widget([
                    'tableOptions' => ['id' => 'pointstbl'],
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    //'pjax'=>true, 
                    'columns' => $gridColumns,
                    
                ]);
                ?>
        </div>
    </div>
</div>
</div>
<?php
    $script = <<<EOD
                
    $(function () {
        var chkall = $("input[name='export_columns_toggle']");   
        chkall.click(function () {
            if($(this).prop("checked") == true){
                var table = $("table tr");
                table.find("th, td").css('display', '');
            }
            else if($(this).prop("checked") == false){
                var table = $("table tr");
                table.find("th, td").toggle();
            }
        });    
        var chk = $("#w0-cols-list input:checkbox"); 
        var tbl = $("#pointstbl");
        var tblhead = $("#pointstbl th");

        chk.prop('checked', true);
        chk.click(function () {
            var cbox_val = $(this).data('key');
            $("table tr").find("th:eq("+cbox_val+")").toggle();  
            $("table tr").find("td:eq("+cbox_val+")").toggle();
        });   
    });      

EOD;
$this->registerJs($script);
    ?>