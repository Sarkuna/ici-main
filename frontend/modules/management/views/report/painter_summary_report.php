<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\painter\models\PainterProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Painter Account Management Summary Report';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-12">
<!--    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-4 col-sm-4 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i></h3></div>
            <div class="col-xs-6"></div>
            <div class="col-lg-2 col-sm-2 col-xs-12 no-padding">
                <div class="col-xs-12 no-padding"></div>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="painter-profile-index">-->
                
                <?php
 
                $gridColumns = [
                        //['class' => 'yii\grid\SerialColumn'],
                        //['class' => 'kartik\grid\SerialColumn'],
                        //'item_id',
                        //'point_order_id',
                    [
                        'attribute' => 'card_id',
                        'label' => 'Membership #',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '95'],
                        'value' => function ($model) {
                            return $model->card_id;
                        },
                    ],
                    [
                        'attribute' => 'full_name',
                        'label' => 'Full Name',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '250'],
                        'value' => function ($model) {
                            return $model->full_name;
                        },
                    ],
                    [
                        'attribute' => 'email',
                        'label' => 'Email ID',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->user->email;
                        },
                    ],
                    [
                        'attribute' => 'dob',
                        'label' => 'Date of Birth',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '150'],
                        'value' => function ($model) {
                            return date('d-m-Y', strtotime($model->dob));
                        },
                    ],            
                    [
                        'attribute' => 'mobile',
                        'label' => 'Mobile No',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '150'],
                        'value' => function ($model) {
                            return $model->mobile;
                        },
                    ],
                    [
                        'attribute' => 'ic_no',
                        'label' => 'NRIC/Passport #',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '150'],
                        'value' => function ($model) {
                            return $model->ic_no;
                        },
                    ],
                    [
                        'attribute' => 'race',
                        'label' => 'Race',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->profileRace->title;
                        },
                    ],
                                
                                        
                    [
                        'attribute' => 'address',
                        'label' => 'Address',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '150'],
                        'value' => function ($model) {
                            return $model->address;
                        },
                    ],
                    [
                        'attribute' => 'race',
                        'label' => 'Race',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->profileRace->title;
                        },
                    ],
                    [
                        'attribute' => 'nationality',
                        'label' => 'Nationality',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->nationality == 'L' ? 'Malaysian' : 'Non Malaysian';
                        },
                    ],            
                    [
                        'attribute' => 'country',
                        'label' => 'Country',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->profileCountry->name;
                        },
                    ],
                    [
                        'attribute' => 'region_id',
                        'label' => 'Region',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->profileRegion->region_name;
                        },
                    ],
                    [
                        'attribute' => 'state_id',
                        'label' => 'State',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            if(!empty($model->state_id)){
                                //return $model->state_id;
                                return $model->profileState->state_name;
                            }else {
                                return "N/A";
                            }
                            
                        },
                    ],            
                    [
                        'attribute' => 'company_name',
                        'label' => 'Company Name',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->company->company_name;
                        },
                    ],
                    [
                        'attribute' => 'no_painters',
                        'label' => 'No. of Employees',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->company->no_painters;
                        },
                    ],            
                    [
                        'attribute' => 'painter_sites',
                        'label' => 'No. of Painting Jobs',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->company->painter_sites;
                        },
                    ],
                    [
                        'attribute' => 'customer_name',
                        'label' => 'Registered At Dealer Outlet',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->company->dealerOutlet->customer_name;
                        },
                    ],
                    /*[
                        'attribute' => 'address',
                        'label' => 'Dealer Address',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->company->dealerOutlet->address;
                        },
                    ],*/            
                    [
                        'attribute' => 'bank_name',
                        'label' => 'Bank Name',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '380'],
                        'value' => function ($model) {
                            $banks = common\models\Banks::find()->where(['id' => $model->bank->bank_name])->one();
                            $bankname = $banks->bank_name;
                            return $bankname;
                        },
                    ],            
                    
                    [
                        'attribute' => 'account_name',
                        'label' => 'Name of Account Holder',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->bank->account_name;
                        },
                    ],
                    [
                        'attribute' => 'account_number',
                        'label' => 'Account Number',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->bank->account_number;
                        },
                    ],
                    [
                        'attribute' => 'created_datetime',
                        'label' => 'Create Date',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return date('d-m-Y',strtotime($model->created_datetime));
                        },
                    ],            
                    /*[
                        'attribute' => 'bank_name',
                        'label' => 'Bank Name',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '380'],
                        'value' => function ($model) {
                            $banks = common\models\Banks::find()->where(['id' => $model->bank->bank_name])->one();
                            $bankname = $banks->bank_name;
                            return $bankname;
                        },
                    ],
                    [
                        'attribute' => 'redemption_created_datetime',
                        'label' => 'Request Date',
                        'format'=>['date', 'php:d-M-y'],
                        'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->redemption_created_datetime;
                        },   
                    ],
                    [
                        'attribute' => 'redemption_invoice_no',
                        'label' => 'Redemption #',
                        'format'=> 'text',
                        'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->redemption_invoice_no;
                        },   
                    ],          

                                     
                        [
                            'attribute' => 'redemption_status_ray_date',
                            'label' => 'Paid Date',
                            //'format'=>['date', 'php:d-M-y'],
                            'headerOptions' => ['width' => '190'],
                            'value' => function ($model) {
                                if(empty($model->redemption_status_ray_date)){
                                    $raydate = "N/A";
                                }else{
                                    $raydate = date('d-M-y', strtotime($model->redemption_status_ray_date));
                                }
                                return $raydate;
                            },   
                        ],*/
                                    
                                    
                    ];
                echo GridView::widget([
                    'dataProvider'=> $dataProvider,
                    'filterModel' => $searchModel,
                    'autoXlFormat'=>true,
                    'columns' => $gridColumns,
                    'pjax' => true,
                    'bordered' => true,
                    'striped' => false,
                    'condensed' => false,
                    'responsive' => true,
                    'hover' => true,
                    'export' => false,
                    'toolbar' =>  [
                        ['content'=>
                            //Html::button('<i class="fa fa-file-excel-o"></i> Export to Excel', ['my-action', 'type'=>'button', 'title'=>'hhh', 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . '&nbsp;&nbsp;&nbsp;'.
                            Html::a(Yii::t('app', '<i class="fa fa-file-excel-o"></i> Main Painter Profile'), ['painter-account-management-excel-profile'], ['class' => 'btn btn-info',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to download?'),
                                    'method' => 'post',
                                ],
                            ]).
                            Html::a(Yii::t('app', '<i class="fa fa-file-excel-o"></i> Main Dealer Profile'), ['painter-account-management-excel-dealer'], ['class' => 'btn btn-success',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to download?'),
                                    'method' => 'post',
                                ],
                            ]). 
                            Html::a(Yii::t('app', '<i class="fa fa-file-excel-o"></i> Export to Excel'), ['painter-account-management-excel'], ['class' => 'btn btn-primary',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to download?'),
                                    'method' => 'post',
                                ],
                            ]).
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/management/report/painterprofilesummery'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'ggg'])
                        ],
                        //'{export}',
                        //'{toggleData}'
                    ],
                    //'exportConfig'     => $exportConfig,
                    'floatHeader' => false,
                    //'floatHeaderOptions' => ['scrollingTop' => $scrollingTop],
                    'showPageSummary' => false,
                    'resizableColumns'=>true,
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => '<i class="fa fa-th-list"></i> '.Html::encode($this->title).''
                    ],
                    
                ]);
                ?>
                
<!--            </div>
        </div>
    </div>-->
</div>