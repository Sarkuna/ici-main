<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\painter\models\PainterProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bulk Payment to Maybank';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php

$amountpaid = 0;
$totbox6 = 0;
$qtypoint = 0;
$bankdatas = $dataProvider->getModels();
if (!empty($bankdatas)) {
        $totalitem1 = count($bankdatas);
        foreach ($bankdatas as $key => $val) {
            //$amount2 = $val->getTotalrmawarded() - $val->getTotal_rm_paid();
           // $amount += $amount2;
            //$totbox6 += $val->getTotalrmawarded();
            $totbox6 += $val->req_amount;
            if($val->redemption_status_ray == '2'){
                $paidam = '0.00';
            }else{
                $paidam = $val->req_amount;
            }
            $amountpaid +=$paidam;
            $qtypoint += $val->req_points;
        }
        $totamount = str_replace(".", "", Yii::$app->formatter->asDecimal($totbox6));
        $totamount = str_replace(",", "", $totamount);
        $vtotamount = str_pad($totamount, 15, "0", STR_PAD_LEFT);
    }
    
        $filename = 'Data-'.Date('YmdGis').'-MAYBANK_MAS.txt';
        header('Content-type: text/plain');
        header("Content-Disposition: attachment; filename=".$filename);
        
        $hd1 = 'H'; //Recordtype *
        $hd2 = 'APS';//Servicetype *
        $hd3 = 'P';//testingindicater
        $hd4 = '005';//comid
        $hd5 = '65391';//OriginatorID*
        $shortname = substr("REWARDSSOL", 0, 13);
        $hd6 = str_pad($shortname, 13, " ", STR_PAD_RIGHT);
        $longname = substr("REWARDS SOLUTION SDN BHD", 0, 40);
        $hd7 = str_pad($longname, 40, " ", STR_PAD_RIGHT);
        $hd8 = '512343627558';//Account Num *
        $hd9 = str_repeat(' ', 8);//paymentexportdate
        $hd10 = str_pad('', 20, " ", STR_PAD_RIGHT);//paymentexportdate
        $hd11 = str_repeat(' ', 8);//paymentexportdate
        $hd12 = $vtotamount;//*
        $hd13 = str_repeat(' ', 14);
        $hd14 = str_repeat(' ', 40);
        $hd15 = 'P';//Type of Module *
        
        echo $hd1.$hd2.$hd3.$hd4.$hd5.$hd6.$hd7.$hd8.$hd9.$hd10.$hd11.$hd12.$hd13.$hd14.$hd15;
        echo "\r\n";
        $totalitem = 0;
        foreach($bankdatas as $bankdata){
            $var1 = str_pad('D', 1, '' );
            $var2 = str_repeat(' ', 8);

            $icnum = substr($bankdata->profile->ic_no, 0, 12);
            $string4 = str_pad($icnum, 12, "*", STR_PAD_RIGHT);
            $var4 = str_replace("*", " ", $string4);
            $noic = str_repeat(' ', 24);
            
            $banks = common\models\Banks::find()->where(['id' => $bankdata->bank->bank_name])->one();
            $bankname = $banks->bank_name;
            $bankcode = $banks->bank_code;
            if($bankcode == '001'){
                $paymode = '011';
            }else{
                $paymode = '012';
            }
            
            $acnum = substr($bankdata->bank->account_number, 0, 20);
            $name = substr($bankdata->bank->account_name, 0, 40);
            $string = str_pad($name, 40, "*", STR_PAD_RIGHT);
            $var3 = str_replace("*", " ", $string);
            
            $ac_num = str_pad($acnum, 20, "*", STR_PAD_RIGHT);
            $account_number = str_replace("*", " ", $ac_num);
            
            $price = str_replace(".", "", $bankdata->req_amount);
            $pricerm = str_pad($price, 15, "0", STR_PAD_LEFT);
            $totalitem++;
            //$totalitem1 .= $totalitem;
            //echo $var1;
            echo $var1.$var2.$var3.$var4.$noic.$paymode.$bankcode.$account_number.$pricerm."\r\n";
        }
        $titem = str_pad($totalitem1, 15, "0", STR_PAD_LEFT);
        echo "T".$titem.$vtotamount;     
die();
    ?>
