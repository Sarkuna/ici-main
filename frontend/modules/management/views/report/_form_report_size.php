<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    input#findpainter-ic_no {font-size: 35px;}
    .field-findpainter-membership_id {display: none;}
</style>
<div class="login-box">
    <div class="login-box-body">
        <div class="point-order-form">
            <p class="text-info"><em>Please select Month from drop-down list</em></p>
            <?php $form = ActiveForm::begin(); ?>
            
            <?php

            $visits = \Yii::$app->getDb()->createCommand("SELECT `order_id`, Year(created_datetime) AS `Year` FROM `point_order` WHERE `redemption` = 'Y' GROUP BY `Year` ORDER BY `Year` DESC")->queryAll();
            $items = ArrayHelper::map($visits, 'Year', 'Year');

            ?>

            <?php

                                echo $form->field($model, 'reportdate')->dropDownList($items, [
                                    'prompt' => '-- Select --'
                                    
                                ]);
                            ?>
            
            

        <button class="btn btn-info btn-flat btn-lg btn-block" type="submit">Go!</button>
        <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<?php
$script = <<< JS

$(document).ready(function () {
    $("#findpainter-type").change(function(){        
        var val = $('#findpainter-type').val();
        if(val == 'mid' ) {
            $('.field-findpainter-membership_id').show();
            $('.field-findpainter-ic_no').hide();
        } else {
            $('.field-findpainter-ic_no').show();
            $('.field-findpainter-membership_id').hide();
        }
    });
});

JS;
$this->registerJs($script);
?>