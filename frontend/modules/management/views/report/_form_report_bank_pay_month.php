<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */
/* @var $form yii\widgets\ActiveForm */



$payoutsummarys = \common\models\PayOutSummary::find()->orderBy(['pay_out_summary_id' => SORT_DESC,])->all();
//$listData = ArrayHelper::map($payoutsummarys, 'pay_out_summary_id', 'start_date');

$listData = ArrayHelper::map($payoutsummarys, 'pay_out_summary_id', function ($data) {
    return date('M-Y', strtotime($data->start_date));
});


?>
<style>
    input#findpainter-ic_no {font-size: 35px;}
    .field-findpainter-membership_id {display: none;}
</style>
<div class="login-box">
    <div class="login-box-body">
        <div class="point-order-form">
            
            <?php $form = ActiveForm::begin(); ?> 
            <?php
            echo $form->field($model, 'reportdate')->widget(Select2::classname(), [
                'data' => $listData,
                'options' => ['prompt' => 'Select a state ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
            
       <?php
        echo $form->field($model, 'status')->dropDownList(
            ['A' => 'All', 'Y' => ' Verified', 'N' => 'Not Verified']
    ); ?>     

        <button class="btn btn-info btn-flat btn-lg" type="submit">Go!</button>
        <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<?php
$script = <<< JS

$(document).ready(function () {
    $("#findpainter-type").change(function(){        
        var val = $('#findpainter-type').val();
        if(val == 'mid' ) {
            $('.field-findpainter-membership_id').show();
            $('.field-findpainter-ic_no').hide();
        } else {
            $('.field-findpainter-ic_no').show();
            $('.field-findpainter-membership_id').hide();
        }
    });
});

JS;
$this->registerJs($script);
?>