<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    input#findpainter-ic_no {font-size: 35px;}
    .field-findpainter-membership_id {display: none;}
</style>
<div class="login-box">
    <div class="login-box-body">
        <div class="point-order-form">
            <p class="text-info"><em>Please select Month from drop-down list</em></p>
            <?php $form = ActiveForm::begin(); ?>

            <?=
        $form->field($model, 'reportdate')->widget(DatePicker::classname(), [
            'options' => ['readOnly' => true, 'placeholder' => 'From date ...'],
            'removeButton' => false,
            'pluginOptions' => [
                'autoclose' => true,
                'todayHighlight' => false,
                'todayBtn' => false,
                'format' => 'yyyy-mm-dd',
                'viewMode' => "months", 
                'minViewMode' => "months",
                //'minDate' => 'tenancy_period_from',
                'endDate' => '+0d', 
                //'startDate' => date('#newtenantform-tenancy_period_from'),
            ]
        ])->label(false);
        ?>
            
            

        <button class="btn btn-info btn-flat btn-lg" type="submit">Go!</button>
        <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<?php
$script = <<< JS

$(document).ready(function () {
    $("#findpainter-type").change(function(){        
        var val = $('#findpainter-type').val();
        if(val == 'mid' ) {
            $('.field-findpainter-membership_id').show();
            $('.field-findpainter-ic_no').hide();
        } else {
            $('.field-findpainter-ic_no').show();
            $('.field-findpainter-membership_id').hide();
        }
    });
});

JS;
$this->registerJs($script);
?>