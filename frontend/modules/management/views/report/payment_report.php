<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\painter\models\PainterProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Redemption Supporting Document';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php

$amountpaid = 0;
$totbox6 = 0;
$qtypoint = 0;

    ?>
<div class="col-xs-12">
<!--    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-4 col-sm-4 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i></h3></div>
            <div class="col-xs-6"></div>
            <div class="col-lg-2 col-sm-2 col-xs-12 no-padding">
                <div class="col-xs-12 no-padding"></div>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="painter-profile-index">-->
                
                <?php
                // Header and Footer options for PDF format
        
        

        $exportFilename = date("Y-m-d_H-m-s").'_'.$this->title;

        
                $gridColumns = [
                        //['class' => 'yii\grid\SerialColumn'],
                        //['class' => 'kartik\grid\SerialColumn'],
                        //'item_id',
                        //'point_order_id',
                        [
                        'attribute' => 'card_id',
                        'label' => 'Membership #',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '95'],
                        'value' => function ($model) {
                            return $model->profile->card_id;
                        },
                    ],
                    [
                        'attribute' => 'full_name',
                        'label' => 'Full Name',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '250'],
                        'value' => function ($model) {
                            return $model->profile->full_name;
                        },
                    ],
                    [
                        'attribute' => 'ic_no',
                        'label' => 'NRIC/Passport #',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '150'],
                        'value' => function ($model) {
                            return $model->profile->ic_no;
                        },
                    ],
                    [
                        'attribute' => 'account_number',
                        'label' => 'Bank Acc #',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->bank->account_number;
                        },
                    ],
                    [
                        'attribute' => 'account_name',
                        'label' => 'Bank Acc Holder Name',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->bank->account_name;
                        },
                    ],
                    [
                        'attribute' => 'bank_name',
                        'label' => 'Bank Name',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '380'],
                        'value' => function ($model) {
                            $banks = common\models\Banks::find()->where(['id' => $model->bank->bank_name])->one();
                            $bankname = $banks->bank_name;
                            return $bankname;
                        },
                    ],
                    [
                        'attribute' => 'redemption_created_datetime',
                        'label' => 'Request Date',
                        'format'=>['date', 'php:d-M-y'],
                        'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->redemption_created_datetime;
                        },   
                    ],
                    [
                        'attribute' => 'redemption_invoice_no',
                        'label' => 'Redemption #',
                        'format'=> 'text',
                        'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->redemption_invoice_no;
                        },   
                    ],          
                    [
                        'attribute' => 'req_points',
                        'label' => 'Points Redeemed',
                        'format'=>'integer',
                        'value' => function ($model) {
                            return $model->req_points;
                        },
                        'contentOptions' =>['class' => 'text-right',],         
                        //'options' => ['width' => '80'],
                        'hAlign'=>'right',        
                        //'pageSummary' => $qtypoint,
                        'pageSummary' => TRUE,        
                    ],
                        [
                            'attribute' => 'req_amount',
                            'label' => 'RM Redeemed',
                            'format' => ['decimal', 2],
                            'value' => function ($model) {
                                return $model->req_amount;
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            //'options' => ['width' => '80'],
                            'hAlign'=>'right',        
                            //'pageSummary' => 'RM '.Yii::$app->formatter->asDecimal($totbox6),
                            'pageSummary' => TRUE,        
                        ],             
                        [
                            'attribute' => 'redemption_status_ray_date',
                            'label' => 'Paid Date',
                            //'format'=>['date', 'php:d-M-y'],
                            'headerOptions' => ['width' => '190'],
                            'value' => function ($model) {
                                if(empty($model->redemption_status_ray_date)){
                                    $raydate = "N/A";
                                }else{
                                    $raydate = date('d-M-y', strtotime($model->redemption_status_ray_date));
                                }
                                return $raydate;
                            },   
                        ],
                        [
                            'attribute' => 'req_amountPaid',
                            'label' => 'RM Paid',
                            'format' => ['decimal', 2],
                            'value' => function ($model) {
                                if($model->redemption_status_ray == '2'){
                                    $raypaid = '0.00';
                                }else{
                                    $raypaid = $model->req_amount;
                                }
                                return $raypaid;
                            },
                            'contentOptions' =>['class' => 'text-right',],         
                            //'options' => ['width' => '100'],
                            'hAlign'=>'right',        
                            //'pageSummary' => 'RM '.Yii::$app->formatter->asDecimal($amountpaid),
                            'pageSummary' => TRUE,
                        ],            
                                    
                    ];
                echo GridView::widget([
                    'dataProvider'=> $dataProvider,
                    //'filterModel' => $searchModel,
                    'autoXlFormat'=>true,
                    'columns' => $gridColumns,
                    'pjax' => true,
                    'bordered' => true,
                    'striped' => false,
                    'condensed' => false,
                    'responsive' => true,
                    'hover' => true,
                    //'exportConfig'     => $exportConfig,
                    'export' => false,
                    'toolbar' =>  [
                        ['content'=>
                            //Html::button('<i class="fa fa-file-excel-o"></i> Export to Excel', ['my-action', 'type'=>'button', 'title'=>'hhh', 'class'=>'btn btn-success', 'onclick'=>'alert("This will launch the book creation form.\n\nDisabled for this demo!");']) . '&nbsp;&nbsp;&nbsp;'.
                            Html::a(Yii::t('app', '<i class="fa fa-file-excel-o"></i> Export to Excel'), ['redemption-supporting-document-excel'], ['class' => 'btn btn-primary',
                                /*'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to download?'),
                                    'method' => 'post',
                                ],*/
                            ]).
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['/management/report/redemptionsd'], ['data-pjax'=>0, 'class' => 'btn btn-default', 'title'=>'redemptionsd'])
                        ],
                        //'{export}',
                        //'{toggleData}'
                    ],
                    'floatHeader' => false,
                    //'floatHeaderOptions' => ['scrollingTop' => $scrollingTop],
                    'showPageSummary' => true,
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => '<i class="fa fa-th-list"></i> '.Html::encode($this->title).''
                    ],
                    
                ]);
                ?>
                
<!--            </div>
        </div>
    </div>-->
</div>