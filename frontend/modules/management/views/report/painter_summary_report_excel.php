<?php

use yii\helpers\Html;
use yii\grid\GridView;
//use kartik\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\painter\models\PainterProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Painter Account Management Summary Report Excel';
$this->params['breadcrumbs'][] = $this->title;
?>

                <?php
                /*$filename = Date('YmdGis').'_Painter_Account_Management.xls';
                header("Content-type: application/vnd-ms-excel");
                header("Content-Disposition: attachment; filename=".$filename);
                header("Pragma: no-cache"); 
                header("Expires: 0");*/
                $gridColumns = [
                    [
                        'attribute' => 'card_id',
                        'label' => 'Membership #',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '95'],
                        'value' => function ($model) {
                            return $model->card_id;
                        },
                    ],
                    [
                        'attribute' => 'full_name',
                        'label' => 'Full Name',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '250'],
                        'value' => function ($model) {
                            return $model->full_name;
                        },
                    ],
                    [
                        'attribute' => 'ic_no',
                        'label' => 'NRIC/Passport #',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '150'],
                        'value' => function ($model) {
                            return $model->ic_no;
                        },
                    ],
                    [
                        'attribute' => 'mobile',
                        'label' => 'Mobile No',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '150'],
                        'value' => function ($model) {
                            return $model->mobile;
                        },
                    ],                     
                    [
                        'attribute' => 'email',
                        'label' => 'Email ID',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->user->email;
                        },
                    ],
                    [
                        'attribute' => 'company_name',
                        'label' => 'Company Name',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->company->company_name;
                        },
                    ],
                    [
                        'attribute' => 'no_painters',
                        'label' => 'No. of Employees',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->company->no_painters;
                        },
                    ],            
                    [
                        'attribute' => 'painter_sites',
                        'label' => 'No. of Painting Jobs',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->company->painter_sites;
                        },
                    ],
                    [
                        'attribute' => 'customer_name',
                        'label' => 'Registered At Dealer Outlet',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->company->dealerOutlet->customer_name;
                        },
                    ],
                    [
                        'attribute' => 'address',
                        'label' => 'Dealer Address',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->company->dealerOutlet->address;
                        },
                    ],            
                    [
                        'attribute' => 'bank_name',
                        'label' => 'Bank Name',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '380'],
                        'value' => function ($model) {
                            $banks = common\models\Banks::find()->where(['id' => $model->bank->bank_name])->one();
                            $bankname = $banks->bank_name;
                            return $bankname;
                        },
                    ],            
                    
                    [
                        'attribute' => 'account_name',
                        'label' => 'Name of Account Holder',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '50'],
                        'value' => function ($model) {
                            return $model->bank->account_name;
                        },
                    ],
                    [
                        'attribute' => 'account_number',
                        'label' => 'Account Number',
                        'format' => 'html',
                        //'headerOptions' => ['width' => '180'],
                        'value' => function ($model) {
                            return $model->bank->account_number;
                        },
                    ],                            
                ];
                echo GridView::widget([
                    'dataProvider'=> $dataProvider,
                    //'autoXlFormat'=>true,
                    'columns' => $gridColumns,                    
                ]);
                die();
                ?>