<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Campaign */

$this->title = $model->campaign_description;
$this->params['breadcrumbs'][] = ['label' => 'Campaigns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>

 <section class="invoice">
     <div class="row">
         <div class="col-xs-12">
             <h2 class="page-header">
                 <i class="fa fa-globe"></i> <b>Campaign - <?= $model->campaign_description?></b>
                 <small class="pull-right" style="margin-right: 15px;"> <?= $model->getCampaignStatus() ?></small>
             </h2>
         </div><!-- /.col -->
     </div>
     <div class="row invoice-info">
         
         <div class="col-sm-6 invoice-col">
             <h3>Start <?= date('d-m-Y', strtotime($model->campaign_start_date)) ?> End <?= date('d-m-Y', strtotime($model->campaign_end_date))  ?></h3>

         </div>
         <div class="col-sm-3 invoice-col"><h3>Per Point (RM) : <?= $model->campaign_rm_per_point  ?></h3></div>
         <div class="col-sm-3 invoice-col text-right">
                
              <?= Html::a('Update', ['update', 'id' => $model->campaign_id], ['class' => 'btn btn-success']) ?>
              <?= Html::a('Excel Import Bulk', ['excelimport', 'campaignid' => $model->campaign_id], ['class' => 'btn btn-info']) ?>
            </div>
         
     </div>
    <div class="campaign-view">
        <div class="row">
            <?= $this->render('_form_campaign', [
                'campaign_assign' => $campaign_assign,
            ]) ?>
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped" id="retc">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Product Name</th>
                            <th>Product Description</th>
                            <th class="text-center">Pack Size (L)</th>
                            <th class="text-center">Current Points per (L)</th>
                            <th class="text-center">New Points per (L)</th>
                            <th class="text-center">Total Points Awarded</th>
                            <th class="text-center">Total Value (RM)</th>
                            <th class="text-center"></th>
                        </tr>
                    </thead>
                    <tbody class="product_item_list" id="product_item_list">
                        <?php
                        //echo '<pre>';
                        //print_r($assignproducts);
                        foreach($assignproducts as $assignproduct){
                            $product_list_id = $assignproduct['product_list_id'];
                            //$productlist = ProductList::find()->where(['product_list_id' => $product_list_id])->one();
                            $productlist = common\models\ProductList::find()->where(['product_list_id' => $product_list_id])->one();
                            //<tr id="+ data.barcodeid + "><td class='hide'>" + data.barcodeid+ "</td><td class='hide'><input class='form-control' type='text' name='barcodeid[]' value='"+ data.barcodeid +"'/><input class='form-control' type='text' name='totqty[]' value='"+ data.qty+ "'/></td><td></td><td>" + data.product_name + "</td><td class='text-center'>" + data.product_description+ "</td><td class='text-center'>" + data.pack_size+ "</td><td class='text-center price'>" + data.points_per_liter+ "</td><td class='text-center'>" + data.new_points_per_liter+ "</td><td class='text-center'>" + data.total_points_awarded+ "</td><td class='text-center'>" + data.total_value_rm+ "</td><td class='text-center'>" + data.status+ "</td><td class='text-center'><a href='javascript::;' class='btnDelete'><i class='fa fa-trash-o' style='opacity:0.5; filter:alpha(opacity=50);'></i></a></td></tr>
                            $qty = $assignproduct['new_points_per_l'];
                            $totvalue = $productlist->pack_size * $qty;
                            $product_name = $productlist->product_name;
                            $product_description = $productlist->product_description;
                            $pack_size = $productlist->pack_size;
                            $points_per_liter = $productlist->points_per_liter;
                            $new_points_per_liter = $qty;
                            $total_points_awarded = $productlist->pack_size * $qty;
                            $total_value_rm = $totvalue * $model->campaign_rm_per_point;  
                            echo '<tr id="'.$assignproduct['cap_id'].'">';
                            echo '<td class="hide">'.$assignproduct['cap_id'].'</td>';
                            echo '<td></td>';
                            echo '<td>'.$product_name.'</td>';
                            echo '<td>'.$product_description.'</td>';
                            echo '<td class="text-center">'.$pack_size.'</td>';
                            echo '<td class="text-center price">'.$points_per_liter.'</td>';
                            echo '<td class="text-center">'.$new_points_per_liter.'</td>';
                            echo '<td class="text-center">'.$total_points_awarded.'</td>';
                            echo '<td class="text-center">'.$total_value_rm.'</td>';
                            echo "<td class='text-center'><a data-confirm='Are you sure to delete this item?' href='javascript::;' class='btnDelete' id=".$assignproduct['cap_id']."><i class='fa fa-trash-o' style='opacity:0.5; filter:alpha(opacity=50);'></i></a></td>";
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>


    </div>
</section>
