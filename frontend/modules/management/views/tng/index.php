<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PayOutSummarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = 'Bar Code Pay Out Summaries';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-lg-12">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-12 col-sm-12 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?></h3></div>

        </div>
        <div class="box-body table-responsive">
            <div class="bcpay-out-summary-index">
                <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'ref_id',
            'customer_sap_id',
            'company_name',
            'staff_name',
            'staff_mobile',
            'total_rewards',
            'rm50',
            'rm100',
            //'tpin:ntext',
            'status',
            [
                            'class' => 'yii\grid\ActionColumn',
                            'headerOptions' => ['width' => '80', 'class' => 'text-center'],
                            'template' => '{view}', //{update} {delete}
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    if($model->status == 'No') {
                                        return (Html::a('<span class="glyphicon glyphicon-eye-open text-warning"></span>', $url, ['title' => Yii::t('app', 'Send'), 'target'=>'_blank']));
                                    }
                                },      
                            ],
                        ],            
        ],
    ]); ?>

            </div>
        </div>
    </div>
</div>

