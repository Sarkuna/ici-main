<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TngSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tng-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ref_id') ?>

    <?= $form->field($model, 'customer_sap_id') ?>

    <?= $form->field($model, 'company_name') ?>

    <?= $form->field($model, 'staff_name') ?>

    <?php // echo $form->field($model, 'staff_mobile') ?>

    <?php // echo $form->field($model, 'total_rewards') ?>

    <?php // echo $form->field($model, 'rm50') ?>

    <?php // echo $form->field($model, 'rm100') ?>

    <?php // echo $form->field($model, 'tpin') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
