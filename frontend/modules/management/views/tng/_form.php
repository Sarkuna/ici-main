<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Tng */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tng-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ref_id')->textInput() ?>

    <?= $form->field($model, 'customer_sap_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'staff_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'staff_mobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_rewards')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rm50')->textInput() ?>

    <?= $form->field($model, 'rm100')->textInput() ?>

    <?= $form->field($model, 'tpin')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'No' => 'No', 'Yes' => 'Yes', 'Resend' => 'Resend', '' => '', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
