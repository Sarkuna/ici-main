<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Tng */

$this->title = 'Create Tng';
$this->params['breadcrumbs'][] = ['label' => 'Tngs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tng-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
