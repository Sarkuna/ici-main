<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DealerListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bulk Transactions';
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
th{vertical-align: middle !important;}
</style>
<div class="col-lg-12">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-8 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?></h3></div>
            <div class="col-lg-4 col-sm-2 col-xs-12 text-right no-padding">
                <p>
                    <?= Html::a('Import Excel', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="dealer-list-index">

                <table id="example1" class="table table-bordered">
                    <thead>
                        <tr>
                            <th rowspan="2">#</th>
                            <th rowspan="2">Date</th>
                            <th rowspan="2">Description</th>
                            <th rowspan="2" class="text-center">Total Transactions Uploaded</th>
                            <th colspan="3" class="text-center">Success</th>
                            <th rowspan="2" class="text-center">Failed</th>
                            <th rowspan="2" class="text-center">By</th>
                        </tr>
                        <tr>
                            <th class="text-center">Pending</th>
                            <th class="text-center">Approve</th>
                            <th class="text-center">Cancel</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $n = 1;
                        foreach ($models as $model) {
                            echo '<tr>
                            <td>'.$n.'</td>
                            <td>'.date('d-M-y', strtotime($model->upload_date)).'</td>
                            <td>'.$model->desceription.'</td>
                            <td class="text-right">'.$model->getTotalTransection().'</td>
                            <td class="text-right">'.$model->getTotalPending().'</td>
                            <td class="text-right">'.$model->getTotalApprove().'</td>
                            <td class="text-right">'.$model->getTotalCancel().'</td>
                            <td class="text-right">'.$model->getTotalFail().'</td>
                            <td>'.$model->getBy().'</td>
                        </tr>';
                            
                            
                        $n++;
                            // display $model here
                        }
                        ?>
                        
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
</div>

<?php
    $script = <<<EOD
                
$(function () {
        $("#example1").DataTable();

      });

EOD;
$this->registerJs($script);
    ?> 