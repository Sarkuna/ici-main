<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CampaignAssignProducts */

$this->title = 'Create Campaign Assign Products';
$this->params['breadcrumbs'][] = ['label' => 'Campaign Assign Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campaign-assign-products-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
