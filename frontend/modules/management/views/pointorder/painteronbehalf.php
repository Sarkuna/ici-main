<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PointOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Redemption on-behalf of '.$modelprofile->full_name;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-12">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-6 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-handshake-o"></i> <?= Html::encode($this->title) ?></h3></div>
            <div class="col-lg-6 col-sm-2 col-xs-12 text-right no-padding">
                <h4><label>Total Point Selected: <span id="output" class="label label-info pull-right">0</span></label>
                    &nbsp;&nbsp;&nbsp;&nbsp;<label>Total Value RM Selected: <span id="outputrm" class="label label-info pull-right">0.00</span></label></h4>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="point-order-index">
                <?php
                
                if ($dataProvider->totalCount > 0) {
                   $disable = false;
                }else{
                   $disable = true; 
                }
                ?>
            <?=Html::beginForm(['pointorder/bulkonbhalf'],'post');?>
                <div class="col-lg-12">
                <div class="col-lg-2 no-padding">
                <input type="hidden" name="painterid" value="<?= $modelprofile->user_id ?>">   
                <?=
                Html::submitButton('Redeem', ['class' => 'btn btn-info', 'disabled' => $disable, 'data' => [
                        'confirm' => 'Are you sure you want to redeem selected items?',
                        'method' => 'post',                        
                    ],]);
                ?>
                <?= Html::a('Cancel', ['onbhalfgroup'], ['class' => 'btn btn-default']) ?>     
                </div>
                <div class="col-lg-10 text-right">
                  <h4>Membership#: <?= $modelprofile->card_id ?></h4>
                  <h4>Painter Name: <?= $modelprofile->full_name ?></h4>
                </div>
            </div>
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn','headerOptions' => ['width' => '10'],],
                        ['class' => 'yii\grid\CheckboxColumn','headerOptions' => ['width' => '10'],],
                        //'order_id',
                        //'order_num',
                        [
                            'attribute' => 'order_num',
                            'label' => 'Transaction #',
                            'format' => 'html',
                            'headerOptions' => ['width' => '110', 'class' => 'text-center'],
                            'value' => function ($model) {
                                return $model->order_num;
                            },
                        ],
                        //'order_dealer_id',
                        [
                            'attribute' => 'customer_name',
                            'label' => 'Dealer',
                            'format' => 'html',
                            'headerOptions' => ['width' => '230'],
                            'value' => function ($model) {
                                return $model->dealerOutlet->customer_name;
                            },
                        ],
                        /*[
                            'attribute' => 'card_id',
                            'label' => 'Membership#',
                            'format' => 'html',
                            'headerOptions' => ['width' => '150'],
                            'value' => function ($model) {
                                return $model->profile->card_id;
                            },
                        ],            
                        [
                            'attribute' => 'full_name',
                            'label' => 'Painter Name',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->profile->full_name;
                            },
                        ],*/            
                                    
                        //'order_qty',
                        // 'order_total_point',
                        // 'order_total_amount',
                        // 'order_status',
                        //'total_items',
                        [
                            'attribute' => 'total_items',
                            //'label' => 'Total Transactions',
                            'format' => 'html',
                            'headerOptions' => ['width' => '80', 'class' => 'text-center'],
                            'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->getTotalItems();
                            },
                        ],            
                        [
                            'attribute' => 'order_total_point',
                            'label' => 'Total Point',
                            'format' => 'html',
                            'headerOptions' => ['width' => '70', 'class' => 'text-center'],
                            'contentOptions' =>['class' => 'text-center'],
                            'value' => function ($model) {
                                return $model->order_total_point;
                            },
                        ],
                        [
                            'attribute' => 'order_total_amount',
                            'label' => 'Total Amount RM',
                            'format' => 'html',
                            'headerOptions' => ['width' => '110', 'class' => 'text-center'],
                            'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->order_total_amount;
                            },
                        ],            
                                    
                        /*[
                            'attribute' => 'order_status',
                            'label' => 'Status',
                            'format' => 'html',
                            'headerOptions' => ['width' => '95'],
                            'value' => function ($model) {
                                //$ord =
                                return $model->orderStatus->name;
                            },
                            'filterInputOptions' => ['class' => 'form-control', 'id' => null, 'prompt' => 'All'],         
                            'filter' => ['1' => 'Pending', '7' => 'Cancel', '17' => 'Approve'],        
                        ],*/            
                        // 'order_remarks:ntext',
                        // 'order_IP',
                        //'created_datetime',
                        /*[
                            'label' => 'Created Date',
                            'attribute' => 'created_datetime',
                            'headerOptions' => ['width' => '100'],
                            'format' => ['date', 'php:d-m-Y']
                        ],*/
                                    
                        /*[
                            'attribute' => 'created_datetime',
                            'label' => 'Created Date',
                            'value' => 'created_datetime',
                            //'format' => ['php:D, d-M-Y H:i:s A'],
                            //'format' => 'datetime',
                            'format' =>  ['date', 'php:d-m-Y h.i A'],

                            'filter' => DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'created_datetime',
                                'convertFormat' => true,
                                'readonly' => true,
                                //'type' => DatePicker::TYPE_BUTTON,
                                //'displayFormat' => 'php:D, d-M-Y H:i:s A',
                                'pluginOptions' => [
                                    'format' => 'yyyy-M-dd'
                                    //'format' => 'dd-M-yyyy'
                                ],
                            ]),
                            'options' => ['width' => '150']
                        ], */           

                        /*[
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}', //{update} {delete}
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                                },
                            ],
                        ],*/            
                    ],
                ]);
                ?>
                <?= Html::endForm();?> 
            </div>
        </div>
    </div>
</div>
<?php
    $clientScript = '
        $("#w0 table tbody tr").click(function()
        {
            recalculate();
        });
        $(".select-on-check-all").change(function ()
        {
            recalculate();
        });
        $("input[type=checkbox]").change(function () {
            //recalculate();
        });
    ';
    $this->registerJs($clientScript, \yii\web\View::POS_END, 'booking-period');
    ?>


<script language="javascript">  
    function recalculate() {
        
        var sum = 0;
        var sumrm = 0.00;
        var sumtot = 0.00;
        var a = "";
        $("input[name='selection[]']:checked").each(function () {
            $this=$(this);
            var amount = $(this).closest("tr").find("td:nth-child(6)").text();
            var amountrm = $(this).closest("tr").find("td:nth-child(7)").text();
            //alert(amount);
            
            if (this.checked) sum = sum + parseFloat(amount);
            if (this.checked) sumrm = sumrm + parseFloat(amountrm);
            if (this.checked) sumtot = sumtot + parseFloat($(this).attr('rel'));
            if (this.checked) {
                if (a.length == 0) {
                    a = $(this).closest("tr").find("td:first").text();
                } else {
                    a = a + "," + $(this).closest("tr").find("td:first").text();
                }
            } else {
                if (a.length == 1) {
                    a = a.slice(0, 1);
                } else {
                    a = a.replace(("," + $(this).closest("tr").find("td:first").text()), "");
                }
            }
        });

        $("#redemption-orderid").val(a); 
        $("#output").text(sum);
        $("#outputrm").text(sumrm.toFixed(2));
        $("#redemption-req_points").val(sum);
        $("#redemption-req_amount").val(sumtot.toFixed(2));
        $("#multiamt").val(sum);
        
    }
    function checkempty() {
        var minamt = $("#csvsendpayment-csv_amount").val(); 
        if(minamt == '0.00' || minamt == '0' || minamt == ''){
            alert('Minimum Ammount Required above 0.00');
            return false;
        }else{
            return true;
        }
        
    }
//});
    </script> 