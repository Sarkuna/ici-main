<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PointOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Painter Transaction Management - Pending';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .summary {
    display: none;
}
</style>
<div class="col-xs-12">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-8 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?></h3></div>
            <div class="col-lg-4 col-sm-2 col-xs-12 text-right no-padding">
                <div class="col-xs-12 no-padding">
                    <?php if (Yii::$app->user->can('/management/pointorder/create')) { ?>
                        <?= Html::a('Create Point Order', ['create'], ['class' => 'btn btn-success']) ?>
                    <?php } ?>
                    <?= Html::a('Record New Transaction', ['findpainter'], ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3><?= $count ?></h3>

                            <p>No of Pending</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="point-order-index">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'pager' => [
                        'class' => \kop\y2sp\ScrollPager::className(),
                        'container' => '.grid-view tbody',
                        'item' => 'tr',
                        'paginationSelector' => '.grid-view .pagination',
                        'triggerTemplate' => '<tr class="ias-trigger"><td colspan="10" style="text-align: center"><a style="cursor: pointer">{text}</a></td></tr>',
                        'noneLeftTemplate' => '<tr class="ias-noneleft"><td colspan="10" style="text-align: center">{text}</td></tr>',                

                     ],
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        //'order_id',
                        //'order_num',
                        [
                            'attribute' => 'order_num',
                            'label' => 'Transaction #',
                            'format' => 'html',
                            'headerOptions' => ['width' => '110', 'class' => 'text-center'],
                            'value' => function ($model) {
                                return $model->order_num;
                            },
                        ],
                        //'order_dealer_id',
                        [
                            'attribute' => 'customer_name',
                            'label' => 'Dealer',
                            'format' => 'html',
                            'headerOptions' => ['width' => '200'],
                            'value' => function ($model) {
                                return $model->dealerOutlet->customer_name;
                            },
                        ],
                        [
                            'attribute' => 'card_id',
                            'label' => 'Membership#',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->profile->card_id;
                            },
                        ],            
                        [
                            'attribute' => 'full_name',
                            'label' => 'Painter Name',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->profile->full_name;
                            },
                        ],            
                                    
                        //'order_qty',
                        // 'order_total_point',
                        // 'order_total_amount',
                        // 'order_status',
                        //'total_items',
                        [
                            'attribute' => 'total_items',
                            //'label' => 'Total Transactions',
                            'format' => 'html',
                            'headerOptions' => ['width' => '60', 'class' => 'text-center'],
                            'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->getTotalItems();
                            },
                        ],            
                        [
                            'attribute' => 'order_total_point',
                            'label' => 'Total Point',
                            'format' => 'html',
                            'headerOptions' => ['width' => '70', 'class' => 'text-center'],
                            'contentOptions' =>['class' => 'text-center'],
                            'value' => function ($model) {
                                return $model->order_total_point;
                            },
                        ],
                        [
                            'attribute' => 'order_total_amount',
                            'label' => 'Total Amount RM',
                            'format' => 'html',
                            'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                            'contentOptions' =>['class' => 'text-right'],
                            'value' => function ($model) {
                                return $model->order_total_amount;
                            },
                        ],                      
                        // 'order_remarks:ntext',
                        // 'order_IP',
                        //'created_datetime',
                        /*[
                            'label' => 'Created Date',
                            'attribute' => 'created_datetime',
                            'headerOptions' => ['width' => '100'],
                            'format' => ['date', 'php:d-m-Y']
                        ],*/
                                    
                        [
                            'attribute' => 'created_datetime',
                            'label' => 'Created Date',
                            'value' => 'created_datetime',
                            //'format' => ['php:D, d-M-Y H:i:s A'],
                            //'format' => 'datetime',
                            'format' =>  ['date', 'php:d-m-Y h.i A'],

                            'filter' => DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'created_datetime',
                                'convertFormat' => true,
                                'readonly' => true,
                                //'type' => DatePicker::TYPE_BUTTON,
                                //'displayFormat' => 'php:D, d-M-Y H:i:s A',
                                'pluginOptions' => [
                                    'format' => 'yyyy-M-dd'
                                    //'format' => 'dd-M-yyyy'
                                ],
                            ]),
                            'options' => ['width' => '150']
                        ],            
                        // 'created_by',
                        // 'updated_datetime',
                        // 'updated_by',
                        //['class' => 'yii\grid\ActionColumn'],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}{editpoint}', //{update} {delete}
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                                },
                                'editpoint' => function ($url, $model) {
                                    //if($model->order_status == '1'){
                                        return (Html::a('<span class="fa fa-pencil-square-o"></span>', $url, ['title' => Yii::t('app', 'Edit'),]));
                                    //}
                                },        
                            ],
                        ],            
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

