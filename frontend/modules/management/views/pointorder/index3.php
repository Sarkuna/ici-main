<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PointOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Painter Transaction Management - Pending';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .summary {
    display: none;
}
</style>
<div class="col-xs-12">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-8 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?></h3></div>
            <div class="col-lg-4 col-sm-2 col-xs-12 text-right no-padding">
                <div class="col-xs-12 no-padding">
                    <?php if (Yii::$app->user->can('/management/pointorder/create')) { ?>
                        <?= Html::a('Create Point Order', ['create'], ['class' => 'btn btn-success']) ?>
                    <?php } ?>
                    <?= Html::a('Record New Transaction', ['findpainter'], ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="row">
                <div class="col-lg-3 col-xs-12">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3><?= $count ?></h3>

                            <p>No of Pending</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3><?= $no_of_item ?></h3>

                            <p>No of Item</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-12">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3><?= Yii::$app->formatter->asCurrency($total_rm) ?></h3>

                            <p>Total RM</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="point-order-index">
                <table id="order1" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Transaction #</th>
                            <th>Dealer Name</th>
                            <th class="text-center">Membership#</th>
                            <th class="text-center">Painter Name</th>
                            <th class="text-center">Invoice #</th>
                            <th class="text-center">Invoice Date</th>
                            <th>Total Qty</th>
                            <th>Total Point</th>
                            <th>Total Amount RM	</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $n = 1;
                        foreach ($models as $model) {

                            echo '<tr>
                            <td>'.$n.'</td>
                            <td>'.$model->order_num.'</td>
                            <td>'.$model->dealerOutlet->customer_name.'</td>
                            <td>'.$model->profile->card_id.'</td>
                            <td>'.$model->profile->full_name.'</td>
                            <td>'.$model->dealer_invoice_no.'</td>
                            <td>'.date('d-M-y', strtotime($model->dealer_invoice_date)).'</td>
                            <td class="text-right">'.$model->getTotalItems().'</td>
                            <td>'.$model->order_total_point.'</td>
                            <td>'.$model->order_total_amount.'</td>
                            <td>
                            <a href="/management/pointorder/view?id='.$model->order_id.'" title="View"><span class="glyphicon glyphicon-eye-open"></span></a>
                            <a href="/management/pointorder/editpoint?id='.$model->order_id.'" title="Edit"><span class="fa fa-pencil-square-o"></span></a>
                            </td>
                               
                        </tr>';
                            
                            
                        $n++;
                            // display $model here
                        }
                        ?>
                        
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
</div>

<?php
    $script = <<<EOD
                
$(function () {
        $("#order1").DataTable();

      });

EOD;
$this->registerJs($script);
    ?> 