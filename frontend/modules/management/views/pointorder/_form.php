<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="point-order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'order_dealer_id')->textInput() ?>

    <?= $form->field($model, 'painter_login_id')->textInput() ?>

    <?= $form->field($model, 'painter_ic')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order_qty')->textInput() ?>

    <?= $form->field($model, 'order_total_point')->textInput() ?>

    <?= $form->field($model, 'order_total_amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order_remarks')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'order_IP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_datetime')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_datetime')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
