<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Redemption */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="invoice no-margin">    
    <!-- title row -->
    <div class="row">
        <div class="redemption-form">
            <?php $form = ActiveForm::begin(); ?>
            <?php
            /* @var $searchModel app\models\UserSearch */
            echo $form->field($searchModel, 'globalSearch', [
                'template' => '<div class="input-group">{input}<span class="input-group-btn">' .
                Html::submitButton('GO', ['class' => 'btn btn-success']) .
                Html::a('<i class="fa fa-refresh"></i>', ['search'], ['class' => 'btn btn-default', 'title' => 'Refresh']).
                '</span></div>',
            ])->textInput(['placeholder' => 'Search Transaction#, Membership#, Full Name']);
            ?>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    
</section>


