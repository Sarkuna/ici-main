<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\date\DatePicker;
use kartik\select2\Select2;

use common\models\DealerList;
use common\models\ProductList;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .point-order-form .dropdown-menu.open {
        z-index: 5000 !important;
    }
    
    .table > thead:first-child > tr:first-child > th, .table > thead:first-child > tr:first-child > td, .table-striped thead tr.primary:nth-child(odd) th {
    background-color: #428BCA;
    color: white;
    border-color: #357EBD;
    border-top: 1px solid #357EBD;
    text-align: center;
}
input#totalqty {
    text-align: right;
    width: 50px;
}

input#grandtotal {
    text-align: right;
    width: 100px;
}
</style>
<div class="col-md-12">
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Record Transaction</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <?php $form = ActiveForm::begin(); ?>
           <div class="box-body">
               <div class="col-md-3">
                   <?= $form->field($profile, 'ic_no', [
                        'template' => '{label} <div class=""><div class="col-lg-12 input-group input-group">{input}
                        <span class="input-group-btn"></span></div>{error}{hint}</div>',
                    ])->textInput(['autocomplete' => 'off', 'disabled' => true]) ?>
                   <?php
                    $listData= ArrayHelper::map(DealerList::find()->where(['status' => 'A'])->orderBy('customer_name ASC')->all(), 'id', 'customer_name');
                        //$d_region = ArrayHelper::map(DealerList::find()->orderBy('region ASC')->groupBy(['region'])->all(), 'id', 'region');
                        $d_area = ArrayHelper::map(DealerList::find()->where(['status' => 'A'])->orderBy('area ASC')->groupBy(['area'])->all(), 'area', 'area');
                    ?>
                   <?=
                    $form->field($pointorder, 'd_area')->dropDownList($d_area, [
                        'prompt' => '-- Select --',
                        'onchange' => '$.get( "' . Url::toRoute('/management/pointorder/dstate') . '", {id: $(this).val() } )
                                 .done(function( data ) {
                                    $("#vipreceipt-product_description").html(data);
                                 }
                             );'
                            ]
                    )
                    ?>
                    <?php

                                echo $form->field($pointorder, 'd_area')->widget(Select2::classname(), [
                                    'data' => $d_area,
                                    //'language' => 'de',
                                    'options' => [
                                        'placeholder' => '-- Select --',
                                        'onchange' => '$.get( "' . Url::toRoute('/management/pointorder/dstate') . '", {id: $(this).val() } )
                                            .done(function( data ) {
                                               $("#vipreceipt-product_description").html(data);
                                            }
                                        );'
                                     ],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);
                            ?>

                        <?php

                                echo $form->field($pointorder, 'order_dealer_id')->widget(Select2::classname(), [
                                    'data' => $listData,
                                    //'language' => 'de',
                                    'options' => [
                                        'placeholder' => '-- Select --',
                                        'onchange' => '$.get( "' . Url::toRoute('/painter/painterprofile/dealeraddress') . '", {id: $(this).val() } )
                                            .done(function( data ) {
                                               $("#dealeraddress1").html(data);
                                            }
                                        );'
                                     ],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);
                            ?>
                   <div><p id="dealeraddress1"></p></div>
                    <?= $form->field($pointorder, 'dealer_invoice_no')->textInput() ?>
                   <?php
                        echo $form->field($pointorder, 'dealer_invoice_date')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'Dealer Invoice Date ...', 'readonly' => 'readonly'],
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'dd-M-yyyy',
                                //'endDate' => '-18y',
                                'maxDate' => date('d-m-Y'),
                            ],
                            'pluginEvents' => [
                                'changeDate' => 'function(ev) {
                                    var sdat = $("#pointorder-dealer_invoice_date").val();
                                    $("#pointorder-campaign_status").val(0);
                                    $("#pointorder-listing_campaign").html("");

                                    $.ajax({
                                        type: "GET",
                                        url: "/management/pointorder/campaignscheck",
                                        data: {sel_date: sdat},
                                        success: function(data){
                                            if(data > "0"){
                                                $("#nocampaign").show();
                                            } else {
                                                $("#nocampaign").hide();
                                            }
                                        }
                                    });
                                }'
                            ]
                        ]);
                        ?>
                   <div id="nocampaign">
                       <?php $pointorder->campaign_status = 'N'; ?>
                   <?php echo $form->field($pointorder, 'campaign_status')->dropDownList(['N' => 'No', 'Y' => 'Yes'],['onchange' => '$.get( "' . Url::toRoute('/management/pointorder/mycampaigns') . '", {id: $(this).val(),invdate: $("#pointorder-dealer_invoice_date").val() } )
                                     .done(function( data ) {
                                        $("#pointorder-listing_campaign").html(data);
                                     }
                                 );'
                                ]); ?>
                   
                   <?php echo $form->field($pointorder, 'listing_campaign')->dropDownList([]); ?>
                   </div>
                   <?= $form->field($pointorder, 'dealer_invoice_price')->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
                                    //'mask' => '9999999999999',
                                    //'mask' => '9',
                                    'clientOptions' => ['alias' =>  'decimal','groupSeparator' => ',',],
                                    //'options' => ['placeholder' => '60XXXXXXXXX', 'class' => 'form-control',],
                                ]) ?> 
                   <?= $form->field($pointorder, 'pay_out_percentage')->textInput(['readonly' => true]) ?>
               </div>
               
               <div class="col-md-9">
                   <div class="row">
                       <?php
                       $brand_name = ArrayHelper::map(\common\models\ProductTypeCategory::find()->orderBy('product_type_id ASC')->all(), 'product_type_id', 'product_type_name');
                       $d_product_name = ArrayHelper::map(ProductList::find()->orderBy('product_name ASC')->groupBy(['product_name'])->all(), 'product_name', 'product_name');
                       $d_product_description = ArrayHelper::map(ProductList::find()->orderBy('product_description ASC')->all(), 'product_list_id', 'product_description');
                       ?>
                       <div class="col-md-12 no-padding">
                           <div class="col-md-2">
                               <?php

                                echo $form->field($pointorder, 'brand')->widget(Select2::classname(), [
                                    'data' => $brand_name,
                                    //'language' => 'de',
                                    'options' => [
                                        'placeholder' => '-- Select --',
                                        'onchange' => '$.get( "' . Url::toRoute('/management/pointorder/brand') . '", {id: $(this).val() } )
                                            .done(function( data ) {
                                               $("#pointorder-product_name").html(data);
                                            }
                                        );'
                                     ],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);
                            ?>
                           </div>
                           <div class="col-md-3">
                               <?php

                                echo $form->field($pointorder, 'product_name')->widget(Select2::classname(), [
                                    'data' => $d_product_name,
                                    //'language' => 'de',
                                    'options' => [
                                        'placeholder' => '-- Select --',
                                        'onchange' => '$.get( "' . Url::toRoute('/management/pointorder/pdescription') . '", {id: $(this).val() } )
                                            .done(function( data ) {
                                               $("#pointorder-product_description").html(data);
                                            }
                                        );'
                                     ],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);
                            ?>
                           </div>
                           <div class="col-md-4">
                               <?php

                                echo $form->field($pointorder, 'product_description')->widget(Select2::classname(), [
                                    'data' => $d_product_description,
                                    //'language' => 'de',
                                    'options' => [
                                        'placeholder' => '-- Select --',
                                     ],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]);
                            ?> 
                           </div>
                           <div class="col-md-2">
                               <?= $form->field($pointorder, 'vqty')->textInput() ?>
                           </div>
                           <div class="col-md-1" style="padding-left: 0px;">
                               <label class="control-label" for="pointorder-vqtybtn" style="height: 14px;"></label>
                               <input type="button" id="addButton" class="addButtonc btn btn-primary" value="Add" />
                           </div>
                       </div>
                   </div>

                   <div class="direct-chat-messages no-padding">
                       <table class="table fixed" id="retc"> 
                           <thead>
                               <tr>
                                   <th class="hide">barcodename</th>
                                   <th class="hide">barcodeid</th>
                                   <th width="40%">Product</th>
                                   <th width="5%">Qty</th>
                                   <th width="15%">Point</th>
                                   <th width="15%">RM value</th>
                                   <th width="15%">Status</th>
                                   <th style="width: 5%; text-align: center;"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                               </tr>
                           </thead>
                           <tbody class="product_item_list" id="product_item_list"></tbody>
                       </table>
                   </div>
                   <table style="width:100%; float:right; padding:5px; color:#000; background: #FFF;" id="totalTable">
                       <tbody>
                           <tr>
                               <td colspan="2" style="padding: 5px 10px; border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;">
                                   Total Items                                        </td>
                               <td align="right" style="padding:5px 10px 5px 10px; font-size: 14px;border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;" class="text-right">

                               </td>
                               <td align="right" style="padding:5px 10px 5px 10px; font-size: 14px;border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;">
                                   <input class="form-control" type="text" id="totalqty" name="totalqty" value="0" />
                               </td>
                           </tr>
                           <tr>
                               <td colspan="2" style="padding: 5px 10px; border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;">
                                   Grand Total(RM)                                        </td>
                               <td align="right" style="padding:5px 10px 5px 10px; font-size: 14px;border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;" class="text-right">

                               </td>
                               <td align="right" style="padding:5px 10px 5px 10px; font-size: 14px;border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;">
                                   <input class="form-control" type="text" id="grandtotal" name="grandtotal" value="0.00" />
                               </td>
                           </tr>
                       </tbody>
                   </table>
    <?php
    $script = <<<EOD
                var handledCount = 0;
                $('.addButtonc').unbind('click').click(function() {
                    var test = $("#pointorder-product_description").val();
                    //alert(test);
                    var flag = 0;
                    $("#retc").find("tr").each(function () {
                        var td1 = $(this).find("td:eq(0)").text();
                        if (test == td1) {
                            flag = 1;
                        }
                    });
            
                    if (handledCount == 0){
                        var itemID = $("#pointorder-product_description").val(),vNode  = $(this);
                        var qty = $("#pointorder-vqty").val();
                        var campaignid = $("#pointorder-listing_campaign").val();
                        var sum = 0.0;
                        $.ajax({
                            url:'getitem?id='+itemID+'&qty='+qty+'&campaignID='+campaignid,
                            dataType: 'json',
                            success : function(data) {
                                if (flag == 1) {
                                    alert('Item already exists');
                                    $("#add_item").val('');
                                    $("#add_item").focus();
                                    $("#pointorder-vqty").val('');
                                } else {
                                    $('#retc tbody').append("<tr id="+ data.barcodeid + "><td class='hide'>" + data.barcodeid+ "</td><td class='hide'><input class='form-control' type='text' name='barcodeid[]' value='"+ data.barcodeid +"'/><input class='form-control' type='text' name='totqty[]' value='"+ data.qty+ "'/></td><td>" + data.product + "</td><td class='text-center'>" + data.qty+ "</td><td class='text-center'>" + data.point+ "</td><td class='text-center price'>" + data.value_price+ "</td><td class='text-center'>" + data.status+ "</td><td class='text-center'><a href='javascript::;' class='btnDelete'><i class='fa fa-trash-o' style='opacity:0.5; filter:alpha(opacity=50);'></i></a></td></tr>");
                                    $("#add_item").val('');
                                    $("#add_item").focus();
                                    var count = $('#retc tbody').children('tr').length;
                                    update_amounts();
                                    update_percentage();
                                    $("#totalitem").html(count);
                                    $("#totalqty").val(count);
                                    $("#pointorder-vqty").val('');
                                }
                            },
                            error : function() {
                                console.log('error');
                            }
                        });
                    }
                    handledCount++;
                    if (handledCount == 1)
                        handledCount = 0;
                    return false;
                    $("#add_item").val('');
                });

            function update_amounts()
            {
                var sum = 0.00;
                $('#retc > tbody  > tr').each(function() {
                    var price = $(this).find('.price').html();
                    sum = sum + Number(price);
                });
                $("#grandtotal").val(sum.toFixed(2));
            }
            function update_percentage()
            {
                var priceOne = parseFloat($('#pointorder-dealer_invoice_price').val());
                var priceTwo = parseFloat($('#grandtotal').val());
                var price3 = priceTwo / priceOne * 100;
                $('#pointorder-pay_out_percentage').val(price3.toFixed(2)); // Set the rate
            }
            
            $('#pointorder-dealer_invoice_price').on('input',function(e){
                update_percentage();
            });

            
            $("#retc").on('click','.btnDelete',function(){
                //$(this).closest('tr').remove();
                var tableRow = $(this).closest('tr');
                
                tableRow.find('td').fadeOut('fast', 
                    function(){ 
                        tableRow.remove();
                        var count = $('#retc tbody').children('tr').length;
                        update_amounts();
                        update_percentage();
                        $("#totalitem").html(count);
                        $("#totalqty").val(count);
                    }
                );
            });
            $('#btnsubmit').on('click', function() {
                if($("#product_item_list tr").length > 0){
                    return true;
                }else{
                    alert('Please ensure there is at least one row in product list table');
                    return false;
                }
            });

EOD;
$this->registerJs($script);
    ?>
    <?= $form->field($pointorder, 'order_remarks')->textarea(['rows' => 6]) ?>
                   
               </div> <!-- close right DIV -->
           </div>

            <div class="box-footer">
                <?= Html::submitButton($pointorder->isNewRecord ? Yii::t('app', 'Submit') : Yii::t('app', 'Save'), ['class' => $pointorder->isNewRecord ? 'btn btn-info btgr' : 'btn btn-info btgr', 'id' => 'btnsubmit']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div><!-- /.box -->

</div>
