<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */

$this->title = $model->profile->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Point Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

  <section class="invoice">
          <!-- title row -->
          <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
                <i class="fa fa-globe"></i> <b>Transaction ID <?= $model->order_num ?></b>
                <?php
                if(!empty($model->order_status)){
                    echo '<small class="pull-right">Approved Date: '.date('d-m-Y', strtotime($model->updated_datetime)).'</small>   ';
                }
                ?>
                <small class="pull-right" style="margin-right: 15px;">Created Date: <?php echo date('d-m-Y', strtotime($model->created_datetime))?></small>
              </h2>
            </div><!-- /.col -->
          </div>
          <!-- info row -->
          <div class="row invoice-info hide">
            <div class="col-sm-4 invoice-col">
              From
              <address>
                <strong><?= $model->dealerOutlet->customer_name ?></strong><br>
                <!--795 Folsom Ave, Suite 600<br>
                San Francisco, CA 94107<br>
                Phone: (804) 123-5432<br/>
                Email: info@almasaeedstudio.com-->
              </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
              To
              <address>
                <strong><?= $model->company->company_name ?></strong><br>
                <?= $model->company->mailing_address ?><br/>
                Phone: <?= $model->company->tel ?><br/>
                Email: <?= $model->profile->email ?>
              </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <?php
                if(!empty($model->order_status)){
                    if($model->order_status == 1){
                        $sts = '<span class="label label-warning">Pending</span>';
                    }else if($model->order_status == 7){
                        $sts = '<span class="label label-Danger">Canceled</span>';
                    }else if($model->order_status == 17){
                        $sts = '<span class="label label-success">Approved</span>';
                    }else{
                       $orderaction = common\models\OrderStatus::find()->where(['order_status_id' => $model->order_status])->one();
                       $sts = $orderaction->name;
                    }
                    echo '<h3 style="margin-top: 0px;">'.$sts.'</h3>';
                }
                ?>
              <br/>
              <?php
              $banks = common\models\Banks::find()->where(['id' => $model->bank->bank_name])->one();
              $bankname = $banks->bank_name;
              ?>
              <b>Bank Name:</b> <?= $bankname ?><br/>
              <b>Account Name:</b> <?= $model->bank->account_name ?><br/>
              <b>Account #:</b> <?= $model->bank->account_number ?>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <!-- Table row -->
          <div class="row">
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Product Description</th>
                    <th class="text-center">Qty</th>
                    <th class="text-center">Points / can</th>
                    <th class="text-center">Total Points Awarded</th>
                    <th class="text-center">Value (RM) / can</th>
                    <th class="text-center">Total Value (RM)</th>
                    <th class="text-center">Status</th>
                  </tr>
                </thead>
                <tbody>
                    <?php
                    $orderitems = \common\models\PointOrderItem::find()->where(['point_order_id' => $model->order_id])->all();
                    $n = 1;
                    foreach($orderitems as $orderitem){
                        $perproduct = \common\models\ProductList::find()->where(['product_list_id' => $orderitem->product_list_id])->one();
                        echo '<tr>
                            <td>'.$n.'</td>
                            <td>'.$perproduct->product_description.'</td>    
                            <td class="text-center">'.$orderitem->total_qty.'</td>
                            <td class="text-center">'.$orderitem->item_bar_total_point.'</td>
                            <td class="text-center">'.$orderitem->total_qty_point.'</td>
                            <td class="text-center">'.$orderitem->item_bar_total_value.'</td>
                            <td class="text-center">'.$orderitem->total_qty_value.'</td>
                            <td class="text-center">'.$orderitem->statusDescription.'</td>     
                          </tr>';
                        $n++;
                    }
                    ?>
                
                </tbody>
              </table>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
              <p class="lead">Remark:</p>
              <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                <?= $model->order_remarks; ?>
              </p>
            </div><!-- /.col -->
            <div class="col-xs-6">
              <?php
              $total_points = \common\models\PointOrderItem::find()->where(['point_order_id' => $model->order_id,'Item_status' => 'G'])->sum('total_qty_point');
              $total_price = \common\models\PointOrderItem::find()->where(['point_order_id' => $model->order_id,'Item_status' => 'G'])->sum('total_qty_value');
              ?>
              <p class="lead">Awarded </p>
              <div class="table-responsive">
                <table class="table align-right">
                  <tr>
                    <th>Total Points:</th>
                    <td><?= $total_points ?></td>
                  </tr>
                  <tr>
                    <th>Total RM:</th>
                    <td><?= $total_price ?></td>
                  </tr>
                </table>
              </div>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->

