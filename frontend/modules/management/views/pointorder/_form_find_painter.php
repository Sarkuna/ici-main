<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Register Painter / Record Transaction';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    input#findpainter-ic_no {font-size: 35px;}
    .field-findpainter-membership_id {display: none;}
</style>
<div class="login-box">
    <div class="login-box-body">
        <div class="point-order-form">
            <p class="text-info"><em>Please select NRIC or Membership ID from drop-down list</em></p>
            <?php $form = ActiveForm::begin(); ?>

            <?php echo $form->field($model, 'type')->dropDownList(['ic' => 'NRIC/Passport', 'mid' => 'Membership ID',]); ?>


            <?=
            $form->field($model, 'ic_no', [
                'template' => '{label} <div class=""><div class="col-lg-12 input-group input-group-lg">{input}
        <span class="input-group-btn"></span></div>{error}{hint}</div>',
            ])->textInput(['autocomplete' => 'off'])
            ?>

            <?=
            $form->field($model, 'membership_id', [
                'template' => '{label} <div class=""><div class="col-lg-12 input-group input-group-lg">{input}
        <span class="input-group-btn"></span></div>{error}{hint}</div>',
            ])->textInput(['autocomplete' => 'off'])->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '9999999999999',
                    'options' => [
                        'placeholder' => '0308101016001',
                        'class' => 'form-control',
                    ]
                ])
            ?>
            <button class="btn btn-info btn-flat btn-lg" type="submit">Go!</button>
        <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<?php
$script = <<< JS

$(document).ready(function () {
    $("#findpainter-type").change(function(){        
        var val = $('#findpainter-type').val();
        if(val == 'mid' ) {
            $('.field-findpainter-membership_id').show();
            $('.field-findpainter-ic_no').hide();
        } else {
            $('.field-findpainter-ic_no').show();
            $('.field-findpainter-membership_id').hide();
        }
    });
});

JS;
$this->registerJs($script);
?>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?= $model->ic_no ?></h4>
      </div>
      <div class="modal-body">
          <p>Sorry NRIC/PP Number not found. </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a href="/painter/painterprofile/create?icno=<?= $model->ic_no ?>" class="btn btn-primary">Create Account</a>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?= $model->ic_no ?></h4>
      </div>
      <div class="modal-body">
          <p>Sorry, your adminstrator not approve this painter account</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

<?php
if($model->showpopup == '1'){
$this->registerJs(
  "$(function() {                     
      $('#myModal').modal('show');   
    });");
}else if($model->showpopup == '2'){
    $this->registerJs(
      "$(function() {                     
          $('#myModal2').modal('show');   
        });");
}
?>
