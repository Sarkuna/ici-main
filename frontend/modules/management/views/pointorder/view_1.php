<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */

$this->title = $model->profile->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Point Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="point-order-view">

    <h1>12333<?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->order_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->order_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'order_id',
            'order_dealer_id',
            'painter_login_id',
            'painter_ic',
            'order_qty',
            'order_total_point',
            'order_total_amount',
            'order_status',
            'order_remarks:ntext',
            'order_IP',
            'created_datetime',
            'created_by',
            'updated_datetime',
            'updated_by',
        ],
    ]) ?>

</div>
