<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PointOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Redemption on-behalf';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-9">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-6 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-handshake-o"></i> <?= Html::encode($this->title) ?></h3></div>
            <div class="col-lg-6 col-sm-2 col-xs-12 text-right no-padding">
                
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="point-order-index">
<?=Html::beginForm(['pointorder/bulkonbhalfredemption'],'post');?>
                <div class="col-lg-12">
                <div class="col-lg-6 no-padding">  
                <?=
                Html::submitButton('Redeem', ['class' => 'btn btn-info', 'data' => [
                        'confirm' => 'Are you sure you want to redeem selected items?',
                        'method' => 'post',                        
                    ],]);
                ?>
                <?= Html::a('Cancel', ['onbhalfgroup'], ['class' => 'btn btn-default']) ?>     
                </div>
            </div>
                
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn','headerOptions' => ['width' => '10'],],
                        //['class' => 'yii\grid\CheckboxColumn','headerOptions' => ['width' => '10'],],
                        //'order_id',
                        //'order_num',
                        /*[
                            'attribute' => 'order_num',
                            'label' => 'Transaction #',
                            'format' => 'html',
                            'headerOptions' => ['width' => '110', 'class' => 'text-center'],
                            'value' => function ($model) {
                                return $model->order_num;
                            },
                        ],*/
                        //'order_dealer_id',
                        [
                            'class' => 'yii\grid\CheckboxColumn', 'checkboxOptions' => function($model) {
                                  return ['value' => $model->painter_login_id];
                              },
                        ],
                        [
                            'attribute' => 'card_id',
                            'label' => 'Membership#',
                            'format' => 'html',
                            'headerOptions' => ['width' => '150'],
                            'value' => function ($model) {
                                return $model->profile->card_id;
                            },
                        ],            
                        [
                            'attribute' => 'full_name',
                            'label' => 'Painter Name',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->profile->full_name;
                            },
                        ],
                        [
                            'attribute' => 'totalItems',
                            'label' => 'Total Items',
                            'format' => 'html',
                            'contentOptions' =>['class' => 'text-right',],
                            'headerOptions' => ['width' => '130'],
                            'value' => function ($model) {
                                return $model->ototalItems;
                            },
                        ],
                        [
                            'attribute' => 'TotalPointValue',
                            'label' => 'Total Point',
                            'format' => 'html',
                            'contentOptions' =>['class' => 'text-right',],
                            'headerOptions' => ['width' => '130'],
                            'value' => function ($model) {
                                return $model->totalPointValue;
                            },
                        ],            
                        [
                            'attribute' => 'totalAmount',
                            'label' => 'Total Amount RM',
                            'format' => 'html',
                            'contentOptions' =>['class' => 'text-right',],
                            'headerOptions' => ['width' => '170'],
                            'value' => function ($model) {
                                return $model->totalAmount;
                            },
                        ],
                                   
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'headerOptions' => ['width' => '130'],
                            'template' => '{view}', //{update} {delete}
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    $url = ['onbhalfindex', 'id'=>$model->painter_login_id];//'/onbhalfindex2'.$model->painter_login_id;
                                    return (Html::a('<span class="glyphicon glyphicon-eye-open"> Redeem</span>', $url, ['title' => Yii::t('app', 'View'),]));
                                },
                            ],
                        ],           
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
<?php
    $clientScript = '
        $("#w0 table tbody tr").click(function()
        {
            recalculate();
        });

        $("input[type=checkbox]").change(function () {
            //recalculate();
        });
    ';
    $this->registerJs($clientScript, \yii\web\View::POS_END, 'booking-period');
    ?>


<script language="javascript">  
    function recalculate() {
        
        var sum = 0;
        var sumrm = 0.00;
        var sumtot = 0.00;
        var a = "";
        $("input[name='selection[]']:checked").each(function () {
            $this=$(this);
            var amount = $(this).closest("tr").find("td:nth-child(8)").text();
            var amountrm = $(this).closest("tr").find("td:nth-child(9)").text();
            //alert(amount);
            
            if (this.checked) sum = sum + parseFloat(amount);
            if (this.checked) sumrm = sumrm + parseFloat(amountrm);
            if (this.checked) sumtot = sumtot + parseFloat($(this).attr('rel'));
            if (this.checked) {
                if (a.length == 0) {
                    a = $(this).closest("tr").find("td:first").text();
                } else {
                    a = a + "," + $(this).closest("tr").find("td:first").text();
                }
            } else {
                if (a.length == 1) {
                    a = a.slice(0, 1);
                } else {
                    a = a.replace(("," + $(this).closest("tr").find("td:first").text()), "");
                }
            }
        });

        $("#redemption-orderid").val(a); 
        $("#output").text(sum);
        $("#outputrm").text(sumrm.toFixed(2));
        $("#redemption-req_points").val(sum);
        $("#redemption-req_amount").val(sumtot.toFixed(2));
        $("#multiamt").val(sum);
        
    }
    function checkempty() {
        var minamt = $("#csvsendpayment-csv_amount").val(); 
        if(minamt == '0.00' || minamt == '0' || minamt == ''){
            alert('Minimum Ammount Required above 0.00');
            return false;
        }else{
            return true;
        }
        
    }
//});
    </script> 
    
    <script>

                    $('#gridID  table tbody tr').click(function()
                     {
                     var vas1Fee=0, vas2Fee=0;
                     var vasParams = "", vas1Param="&ccover=yes", vas2Param="&&ccar=yes", buyLink = "";
                    $this=$(this);    
                    var firstColVal= $this.find('td:first-child').text();
                    //var secondColVal= $this.find('td:nth-child(2)').text();


var chk = $(this).closest('tr').find('input:checkbox');
var rate = $this.find('td:nth-child(2)').text().substring(0, $this.find('td:nth-child(2)').text().length - 1);
var amount = parseInt($this.find('td:nth-child(8)').text());
var total = parseInt($this.find('td:nth-child(9)').val()); 
var cd = $this.find('td:nth-child(3)').html();

cd = cd.match("href=\"(.*?)\""); // returns array

if(chk[0].checked) {
    //alert("1 - condition");
    vas1Fee = parseInt($('#vas1').attr('value'));
    vas1Param="&ccover=yes";
}
else
{
    vas1Param="&ccover=no";
}

if(chk[1].checked)
{
    //alert("2 - condition");
    vas2Fee = parseInt($('#vas2').attr('value'));
    vas2Param="&&ccar=yes";
}
else
{
    vas2Param="&&ccar=no";
}

vasParams = vas1Param + vas2Param;
total = amount + vas1Fee + vas2Fee;

buyLink +=  "<a href=\'motor-insurance-confirm?company=" + $this.find('td:first-child').text() + "&rate=" + rate  + vasParams +     
"&cd=" +  cd[1] + "&amount=" + amount.toString() + "&total=" + total + "\'>Buy</a>";

//alert(buyLink);


//alert("amount : " + amount + " vas1Fee : " + vas1Fee + " vas2Fee : " + vas2Fee + " TOTAL : " + total);    
$this.find('td:nth-child(9)').text(total.toString());
$this.find('td:last-child').html(buyLink);
//var lastColVal= $this.find('td:last-child').text();

$.pjax.reload({container:'#pjaxID'});

</script> 