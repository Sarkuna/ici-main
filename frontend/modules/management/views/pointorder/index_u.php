<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
//use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PointOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Painter Transaction Management - Pending';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .summary {
    display: none;
}
</style>
<div class="col-xs-12">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-8 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?> (<small style="color: black !important;font-size: 14px;">All Pending record Transaction# Desc</small>)</h3></div>
            <div class="col-lg-4 col-sm-2 col-xs-12 text-right no-padding">
                <div class="col-xs-12 no-padding">
                    <?php if (Yii::$app->user->can('/management/pointorder/create')) { ?>
                        <?= Html::a('Create Point Order', ['create'], ['class' => 'btn btn-success']) ?>
                    <?php } ?>
                    <?= Html::a('Record New Transaction', ['findpainter'], ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="row">
                <div class="col-lg-3 col-xs-12">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3><?= $count ?></h3>

                            <p>No of Pending</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3><?= $no_of_item ?></h3>

                            <p>No of Item</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-12">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3><?= Yii::$app->formatter->asCurrency($total_rm) ?></h3>

                            <p>Total RM</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="point-order-index">
                <table id='example' class="table table-bordered table-striped dataTable">
                <thead>
                    <tr>

                        <th>Transaction #</th>
                        <th>Dealer Name</th>
                        <th>Membership#</th>
                        <th>Painter Name</th>
                        <th>Invoice #</th>
                        <th>Invoice Date</th>
                        <th>Total Point</th>
                        <th>Total Amount RM</th>
                        <th class="action-column">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                <?php $n = 1; foreach ($models as $model): ?>
                <?php
                
                echo '<tr>';
                echo '<td>'.$model->order_num.'</td>';  
                echo '<td>'.$model->dealerOutlet->customer_name.'</td>';
                echo '<td>'.$model->profile->card_id.'</td>';
                echo '<td>'.$model->profile->full_name.'</td>'; 
                echo '<td>'.$model->dealer_invoice_no.'</td>'; 
                echo '<td>'.date('d-M-y', strtotime($model->dealer_invoice_date)).'</td>'; 
                echo '<td>'.$model->order_total_point.'</td>';
                echo '<td>'.$model->order_total_amount.'</td>';
                echo '<td>'
                . '<a href="/management/pointorder/view?id='.$model->order_id.'" title="View"><span class="glyphicon glyphicon-eye-open"></span></a>'
                . '<a href="/management/pointorder/editpoint?id='.$model->order_id.'" title="Edit"><span class="fa fa-pencil-square-o"></span></a></td>';
                echo '</tr>';
                $n++;
                ?>
                
<?php endforeach; ?>
                    </tbody>
   </table>
<?php
   // display pagination
   echo LinkPager::widget([
      'pagination' => $pagination,
   ]);
?>
            </div>
        </div>
    </div>
</div>



<?php
    $script = <<<EOD
                $(function () {
    $('#example13').DataTable()

  })
            

EOD;
$this->registerJs($script);
    ?>

