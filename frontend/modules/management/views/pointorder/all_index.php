<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\widgets\Pjax;

$session = Yii::$app->session;
$this->title = 'Transaction Search';
$this->params['breadcrumbs'][] = $this->title;

?>
    <div class="col-xs-12">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-12 col-sm-12 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?></h3></div>
        </div>
        <div class="box-body table-responsive">
            <?= $this->render('_formSearch', ['searchModel' => $searchModel]); ?>
            <div class="point-order-index">

                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        //'order_id',
                        //'order_num',
                        [
                            'attribute' => 'order_num',
                            'label' => 'Transaction #',
                            'format' => 'html',
                            'headerOptions' => ['width' => '110', 'class' => 'text-center'],
                            'value' => function ($model) {
                                return $model->order_num;
                            },
                        ],
                        //'order_dealer_id',
                        /*[
                            'attribute' => 'customer_name',
                            'label' => 'Dealer',
                            'format' => 'html',
                            'headerOptions' => ['width' => '200'],
                            'value' => function ($model) {
                                return $model->dealerOutlet->customer_name;
                            },
                        ],*/
                        [
                            'attribute' => 'card_id',
                            'label' => 'Membership#',
                            'format' => 'html',
                            'value' => function ($model) {
                                return $model->profile->card_id;
                            },
                        ],            
                        [
                            'attribute' => 'full_name',
                            'label' => 'Painter Name',
                            'format' => 'html',
                            'headerOptions' => ['width' => '180'],
                            'value' => function ($model) {
                                return $model->profile->full_name;
                            },
                        ],            
                                   
                                    
                        [
                            'attribute' => 'created_datetime',
                            'label' => 'Created Date',
                            'value' => 'created_datetime',
                            //'format' => ['php:D, d-M-Y H:i:s A'],
                            //'format' => 'datetime',
                            'format' =>  ['date', 'php:d-m-Y h.i A'],

                            'filter' => DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'created_datetime',
                                'convertFormat' => true,
                                'readonly' => true,
                                //'type' => DatePicker::TYPE_BUTTON,
                                //'displayFormat' => 'php:D, d-M-Y H:i:s A',
                                'pluginOptions' => [
                                    'format' => 'yyyy-M-dd'
                                    //'format' => 'dd-M-yyyy'
                                ],
                            ]),
                            'options' => ['width' => '150']
                        ],            
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}{editpoint}', //{update} {delete}
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                                },
                                'editpoint' => function ($url, $model) {
                                    //if($model->order_status == '1'){
                                        return (Html::a('<span class="fa fa-pencil-square-o"></span>', $url, ['title' => Yii::t('app', 'Edit'),]));
                                    //}
                                },        
                            ],
                        ],            
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>



