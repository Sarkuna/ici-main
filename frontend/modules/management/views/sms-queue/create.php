<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SMSQueue */

$this->title = 'Create Smsqueue';
$this->params['breadcrumbs'][] = ['label' => 'Smsqueues', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smsqueue-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
