<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SMSQueueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Smsqueues';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="smsqueue-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Smsqueue', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'sms_queue_id',
            'painterID',
            'name',
            'mobile',
            'redemption_invoice_no',
            // 'r_points',
            // 'r_amount',
            // 'data:ntext',
            // 'email_template_id:email',
            // 'date_to_send',
            // 'created_datetime',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
