<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Import Bank CSV';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bar Code Pay Out Summaries'), 'url' => ['/management/bar-code']];
$this->params['breadcrumbs'][] = $this->title;
?>


<section class="invoice no-margin">    
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-edit"></i> <?= Html::encode($this->title) ?>                
            </h2>
        </div><!-- /.col -->
        <div class="col-xs-12">
        <div class="redemption-form">

            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <?= $form->field($model, 'file')->fileInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
    </div>
    
</section>

