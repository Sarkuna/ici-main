<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BCRedemption */

$this->title = 'Create Bc Redemption';
$this->params['breadcrumbs'][] = ['label' => 'Bc Redemptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bcredemption-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
