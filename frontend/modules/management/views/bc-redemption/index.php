<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BCRedemptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bc Redemptions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bcredemption-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Bc Redemption', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'redemptionID',
            'painterID',
            'crm_id',
            'redemption_invoice_no',
            'count_of_sku',
            //'sum_of_points',
            //'per_rm',
            //'total_rm',
            //'redemption_remarks:ntext',
            //'redemption_IP',
            //'created_by',
            //'created_datetime',
            //'updated_by',
            //'updated_datetime',
            //'pay_out_summary_id',
            //'paid_date',
            //'internel_status',
            //'comment:ntext',
            //'review_comment:ntext',
            //'ticket',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, BCRedemption $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'redemptionID' => $model->redemptionID]);
                 }
            ],
        ],
    ]); ?>


</div>
