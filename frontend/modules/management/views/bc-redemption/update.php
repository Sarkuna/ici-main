<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BCRedemption */

$this->title = 'Update Bc Redemption: ' . $model->redemptionID;
$this->params['breadcrumbs'][] = ['label' => 'Bc Redemptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->redemptionID, 'url' => ['view', 'redemptionID' => $model->redemptionID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bcredemption-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
