<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BCRedemptionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bcredemption-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'redemptionID') ?>

    <?= $form->field($model, 'painterID') ?>

    <?= $form->field($model, 'crm_id') ?>

    <?= $form->field($model, 'redemption_invoice_no') ?>

    <?= $form->field($model, 'count_of_sku') ?>

    <?php // echo $form->field($model, 'sum_of_points') ?>

    <?php // echo $form->field($model, 'per_rm') ?>

    <?php // echo $form->field($model, 'total_rm') ?>

    <?php // echo $form->field($model, 'redemption_remarks') ?>

    <?php // echo $form->field($model, 'redemption_IP') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_datetime') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'updated_datetime') ?>

    <?php // echo $form->field($model, 'pay_out_summary_id') ?>

    <?php // echo $form->field($model, 'paid_date') ?>

    <?php // echo $form->field($model, 'internel_status') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <?php // echo $form->field($model, 'review_comment') ?>

    <?php // echo $form->field($model, 'ticket') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
