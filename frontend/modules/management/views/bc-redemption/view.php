<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\BCRedemption */

$this->title = $model->redemptionID;
$this->params['breadcrumbs'][] = ['label' => 'Bc Redemptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="bcredemption-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'redemptionID' => $model->redemptionID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'redemptionID' => $model->redemptionID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'redemptionID',
            'painterID',
            'crm_id',
            'redemption_invoice_no',
            'count_of_sku',
            'sum_of_points',
            'per_rm',
            'total_rm',
            'redemption_remarks:ntext',
            'redemption_IP',
            'created_by',
            'created_datetime',
            'updated_by',
            'updated_datetime',
            'pay_out_summary_id',
            'paid_date',
            'internel_status',
            'comment:ntext',
            'review_comment:ntext',
            'ticket',
        ],
    ]) ?>

</div>
