<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BCRedemption */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bcredemption-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'painterID')->textInput() ?>

    <?= $form->field($model, 'crm_id')->textInput() ?>

    <?= $form->field($model, 'redemption_invoice_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'count_of_sku')->textInput() ?>

    <?= $form->field($model, 'sum_of_points')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'per_rm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_rm')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'redemption_remarks')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'redemption_IP')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'pay_out_summary_id')->textInput() ?>

    <?= $form->field($model, 'paid_date')->textInput() ?>

    <?= $form->field($model, 'internel_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'review_comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'ticket')->dropDownList([ 'open' => 'Open', 'close' => 'Close', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
