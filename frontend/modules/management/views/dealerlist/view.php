<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\DealerList */

$this->title = $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Manage Dealer Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            <h3 class="box-title pull-right"><?= $model->getStatus() ?></h3>
        </div><!-- /.box-header -->
        <div class="dealer-list-view">
            <div class="box-body">
                <?=
                DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        //'id',
                        'customer_name',
                        //'account_type',
                        'region',
                        'state',
                        'area',
                        'address:ntext',
                        'contact_no',
                        'contact_person',
                        //'status',
                        //'created_datetime',
                        //'created_by',
                        //'updated_datetime',
                        //'updated_by',
                    ],
                ])
                ?>
            </div>
            <div class="box-footer">
                <p>
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?=
                    Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>
