<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\PointOrder */

$this->title = 'Import Excel File';
$this->params['breadcrumbs'][] = ['label' => 'Manage Dealer Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-6">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        </div><!-- /.box-header -->
        <div class="box-body">
        <div class="dealer-list-create">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                    <?= $form->field($model, 'excel')->fileInput() ?>
                    <div class="form-group">
                        <?= Html::submitButton('Import', ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
        </div>
        </div>
    </div>
</div>

