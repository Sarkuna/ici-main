<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\DealerList */
/* @var $form yii\widgets\ActiveForm */
use common\models\Region;
use common\models\State;
$d_region = ArrayHelper::map(Region::find()->orderBy('region_id ASC')->all(), 'region_id', 'region_name');
?>
<div class="dealer-list-form">
    <?php $form = ActiveForm::begin(); ?>
        <div class="box-body">
            <div class="col-md-6 no-padding">
               <?= $form->field($model, 'customer_name')->textInput(['maxlength' => true]) ?>
               <?= $form->field($model, 'contact_person')->textInput(['maxlength' => true]) ?>
               <?= $form->field($model, 'contact_no')->textInput(['maxlength' => true]) ?> 
                <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
                <?=
                    $form->field($model, 'status')->dropDownList(['A' => 'Active', 'X' => 'Deleted'], ['prompt' => '-- Select --']
                    )
                    ?>
            </div>
            <div class="col-md-6 no-padding">
                <div class="col-xs-12 col-sm-6 col-lg-6">
                    <?=
                        $form->field($model, 'region_id')->dropDownList($d_region, [
                            'prompt' => '-- Select --',
                            'onchange' => '$.get( "' . Url::toRoute('/management/dealerlist/dregion') . '", {id: $(this).val() } )
                                     .done(function( data ) {
                                        $("#dealerlist-state").html(data);
                                     }
                                 );'
                                ]
                        )
                        ?> 
                </div>    
                <div class="col-xs-12 col-sm-6 col-lg-6">
                    <?=
                    $form->field($model, 'state')->dropDownList(
                            ArrayHelper::map(State::find()
                                            ->orderBy('state_id ASC')
                                            ->all(), 'state_name', 'state_name'), ['prompt' => '-- Select --']
                    )
                    ?>
                </div>
                <div class="col-xs-12 col-sm-12 col-lg-12">
                    <?= $form->field($model, 'area')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model, 'address')->textarea(['rows' => 4]) ?>
                </div>           
            </div>
        </div>
        <div class="box-footer">
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
