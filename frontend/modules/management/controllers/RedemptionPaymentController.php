<?php

namespace app\modules\management\controllers;

use Yii;
use common\models\PayOutSummary;
use common\models\PayOutSummarySearch;
use common\models\RedemptionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RedemptionPaymentController implements the CRUD actions for PayOutSummary model.
 */
class RedemptionPaymentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PayOutSummary models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PayOutSummarySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PayOutSummary model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModel = new RedemptionSearch();
        //$searchModel->redemption_status_ray = ['2'];
        //$searchModel->pay_out_summary_id = 0;
        $dataProvider = $searchModel->searchray(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['pay_out_summary_id' => $id]);

        return $this->render('view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionUpdateBank($id)
    {
        
        $model = new \common\models\PayoutForm();
        $redemption = \common\models\Redemption::find()->where(['redemptionID' => $id])->one();
        $bank = \app\modules\painter\models\BankingInformation::find()->where(['user_id' => $redemption->painterID])->one();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
              
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            $bankupdate = \app\modules\painter\models\BankingInformation::find()->where(['id' => $bank->id])->one();
            if($model->account_verification == 'Y') {
               $verification =  'Y';
            }else {
               $verification =  'N';
            }

            $bankupdate->account_no_verification = $verification;
            if($bankupdate->save(false)) {
                if($model->status == 'review') {
                    \common\models\Redemption::updateAll(['review_comment' => $model->comment, 'internel_status' => $model->status], ['=', 'painterID',  $redemption->painterID]);
                }else {
                    $redemption->internel_status = $model->status;
                    $redemption->comment = $model->comment;
                    $redemption->save(false);
                }                
                \Yii::$app->getSession()->setFlash('success',['title' => 'Bank Account#', 'text' => 'Bank Acc# ('.$bankupdate->account_number.') verfication updated']);
            }else {
                \Yii::$app->getSession()->setFlash('error',['title' => 'Bank Account#', 'text' => 'Bank Acc# ('.$bankupdate->account_number.') updated fail']);
            }
            return $this->redirect(['view', 'id' => $redemption->pay_out_summary_id]);
        } else {
            if($bank->account_no_verification == 'Y') {
                $model->account_verification = 'Y';
            }else {
                $model->account_verification = 'N';
            }
            $model->status = $redemption->internel_status;
            $model->comment = $redemption->review_comment;
            return $this->renderAjax('_formpop', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionUpdatePayment($id)
    {
        
        $model = new \common\models\PayoutForm();
        $redemption = \common\models\Redemption::find()->where(['redemptionID' => $id])->one();
        $bank = \app\modules\painter\models\BankingInformation::find()->where(['user_id' => $redemption->painterID])->one();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
              
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            /*if($model->status == 'Y') {
               $bank->account_no_verification =  'Y';
               $bank->save(false);
            }else {
               $bank->account_no_verification =  'N';
               $bank->save(false); 
            }*/
            $redemption->internel_status = $model->status;
            $redemption->comment = $model->comment;
            /*if($model->status == 'review') {
                $redemption->review_comment = $model->comment;
            }else {
                $redemption->comment = $model->comment;
            }*/
            
            if($redemption->save()) {
                \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'Redemption '.$redemption->redemption_invoice_no.' has been updated']);
                
            }  else {
                \Yii::$app->getSession()->setFlash('fail',['title' => 'Fail', 'text' => 'Redemption '.$redemption->redemption_invoice_no.' not updated']);
            }
            return $this->redirect(['view', 'id' => $redemption->pay_out_summary_id]);  
        } else {
            $model->status = $redemption->internel_status;
            $model->comment = $redemption->comment;
            return $this->renderAjax('_formpop_payment', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new PayOutSummary model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PayOutSummary();

        if ($model->load(Yii::$app->request->post())) {
            $createdate = $model->date_created;
            $createdate = explode(' - ',$createdate);
            $start_date = date('Y-m-d', strtotime($createdate[0])).' 00:00:00';
            $end_date = date('Y-m-d', strtotime($createdate[1])).' 23:59:59';

            $countredemption = \common\models\Redemption::find()->where(['between','redemption_created_datetime', $start_date, $end_date])->andWhere(['=','redemption_status_ray', '2'])->count();

            if($countredemption > 0) {
                $countredemptioncheck = \common\models\Redemption::find()->where(['between','redemption_created_datetime', $start_date, $end_date])->andWhere(['is','pay_out_summary_id', null])->andWhere(['=','redemption_status_ray', '2'])->count();
                if($countredemptioncheck > 0) {
                    $model->date_created = date('Y-m-d');
                    $model->start_date = $start_date;
                    $model->end_date = $end_date;
                    //$model->save();
                    if($model->save()) {
                        $bulkRedemption = \common\models\Redemption::updateAll(['pay_out_summary_id' => $model->pay_out_summary_id, 'internel_status' => 'processing'], ['and',['between','redemption_created_datetime', $start_date, $end_date],['=','redemption_status_ray', '2']]);
                        \Yii::$app->getSession()->setFlash('success',['title' => 'Action', 'text' => 'Bulk Action has been updated!']);                        
                    }
                }else {
                    //->andWhere(['=','redemption_status_ray', '2'])
                    $countredemptioncheck = \common\models\Redemption::find()->where(['between','redemption_created_datetime', $start_date, $end_date])->andWhere(['=','redemption_status_ray', '2'])->one();
                    $model = $this->findModel($countredemptioncheck->pay_out_summary_id);
                    $bulkRedemption = \common\models\Redemption::updateAll(['pay_out_summary_id' => $model->pay_out_summary_id], ['and',['between','redemption_created_datetime', $start_date, $end_date],['=','redemption_status_ray', '2']]);
                    \Yii::$app->getSession()->setFlash('success',['title' => 'Action', 'text' => 'Bulk Action has been updated!']);
                }
                
                $connection = Yii::$app->getDb();
                $command = $connection->createCommand("UPDATE redemption LEFT JOIN `banking_information` ON `redemption`.`painterID` = `banking_information`.`user_id` SET redemption.internel_status = 'not_verified' WHERE (`redemption`.`pay_out_summary_id`= '$model->pay_out_summary_id') AND (`banking_information`.`account_no_verification` != 'Y')");
                $result = $command->execute();
                return $this->redirect(['view', 'id' => $model->pay_out_summary_id]);
            }else {
                \Yii::$app->getSession()->setFlash('info',['title' => 'Oops', 'text' => 'No redemptions for this date range '.$model->date_created]);
                return $this->redirect(['create']);
            }
            //$model->save()
            //return $this->redirect(['view', 'id' => $model->pay_out_summary_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }

    }
    /**
     * Updates an existing PayOutSummary model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pay_out_summary_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PayOutSummary model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PayOutSummary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PayOutSummary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PayOutSummary::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
