<?php

namespace app\modules\management\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;

use common\models\ExcelTransaction;
use common\models\ExcelTransactionSearch;

/**
 * ExcelTransactionController implements the CRUD actions for ExcelTransaction model.
 */
class ExcelTransactionController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all ExcelTransaction models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = '@app/themes/AdminLTE/layouts/datatable';
        //$searchModel = new ExcelTransactionSearch();
        //$dataProvider = $searchModel->search($this->request->queryParams);
        
        $models = ExcelTransaction::find()->orderBy(['id' => SORT_DESC])->all();
        /*$countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $models = $query->offset($pages->offset)->limit($pages->limit)->all();*/

        return $this->render('index', [
            //'searchModel' => $searchModel,
            'models' => $models
        ]);
    }

    /**
     * Displays a single ExcelTransaction model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ExcelTransaction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new ExcelTransaction();

        if ($model->load(Yii::$app->request->post())) {
            $model->excel = UploadedFile::getInstance($model, 'excel');

                $rpath = realpath(Yii::$app->basePath);
                $model->excel->saveAs($rpath.'/web/upload/excelfiles/' . $model->excel->baseName . '.' . $model->excel->extension);

                $inputFile = $rpath.'/web/upload/excelfiles/' . $model->excel->baseName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                $pre_defined = array( "transaction #", "status");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));

                if($checkmatch == 2){
                    $countbatch = ExcelTransaction::find()->where(['approval_month' => $model->approval_month])->count();
                    if($countbatch == 0){
                        $batch = 1;
                    }else {
                        $batch = $countbatch + 1;
                    }
                    $model->upload_date = date('Y-m-d');
                    $model->upload_by = Yii::$app->user->id;
                    $model->desceription = 'Transaction Approval for the Month of '.date('F', strtotime($model->approval_month)).' : Batch '.$batch;
                    $model->save();
                    $newmsg = '';
                    $data = array();
                    for ($row = 2; $row <= $highestRow; ++ $row) {
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);

                        $transaction = trim($rowData[0]['transaction #']);
                        $transaction = str_replace(' ', '', $transaction);
                        $status = $rowData[0]['status'];
                        
                        $data[] = [$model->id,$transaction,$status,'pending'];
                    }
                    Yii::$app->db
                        ->createCommand()
                        ->batchInsert('excel_transaction_item', ['auto_transaction_id','transaction_num','transaction_status','status'],$data)
                        ->execute();
                    
                    Yii::$app->mailer->compose()
                    ->setTo('shihanagni@gmail.com')
                    ->setFrom([Yii::$app->params['supportEmail'] => 'Dulux Painter\'s Club'])
                    ->setSubject('New Excel File Uploaded Batch ID '.$model->id)
                    ->setHtmlBody('Dear Shihan,<br> Please check this '.$model->desceription)
                    ->send();
                    
                    unlink($inputFile);
                    //return $this->redirect(['view', 'id' => $model->id]);
                    return $this->redirect('index');
                }else {
                    echo 'Template format not match!';
                }
                
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }

        
    }

    /**
     * Updates an existing ExcelTransaction model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ExcelTransaction model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ExcelTransaction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return ExcelTransaction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ExcelTransaction::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
