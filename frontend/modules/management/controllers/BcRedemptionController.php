<?php

namespace app\modules\management\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use yii\data\ArrayDataProvider;
use yii\web\UploadedFile;

use common\models\BCRedemption;
use common\models\Redemption;
use common\models\BCRedemptionSearch;

/**
 * BcRedemptionController implements the CRUD actions for BCRedemption model.
 */
class BcRedemptionController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all BCRedemption models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new BCRedemptionSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionImportExcelRedemption() {

        $model = new \common\models\ExcelForm();
        $msg = '';
        if ($model->load(Yii::$app->request->post())) {

            $model->excel = UploadedFile::getInstance($model, 'excel');

            if ($model->excel && $model->validate()) {
                $newName = Yii::$app->security->generateRandomString();
                $model->excel->saveAs(Yii::getAlias('@webroot') .'/upload/excelfiles/' . $newName . '.' . $model->excel->extension);
                $inputFile = Yii::getAlias('@webroot') .'/upload/excelfiles/' . $newName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }
                
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                $pre_defined = array( "id","redemption #", "status", "paid date", "intake month", "comment");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));
                

                if($checkmatch >= 5){
                    $dataemailQueue = array();
                    for ($row = 2; $row <= $highestRow; $row++) {
                        //  Read a row of data into an array
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);
                        
                        $redemptionID = $rowData[0]['id'];
                        $redemption_no = $rowData[0]['redemption #'];
                        $status = $rowData[0]['status'];
                        $paid_date = date('Y-m-d', \PHPExcel_Shared_Date::ExcelToPHP($rowData[0]['paid date']));
                        $comment = $rowData[0]['comment'];
                        $intake_month = date('M-y', \PHPExcel_Shared_Date::ExcelToPHP($rowData[0]['intake month']));
                        
                        //print_r($rowData[0]['intake month'].$intake_month);
                        //die;
                        
                        if ($status == 'p' || $status == 'P' || $status == 'paid' || $status == 'Paid') {
                            $redemptionno = BCRedemption::find()->where(['redemptionID' => $redemptionID,])->asArray()->one();

                            $model = $this->findModel($redemptionID);
                            //$model->redemption_status_ray = '19';
                            $model->redemption_remarks = $comment;
                            //$model->redemption_status_ray_date = date('Y-m-d');
                            $model->internel_status = 'paid';
                            if (!empty($paid_date)) {
                                $model->paid_date = $paid_date;
                                $re_paid_date = date('d-m-Y', strtotime($paid_date));
                            } else {
                                $re_paid_date = date('d-m-Y');
                            }

                            if ($model->save()) {
                                $painterId = $model->painterID;
                                $user = \common\models\User::find()->where(['id' => $painterId])->asArray()->one();
                                $banks = \common\models\Banks::find()->where(['id' => $model->bank->bank_name])->asArray()->one();
                                $data = \yii\helpers\Json::encode(array(
                                            'redemption_ID' => $model->redemption_invoice_no,
                                            'redemption_point' => $model->sum_of_points,
                                            'redemption_value' => $model->total_rm,
                                            're_paid_date' => $re_paid_date,
                                            'bank' => $banks['bank_name'],
                                            'account_holder_name' => $model->bank->account_name,
                                            'bank_account_no' => $model->bank->account_number,
                                            'intake_month' => $intake_month,
                                ));

                                $painterinfo = \common\models\PainterProfile::find()
                                        ->where(['user_id' => $user['id']])
                                        ->one();
                                
                                $banking = \app\modules\painter\models\BankingInformation::find()
                                    ->where(['user_id' => $user['id']])
                                    ->one();
                                    
                                $banking->account_no_verification = 'Y';
                                $banking->remark = 'system';
                                $banking->save(false);
                                
                                Redemption::updateAll(['internel_status' => 're-verified'], ['and', ['painterID' => $user['id']], ['internel_status' => 'not_verified'] ]);
                                BCRedemption::updateAll(['internel_status' => 're-verified'], ['and', ['painterID' => $user['id']], ['internel_status' => 'not_verified'] ]);

                                $dataemailQueue[] = ['5', 'S', $painterId, $painterinfo->mobile, $user['username'], $user['email'], $data, 'P', 'P', date('Y-m-d H:i:s'), date('Y-m-d H:i:s'), Yii::$app->user->id, Yii::$app->user->id];

                                $msg .= '<li class="list-group-item"><span class="badge pull-right bg-green"><i class="fa fa-check" aria-hidden="true"></i></span>  Redemption# <b>(' . $redemption_no . ')</b> Sccessfuly Updated.</li>';
                            } else {
                                $msg .= '<li class="list-group-item"><span class="badge pull-right bg-yellow"><i class="fa fa-exclamation" aria-hidden="true"></i></span>Redemption# <b>(' . $redemption_no . ')</b> Fail.</li>';
                            }
                        } else {
                            $msg .= '<li class="list-group-item"><span class="badge pull-right bg-yellow"><i class="fa fa-ban" aria-hidden="true"></i></span>  Redemption# <b>(' . $redemption_no . ')</b> status column not match </li>';
                        }
                    }

                    Yii::$app->db
                        ->createCommand()
                        ->batchInsert('email_queue', ['email_template_global_id','type','user_id','mobile','name','email','data','status','sms_status','created_datetime','updated_datetime','created_by','updated_by'],$dataemailQueue)
                        ->execute();
                    
                    //summary
                    
                    unlink($inputFile);
                    return $this->render('summary', [
                                'msg' => $msg,
                    ]);
                    exit();
                }else {
                    \Yii::$app->getSession()->setFlash('danger',['title' => 'Import Redemptions Data in Excel', 'text' => 'Sorry your excel template not match with system template!']);
                    return $this->redirect(['/management/bar-code']);
                }
                return $this->redirect(['index']);
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('import_excel', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionImportCsvBank()
    {
        $model = new \common\models\FileForm();
        $msg = '';

        if ($model->load(Yii::$app->request->post()) ) {

            $model->file = UploadedFile::getInstance($model, 'file');

            if ( $model->file )
                {
                    $time = time();
                    //$model->file->saveAs('csv/' .$time. '.' . $model->file->extension);
                    $model->file->saveAs(Yii::getAlias('@webroot') .'/upload/excelfiles/' . $time . '.' . $model->file->extension);
                    //$model->file = 'csv/' .$time. '.' . $model->file->extension;
                    $model->file = Yii::getAlias('@webroot') .'/upload/excelfiles/' . $time . '.' . $model->file->extension;

                     $handle = fopen($model->file, "r");
                     $status = '';
                     while (($fileop = fgetcsv($handle, 1000, ",")) !== false) 
                     {
                         //echo trim($fileop[10]);
                         //die;
                         $status = trim($fileop[10]);
                         if($status == 'Processed' || $status == 'Success') {
                             $status = 'paid';
                         }else {
                             $arr = explode(" ", $fileop[10]);
                             $status = $arr[0];
                             
                         }
                         //echo $fileop[10].'<br>'.$status.'<br>';
                        $AccountNo = $fileop[3];
                        echo '<p>'.$AccountNo .' '.$fileop[9].' '.$status.'</p>';
                        //$age = $fileop[1];
                        //$location = $fileop[2];
                        //print_r($fileop);exit();
                        //$sql = "INSERT INTO details(name, age, location) VALUES ('$name', '$age', '$location')";
                        //$query = Yii::$app->db->createCommand($sql)->execute();
                     }
                     die;

                }

            //$model->save();
            return $this->redirect(['index']);
        } else {
            return $this->render('import_csv', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionImportExcelBank() {

        $model = new \common\models\ExcelForm();
        $msg = '';
        if ($model->load(Yii::$app->request->post())) {

            $model->excel = UploadedFile::getInstance($model, 'excel');

            if ($model->excel && $model->validate()) {
                $newName = Yii::$app->security->generateRandomString();
                $model->excel->saveAs(Yii::getAlias('@webroot') .'/upload/excelfiles/' . $newName . '.' . $model->excel->extension);
                $inputFile = Yii::getAlias('@webroot') .'/upload/excelfiles/' . $newName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }
                
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                $pre_defined = array( "id","redemption #", "status", "paid date", "intake month", "comment");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));
                

                //if($checkmatch >= 5){
                    $dataemailQueue = array();
                    for ($row = 2; $row <= $highestRow; $row++) {
                        //  Read a row of data into an array
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);
                        
                        $no = $rowData[0]['no.'];
                        $redemptionID = $rowData[0]['bene account no'];
                        echo $no.'-'.$redemptionID;
                        echo '<br>';
                        /*$redemption_no = $rowData[0]['redemption #'];
                        $status = $rowData[0]['status'];
                        $paid_date = date('Y-m-d', \PHPExcel_Shared_Date::ExcelToPHP($rowData[0]['paid date']));
                        $comment = $rowData[0]['comment'];
                        $intake_month = date('M-y', \PHPExcel_Shared_Date::ExcelToPHP($rowData[0]['intake month']));*/

                    }
                    unlink($inputFile);
                    exit();
                /*}else {
                    \Yii::$app->getSession()->setFlash('danger',['title' => 'Import Redemptions Data in Excel', 'text' => 'Sorry your excel template not match with system template!']);
                    return $this->redirect(['/management/bar-code']);
                }
                return $this->redirect(['index']);*/
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('import_excel', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Displays a single BCRedemption model.
     * @param int $redemptionID Redemption ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($redemptionID)
    {
        return $this->render('view', [
            'model' => $this->findModel($redemptionID),
        ]);
    }

    /**
     * Creates a new BCRedemption model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new BCRedemption();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'redemptionID' => $model->redemptionID]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing BCRedemption model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $redemptionID Redemption ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($redemptionID)
    {
        $model = $this->findModel($redemptionID);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'redemptionID' => $model->redemptionID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BCRedemption model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $redemptionID Redemption ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($redemptionID)
    {
        $this->findModel($redemptionID)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BCRedemption model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $redemptionID Redemption ID
     * @return BCRedemption the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($redemptionID)
    {
        if (($model = BCRedemption::findOne(['redemptionID' => $redemptionID])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
