<?php

namespace app\modules\management\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use yii\data\ArrayDataProvider;
use yii\web\UploadedFile;

use common\models\Redemption;
use common\models\RedemptionSearch;
use common\models\RedemptionHistory;
use common\models\RedemptionRemark;
use common\models\PointOrder;


/**
 * RedemptionController implements the CRUD actions for Redemption model.
 */
class RedemptionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Redemption models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RedemptionSearch();
        //$searchModel->redemption_status = ['1','7','17'];
        $searchModel->redemption_status = '1';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $bulkredem = new \common\models\PointsRedemptionBulk();
        
        //if ($bulkredem->load(Yii::$app->request->post())) {
            
        //}else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'bulkredem' => $bulkredem
            ]);
        //}
    }
    
    public function actionApproved()
    {
        Yii::$app->cache->flush();
        $searchModel = new RedemptionSearch();
        //$searchModel->redemption_status = ['1','7','17'];
        $searchModel->redemption_status = '17';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $bulkredem = new \common\models\PointsRedemptionBulk();
        
        //if ($bulkredem->load(Yii::$app->request->post())) {
            
        ///}else {
            return $this->render('index_approved', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'bulkredem' => $bulkredem
            ]);
        //}
    }
    
    public function actionCanceled()
    {
        Yii::$app->cache->flush();
        $searchModel = new RedemptionSearch();
        //$searchModel->redemption_status = ['1','7','17'];
        $searchModel->redemption_status = '7';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $bulkredem = new \common\models\PointsRedemptionBulk();
        
        //if ($bulkredem->load(Yii::$app->request->post())) {
            
        //}else {
            return $this->render('index_canceled', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'bulkredem' => $bulkredem
            ]);
        //}
    }
    
    public function actionBulkPointsRedemption(){        
        $selection=(array)Yii::$app->request->post('selection');//typecasting
        $vstatus= Yii::$app->request->post('redemption_status');
        $vcomment= Yii::$app->request->post('comment');

        $redemption_status = $vstatus;
        $comment = $vcomment;
        $msg = 0;
        if(count($selection) > 0){
            foreach($selection as $valueid){
                $id = $valueid;
                $model = $this->findModel($id);
                $model->redemption_status = $redemption_status;
                $model->redemption_status_date = date('Y-m-d');
                if($redemption_status == '17'){
                    $model->redemption_status_ray = '2';
                }                    

                if($model->save()){
                    $redemptionhistory = new RedemptionHistory();
                    $redemptionhistory->redemptionID = $id;
                    $redemptionhistory->redemption_status = $redemption_status;
                    $redemptionhistory->comment = $comment;
                    $redemptionhistory->date_added = date('Y-m-d H:i:s');
                    $redemptionhistory->save(false);

                    $painterId = $model->painterID;
                    $user = \common\models\User::find()->where(['id' => $painterId])->one();
                    
                    
                    $painterinfo = \common\models\PainterProfile::find()
                    ->where(['user_id' => $user->id])
                    ->one();
                    
                    $data = \yii\helpers\Json::encode(array(
                        'membership_ID' => $painterinfo->card_id,
                        'redemption_ID' => $model->redemption_invoice_no,
                        'redemption_point' => $model->req_points,
                        'redemption_value' => $model->req_amount,
                        'total_approved_transactions' => $model->getTotalTransactions()
                    ));
                    
                    if (!empty($user->email) && $redemption_status == '17') {
                        $subject = '';
                        $emailQueue = new \common\models\EmailQueue();
                        $emailQueue->email_template_global_id = '4';
                        $emailQueue->user_id = $painterId;
                        $emailQueue->name = $painterinfo->full_name;
                        
                        if (!empty($user->email) && !empty($painterinfo->mobile)) {
                            $emailQueue->email = $user->email;
                            $emailQueue->mobile = $painterinfo->mobile;
                        }else if (!empty($user->email)) {
                            $emailQueue->email = $user->email;
                        }else if (!empty($painterinfo->mobile)) {
                            $emailQueue->mobile = $painterinfo->mobile;
                        }
                        
                        
                        if ($data != null) {
                            $emailQueue->data = $data;
                        }
                        $emailQueue->status = 'P';
                        $emailQueue->save(false);
                    }

                    

                    if(!empty($painterinfo->mobile) && $redemption_status == '17'){
                        
                        $emailQueue = new \common\models\EmailQueue();
                        $emailQueue->email_template_global_id = '4';
                        $emailQueue->user_id = $painterId;
                        $emailQueue->name = $painterinfo->full_name;
                        $emailQueue->mobile = $painterinfo->mobile;
                        $emailQueue->type = 'S';
                        if ($data != null) {
                            $emailQueue->data = $data;
                        }
                        $emailQueue->status = 'P';
                        $emailQueue->save(false);
                    }       
                }else{
                    print_r($model->getErrors());
                }
                $msg++;
            }
            if($msg > 0){
                \Yii::$app->getSession()->setFlash('success',['title' => 'Action', 'text' => 'Bulk Action has been updated!']);
                return $this->redirect(['index']);
            }
        }else{ // End Selection Count
            return $this->redirect(['index']);
        }
    }
    
    public function actionBulkPointsRedemption2(){
        
        $selection= \common\models\Redemption::find()->where(['redemption_status' => 17])->andWhere(['like', 'redemption_status_date', '2020-05-05'])->all();
        $redemption_status = 17;
        $notrn = 1;
        if(count($selection) > 0){
            foreach($selection as $valueid){
                $id = $valueid->redemptionID;
                $model = $this->findModel($id);

                
                $painterId = $model->painterID;
                $user = \common\models\User::find()->where(['id' => $painterId])->one();


                $painterinfo = \common\models\PainterProfile::find()
                ->where(['user_id' => $user->id])
                ->one();
                $notrn = $model->getTotalTransactions();
                if($notrn == 0){
                    $notrn = 1;
                }
                $data = \yii\helpers\Json::encode(array(
                    'membership_ID' => $painterinfo->card_id,
                    'redemption_ID' => $model->redemption_invoice_no,
                    'redemption_point' => $model->req_points,
                    'redemption_value' => $model->req_amount,
                    'total_approved_transactions' => $notrn
                ));

                if(!empty($painterinfo->mobile)){
                    $countduplicate = \common\models\EmailQueue::find()
                        ->where(['user_id' => $painterId, 'email_template_global_id' => '4'])
                        ->andWhere(['like', 'created_datetime', '2020-05-05'])    
                        ->count();
                    if($countduplicate == 0) {
                        $emailQueue = new \common\models\EmailQueue();
                        $emailQueue->email_template_global_id = '4';
                        $emailQueue->user_id = $painterId;
                        $emailQueue->name = $painterinfo->full_name;
                        $emailQueue->mobile = $painterinfo->mobile;
                        $emailQueue->type = 'S';
                        if ($data != null) {
                            $emailQueue->data = $data;
                        }
                        $emailQueue->status = 'P';
                        $emailQueue->save(false);
                        echo $painterinfo->mobile.'<br>';
                    }
                }else {
                    echo 'No Mobile<br>';
                }
            }

        }else{ // End Selection Count
            echo 'No data';
        }
    }
    
    public function actionPayment()
    {
        $searchModel = new RedemptionSearch();
        $searchModel->redemption_status_ray = ['2','8','10','19'];
        $dataProvider = $searchModel->searchray(Yii::$app->request->queryParams);

        return $this->render('payment', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionProcessing()
    {
        $selection=(array)Yii::$app->request->post('selection');//typecasting
        $comment=Yii::$app->request->post('comment');//typecasting
        //$redemption_status_ray=Yii::$app->request->post('redemption_status_ray');//typecasting
        $intake_month=Yii::$app->request->post('intake_month');//typecasting
        if(!empty($selection) && !empty($intake_month) && !empty($comment)){          
            $msg = 0;
            if(count($selection) > 0){
                $payoutprocess = new \common\models\PayOutSummary();
                $payoutprocess->date_created = date('Y-m-d', strtotime($intake_month));
                $payoutprocess->description = $comment;                
                if($payoutprocess->save()){
                    $bulkRedemption = \common\models\Redemption::updateAll(['pay_out_summary_id' => $payoutprocess->pay_out_summary_id], ['IN', 'redemptionID', $selection]);
                    \Yii::$app->getSession()->setFlash('success',['title' => 'Action', 'text' => 'Bulk Action has been updated!']);
                    return $this->redirect(['processing']);
                }
            }else{ // End Selection Count
                return $this->redirect(['processing']);
            }
        }else {
            $searchModel = new RedemptionSearch();
            $searchModel->redemption_status_ray = ['2'];
            //$searchModel->pay_out_summary_id = 0;
            $dataProvider = $searchModel->searchray(Yii::$app->request->queryParams);
            $dataProvider->query->andWhere(['pay_out_summary_id' => null]);

            return $this->render('processing', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                //'bulkredem' => $bulkredem,
            ]);
        }
    }
    
    public function actionImportExcelRedemption() {
        $model = new \common\models\ExcelForm();
        $msg = '';
        if ($model->load(Yii::$app->request->post())) {

            $model->excel = UploadedFile::getInstance($model, 'excel');

            if ($model->excel && $model->validate()) {
                $newName = Yii::$app->security->generateRandomString();
                $model->excel->saveAs(Yii::getAlias('@webroot') .'/upload/excelfiles/' . $newName . '.' . $model->excel->extension);
                $inputFile = Yii::getAlias('@webroot') .'/upload/excelfiles/' . $newName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }
                
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                $pre_defined = array( "id","redemption #", "status", "intake month", "comment");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));
                

                if($checkmatch == 5){
                    $dataredemptionhistory = array();
                    $dataemailQueue = array();
                    for ($row = 2; $row <= $highestRow; $row++) {
                        //  Read a row of data into an array
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);
                        
                        $redemptionID = $rowData[0]['id'];
                        $redemption_no = $rowData[0]['redemption #'];
                        $status = $rowData[0]['status'];
                        $comment = $rowData[0]['comment'];
                        $intake_month = date('M-y', \PHPExcel_Shared_Date::ExcelToPHP($rowData[0]['intake month']));

                        $numRows = Redemption::find()
                            ->where([
                                'redemptionID' => $redemptionID,
                                'redemption_status_ray' => '2',
                            ])
                            ->count();
                        
                        if($numRows > 0) {
                            if($status == 'p' || $status == 'P' || $status == 'paid' || $status == 'Paid'){
                                $redemptionno = Redemption::find()->where([
                                    'redemptionID' => $redemptionID,
                                ])
                                ->asArray()->one();
                                //echo $redemptionno['redemptionID'];die;
                                //$valueid = $redemptionno['redemptionID'];
                                $model = $this->findModel($redemptionID);
                                //echo $valueid.'-'.$redemption_status_ray.'-'.$comment.'<br>';
                                $model->redemption_status_ray = '19';
                                $model->redemption_remarks = $comment;
                                $model->redemption_status_ray_date = date('Y-m-d');

                                if($model->save()){
                                    /*$redemptionhistory = new RedemptionHistory();
                                    $redemptionhistory->redemptionID = $redemptionID;
                                    $redemptionhistory->redemption_status = '19';
                                    $redemptionhistory->comment = $comment;
                                    $redemptionhistory->date_added = date('Y-m-d H:i:s');
                                    $redemptionhistory->save(false);*/
                                    $dataredemptionhistory[] = [$redemptionID,'19',0,$comment,date('Y-m-d H:i:s')];

                                    $painterId = $model->painterID;
                                    $user = \common\models\User::find()->where(['id' => $painterId])->asArray()->one();
                                    $banks = \common\models\Banks::find()->where(['id' => $model->bank->bank_name])->asArray()->one();
                                    $data = \yii\helpers\Json::encode(array(
                                        'redemption_ID' => $model->redemption_invoice_no,
                                        'redemption_point' => $model->req_points,
                                        'redemption_value' => $model->req_amount,
                                        're_paid_date' => date('d-m-Y'),
                                        'bank' => $banks['bank_name'],
                                        'account_holder_name' => $model->bank->account_name,
                                        'bank_account_no' => $model->bank->account_number,
                                        'intake_month' => $intake_month,
                                    ));
                                    
                                    $painterinfo = \common\models\PainterProfile::find()
                                    ->where(['user_id' => $user['id']])
                                    ->one();
                                    
                                    $banking = \app\modules\painter\models\BankingInformation::find()
                                    ->where(['user_id' => $user['id']])
                                    ->one();
                                    
                                    $banking->account_no_verification = 'Y';
                                    $banking->save(false);
                                    
                                    /*$emailQueue = new \common\models\EmailQueue();
                                    $emailQueue->email_template_global_id = '5';
                                    $emailQueue->user_id = $painterId;
                                    $emailQueue->mobile = $painterinfo->mobile;;
                                    $emailQueue->name = $user['username'];
                                    $emailQueue->email = $user['email'];
                                    if ($data != null) {
                                        $emailQueue->data = $data;
                                    }
                                    $emailQueue->status = 'P';
                                    $emailQueue->save(false);*/
                                    
                                    $dataemailQueue[] = ['5',$painterId,$painterinfo->mobile,$user['username'],$user['email'],$data,'P','P',date('Y-m-d H:i:s'),date('Y-m-d H:i:s'),Yii::$app->user->id,Yii::$app->user->id];
                                    
                                    $msg .= '<li class="list-group-item"><span class="badge pull-right bg-green"><i class="fa fa-check" aria-hidden="true"></i></span>  Redemption# <b>('.$redemption_no.')</b> Sccessfuly Updated.</li>';
                                }else {
                                    $msg .= '<li class="list-group-item"><span class="badge pull-right bg-yellow"><i class="fa fa-exclamation" aria-hidden="true"></i></span>Redemption# <b>('.$redemption_no.')</b> Fail.</li>';
                                }
                            }else {
                                $msg .= '<li class="list-group-item"><span class="badge pull-right bg-yellow"><i class="fa fa-ban" aria-hidden="true"></i></span>  Redemption# <b>('.$redemption_no.')</b> status column not match </li>';
                            }
                        }else {
                           $msg .= '<li class="list-group-item"><span class="badge pull-right bg-red"><i class="fa fa-ban" aria-hidden="true"></i></span>  Redemption# <b>('.$redemption_no.')</b> not found or PAID!</li>'; 
                        }
                        //$msg++;
                    }
                    Yii::$app->db
                        ->createCommand()
                        ->batchInsert('redemption_history', ['redemptionID','redemption_status','notify','comment','date_added'],$dataredemptionhistory)
                        ->execute();
                    Yii::$app->db
                        ->createCommand()
                        ->batchInsert('email_queue', ['email_template_global_id','user_id','mobile','name','email','data','status','sms_status','created_datetime','updated_datetime','created_by','updated_by'],$dataemailQueue)
                        ->execute();
                    
                    //summary
                    
                    unlink($inputFile);
                    return $this->render('summary', [
                                'msg' => $msg,
                    ]);
                    exit();
                }else {
                    \Yii::$app->getSession()->setFlash('danger',['title' => 'Import Redemptions Data in Excel', 'text' => 'Sorry your excel template not match with system template!']);
                    return $this->redirect(['import-excel-redemption']);
                }
                return $this->redirect(['index']);
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('import_excel', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionImportExcelRedemptionwithoutnotification() {
        $model = new \common\models\ExcelForm();
        $msg = '';
        if ($model->load(Yii::$app->request->post())) {

            $model->excel = UploadedFile::getInstance($model, 'excel');

            if ($model->excel && $model->validate()) {
                $newName = Yii::$app->security->generateRandomString();
                $model->excel->saveAs(Yii::getAlias('@webroot') .'/upload/excelfiles/' . $newName . '.' . $model->excel->extension);
                $inputFile = Yii::getAlias('@webroot') .'/upload/excelfiles/' . $newName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }
                
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                $pre_defined = array( "redemption #", "status", "intake month", "comment", "paid date");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));


                if($checkmatch == 5){
                    for ($row = 2; $row <= $highestRow; $row++) {
                        //  Read a row of data into an array
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);

                        $redemption_no = $rowData[0]['redemption #'];
                        $status = $rowData[0]['status'];
                        $comment = $rowData[0]['comment'];
                        $intake_month = date('M-y', \PHPExcel_Shared_Date::ExcelToPHP($rowData[0]['intake month']));
                        $paid_date = date('Y-m-d', \PHPExcel_Shared_Date::ExcelToPHP($rowData[0]['paid date']));

                        $numRows = Redemption::find()
                            ->where([
                                'redemption_invoice_no' => $redemption_no,
                                'redemption_status_ray' => '2',
                            ])
                            ->count();
                        
                        if($numRows > 0) {
                            if($status == 'p' || $status == 'P' || $status == 'paid' || $status == 'Paid'){
                                $redemptionno = Redemption::find()->where([
                                    'redemption_invoice_no' => $redemption_no,
                                ])
                                ->asArray()->one();
                                //echo $redemptionno['redemptionID'];die;
                                $valueid = $redemptionno['redemptionID'];
                                $model = $this->findModel($valueid);
                                //echo $valueid.'-'.$redemption_status_ray.'-'.$comment.'<br>';
                                $model->redemption_status_ray = '19';
                                $model->redemption_remarks = $comment;
                                $model->redemption_status_ray_date = $paid_date;

                                if($model->save()){
                                    $redemptionhistory = new RedemptionHistory();
                                    $redemptionhistory->redemptionID = $valueid;
                                    $redemptionhistory->redemption_status = '19';
                                    $redemptionhistory->comment = $comment;
                                    $redemptionhistory->date_added = $paid_date.' '.'12:44:43';
                                    $redemptionhistory->save(false);
                                    $msg .= '<li class="list-group-item"><span class="badge pull-right bg-green"><i class="fa fa-check" aria-hidden="true"></i></span>  Redemption# <b>('.$redemption_no.')</b> Sccessfuly Updated.</li>';
                                }else {
                                    $msg .= '<li class="list-group-item"><span class="badge pull-right bg-yellow"><i class="fa fa-exclamation" aria-hidden="true"></i></span>Redemption# <b>('.$redemption_no.')</b> Fail.</li>';
                                }
                            }else {
                                $msg .= '<li class="list-group-item"><span class="badge pull-right bg-yellow"><i class="fa fa-ban" aria-hidden="true"></i></span>  Redemption# <b>('.$redemption_no.')</b> status column not match </li>';
                            }
                        }else {
                           $msg .= '<li class="list-group-item"><span class="badge pull-right bg-red"><i class="fa fa-ban" aria-hidden="true"></i></span>  Redemption# <b>('.$redemption_no.')</b> not found or PAID!</li>'; 
                        }
                        //$msg++;
                    }
                    //summary
                    
                    unlink($inputFile);
                    return $this->render('summary', [
                                'msg' => $msg,
                    ]);
                    exit();
                }else {
                    \Yii::$app->getSession()->setFlash('danger',['title' => 'Import Redemptions Data in Excel', 'text' => 'Sorry your excel template not match with system template!']);
                    return $this->redirect(['import-excel-redemption']);
                }
                return $this->redirect(['index']);
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('import_excel', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionImportExcelRedemption2() {
        $model = new \common\models\ExcelForm();
        $msg = '';
        if ($model->load(Yii::$app->request->post())) {

            $model->excel = UploadedFile::getInstance($model, 'excel');

            if ($model->excel && $model->validate()) {
                $newName = Yii::$app->security->generateRandomString();
                $model->excel->saveAs(Yii::getAlias('@webroot') .'/upload/excelfiles/' . $newName . '.' . $model->excel->extension);
                $inputFile = Yii::getAlias('@webroot') .'/upload/excelfiles/' . $newName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }
                
                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                

                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                $pre_defined = array( "redemption_ID", "membership_ID", "name", "redemption_point", "redemption_value", "email");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));

                if($checkmatch == 4){
                    for ($row = 2; $row <= $highestRow; $row++) {
                        //  Read a row of data into an array
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);

                        $re_ID = $rowData[0]['redemption_id'];
                        $membership_ID = $rowData[0]['membership_id'];
                        $name = $rowData[0]['name'];
                        
                        $re_point = $rowData[0]['redemption_point'];
                        $re_rm_value = $rowData[0]['redemption_value'];
                        $emailTo = $rowData[0]['email'];

                        $emailTemplate = \common\models\EmailTemplate::find()
                            ->where(['code' => 'SRRCM03'])
                            ->one();
                        $subject = '';
                        if ($subject) {
                            $emailSubject = $subject;
                        } else {
                            $emailSubject = $emailTemplate->subject;
                            $emailSubject = str_replace('{membership_ID}', $membership_ID, $emailSubject);
                        }

                        $emailBody = $emailTemplate->template;

                        $emailBody = str_replace('{name}', $name, $emailBody);
                        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);

                        $emailBody = str_replace('{redemption_ID}', $re_ID, $emailBody);
                        $emailBody = str_replace('{redemption_point}', $re_point, $emailBody);
                        $emailBody = str_replace('{redemption_value}', $re_rm_value, $emailBody);
                        
                        Yii::$app->mailer->compose()
                            ->setTo($emailTo)
                            ->setFrom([Yii::$app->params['supportEmail'] =>  'Dulux Painter\'s Club'])
                            ->setSubject($emailSubject)
                            ->setHtmlBody($emailBody)
                            ->send();

                    }
                    //summary
                    unlink($inputFile);
                    return $this->render('summary', [
                                'msg' => $msg,
                    ]);
                    exit();
                }else {
                    \Yii::$app->getSession()->setFlash('danger',['title' => 'Import Redemptions Data in Excel', 'text' => 'Sorry your excel template not match with system template 123!']);
                    return $this->redirect(['import-excel-redemption2']);
                }
                return $this->redirect(['index']);
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('import_excel1', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionDenied()
    {
        $searchModel = new RedemptionSearch();
        $searchModel->redemption_status_ray = ['8'];
        $dataProvider = $searchModel->searchray(Yii::$app->request->queryParams);

        return $this->render('denied', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionFailed()
    {
        $searchModel = new RedemptionSearch();
        $searchModel->redemption_status_ray = ['10'];
        $dataProvider = $searchModel->searchray(Yii::$app->request->queryParams);

        return $this->render('failed', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionPaid()
    {
        $searchModel = new RedemptionSearch();
        $searchModel->redemption_status_ray = ['19'];
        $dataProvider = $searchModel->searchray(Yii::$app->request->queryParams);

        return $this->render('paid', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Redemption model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }
    
    public function actionInvoiceapprove($id)
    {
        $model = $this->findModel($id);

        $newcomment = new RedemptionRemark();
        if ($model->load(Yii::$app->request->post())) {
            $newcomment->load(Yii::$app->request->post());
            $model->redemption_remarks = $newcomment->comment;
            if($model->redemption_status == '17'){
                $model->redemption_status_ray = '2';
                $model->internel_status = 'processing';
            }
            
            if($model->redemption_status == '7'){
                $model->redemption_status_ray = '1';
            }

            $model->redemption_status_date = date('Y-m-d');
            if($model->save()){
                $redemptionhistory = new RedemptionHistory();
                $redemptionhistory->redemptionID = $id;
                $redemptionhistory->redemption_status = $model->redemption_status;
                $redemptionhistory->comment = $newcomment->comment;
                $redemptionhistory->date_added = date('Y-m-d H:i:s');
                $redemptionhistory->save(false);
                
                $painterId = $model->painterID;
                $user = \common\models\User::find()->where(['id' => $painterId])->one();
                $data = \yii\helpers\Json::encode(array(
                    'redemption_ID' => $model->redemption_invoice_no,
                    'redemption_point' => $model->req_points,
                    'redemption_value' => $model->req_amount,
                    'total_approved_transactions' => $model->getTotalTransactions()
                ));
                if (!empty($user->email) && $model->redemption_status == '17') {
                    $subject = '';
                    Yii::$app->ici->sendEmailpainterSetting($user->id, $user->email, Yii::$app->params['email.template.code.successful.redemption.approve'], $data, $subject);
                }
                
                $painterinfo = \common\models\PainterProfile::find()
                ->where(['user_id' => $user->id])
                ->one();
                

                    if(!empty($painterinfo->mobile) && $model->redemption_status == '17'){
                        $mobile = $painterinfo->mobile;
                        $result = Yii::$app->ici->sendSMSpaintersetting($user->id, $mobile, Yii::$app->params['email.template.code.successful.redemption.approve'], $data);
                    }

                \Yii::$app->getSession()->setFlash('success',['title' => 'Points Redemption Management', 'text' => 'Action has been updated!']);
                return $this->redirect(['index']); 
            }else{
                print_r($model->getErrors());
            }
            return $this->redirect(['view', 'id' => $model->redemptionID]);
        } else {
            return $this->render('invoiceapprove', [
                'model' => $model,
                'newcomment' => $newcomment,
            ]);
        }
    }
    
    public function actionMakepayment($id)
    {
        $model = $this->findModel($id);
        $newcomment = new RedemptionRemark();
        if ($model->load(Yii::$app->request->post())) {
            $newcomment->load(Yii::$app->request->post());
            $model->redemption_remarks = $newcomment->comment;
            $model->redemption_status_ray_date = date('Y-m-d');
            if($model->save()){
                $redemptionhistory = new RedemptionHistory();
                $redemptionhistory->redemptionID = $id;
                $redemptionhistory->redemption_status = $model->redemption_status;
                $redemptionhistory->comment = $newcomment->comment;
                $redemptionhistory->date_added = date('Y-m-d H:i:s');
                $redemptionhistory->save(false);
                
                $painterId = $model->painterID;
                $user = \common\models\User::find()->where(['id' => $painterId])->one();
                $banks = \common\models\Banks::find()->where(['id' => $model->bank->bank_name])->one();
                $data = \yii\helpers\Json::encode(array(
                    'redemption_ID' => $model->redemption_invoice_no,
                    'redemption_point' => $model->req_points,
                    'redemption_value' => $model->req_amount,
                    're_paid_date' => date('d-m-Y'),
                    'bank' => $banks->bank_name,
                    'account_holder_name' => $model->bank->account_name,
                    'bank_account_no' => $model->bank->account_number,
                    'intake_month' => $newcomment->intake_month,
                ));
                if (!empty($user->email) && $model->redemption_status_ray == '19') {
                    $subject = '';                    
                    Yii::$app->ici->sendEmailpainterSetting($user->id, $user->email, Yii::$app->params['email.template.code.successful.redemption.payout'], $data, $subject);
                }
                
                $painterinfo = \common\models\PainterProfile::find()
                ->where(['user_id' => $user->id])
                ->one();
                
                    if(!empty($painterinfo->mobile) && $model->redemption_status_ray == '19'){
                        $mobile = $painterinfo->mobile;
                        $result = Yii::$app->ici->sendSMSpaintersetting($user->id, $mobile, Yii::$app->params['email.template.code.successful.redemption.payout'], $data);
                    }
                \Yii::$app->getSession()->setFlash('success',['title' => 'Action', 'text' => 'Action has been updated!']);
                return $this->redirect(['processing']);
                //return $this->redirect(Yii::$app->request->referrer);
            }else{
                print_r($model->getErrors());
            }
            return $this->redirect(['view', 'id' => $model->redemptionID]);
        } else {
            return $this->render('invoicepay', [
                'model' => $model,
                'newcomment' => $newcomment,
            ]);
        }
    }
    

        public function actionMakepaymentbulk(){
        $bulkredem = new \common\models\RedemptionBulk();
        $action=Yii::$app->request->post('action');
        $selection=(array)Yii::$app->request->post('selection');//typecasting
        $redemption_status_ray = $bulkredem->redemption_status_ray;
        $comment = Yii::$app->request->post('RedemptionBulk[comment]');
        echo $redemption_status_ray.$comment;
        //
        //print_r($selection);
        if(count($selection) > 0){
            foreach($selection as $valueid){
                echo $valueid.$redemption_status_ray.$comment.'<br>';
            }
        }
        /*$painterid = Yii::$app->request->post('painterid');
        $selection_orderid = implode(",", $selection);*/
        die();
        
    }
    
    public function actionOnBehalf()
    {
        $model = new \common\models\Redemption();
        //'=','painter_login_id', Yii::$app->user->id
        if ($model->load(Yii::$app->request->post())) {
            $model->painterID = Yii::$app->user->id;            
            $model->redemption_status = '1';
            $model->redemption_status_ray = '1';
            $chkinvoiceid = \common\models\Redemption::find()->count();
            if($chkinvoiceid > 0){
                $newinvoiceid = \common\models\Redemption::find()->orderBy(['redemption_invoice_no' => SORT_DESC,])->one();
                $incinvoice = str_replace(Yii::$app->params['invoice.prefix'],"",$newinvoiceid->redemption_invoice_no);
                $incinvoice = $incinvoice + 1;
            }else{
                $incinvoice = Yii::$app->params['invoice.prefix.dft'];
            }
            $model->redemption_invoice_no = Yii::$app->params['invoice.prefix'].$incinvoice;
            $model->redemption_IP = $model->getRealIp();
            if($model->save()) {
                $orderids = explode(',', $model->orderID);
                foreach($orderids as $value){
                    $total_per_point = PointOrderItem::find()
                            ->where("painter_login_id = " . Yii::$app->user->id . " AND point_order_id = " . $value . " AND Item_status = 'G'")
                            ->sum('total_qty_point');

                    $total_per_price = PointOrderItem::find()
                            ->where("painter_login_id = " . Yii::$app->user->id . " AND point_order_id = " . $value . " AND Item_status = 'G'")
                            ->sum('total_qty_value');
                    $redemptionitem = new RedemptionItems();
                    $redemptionitem->redemptionID = $model->redemptionID;
                    $redemptionitem->order_id = $value;
                    $redemptionitem->req_per_points = $total_per_point;
                    $redemptionitem->req_per_amount = $total_per_price;
                    $redemptionitem->save(false); 
                    unset($redemptionitem);
                }
                \Yii::$app->getSession()->setFlash('success',['title' => 'Redemption Request', 'text' => 'Action successful!']);
                return $this->redirect(['redemption']);
            }else{
                print_r($model->getErrors());
            }
        }else{            
            /*$myorders = (new \yii\db\Query())->from('point_order po')
            ->join('JOIN', 'redemption_items ri', 'po.order_id = ri.order_id')
            ->where(['po.painter_login_id' => Yii::$app->user->getId(), 'po.order_status' => '17'])
            ->all();*/

            $myorders = PointOrder::find()
                //->where("painter_login_id = " . Yii::$app->user->id. " AND order_status = 17 ")
                ->where("order_status = 17 AND redemption = 'N'")
                ->orderBy('order_status ASC')    
                ->all(); 
            return $this->render('onbehalf', [
                'model' => $model,
                'myorders' => $myorders,
            ]);
        }
        /*return $this->render('onbehalf', [
                //'model' => $model,
            ]);*/
    }

    /**
     * Creates a new Redemption model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Redemption();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->redemptionID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Redemption model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->redemptionID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionViewpdf($id)
    {
        //$this->layout = 'printlayout';
        //$stu_id = Yii::$app->miedz->url_decode($sid);
        //$model = $this->findModel($stu_id);
        $model = $this->findModel($id);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('_view_pdf', [
            'model' => $model,]),
            'cssFile' => '@frontend/web/css/print.css',
            'cssInline' => '.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
    border: 1px solid #ddd;
}', 
            'options' => [
                'title' => 'Akzo Nobel',
                'subject' => 'APPLICATION FORM'
            ],
            'methods' => [
                'SetHeader' => ['Generated By: Akzo Nobel||Generated On: ' . date("d-m-Y")],
                'SetFooter' => ['|Page {PAGENO}|'],
            ]
        ]);
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        return $pdf->render();
    }
    

    public function actionPayOutSummary()
    {
        $model = new \common\models\Payout_Form();
        if ($model->load(Yii::$app->request->post())) {
            $request_date = date('Y-m-d', strtotime($model->date_month));
            $count = \common\models\Redemption::find()
                    ->select('redemption_created_datetime')      
                    ->where(['redemption_created_datetime' => $request_date])           
                    ->count();
            echo $count;
        }else {
            return $this->render('pay_out_summary', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Deletes an existing Redemption model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Redemption model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Redemption the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Redemption::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}