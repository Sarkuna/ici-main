<?php

namespace app\modules\management\controllers;

use common\models\Tng;
use common\models\TngSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TngController implements the CRUD actions for Tng model.
 */
class TngController extends Controller
{
    /**
     * @inheritDoc
     */

    /**
     * Lists all Tng models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new TngSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tng model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model->status = 'Yes';
        $model->save();
        $phone = $model->staff_mobile; 
        $message = "Dear $model->staff_name from $model->company_name, on behalf of AkzoNobel, here's your Dulux In-Store Incentive Rewards for May-Jun '23 Campaign.

TnG E-Wallet Reload Pin: $model->tpin

Total: RM$model->total_rewards. Expiry Date: 26-09-2024. 

Kindly acknowledge the receipt of rewards by replying *RECEIVED* to this message.

Any queries please contact 0374909139 or reply message to this number. Thank You.";
        
        $url='https://api.whatsapp.com/send/?phone='.$phone.'&text='.urlencode($message);
        //echo $url;
        return $this->redirect($url);
    }

    /**
     * Creates a new Tng model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Tng();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tng model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tng model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tng model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Tng the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tng::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
