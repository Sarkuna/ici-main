<?php

namespace app\modules\management\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;


class ImportController extends \yii\web\Controller
{
    public function actionIndex()
    {
        echo 'test';
    }
    
    public function actionBulkAccountVerified()
    {
        $model = new \common\models\ImportExcelForm();
        $msg = '';


        if ($model->load(Yii::$app->request->post())) {
            $model->excel = UploadedFile::getInstance($model, 'excel');
            if ($model->excel && $model->validate()) {
                $rpath = realpath(Yii::$app->basePath);
                $model->excel->saveAs($rpath.'/web/upload/excelfiles/' . $model->excel->baseName . '.' . $model->excel->extension);

                $inputFile = $rpath.'/web/upload/excelfiles/' . $model->excel->baseName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                //$objPHPExcel = \PHPExcel_IOFactory::load('./test.xlsx');
                $sheetData = $objPHPExcel->getActiveSheet(0)->toArray(null, true, true, true);
                for ($row = 1; $row <= $highestRow; ++ $row) {
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    if ($row == 1) {
                        continue;
                    }
                    $ic_no = $rowData[0][3];
                    $accno = $rowData[0][8];
                    $stringic_no = htmlentities($ic_no, null, 'utf-8');
                    $icno = str_replace("&nbsp;", "", $stringic_no);

                    $panterprofile = \common\models\PainterProfile::find()->where(['ic_no' => $icno])->one();
                    $count = count($panterprofile);
                    if($count > 0){
                        $userid = $panterprofile['user_id'];
                        $bankverify = \app\modules\painter\models\BankingInformation::find()->where(['user_id' => $userid])->andWhere(['!=', 'account_no_verification', 'Y'])->count();
                        if($bankverify > 0){                            
                            $verify = $rowData[0][10];
                            if(!empty($verify)){
                                $bankingInformation = \app\modules\painter\models\BankingInformation::find()->where(['user_id' => $userid])->one();
                                $bankingInformation->account_no_verification = $verify;
                                if($bankingInformation->save()){
                                    $msg .= '<li class="list-group-item"><span class="badge pull-right bg-green"><i class="fa fa-check" aria-hidden="true"></i></span>  Bank Account No <b>('.$bankingInformation->account_number.')</b> Verified.</li>';
                                }else{
                                    $msg .= '<li class="list-group-item"><span class="badge pull-right bg-yellow"><i class="fa fa-exclamation" aria-hidden="true"></i></span>  Bank Account No <b>('.$bankingInformation->account_number.')</b> Not Verified.</li>';
                                }
                            }else{
                                $msg .= '<li class="list-group-item"><span class="badge pull-right bg-yellow"><i class="fa fa-exclamation" aria-hidden="true"></i></span><b>('.$rowData[0][9].')</b>. No input given [Account Verified] Column</li>';
                            }
                        }
                    }else{
                        $msg .= '<li class="list-group-item"><span class="badge pull-right bg-red"><i class="fa fa-ban" aria-hidden="true"></i></span>  Profile Not Found this NRIC/PP <b>('.$icno.')</b></li>';
                    }
                    
                }
                unlink($inputFile);
                return $this->render('summary', [
                    'msg' => $msg,
                ]);
            }else{
                print_r($model->getErrors());
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('uploadexcel', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionSummaryViewExcel()
    {
        $query = ReportPainterProfile::find();
        $query->joinWith(['user']);
        $data = $query->asArray()->all();
        $items = ArrayHelper::toArray($data,'');
        $bal = '0.00';
        $footer_totalitems = 0;
        $footer_totalrmawarded = 0;
        $footer_Balance = '0.00';
        $filename = Date('YmdGis').'_Summary_View.xls';
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache"); 
        header("Expires: 0");
        
        echo '<table border="1" width="100%">';
        echo '<thead><tr>
            <th>Full Name</th><th>NRIC/PP Number</th><th>Total # of Transactions</th><th>Total Items</th><th>Total Points Awarded</th><th>Total RM Awarded</th><th>Total # of Redemptions</th><th>Total Points Redeemed</th><th>Total RM paid</th><th>Balance Total Points</th><th>Balance RM to be paid</th>
        </tr></thead>';
        echo '<tbody>';
        foreach ($items as $item){
            $userID = $item['user_id'];
            $totalnotransactions = \common\models\PointOrder::find()->where(['painter_login_id' => $userID, 'order_status' => '17'])->count();
            $totalitems = \common\models\PointOrder::find()->where(['painter_login_id' => $userID, 'order_status' => '17'])->sum('order_qty');
            $totalpointsawarded = \common\models\PointOrder::find()->where(['painter_login_id' => $userID, 'order_status' => '17'])->sum('order_total_point');
            $totalrmawarded = \common\models\PointOrder::find()->where(['painter_login_id' => $userID, 'order_status' => '17'])->sum('order_total_amount');
            $total_no_of_redemptions = \common\models\Redemption::find()->where(['painterID' => $userID, 'redemption_status_ray' => '19'])->count();
            $total_points_redeemed = \common\models\Redemption::find()->where(['painterID' => $userID, 'redemption_status_ray' => '19'])->sum('req_points');
            $total_rm_paid = \common\models\Redemption::find()->where(['painterID' => $userID, 'redemption_status_ray' => '19'])->sum('req_amount');
            $vbalance_total_points = $totalpointsawarded - $total_points_redeemed;
            
            $bal = $totalrmawarded - $total_rm_paid;
            $ic = strval($item['ic_no']);
            //$ic = "$ic";
            //echo '<pre>';
            echo '<tr>
            <td>'.$item['full_name'].'</td>
            <td style="text-align: left;">'.$ic.'&nbsp;</td>
            <td>'.$totalnotransactions.'</td>
            <td>'.$totalitems.'</td>
            <td>'.$totalpointsawarded.'</td>
            <td>'.$totalrmawarded.'</td>
            <td>'.$total_no_of_redemptions.'</td>
            <td>'.$total_points_redeemed.'</td>
            <td>'.$total_rm_paid.'</td>
            <td>'.$vbalance_total_points.'</td>
            <td>'.$bal.'</td>
            </tr>';
            $footer_totalitems += $totalitems;
            $footer_totalrmawarded += $totalrmawarded;
            $footer_Balance += $bal;
            //die();  
        }
        echo '</tbody>';
        echo '<tbody><tr><td></td><td></td><td></td><td>'.$footer_totalitems.'</td><td></td><td>'.$footer_totalrmawarded.'</td><td></td><td></td><td></td><td></td><td>'.$footer_Balance.'</td></tr></tbody>';
        echo '</table>'; 
    }
    
    public function actionPainterAccountManagementExcel()
    {
        $query = PainterProfile::find()->where(['profile_status' => 'A']);
        $query->joinWith(['user']);
        $data = $query->asArray()->all();
        $items = ArrayHelper::toArray($data,'');
        
        $filename = Date('YmdGis').'_Painter_Account_Management_Summary_Report.xls';
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache"); 
        header("Expires: 0");

        echo '<table border="1" width="100%">';
        echo '<thead><tr>
                <th>Membership</th>
                <th>Full Name</th>
                <th>NRIC/Passport #</th>
                <th>Mobile No</th><th>Email ID</th>
                <th>Company Name</th>
                <th>No. of Employees</th>
                <th>No. of Painting Jobs</th>
                <th>Registered At Dealer Outlet</th>
                <th>Dealer Address</th>
                <th>Bank Name</th>
                <th>Name of Account Holder</th>
                <th>Account Number</th>
        </tr></thead>';
        echo '<tbody>';
        foreach ($items as $item){
            $userID = $item['user_id'];
            $company = \app\modules\painter\models\CompanyInformation::find()->where(['user_id' => $userID])->one();
            $dealerOutlet = \common\models\DealerList::find()->where(['id' => $company->dealer_outlet])->one();
            $bank = \app\modules\painter\models\BankingInformation::find()->where(['user_id' => $userID])->one();
            $banks = \common\models\Banks::find()->where(['id' => $bank->bank_name])->one();
            $bankname = $banks->bank_name;
            $ic = strval($item['ic_no']);
            //$ic = "$ic";
            //echo '<pre>';
            echo '<tr>
            <td>'.$item['card_id'].'&nbsp;</td>
            <td>'.$item['full_name'].'</td>    
            <td style="text-align: left;">'.$ic.'&nbsp;</td>
            <td>'.$item['mobile'].'&nbsp;</td>
            <td>'.$item['user']['email'].'&nbsp;</td>      
            <td>'.$company['company_name'].'</td>
            <td>'.$company->no_painters.'</td>
            <td>'.$company->painter_sites.'</td>
            <td>'.$dealerOutlet->customer_name.'</td>
            <td>'.$dealerOutlet->address.'</td>
            <td>'.$bankname.'</td>
            <td>'.$bank->account_name.'</td>
            <td>'.$bank->account_number.'</td>
            </tr>';
            
        }
        echo '</tbody>';
        //echo '<tbody><tr><td></td><td></td><td></td><td>'.$footer_totalitems.'</td><td></td><td>'.$footer_totalrmawarded.'</td><td></td><td></td><td></td><td></td><td>'.$footer_Balance.'</td></tr></tbody>';
        echo '</table>';         
    }
    
    public function actionTransactionSupportingDocumentExcel()
    {
        $query = \common\models\PointOrderItem::find();
        $query->joinWith(['order']);
        $data = $query->asArray()->all();
        $items = ArrayHelper::toArray($data,'');
        
        $filename = Date('YmdGis').'_Transaction_Supporting_Document_Report.xls';
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache"); 
        header("Expires: 0");
        
        echo '<table border="1" width="100%">';
        echo '<thead><tr>
                <th>Membership#</th>
                <th>Full Name</th>
                <th>NRIC / Passport #</th>
                <th>Transaction #</th>
                <th>Dealer Name</th>
                <th>Dealer Address</th>
                <th>Dealer Invoice #</th>
                <th>Dealer Invoice (RM)</th>
                <th>Product Name</th>
                <th>Product Description</th>
                <th>Qty</th>
                <th>Total Points</th>
                <th>Total RM award</th>
                <th>Transaction Date</th>
                <th>Redemption #</th>
        </tr></thead>';
        echo '<tbody>';
        foreach ($items as $item){
            $userID = $item['painter_login_id'];
            $dealerID = $item['order']['order_dealer_id'];
            $productID = $item['product_list_id'];
            $profile = \app\modules\painter\models\PainterProfile::find()->where(['user_id' => $userID,])->one();
            $dealerOutlet = \common\models\DealerList::find()->where(['id' => $dealerID])->one();
            $productListItem = \common\models\ProductList::find()->where(['product_list_id' => $productID])->one();
            $rd = \common\models\Redemption::find()->where(['LIKE', 'orderID', $item['point_order_id']])->all();
            foreach($rd as $r){
                $rid = '<td>'.$r->redemption_invoice_no.'</td>';
            }

            //$ic = "$ic";
            //echo '<pre>';
            echo '<tr>
            <td>'.$profile->card_id.'&nbsp;</td>
            <td>'.$profile->full_name.'</td>  
            <td style="text-align: left;">'.$profile->ic_no.'&nbsp;</td>
            <td>'.$item['order']['order_num'].'</td>
            <td>'.$dealerOutlet->customer_name.'</td>
            <td>'.$dealerOutlet->address.'</td>
            <td>'.$item['order']['dealer_invoice_no'].'</td>
            <td>'.$item['order']['dealer_invoice_price'].'</td>
            <td>'.$productListItem->product_name.'</td>
            <td>'.$productListItem->product_description.'</td>
            <td>'.$item['total_qty'].'</td>
            <td>'.$item['total_qty_point'].'</td>
            <td>'.$item['total_qty_value'].'</td>
            <td>'.date('d-M-y H:i:s', strtotime($item['order']['created_datetime'])).'</td>
            '.$rid.' 
            </tr>';
            //$footer_totalitems += $totalitems;
            //$footer_totalrmawarded += $totalrmawarded;
            //$footer_Balance += $bal;
            //die();  
        }
        echo '</tbody>';
        echo '</table>';
        //$filename = Date('YmdGis').'_Painter_Account_Management_Summary_Report.xls';
        
    }
    
    public function actionRedemptionSupportingDocumentExcel()
    {
        //'2','19'
        $query = \common\models\Redemption::find()->where(['or','redemption_status_ray=2','redemption_status_ray=19']);
        $query->joinWith(['profile','bank']);
        $data = $query->asArray()->all();
        $items = ArrayHelper::toArray($data,'');
        
        $filename = Date('YmdGis').'_Redemption_Supporting_Document_Report.xls';
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache"); 
        header("Expires: 0");
        
        echo '<table border="1" width="100%">';
        echo '<thead><tr>
                <th>Membership #</th>
                <th>Full Name</th>
                <th>NRIC/Passport #</th>
                <th>Bank Acc #</th>
                <th>Bank Acc Holder Name</th>
                <th>Bank Name</th>
                <th>Request Date</th>
                <th>Redemption #</th>
                <th>Points Redeemed</th>
                <th>RM Redeemed</th>
                <th>Paid Date</th>
                <th>RM Paid</th>
        </tr></thead>';
        echo '<tbody>';
        foreach ($items as $item){
            $banks = \common\models\Banks::find()->where(['id' => $item['bank']['bank_name']])->one();
            $bankname = $banks->bank_name;
            $paiddate = $item['redemption_status_ray_date'];
            if (empty($paiddate)) {
                $raydate = "N/A";
            } else {
                $raydate = date('d-M-y', strtotime($paiddate));
            }
            if ($item['redemption_status_ray'] == '2') {
                $raypaid = '0.00';
            } else {
                $raypaid = $item['req_amount'];
            }
            echo '<tr>
            <td>'.$item['profile']['card_id'].'&nbsp;</td>
            <td>'.$item['profile']['full_name'].'</td>  
            <td style="text-align: left;">'.$item['profile']['ic_no'].'&nbsp;</td>
            <td>'.$item['bank']['account_number'].'&nbsp;</td>
            <td>'.$item['bank']['account_name'].'</td>
            <td>'.$bankname.'</td>
            <td>'.date('d-M-y H:i:s', strtotime($item['redemption_created_datetime'])).'</td>
            <td>'.$item['redemption_invoice_no'].'</td>
            <td>'.$item['req_points'].'</td>
            <td>'.$item['req_amount'].'</td>
            <td>'.$raydate.'</td>
            <td>'.$raypaid.'</td>
            </tr>';
        }
        echo '</tbody>';
        echo '</table>';
    }
    
    public function actionRedemptionSummaryExcel()
    {
        $query = \common\models\Redemption::find()->where(['redemption_status_ray' => '2']);
        $query->joinWith(['profile','bank']);
        $data = $query->asArray()->all();
        $items = ArrayHelper::toArray($data,'');

        $filename = Date('YmdGis').'_Redemption_Summary_Report.xls';
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache"); 
        header("Expires: 0");
        
        echo '<table border="1" width="100%">';
        echo '<thead><tr>
                <th>Redemption #</th>
                <th>Membership #</th>
                <th>Painter Name</th>
                <th>NRIC/Passport #</th>
                <th>Total Transactions</th>
                <th>Total Points</th>
                <th>Total RM Amount</th>
                <th>Bank Name</th>
                <th>Bank Acc Holder Name</th>
                <th>Bank Acc #</th>
                <th>Account Verified</th>                
                <th>Email</th>                
                <th>Mobile</th>                
                <th>Status</th>
                <th>Request Date</th>
        </tr></thead>';
        echo '<tbody>';
        foreach ($items as $item){
            $total_tr = \common\models\RedemptionItems::find()->where(['redemptionID' => $item['redemptionID']])->count();
            $banks = \common\models\Banks::find()->where(['id' => $item['bank']['bank_name']])->one();
            $bankname = $banks->bank_name;
            $paiddate = $item['redemption_status_ray_date'];
            if (empty($paiddate)) {
                $raydate = "N/A";
            } else {
                $raydate = date('d-M-y', strtotime($paiddate));
            }
            if ($item['redemption_status_ray'] == '2') {
                $raypaid = '0.00';
            } else {
                $raypaid = $item['req_amount'];
            }
            
            $verify = $item['bank']['account_no_verification'];
            if($verify == 'Y'){
                $veryfied = 'Y';
            }else{
                $veryfied = '';
            }
            
            echo '<tr>
            <td>'.$item['redemption_invoice_no'].'</td>    
            <td>'.$item['profile']['card_id'].'&nbsp;</td>
            <td>'.$item['profile']['full_name'].'</td>  
            <td style="text-align: left;">'.$item['profile']['ic_no'].'&nbsp;</td>
            <td>'.$total_tr.'&nbsp;</td>  
            <td>'.$item['req_points'].'</td>
            <td>'.$item['req_amount'].'</td>    
            <td>'.$bankname.'</td>
            <td>'.$item['bank']['account_name'].'</td>
            <td>'.$item['bank']['account_number'].'</td>      
            <td>'.$veryfied.'</td>
            <td>'.$item['profile']['email'].'</td>
            <td>'.$item['profile']['mobile'].'</td>
            <td>Processing</td>    
            <td>'.date('d-M-y H:i:s', strtotime($item['redemption_created_datetime'])).'</td>            
            </tr>';
        }
        echo '</tbody>';
        echo '</table>';
        
    }
    

}
