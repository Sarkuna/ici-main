<?php

namespace app\modules\management\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

use common\models\ReportPainterProfile;
use common\models\ReportPainterProfileSearch;
use common\models\PointOrderSearch;
use common\models\PointOrderItemSearch;
use common\models\RedemptionSearch;
use common\models\PainterProfile;
use common\models\PainterProfileSearch;

use app\components\ExportBehavior;
use app\components\ExportExcelUtil;

class ReportController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'export2excel' => [
                'class' => ExportBehavior::className(),
            ]
        ];
    }
    public function actionIndex()
    {
        set_time_limit(0);
        $searchModel = new ReportPainterProfileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=50;
        return $this->render('index_2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionTransaction()
    {
        $searchModel = new PointOrderSearch();
        $searchModel->order_status = '17';
        $dataProvider = $searchModel->searchtra(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=50;
        return $this->render('transaction', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionTransactionitem()
    {

        $searchModel = new PointOrderItemSearch();
        //$searchModel->order_status = '17';
        $dataProvider = $searchModel->search2(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize=20;
        return $this->render('transaction_item_1', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionRedemptionsd()
    {
        $searchModel = new RedemptionSearch();
        $searchModel->redemption_status_ray = ['2','19'];
        
        $dataProvider = $searchModel->searchray(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['Like','redemption_created_datetime', '2021']);
        $dataProvider->pagination->pageSize=20;
        
        return $this->render('payment_report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionPainterprofilesummery()
    {
        set_time_limit(0);
        //set_time_limit(500); // 
        $searchModel = new PainterProfileSearch();
        $searchModel->profile_status = ['A'];
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=100;
        return $this->render('painter_summary_report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionPainterprofilelogin()
    {
        $searchModel = new PainterProfileSearch();
        //$searchModel->user_type = ['P'];
        
        $dataProvider = $searchModel->searchemails(Yii::$app->request->queryParams);
        
        $dataProvider->pagination->pageSize=50;
        return $this->render('painter_summary_report_log', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionRedemptionsummery()
    {
        $searchModel = new RedemptionSearch();
        $searchModel->redemption_status_ray = ['2'];
        //$dataProvider->pagination->pageSize=100;
        $dataProvider = $searchModel->searchray(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=50;
        //$dataProvider->pagination  = false;

        return $this->render('redemption_summary_report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionBulkpayment()
    {
        $model = new \common\models\Report_Form_Bank();
        if ($model->load(Yii::$app->request->post())) {
            $start_time = date('Y-m-d', strtotime($model->reportdate));
            $end_time = date('Y-m-t',strtotime($start_time));
            $status = array();
            if($model->status == 'Y'){
                $status = 'Y';
            }else if($model->status == 'N'){
                $status = 'N';
            }else {
                $status = array('Y','N');
            }
            

            $bankdatas = \common\models\Redemption::find()
                ->select('redemption.redemptionID, redemption.painterID, redemption.req_amount, redemption.redemption_status')      
                ->joinWith([
                    'profile' => function($que) {
                        $que->select(['user_id','ic_no']);
                    },
                    'bank' => function($que) {
                        $que->select(['user_id','bank_name', 'account_name','account_number', 'account_no_verification']);
                    },        
                ])->where(['redemption_status_ray' => '2'])
                ->andWhere(['between','redemption_status_date',$start_time, $end_time ])
                ->andwhere(['in', 'account_no_verification', $status])            
                //->andWhere(['sign_off' => 'O'])
                //->andWhere(['>=','vms_visits.updated_datetime',date($date_from)])
                //->andWhere(['<=','vms_visits.updated_datetime',date($date_to)])
                //->orderBy(['vms_visits.updated_datetime' => SORT_DESC])

                ->asArray()
                ->all();
                    
            return $this->render('bulkpayment', [
                'bankdatas' => $bankdatas,
            ]);
        }else {
           return $this->render('_form_report_bank', [
                'model' => $model,
            ]); 
        }
        
        
    }
    
    public function actionSummaryViewExcel()
    {
        $query = ReportPainterProfile::find();
        $query->joinWith(['user']);
        $data = $query->asArray()->all();
        $items = ArrayHelper::toArray($data,'');
        $bal = '0.00';
        $footer_totalitems = 0;
        $footer_totalrmawarded = 0;
        $footer_Balance = '0.00';
        $filename = Date('YmdGis').'_Summary_View.xls';
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache"); 
        header("Expires: 0");
        
        echo '<table border="1" width="100%">';
        echo '<thead><tr>
            <th>Full Name</th><th>NRIC/PP Number</th><th>Total # of Transactions</th><th>Total Items</th><th>Total Points Awarded</th><th>Total RM Awarded</th><th>Total # of Redemptions</th><th>Total Points Redeemed</th><th>Total RM paid</th><th>Balance Total Points</th><th>Balance RM to be paid</th>
        </tr></thead>';
        echo '<tbody>';
        foreach ($items as $item){
            $userID = $item['user_id'];
            $totalnotransactions = \common\models\PointOrder::find()->where(['painter_login_id' => $userID, 'order_status' => '17'])->count();
            $totalitems = \common\models\PointOrder::find()->where(['painter_login_id' => $userID, 'order_status' => '17'])->sum('order_qty');
            $totalpointsawarded = \common\models\PointOrder::find()->where(['painter_login_id' => $userID, 'order_status' => '17'])->sum('order_total_point');
            $totalrmawarded = \common\models\PointOrder::find()->where(['painter_login_id' => $userID, 'order_status' => '17'])->sum('order_total_amount');
            $total_no_of_redemptions = \common\models\Redemption::find()->where(['painterID' => $userID, 'redemption_status_ray' => '19'])->count();
            $total_points_redeemed = \common\models\Redemption::find()->where(['painterID' => $userID, 'redemption_status_ray' => '19'])->sum('req_points');
            $total_rm_paid = \common\models\Redemption::find()->where(['painterID' => $userID, 'redemption_status_ray' => '19'])->sum('req_amount');
            $vbalance_total_points = $totalpointsawarded - $total_points_redeemed;
            
            $bal = $totalrmawarded - $total_rm_paid;
            $ic = strval($item['ic_no']);
            //$ic = "$ic";
            //echo '<pre>';
            echo '<tr>
            <td>'.$item['full_name'].'</td>
            <td style="text-align: left;">'.$ic.'&nbsp;</td>
            <td>'.$totalnotransactions.'</td>
            <td>'.$totalitems.'</td>
            <td>'.$totalpointsawarded.'</td>
            <td>'.$totalrmawarded.'</td>
            <td>'.$total_no_of_redemptions.'</td>
            <td>'.$total_points_redeemed.'</td>
            <td>'.$total_rm_paid.'</td>
            <td>'.$vbalance_total_points.'</td>
            <td>'.$bal.'</td>
            </tr>';
            $footer_totalitems += $totalitems;
            $footer_totalrmawarded += $totalrmawarded;
            $footer_Balance += $bal;
            //die();  
        }
        echo '</tbody>';
        echo '<tbody><tr><td></td><td></td><td></td><td>'.$footer_totalitems.'</td><td></td><td>'.$footer_totalrmawarded.'</td><td></td><td></td><td></td><td></td><td>'.$footer_Balance.'</td></tr></tbody>';
        echo '</table>'; 
    }
    
    public function actionPainterAccountManagementExcel()
    {
        //set_time_limit(0);
        $query = PainterProfile::find()->where(['profile_status' => 'A']);
        $query->joinWith(['user']);
        $data = $query->asArray()->all();
        $items = ArrayHelper::toArray($data,'');
        
        $filename = Date('YmdGis').'_Painter_Account_Management_Summary_Report.xls';
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache"); 
        header("Expires: 0");

        echo '<table border="1" width="100%">';
        echo '<thead><tr>
                <th>Membership</th>
                <th>Full Name</th>
                <th>Email ID</th>
                <th>Date of Birth</th>
                <th>Mobile No</th>
                <th>NRIC/Passport #</th>                
                <th>Company Name</th>
                <th>No. of Employees</th>
                <th>No. of Painting Jobs</th>
                <th>Registered At Dealer Outlet</th>
                <th>Dealer Address</th>
                <th>Bank Name</th>
                <th>Name of Account Holder</th>
                <th>Account Number</th>
                <th>Create Date</th>
        </tr></thead>';
        echo '<tbody>';
        $tr = '';
        foreach ($items as $item){
            
            $userID = $item['user_id'];
            //$profile = PainterProfile::find()->where(['user_id' => $userID])->one();
            $company = \app\modules\painter\models\CompanyInformation::find()->where(['user_id' => $userID])->one();
            $dealerOutlet = \common\models\DealerList::find()->where(['id' => $company->dealer_outlet])->one();
            $bank = \app\modules\painter\models\BankingInformation::find()->where(['user_id' => $userID])->one();
            $banks = \common\models\Banks::find()->where(['id' => $bank->bank_name])->one();
            $bankname = $banks->bank_name;
            $ic = strval($item['ic_no']);
            //$ic = "$ic";
            //echo '<pre>';

            $tr .= '<tr>
            <td>'.$item['card_id'].'&nbsp;</td>
            <td>'.$item['full_name'].'</td> 
            <td>'.$item['user']['email'].'</td> 
            <td>'.date('d-m-Y', strtotime($item['dob'])).'</td>
            <td>'.$item['mobile'].'&nbsp;</td>  
            <td style="text-align: left;">'.$ic.'&nbsp;</td>               
            <td>'.$company['company_name'].'</td>
            <td>'.$company->no_painters.'</td>
            <td>'.$company->painter_sites.'</td>
            <td>'.$dealerOutlet->customer_name.'</td>
            <td>'.$dealerOutlet->address.'</td>
            <td>'.$bankname.'</td>
            <td>'.$bank->account_name.'</td>
            <td>'.$bank->account_number.'</td>
            <td>'.date('d-m-Y',strtotime($item['created_datetime'])).'</td> 
            </tr>';
            
        }
        echo $tr;
        echo '</tbody>';
        //echo '<tbody><tr><td></td><td></td><td></td><td>'.$footer_totalitems.'</td><td></td><td>'.$footer_totalrmawarded.'</td><td></td><td></td><td></td><td></td><td>'.$footer_Balance.'</td></tr></tbody>';
        echo '</table>';
        die;
    }
    
    public function actionPainterAccountManagementExcelDealer()
    {
        //set_time_limit(0);
        $query = PainterProfile::find()->select(['painter_profile.user_id','card_id','full_name','painter_profile.created_datetime'])->where(['profile_status' => 'A']);
        $query->joinWith([
            /*'user' => function($que) {
                $que->select(['email', 'email_verification', 'status', 'type', 'created_datetime']);
            },*/
            'company' => function($que) {
                $que->select(['user_id', 'company_name', 'no_painters','painter_sites','dealer_outlet']);
            },        
        ]);
        $items = $query->asArray()->all();
        $dataVisits = array();
        
        foreach ($items as $key=>$item){
            $dealerOutlet = \common\models\DealerList::find()->where(['id' => $item['company']['dealer_outlet']])->one();
            $visit_temp = array(
                    'Membership' => $item['card_id'],
                    'Full Name' => $item['full_name'],
                    'Company Name' => $item['company']['company_name'],
                    'No. of Employees' => $item['company']['no_painters'],
                    'No. of Painting Jobs' => $item['company']['painter_sites'],
                    'Registered At Dealer Outlet' => $dealerOutlet->customer_name,
                    'Dealer Address' => $dealerOutlet->address,
                    'Create Date' => date('d-m-Y',strtotime($item['created_datetime'])),
                );
                $dataVisits[] = $visit_temp;
        }
        
        
        $excel_data = ExportExcelUtil::excelDataFormat($dataVisits); 
        $excel_title = $excel_data['excel_title'];
        $excel_ceils = $excel_data['excel_ceils'];
        $excel_content = array(
            //sheet 1
            array(
                'sheet_name' => 'Main Dealer Profile',
                'sheet_title' => $excel_title,
                'ceils' => $excel_ceils,
                'freezePane' => 'B2',
                'headerColor' => ExportExcelUtil::getCssClass("header"),
                'headerColumnCssClass' => array(
                    'id' => ExportExcelUtil::getCssClass('blue'),
                    'Status_Description' => ExportExcelUtil::getCssClass('grey'),
                ), //define each column's cssClass for header line only.  You can set as blank.
                'oddCssClass' => ExportExcelUtil::getCssClass("odd"),
                'evenCssClass' => ExportExcelUtil::getCssClass("even"),
                'hightLight' => ExportExcelUtil::getCssClass("highlight"),
            ),array(
                'sheet_name' => 'Sheet 2',
                'sheet_title' => array(),
                'ceils' => array()
            ),
            //sheet 2,....            
        );
        $excel_file = "Painter_Account_Management_Summary_Report_Dealer";
        $this->export2excel($excel_content,$excel_file);
    }
    
    public function actionRedemptionPayment($id)
    {
        $payout = \common\models\PayOutSummary::find()->where(['pay_out_summary_id' => $id])->one();
        //set_time_limit(0);
        $query = \common\models\Redemption::find()->joinWith([
            'profile' => function($query) {
                $query->select(['painter_profile.id','painter_profile.user_id','painter_profile.card_id','painter_profile.full_name','painter_profile.ic_no','painter_profile.mobile']);
                //$query->select(['profile.id','profile.username'])->where(['user.status' => 'A']);
            },            
            'bank',
            ])->where(['pay_out_summary_id' => $id]);
        $items = $query->asArray()->all();
        $dataVisits = array();
        $paiddate = '';
        foreach ($items as $key=>$item){
            if($item['bank']['account_no_verification'] == 'Y') {
                $verfication = 'Yes';
            }else {
                $verfication = 'No';
            }
            $banks = \common\models\Banks::find()->where(['id' => $item['bank']['bank_name']])->one();
            $bankname = $banks->bank_name;

            if(!empty($item['internel_status'])) {
                //if($item['bank']['account_no_verification'] == 'Y') {
                    $sttext = str_replace('_', ' ', $item['internel_status']);                                            
                    $instatus = ucwords($sttext);
                    if($item['internel_status'] == 'paid') {
                       $paiddate = date('d-m-Y', strtotime($item['paid_date'])); 
                    }else {
                        $paiddate = 'NA';
                    }
                /*}else {
                    $instatus = 'Not Verified';
                    $paiddate = 'NA';
                }*/
            }else {
                if($item['bank']['account_no_verification'] == 'Y') {
                    $instatus = 'Processing';                    
                }else {
                    $instatus = 'Not Verified';
                }
                $paiddate = 'NA';
            } 
            
            $visit_temp = array(
                    'ID' => $item['redemptionID'],
                    'Redemption #' => $item['redemption_invoice_no'],
                    'Membership #' => $item['profile']['card_id'],//$model->profile->card_id
                    'Full Name' => $item['bank']['account_name'],
                    'Bank Name' => $bankname,
                    'Bank Acc #' => $item['bank']['account_number'],
                    'Total' => $item['req_amount'],
                    'Mobile' => $item['profile']['mobile'],
                    'Account Verified' => $verfication,
                    'Status' => $instatus,
                    'Paid Date' => $paiddate,
                    'Request Date' => date('d-m-Y', strtotime($item['redemption_created_datetime'])),
                    //'Total Transactions' =>
                    //'Total Points' =>
                    //'Total RM Amount' =>

                    /*'Full Name' => $item['full_name'],
                    'Company Name' => $item['company']['company_name'],
                    'No. of Employees' => $item['company']['no_painters'],
                    'No. of Painting Jobs' => $item['company']['painter_sites'],
                    'Registered At Dealer Outlet' => $dealerOutlet->customer_name,
                    'Dealer Address' => $dealerOutlet->address,
                    'Create Date' => date('d-m-Y',strtotime($item['created_datetime'])),*/
                );
                $dataVisits[] = $visit_temp;
        }
        
        
        $excel_data = ExportExcelUtil::excelDataFormat($dataVisits); 
        $excel_title = $excel_data['excel_title'];
        $excel_ceils = $excel_data['excel_ceils'];
        $excel_content = array(
            //sheet 1
            array(
                'sheet_name' => date('M-Y', strtotime($payout->date_created)),
                'sheet_title' => $excel_title,
                'ceils' => $excel_ceils,
                'freezePane' => 'B2',
                'headerColor' => ExportExcelUtil::getCssClass("header"),
                'headerColumnCssClass' => array(
                    'id' => ExportExcelUtil::getCssClass('blue'),
                    'Status_Description' => ExportExcelUtil::getCssClass('grey'),
                ), //define each column's cssClass for header line only.  You can set as blank.
                'oddCssClass' => ExportExcelUtil::getCssClass("odd"),
                'evenCssClass' => ExportExcelUtil::getCssClass("even"),
                'hightLight' => ExportExcelUtil::getCssClass("highlight"),
            ),array(
                'sheet_name' => 'Sheet 2',
                'sheet_title' => array(),
                'ceils' => array()
            ),
            //sheet 2,....            
        );
        $excel_file = "Redemption-Payment-". date('M-Y', strtotime($payout->date_created));
        $this->export2excel($excel_content,$excel_file);
    }
    
    public function actionPainterAccountManagementExcelProfile()
    {
        //set_time_limit(0);
        $query = PainterProfile::find()->where(['profile_status' => 'A']);
        $query->joinWith(['user','profileRegion','profileState','bank']);
        $data = $query->asArray()->all();
        //echo '<pre>';print_r($data);die;
        $items = ArrayHelper::toArray($data,'');
        
        $filename = Date('YmdGis').'_Profile_Summary_Report.xls';
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache"); 
        header("Expires: 0");

        echo '<table border="1" width="100%">';
        echo '<thead><tr>
                <th>Membership</th>
                <th>Full Name</th>
                <th>Address</th>
                <th>Region</th>
                <th>State</th>
                <th>Email ID</th>
                <th>Date of Birth</th>
                <th>Mobile No</th>
                <th>NRIC/Passport #</th>                
                <th>Bank Name</th>
                <th>Name of Account Holder</th>
                <th>Account Number</th>
                <th>Create Date</th>
        </tr></thead>';
        echo '<tbody>';
        $tr = '';
        foreach ($items as $item){
            
            $userID = $item['user_id'];
            $banks = \common\models\Banks::find()->where(['id' => $item['bank']['bank_name']])->one();
            $bankname = $banks->bank_name;
            $ic = strval($item['ic_no']);

            $tr .= '<tr>
            <td>'.$item['card_id'].'&nbsp;</td>
            <td>'.$item['full_name'].'</td>
            <td>'.$item['address'].'</td>
            <td>'.$item['profileRegion']['region_name'].'</td> 
            <td>'.$item['profileState']['state_name'].'</td> 
            <td>'.$item['user']['email'].'</td> 
            <td>'.date('d-m-Y', strtotime($item['dob'])).'</td>
            <td>'.$item['mobile'].'&nbsp;</td>  
            <td style="text-align: left;">'.$ic.'&nbsp;</td>               
            <td>'.$bankname.'</td>
            <td>'.$item['bank']['account_name'].'</td>
            <td>'.$item['bank']['account_number'].'</td>
            <td>'.date('d-m-Y',strtotime($item['created_datetime'])).'</td>   
            </tr>';
            
        }
        echo $tr;
        echo '</tbody>';
        //echo '<tbody><tr><td></td><td></td><td></td><td>'.$footer_totalitems.'</td><td></td><td>'.$footer_totalrmawarded.'</td><td></td><td></td><td></td><td></td><td>'.$footer_Balance.'</td></tr></tbody>';
        echo '</table>';
        die;
    }
    
    
    public function actionPainterAccountManagementExcel2()
    {
        set_time_limit(0);
        $query = PainterProfile::find()->where(['profile_status' => 'A']);
        $query->joinWith(['user']);
        $data = $query->asArray()->all();
        $items = ArrayHelper::toArray($data,'');
        
        $filename = Date('YmdGis').'_Painter_Account_Management_Summary_Report.xls';
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache"); 
        header("Expires: 0");

        echo '<table border="1" width="100%">';
        echo '<thead><tr>
                <th>Membership</th>              
                <th>Address</th>
                <th>Country</th>
                <th>State</th>
                <th>Create Date</th>
        </tr></thead>';
        echo '<tbody>';
        $tr = '';
        foreach ($items as $item){
            $userID = $item['user_id'];
            $profile = PainterProfile::find()->where(['user_id' => $userID])->one();
            $tr .= '<tr>
            <td>'.$item['card_id'].'&nbsp;</td>           
            <td>'.$item['address'].'</td>
            <td>'.$profile->profileCountry->name.'</td>
            <td>'.$profile->profileState->state_name.'</td>
            <td>'.date('d-m-Y',strtotime($item['created_datetime'])).'</td>
            </tr>';
            
        }
        echo $tr;
        echo '</tbody>';
        //echo '<tbody><tr><td></td><td></td><td></td><td>'.$footer_totalitems.'</td><td></td><td>'.$footer_totalrmawarded.'</td><td></td><td></td><td></td><td></td><td>'.$footer_Balance.'</td></tr></tbody>';
        echo '</table>';         
    }
    
    
    public function actionTransactionSupportingDocumentExcel(){
        set_time_limit(0);
        $model = new \common\models\Report_Form();
        if ($model->load(Yii::$app->request->post())) {
            $mont = $model->reportdate;
            
            $start_time = date('Y-m-d', strtotime($model->reportdate));
            $end_time = date('Y-m-d', strtotime($model->reportdateto));
            $start_time = $start_time.' 00:00:00';
            $end_time = $end_time.' 23:59:59';
            $rpdate = date('d-m-Y', strtotime($model->reportdate)).'-'.date('d-m-Y', strtotime($model->reportdateto));
            
            $visits = \Yii::$app->getDb()->createCommand("SELECT po.order_num, po.dealer_invoice_no, po.dealer_invoice_price, po.dealer_invoice_date, po.created_datetime, po.updated_datetime, poi.total_qty, poi.total_qty_point, poi.total_qty_value, pl.product_name, pl.product_description, pp.card_id, pp.full_name, pp.ic_no, dl.customer_name, dl.address      
            FROM point_order AS po 
            INNER JOIN point_order_item AS poi        
            ON po.order_id=poi.point_order_id 
            INNER JOIN product_list AS pl        
            ON poi.product_list_id=pl.product_list_id
            INNER JOIN painter_profile AS pp        
            ON po.painter_login_id=pp.user_id
            INNER JOIN dealer_list AS dl        
            ON po.order_dealer_id=dl.id 
            WHERE (po.created_datetime BETWEEN '$start_time' AND '$end_time') AND (po.order_status = '17')")->queryAll();

            $dataVisits = array();
            if(!empty($visits)){
                $numRow = 0;
                foreach($visits as $key=>$item){
                    $visit_temp = array(
                        'no' => ++$numRow,
                        'Membership#' => $item['card_id'],
                        'Full Name' => $item['full_name'],
                        'NRIC / Passport #' => $item['ic_no'],
                        'Transaction #' => $item['order_num'],
                        'Dealer Name' => $item['customer_name'],
                        'Dealer Address' => $item['address'],
                        'Dealer Invoice #' => $item['dealer_invoice_no'],
                        'Dealer Invoice (RM)' => $item['dealer_invoice_price'],
                        'Dealer Invoice Date' => date('d-M-y', strtotime($item['dealer_invoice_date'])),
                        'Product Name' => $item['product_name'],
                        'Product Description' => $item['product_description'],
                        'Qty' => $item['total_qty'],
                        'Total Points' => $item['total_qty_point'],
                        'Total RM award' => $item['total_qty_value'],
                        'Transaction Date' => date('d-M-y H:i:s', strtotime($item['created_datetime'])),
                        'Last Modify Date' => date('d-M-y H:i:s', strtotime($item['updated_datetime'])),                  
                        //'Redemption #' => $rid
                    );
                    $dataVisits[] = $visit_temp;
                }
            }

            $excel_data = ExportExcelUtil::excelDataFormat($dataVisits); 
            $excel_title = $excel_data['excel_title'];
            $excel_ceils = $excel_data['excel_ceils'];
            $excel_content = array(
                //sheet 1
                array(
                    'sheet_name' => 'Report',
                    'sheet_title' => $excel_title,
                    'ceils' => $excel_ceils,
                    'freezePane' => 'B2',
                    'headerColor' => ExportExcelUtil::getCssClass("header"),
                    'headerColumnCssClass' => array(
                        'id' => ExportExcelUtil::getCssClass('blue'),
                        'Status_Description' => ExportExcelUtil::getCssClass('grey'),
                    ), //define each column's cssClass for header line only.  You can set as blank.
                    'oddCssClass' => ExportExcelUtil::getCssClass("odd"),
                    'evenCssClass' => ExportExcelUtil::getCssClass("even"),
                ),array(
                    'sheet_name' => 'Sheet 2',
                    'sheet_title' => array(),
                    'ceils' => array()
                ),
                //sheet 2,....            
            );
            $excel_file = "Transaction_Supporting_Document";
            $this->export2excel($excel_content,$excel_file);
        }else{
            return $this->render('_form_report', [
                'model' => $model,
            ]);
        }        
    }
    
    public function actionExportExcel(){
        set_time_limit(0);
        Yii::$app->cache->flush();
        $start_time = '2018-09-01 00:00:00';
        $end_time = '2018-09-30 23:59:59';
        
        $visits = \Yii::$app->getDb()->createCommand("SELECT po.order_num, po.dealer_invoice_no, po.dealer_invoice_price, po.created_datetime, po.updated_datetime, poi.total_qty, poi.total_qty_point, poi.total_qty_value, pl.product_name, pl.product_description, pp.card_id, pp.full_name, pp.ic_no, dl.customer_name, dl.address      
            FROM point_order AS po 
            INNER JOIN point_order_item AS poi        
            ON po.order_id=poi.point_order_id 
            INNER JOIN product_list AS pl        
            ON poi.product_list_id=pl.product_list_id
            INNER JOIN painter_profile AS pp        
            ON po.painter_login_id=pp.user_id
            INNER JOIN dealer_list AS dl        
            ON po.order_dealer_id=dl.id 
            WHERE (po.created_datetime BETWEEN '2019-09-01 00:00:00' AND '2019-09-30 23:59:59') AND (po.order_status = '17')")->queryAll();

        $dataVisits = array();
        if(!empty($visits)){
            $numRow = 0;
            foreach($visits as $key=>$item){
                $visit_temp = array(
                    'no' => ++$numRow,
                    'Membership#' => $item['card_id'],
                    'Full Name' => $item['full_name'],
                    'NRIC / Passport #' => $item['ic_no'],
                    'Transaction #' => $item['order_num'],
                    'Dealer Name' => $item['customer_name'],
                    'Dealer Address' => $item['address'],
                    'Dealer Invoice #' => $item['dealer_invoice_no'],
                    'Dealer Invoice (RM)' => $item['dealer_invoice_price'],
                    'Product Name' => $item['product_name'],
                    'Product Description' => $item['product_description'],
                    'Qty' => $item['total_qty'],
                    'Total Points' => $item['total_qty_point'],
                    'Total RM award' => $item['total_qty_value'],
                    'Transaction Date' => date('d-M-y H:i:s', strtotime($item['created_datetime'])),
                    'Last Modify Date' => date('d-M-y H:i:s', strtotime($item['updated_datetime'])),                  
                    //'Redemption #' => $rid
                );
                $dataVisits[] = $visit_temp;
            }
        }

        $excel_data = ExportExcelUtil::excelDataFormat($dataVisits); 
        $excel_title = $excel_data['excel_title'];
        $excel_ceils = $excel_data['excel_ceils'];
        $excel_content = array(
            //sheet 1
            array(
                'sheet_name' => 'Report',
                'sheet_title' => $excel_title,
                'ceils' => $excel_ceils,
                'freezePane' => 'B2',
                'headerColor' => ExportExcelUtil::getCssClass("header"),
                'headerColumnCssClass' => array(
                    'id' => ExportExcelUtil::getCssClass('blue'),
                    'Status_Description' => ExportExcelUtil::getCssClass('grey'),
                ), //define each column's cssClass for header line only.  You can set as blank.
                'oddCssClass' => ExportExcelUtil::getCssClass("odd"),
                'evenCssClass' => ExportExcelUtil::getCssClass("even"),
            ),array(
                'sheet_name' => 'Sheet 2',
                'sheet_title' => array(),
                'ceils' => array()
            ),
            //sheet 2,....            
        );
        $excel_file = "Report_for_";
        $this->export2excel($excel_content,$excel_file);
    }
    public function actionTransactionSupportingDocumentExcel1()
    {
        //\Yii::setLogger( \Yii::createObject( 'app\components\yiiExt\Logger' ) );
        $cacheMethod = \PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
        $cacheSettings = array( 'memoryCacheSize' => '256MB');
        \PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings); 
        $objPHPExcel = new \PHPExcel();
        $sheet=0;
        $objPHPExcel->setActiveSheetIndex($sheet);
        
        $query = \common\models\PointOrderItem::find();
        $query->joinWith(['order','profile','productListItem']);
        $query->andWhere(['=','order_status', '17']);
        $query->andWhere(['Like','created_datetime', '%2017-01%']);
        $data = $query->asArray()->all();
        $items = ArrayHelper::toArray($data,'');
        
        /*echo '<pre>';
        print_r($items);
        die();*/
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                
        $objPHPExcel->getActiveSheet()->setTitle('xxx')                     
             ->setCellValue('A1', 'Membership#')
             ->setCellValue('B1', 'Full Name');
                 
        $row=2;
        //set_time_limit(30);
        //for ($i = 0; $i < 1000; $i++) {
            foreach ($items as $item){
                $userID = $item['painter_login_id'];
                $dealerID = $item['order']['order_dealer_id'];
                $orderID = $item['order']['order_id'];
                $productID = $item['product_list_id'];
                //$profile = \app\modules\painter\models\PainterProfile::find()->where(['user_id' => $userID,])->one();
                //$dealerOutlet = \common\models\DealerList::find()->select(['customer_name','address'])->where(['id' => $dealerID])->asArray()->one();
                //$dealerOutletitems = ArrayHelper::toArray($dealerOutlet,'');

                //$productListItem = \common\models\ProductList::find()->where(['product_list_id' => $productID])->one();
                //$rd = \common\models\Redemption::find()->where(['LIKE', 'orderID', $orderID])->one();

                $objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$item['profile']['card_id']); 
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$row,$item['profile']['full_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$row,$item['profile']['ic_no']);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$row,$item['order']['order_num']);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$row,'N/A');
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$row,'N/A');
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$row,$item['productListItem']['product_name']);
                //$objPHPExcel->getActiveSheet()->setCellValue('H'.$row,$item['productListItem']['product_description']);
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$row,$item['total_qty']);
                $objPHPExcel->getActiveSheet()->setCellValue('J'.$row,$item['total_qty_point']);
                $objPHPExcel->getActiveSheet()->setCellValue('K'.$row,$item['total_qty_value']);
                $objPHPExcel->getActiveSheet()->setCellValue('L'.$row,date('d-M-y H:i:s', strtotime($item['order']['created_datetime'])));
                $objPHPExcel->getActiveSheet()->setCellValue('M'.$row,'N/A');
                $row++ ;
            }
           
        header('Content-Type: application/vnd.ms-excel');
        $filename = "MyExcelReport_".date("d-m-Y-His").".xls";
        header('Content-Disposition: attachment;filename='.$filename .' ');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        
        //print_r($objPHPExcel);
        
        
    }
    
    public function actionRedemptionSupportingDocument()
    {
        echo 'test';
    }
    
    public function actionRedemptionSupportingDocumentExcel()
    {
        //'2','19'
        $model = new \common\models\Report_Form();
        if ($model->load(Yii::$app->request->post())) {
            $month = $model->reportdate;
            $newmonth = date('Y-m',strtotime($month));
            $query = \common\models\Redemption::find()
                    ->where(['or','redemption_status_ray=2','redemption_status_ray=19'])
                    ->andwhere(['like','redemption_created_datetime',$newmonth])
                    ->orderBy(['redemption_created_datetime' => SORT_ASC]);            
            $query->joinWith(['profile','bank']);
            $data = $query->asArray()->all();
            
            if(count($data) > 0) {
                $items = ArrayHelper::toArray($data,'');

                $filename = Date('YmdGis').'_Redemption_Supporting_Document_Report.xls';
                header("Content-type: application/vnd-ms-excel");
                header("Content-Disposition: attachment; filename=".$filename);
                header("Pragma: no-cache"); 
                header("Expires: 0");

                echo '<table border="1" width="100%">';
                echo '<thead><tr>
                        <th>Membership #</th>
                        <th>Full Name</th>
                        <th>NRIC/Passport #</th>
                        <th>Bank Acc #</th>
                        <th>Bank Acc Holder Name</th>
                        <th>Bank Name</th>
                        <th>Request Date</th>
                        <th>Redemption #</th>
                        <th>Points Redeemed</th>
                        <th>RM Redeemed</th>
                        <th>Paid Date</th>
                        <th>RM Paid</th>
                </tr></thead>';
                echo '<tbody>';
                $tr = '';
                foreach ($items as $item){
                    $banks = \common\models\Banks::find()->where(['id' => $item['bank']['bank_name']])->one();
                    $bankname = $banks->bank_name;
                    $paiddate = $item['redemption_status_ray_date'];
                    if (empty($paiddate)) {
                        $raydate = "N/A";
                    } else {
                        $raydate = date('d-M-y', strtotime($paiddate));
                    }
                    if ($item['redemption_status_ray'] == '2') {
                        $raypaid = '0.00';
                    } else {
                        $raypaid = $item['req_amount'];
                    }
                    $tr .= '<tr>
                    <td>'.$item['profile']['card_id'].'&nbsp;</td>
                    <td>'.$item['profile']['full_name'].'</td>  
                    <td style="text-align: left;">'.$item['profile']['ic_no'].'&nbsp;</td>
                    <td>'.$item['bank']['account_number'].'&nbsp;</td>
                    <td>'.$item['bank']['account_name'].'</td>
                    <td>'.$bankname.'</td>
                    <td>'.date('d-M-y H:i:s', strtotime($item['redemption_created_datetime'])).'</td>
                    <td>'.$item['redemption_invoice_no'].'</td>
                    <td>'.$item['req_points'].'</td>
                    <td>'.$item['req_amount'].'</td>
                    <td>'.$raydate.'</td>
                    <td>'.$raypaid.'</td>
                    </tr>';
                }
                echo $tr;
                echo '</tbody>';
                echo '</table>';
                exit;
            }else {
                \Yii::$app->getSession()->setFlash('warning',['title' => 'Action', 'text' => 'Sorry No Records Found...']);
                return $this->redirect(['redemption-supporting-document-excel']);
            }
        } else {
            return $this->render('_form_report_supporting_document', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionPainterTransactionManagement()
    {
        //'2','19'
        $model = new \common\models\Report_Form();
        if ($model->load(Yii::$app->request->post())) {
            
            $monthfrom = date('Y-m-d',strtotime($model->reportdate));
            $monthto = date('Y-m-d',strtotime($model->reportdateto));
            $status = $model->status;
            $status = $model->status;
            if($status == 0) {
                $status = array(1, 17, 7);
            }else {
                $status = array($status);
            }
            
            $query = \common\models\PointOrderItem::find();
            $query->joinWith([
                'order',
                //'order.profile'
                //'productListItem',
                'productListItem' => function($query) {
                    $query->select(['product_list_id','product_name','product_description']);
                    //$query->select(['profile.id','profile.username'])->where(['user.status' => 'A']);
                },
                'order.profile' => function($query) {
                    $query->select(['painter_profile.id','painter_profile.user_id','painter_profile.card_id','painter_profile.full_name','painter_profile.ic_no']);
                    //$query->select(['profile.id','profile.username'])->where(['user.status' => 'A']);
                },
                'order.dealerOutlet' => function($query) {
                    $query->select(['dealer_list.id','dealer_list.customer_name']);
                    //$query->select(['profile.id','profile.username'])->where(['user.status' => 'A']);
                },        
            ])->where(['BETWEEN', 'point_order.created_datetime',
                $monthfrom.' 00:00:00',
                $monthto.' 23:59:59'
            ])
            ->andWhere(['IN', 'point_order.order_status', $status])             
            ->orderBy(['point_order.created_datetime' => SORT_ASC]);

            /*$query = \common\models\PointOrder::find()
                    ->where(['point_order.order_status'=>$status])
                    ->andWhere(['BETWEEN', 'point_order.created_datetime',
                        $monthfrom.' 00:00:00',
                        $monthto.' 23:59:59'
                    ])                    
                    ->orderBy(['point_order.created_datetime' => SORT_ASC]);            
            $query->joinWith([
                //'dealerOutlet',
                'dealerOutlet' => function($query) {
                    $query->select(['dealer_list.id','dealer_list.customer_name']);
                    //$query->select(['profile.id','profile.username'])->where(['user.status' => 'A']);
                },
                'profile' => function($query) {
                    $query->select(['painter_profile.id','painter_profile.user_id','painter_profile.card_id','painter_profile.full_name','painter_profile.ic_no']);
                    //$query->select(['profile.id','profile.username'])->where(['user.status' => 'A']);
                },
                'orderStatus']);
            //->select(['point_order.order_id','point_order.order_num','point_order.painter_login_id','point_order.order_total_point','point_order.order_total_amount','point_order.order_status','point_order.created_datetime', 'point_order.dealer_invoice_no', 'point_order.dealer_invoice_date']);
                */
            $data = $query->asArray()->all();
            
            echo '<pre>';
            print_r($data);
            die;
            if(count($data) > 0) {
                $items = ArrayHelper::toArray($data,'');

                $filename = Date('YmdGis').'_Painter Transaction Management.xls';
                /*header("Content-type: application/vnd-ms-excel");
                header("Content-Disposition: attachment; filename=".$filename);
                header("Pragma: no-cache"); 
                header("Expires: 0");*/

                echo '<table border="1" width="100%">';
                echo '<thead><tr>
                        <th>Membership #</th>
                        <th>Full Name</th>
                        <th>NRIC/Passport #</th>
                        <th>Transaction #</th>
                        <th>Dealer Name</th>
                        <th>Dealer Invoice #</th>
                        <th>Dealer Invoice (RM)</th>
                        <th>Dealer Invoice (RM)</th>
                        <th>Product Name</th>
                        <th>Product Description</th>
                        <th>Qty</th>
                        <th>Total Points</th>
                        <th>Total RM award</th>
                        <th>Transaction Date</th>
                </tr></thead>';
                echo '<tbody>';
                $tr = '';
                foreach ($items as $item){
                    /*$banks = \common\models\Banks::find()->where(['id' => $item['bank']['bank_name']])->one();
                    $bankname = $banks->bank_name;
                    $paiddate = $item['redemption_status_ray_date'];
                    if (empty($paiddate)) {
                        $raydate = "N/A";
                    } else {
                        $raydate = date('d-M-y', strtotime($paiddate));
                    }
                    if ($item['redemption_status_ray'] == '2') {
                        $raypaid = '0.00';
                    } else {
                        $raypaid = $item['req_amount'];
                    }*/
                    $tr .= '<tr>
                    <td>'.$item['order']['profile']['card_id'].'&nbsp;</td>
                    <td>'.$item['order']['profile']['full_name'].'</td>  
                    <td style="text-align: left;">'.$item['order']['profile']['ic_no'].'&nbsp;</td>
                    <td>'.$item['order']['order_num'].'&nbsp;</td> 
                    <td>'.$item['order']['dealerOutlet']['customer_name'].'</td>  
                    <td>'.$item['order']['dealer_invoice_no'].'</td>
                    <td>'.$item['order']['dealer_invoice_price'].'</td>
                    <td>'.date('d-M-y', strtotime($item['order']['dealer_invoice_date'])).'</td> 
                    <td>'.$item['productListItem']['product_name'].'</td>
                    <td>'.$item['productListItem']['product_description'].'</td>    
                    </tr>';
                }
                echo $tr;
                echo '</tbody>';
                echo '</table>';
                exit;
            }else {
                \Yii::$app->getSession()->setFlash('warning',['title' => 'Action', 'text' => 'Sorry No Records Found...']);
                return $this->redirect(['painter-transaction-management-pending']);
            }
        } else {
            return $this->render('_form_report_status', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionRedemptionSummaryExcel()
    {
        $query = \common\models\Redemption::find()->where(['redemption_status_ray' => '2']);
        $query->joinWith(['profile','bank']);
        $data = $query->asArray()->all();
        $items = ArrayHelper::toArray($data,'');

        $filename = Date('YmdGis').'_Redemption_Summary_Report.xls';
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache"); 
        header("Expires: 0");
        
        echo '<table border="1" width="100%">';
        echo '<thead><tr>
                <th>ID</th>
                <th>Redemption #</th>
                <th>Membership #</th>
                <th>Painter Name</th>
                <th>NRIC/Passport #</th>
                <th>Total Transactions</th>
                <th>Total Points</th>
                <th>Total RM Amount</th>
                <th>Bank Name</th>
                <th>Bank Acc Holder Name</th>
                <th>Bank Acc #</th>
                <th>Account Verified</th>                
                <th>Email</th>                
                <th>Mobile</th>                
                <th>Status</th>
                <th>Request Date</th>
        </tr></thead>';
        echo '<tbody>';
        $tr = '';
        foreach ($items as $item){
            $total_tr = \common\models\RedemptionItems::find()->where(['redemptionID' => $item['redemptionID']])->count();
            $banks = \common\models\Banks::find()->where(['id' => $item['bank']['bank_name']])->one();
            $bankname = $banks->bank_name;
            $paiddate = $item['redemption_status_ray_date'];
            if (empty($paiddate)) {
                $raydate = "N/A";
            } else {
                $raydate = date('d-M-y', strtotime($paiddate));
            }
            if ($item['redemption_status_ray'] == '2') {
                $raypaid = '0.00';
            } else {
                $raypaid = $item['req_amount'];
            }
            
            $verify = $item['bank']['account_no_verification'];
            if($verify == 'Y'){
                $veryfied = 'Y';
            }else{
                $veryfied = '';
            }
            
            $tr .= '<tr>
            <td>'.$item['redemptionID'].'</td>
            <td>'.$item['redemption_invoice_no'].'</td>    
            <td>'.$item['profile']['card_id'].'&nbsp;</td>
            <td>'.$item['profile']['full_name'].'</td>  
            <td style="text-align: left;">'.$item['profile']['ic_no'].'&nbsp;</td>
            <td>'.$total_tr.'&nbsp;</td>  
            <td>'.$item['req_points'].'</td>
            <td>'.$item['req_amount'].'</td>    
            <td>'.$bankname.'</td>
            <td>'.$item['bank']['account_name'].'</td>
            <td># '.$item['bank']['account_number'].'</td>      
            <td>'.$veryfied.'</td>
            <td>'.$item['profile']['email'].'</td>
            <td>'.$item['profile']['mobile'].'</td>
            <td>Processing</td>    
            <td>'.date('d-M-y H:i:s', strtotime($item['redemption_created_datetime'])).'</td>            
            </tr>';
        }
        echo $tr;
        echo '</tbody>';
        echo '</table>';
        exit;
    }
    
    public function actionDealerlistExcel()
    {
        $query = \common\models\DealerList::find();
        $data = $query->asArray()->all();
        $items = ArrayHelper::toArray($data,'');

        $filename = Date('YmdGis').'_Dealer_List_Report.xls';
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache"); 
        header("Expires: 0");
        
        echo '<table border="1" width="100%">';
        echo '<thead><tr>
                <th>Code</th>
                <th>Dealer</th>
                <th>Region</th>
                <th>State</th>
                <th>Contact Person</th>
                <th>Contact No</th>
                <th>Address</th>                
                <th>Status</th>
        </tr></thead>';
        echo '<tbody>';
        $tr = '';
        foreach ($items as $item){

            
            $status = $item['status'];
            if($status == 'A'){
                $status = 'Active';
            }else{
                $status = 'Deleted';
            }
            
            $tr .= '<tr>
            <td>'.$item['code'].'</td>
            <td>'.$item['customer_name'].'</td>    
            <td>'.$item['region'].'&nbsp;</td>
            <td>'.$item['state'].'</td>
            <td>'.$item['contact_person'].'</td>
            <td>'.$item['contact_no'].'</td>
            <td>'.$item['address'].'</td>    
            <td>'.$status.'</td>         
            </tr>';
        }
        echo $tr;
        echo '</tbody>';
        echo '</table>';
        exit;
    }
    
    public function actionDownload($file_name, $file_type = 'excel', $deleteAfterDownload = false) {
        if (empty($file_name)) {
            return 0;
        }
        $baseRoot = Yii::getAlias('@webroot') . "/upload/";
        $file_name = $baseRoot . $file_name;
        if (!file_exists($file_name)) {
            return 0;
        }
        $fp = fopen($file_name, "r");
        $file_size = filesize($file_name);

        if ($file_type == 'excel') {
            ob_end_clean();
            header('Pragma: public');
            header('Expires: 0');
            header('Content-Encoding: none');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Cache-Control: public');
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Description: File Transfer');
            Header("Content-Disposition: attachment; filename=" . basename($file_name));
            header('Content-Transfer-Encoding: binary');
            Header("Content-Length:" . $file_size);
        } else if ($file_type == 'picture') { //pictures
            Header("Content-Type:image/jpeg");
            Header("Accept-Ranges: bytes");
            Header("Content-Disposition: attachment; filename=" . basename($file_name));
            Header("Content-Length:" . $file_size);
        } else { //other files
            Header("Content-type: application/octet-stream");
            Header("Accept-Ranges: bytes");
            Header("Content-Disposition: attachment; filename=" . basename($file_name));
            Header("Content-Length:" . $file_size);
        }
 
        $buffer = 1024;
        $file_count = 0;

        while (!feof($fp) && $file_count < $file_size) {
            $file_con = fread($fp, $buffer);
            $file_count+=$buffer;
            echo $file_con;
        }
        //echo fread($fp, $file_size);
        fclose($fp);
        if ($deleteAfterDownload) {
            unlink($file_name);
        }
        return 1;
    }
    

}
