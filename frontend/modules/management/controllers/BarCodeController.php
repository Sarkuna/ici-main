<?php

namespace app\modules\management\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;

use common\models\BCPayOutSummary;
use common\models\BCPayOutSummarySearch;
use common\models\BCExcelSummery;
use common\models\BCRedemption;
use common\models\BCRedemptionSearch;
use common\models\PainterProfile;

/**
 * BarCodeController implements the CRUD actions for BCPayOutSummary model.
 */
class BarCodeController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all BCPayOutSummary models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new BCPayOutSummarySearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BCPayOutSummary model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchModel = new BCRedemptionSearch();
        //$searchModel->redemption_status_ray = ['2'];
        //$searchModel->pay_out_summary_id = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['pay_out_summary_id' => $id]);

        return $this->render('view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionBulkPayment($id)
    {
        if ($this->request->isPost) {
            $selection=(array)Yii::$app->request->post('selection');//typecasting
            if(count($selection) > 0){
                foreach($selection as $valueid){
                    $this->mobibulk($valueid);
                }
            }
            exit;
        }else {
            $searchModel = new BCRedemptionSearch();
            //$searchModel->redemption_status_ray = ['2'];
            //$searchModel->pay_out_summary_id = 0;
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->query->andWhere(['pay_out_summary_id' => $id, 'internel_status' => 'processing']);
            $dataProvider->query->andWhere(['>=', 'total_rm', '100']);

            $dataProvider->pagination->pageSize=100;
            return $this->render('view_bulk', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $this->findModel($id),
            ]);
        }
    }
    
    

    /**
     * Creates a new BCPayOutSummary model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new BCPayOutSummary();
        $batch = \common\models\BCPayOutSummary::find()->count();
        $batch = $batch + 1;
        $model->batch = $batch;
        if ($this->request->isPost) {
            if ($model->load(Yii::$app->request->post())) {
                
                $model->excel = UploadedFile::getInstance($model, 'excel');

                $rpath = realpath(Yii::$app->basePath);
                $model->excel->saveAs($rpath.'/web/upload/excelfiles/' . 'Batch'.$batch.'_'.$model->excel->baseName . '.' . $model->excel->extension);

                $inputFile = $rpath.'/web/upload/excelfiles/' . 'Batch'.$batch.'_'.$model->excel->baseName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                $pre_defined = array("points","timestamp","product name","product id","magnitude","sk u","serial no","type","painter id");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));

                if($checkmatch > 5){      
                    $model->start_date = date('Y-m-d', strtotime($model->start_date));
                    $model->end_date = date('Y-m-d', strtotime($model->end_date));
                    $model->save();
                    
                    $newmsg = '';
                    $data = array();
                    for ($row = 2; $row <= $highestRow; ++ $row) {
                        
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);

                        $points = ROUND($rowData[0]['points'],2);
                        $vtimestamps = $rowData[0]['timestamp'];
                        $product_name = $rowData[0]['product name'];
                        $product_id = $rowData[0]['product id'];
                        $magnitude = $rowData[0]['magnitude'];
                        $sku = $rowData[0]['sk u'];
                        $serial_no = $rowData[0]['serial no'];
                        $type = $rowData[0]['type'];
                        $crm_id = $rowData[0]['painter id'];
                        
                        if($type == 'PRIMARY' || $type == 'PROMOTIONAL') {
                            $points = $points;
                        }else {
                            $points = '-'.$points;
                        }
                        
                        $data[] = [$model->id,0,$points,$vtimestamps,$product_name,$product_id,$magnitude,$sku,$serial_no,$type,$crm_id,'pending'];
                    }
                    Yii::$app->db
                        ->createCommand()
                        ->batchInsert('bc_excel_summery', ['bc_pay_out_id','bc_redemption_id','points','timestamps','product_name','product_id','magnitude','sku','serial_no','type','crm_id','status'],$data)
                        ->execute();

                    unlink($inputFile);
                    return $this->redirect(['new-redemption', 'id' => $model->id]);
                }else {
                    echo 'Template format not match!';
                }
                
                
                
                
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    /**
     * Updates an existing BCPayOutSummary model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $checklastid = BCPayOutSummary::find()->orderBy(['id' => SORT_DESC])->one();
        $last_id = $checklastid->id;
        if($id == $last_id) {
            $model1 = $this->findModel($id);
            
            $model = new \common\models\ExcelForm();
            if ($model->load(Yii::$app->request->post())) {
                
                $model->excel = UploadedFile::getInstance($model, 'excel');

                $rpath = realpath(Yii::$app->basePath);
                $model->excel->saveAs($rpath.'/web/upload/excelfiles/' . 'Batch'.$checklastid->batch.'_'.$model->excel->baseName . '.' . $model->excel->extension);

                $inputFile = $rpath.'/web/upload/excelfiles/' . 'Batch'.$checklastid->batch.'_'.$model->excel->baseName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                $pre_defined = array("points","timestamp","product name","product id","magnitude","sk u","serial no","type","painter id");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));

                if($checkmatch > 5){      

                    $newmsg = '';
                    $data = array();
                    for ($row = 2; $row <= $highestRow; ++ $row) {
                        
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);

                        $points = ROUND($rowData[0]['points'],2);
                        $vtimestamps = $rowData[0]['timestamp'];
                        $product_name = $rowData[0]['product name'];
                        $product_id = $rowData[0]['product id'];
                        $magnitude = $rowData[0]['magnitude'];
                        $sku = $rowData[0]['sk u'];
                        $serial_no = $rowData[0]['serial no'];
                        $type = $rowData[0]['type'];
                        $crm_id = $rowData[0]['painter id'];
                        
                        if($type == 'PRIMARY' || $type == 'PROMOTIONAL') {
                            $points = $points;
                        }else {
                            $points = '-'.$points;
                        }
                        
                        $data[] = [$id,0,$points,$vtimestamps,$product_name,$product_id,$magnitude,$sku,$serial_no,$type,$crm_id,'pending'];
                    }
                    Yii::$app->db
                        ->createCommand()
                        ->batchInsert('bc_excel_summery', ['bc_pay_out_id','bc_redemption_id','points','timestamps','product_name','product_id','magnitude','sku','serial_no','type','crm_id','status'],$data)
                        ->execute();

                    unlink($inputFile);
                    return $this->redirect(['new-redemption', 'id' => $id]);
                }else {
                    echo 'Template format not match!';
                }
                
                
                
                
                return $this->redirect(['view', 'id' => $model->id]);
            }
            
            if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionNewRedemption($id) {
        $message = '';
        $count = BCExcelSummery::find()->where("status = 'pending'")->count();
        if($count > 0) {
            $excelitems = BCExcelSummery::find()->where(['bc_pay_out_id' => $id,'status' => 'pending'])->groupBy(['crm_id'])->all();
            foreach ($excelitems as $myitem) {
                $status = 'processing'; $painterid = 0;
                $crm_id = $myitem->crm_id;
                $count_crm = PainterProfile::find()->where(['crm_id' => $crm_id])->count();
                if($count_crm > 0){
                    $painterid = $myitem->painter->user_id;
                    if($myitem->painter->bank->account_no_verification == 'Y') {
                        $status = 'processing';
                    }else {
                        $status = 'not_verified';
                    }
                }else {
                    $status = 'crm_id_not_found';
                }

                $count_sku = BCExcelSummery::find()->where(['bc_pay_out_id' => $id,'status' => 'pending','crm_id' => $crm_id])->count();
                $sum_points = BCExcelSummery::find()->where(['bc_pay_out_id' => $id,'status' => 'pending','crm_id' => $crm_id])->sum('points');
                $total_rm = $myitem->payout->rm_value * $sum_points;
                $truncatedVar = round($total_rm, 2);

                $redemption = new BCRedemption();
                $redemption->painterID = $painterid;            
                $redemption->crm_id = $myitem->crm_id;
                $redemption->count_of_sku = $count_sku;
                $redemption->sum_of_points = $sum_points;
                $redemption->per_rm = $myitem->payout->rm_value;
                $redemption->total_rm = $truncatedVar;
                $redemption->total_rm_crm = $total_rm;
                $redemption->pay_out_summary_id = $id;
                $redemption->redemption_IP = isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : '127.0.0.1';
                $redemption->internel_status = $status;
                $redemption->redemption_remarks = 'Redemption Created';
                
                if($redemption->save(false)) {
                    BCExcelSummery::updateAll(['status' => 'approved', 'bc_redemption_id' => $redemption->redemptionID], 'crm_id = '.$crm_id.'');
                    $message = 'Redemption created successfuly';
                }else {
                    $message = 'Redemption Not Crated'.$redemption->getErrors();
                }

            }
        }else {
            $message = 'No data to create redemption!';
        }
        
        return $this->redirect(['index']);
    }
    
    public function actionUpdateBank($id)
    {
        
        $model = new \common\models\PayoutForm();
        $redemption = BCRedemption::find()->where(['redemptionID' => $id])->one();
        $bank = \app\modules\painter\models\BankingInformation::find()->where(['user_id' => $redemption->painterID])->one();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
              
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            $bankupdate = \app\modules\painter\models\BankingInformation::find()->where(['id' => $bank->id])->one();
            if($model->account_verification == 'Y') {
               $verification =  'Y';
            }else {
               $verification =  'N';
            }

            $bankupdate->account_no_verification = $verification;
            if($bankupdate->save(false)) {
                if($model->status == 'review') {
                    BCRedemption::updateAll(['review_comment' => $model->comment, 'internel_status' => $model->status], ['=', 'painterID',  $redemption->painterID]);
                }else {
                    $redemption->internel_status = $model->status;
                    $redemption->comment = $model->comment;
                    $redemption->save(false);
                }                
                \Yii::$app->getSession()->setFlash('success',['title' => 'Bank Account#', 'text' => 'Bank Acc# ('.$bankupdate->account_number.') verfication updated']);
            }else {
                \Yii::$app->getSession()->setFlash('error',['title' => 'Bank Account#', 'text' => 'Bank Acc# ('.$bankupdate->account_number.') updated fail']);
            }
            return $this->redirect(['view', 'id' => $redemption->pay_out_summary_id]);
        } else {
            if($bank->account_no_verification == 'Y') {
                $model->account_verification = 'Y';
            }else {
                $model->account_verification = 'N';
            }
            $model->status = $redemption->internel_status;
            $model->comment = $redemption->review_comment;
            return $this->renderAjax('_formpop', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionMobi($id)
    {
        /*$url = 'https://san.gomobi.io/externalapi/payoutservice';
        $mobiApiKey = 'b07ad9f31df158edb188a41f725899bc';
        $businessRegNo = '12345';
        $subMID = '201100000012450';*/
        
        $redemption = BCRedemption::find()->where(['redemptionID' => $id])->one();
        if(!empty($redemption)) {
            $mobile = $redemption->profile->mobile;
            $crm_id = $redemption->profile->crm_id;
            $paintername = $redemption->profile->full_name;
            $bankAccNo = substr($redemption->bank->account_number, 0, 20);
            $accountName = $redemption->bank->account_name;
            $bankName = $redemption->bank->bankName->bank_name;
            $bicCode = $redemption->bank->bankName->bic_code;
            $amount = number_format((float)$redemption->total_rm, 2, '.', '');
            //$amount = "1.00";

            if($amount > 100){
                $mobi_charge = (1 / 100) * $amount;
            }else {
                $mobi_charge = 1.00;
            }

            $jsonData = array(
                'service' => 'PAYOUT_TXN_REQ',
                'mobiApiKey' => Yii::$app->params['mobi.api.key'],
                'businessRegNo' => Yii::$app->params['mobi.api.RegNo'],
                'bankName' => $bankName,
                'bankAccNo' => trim($bankAccNo),
                'customerName' => $accountName,
                'amount' => $amount,
                'subMID' => Yii::$app->params['mobi.api.subMID'],
                'bicCode' => $bicCode,
            );

            // Convert the data to JSON format
            $jsonString = json_encode($jsonData);

            // Initialize cURL session
            $ch = curl_init(Yii::$app->params['mobi.api.url']);

            // Set cURL options
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonString);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonString)
            ));

            // Execute cURL session and get the result
            $response = curl_exec($ch);

            // Check for cURL errors
            if (curl_errno($ch)) {
                echo 'Curl error: ' . curl_error($ch);
            }

            // Close cURL session
            curl_close($ch);

            $data = json_decode($response, true);

            if(!empty($data['responseData'])) {
                if($data['responseMessage'] == 'FAILURE') {
                    $code = 'M1';
                    $redemption->redemption_remarks = $data['failureReason'];
                    $redemption->internel_status = 'rejected';
                    $redemption->save(false);
                    $msg_status = "error";        
                }else {
                    $code = 'M2';
                    $redemption->mobi_trxId = $data['responseData']['trxId'];
                    $redemption->redemption_remarks = $data['responseDescription'];
                    $redemption->internel_status = 'paid';
                    $redemption->paid_date = date('Y-m-d');
                    $redemption->merchant = 'mobi';
                    $redemption->mobi_charge = $mobi_charge;
                    $redemption->save(false);
                    $msg_status = "success";
                }

                $emailTemplate = \common\models\EmailTemplate::find()->where(['code' => $code])->one();
                $emailBody = $emailTemplate->sms_text;

                $emailBody = str_replace('{name}', $paintername, $emailBody);
                $emailBody = str_replace('{crm_id}', $crm_id, $emailBody);
                $emailBody = str_replace('{paid_date}', date('d-m-Y'), $emailBody);
                $emailBody = str_replace('{amount}', $amount, $emailBody);
                $emailBody = str_replace('{account_name}', $accountName, $emailBody);

                $mobile = str_replace(' ','',$mobile); 
                $message = $emailBody;
                $message = html_entity_decode($message, ENT_QUOTES, 'utf-8'); 
                $message = urlencode($message);

                $username = urlencode(Yii::$app->params['sms.username']);
                $password = urlencode(Yii::$app->params['sms.password']);
                $sender_id = urlencode("66300");
                $type = '1';

                $fp = "https://www.isms.com.my/isms_send.php?un=$username&pwd=$password&dstno=$mobile&msg=$message&type=$type&sendid=$sender_id";
                $handle = @fopen($fp, "r");
                if ($handle) {
                  while (!feof($handle)) {
                      $buffer = fgets($handle, 10000);
                      $st = $buffer;
                  }
                }

                \Yii::$app->getSession()->setFlash($msg_status,['title' => $data['responseMessage'], 'text' => $data['responseDescription']]);
            }else {
                \Yii::$app->getSession()->setFlash('error',['title' => 'API Error', 'text' => $data['responseDescription']]);
            }
            return $this->redirect(['view', 'id' => $redemption->pay_out_summary_id]);
        }
        
        throw new NotFoundHttpException('The requested page does not exist.');

    }
    
    public function actionUpdatePayment($id)
    {
        
        $model = new \common\models\PayoutForm();
        $redemption = BCRedemption::find()->where(['redemptionID' => $id])->one();
        $bank = \app\modules\painter\models\BankingInformation::find()->where(['user_id' => $redemption->painterID])->one();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
              
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            $redemption->internel_status = $model->status;
            $redemption->comment = $model->comment;

            if($redemption->save()) {
                \Yii::$app->getSession()->setFlash('success',['title' => 'Success', 'text' => 'Redemption '.$redemption->redemption_invoice_no.' has been updated']);
                
            }  else {
                \Yii::$app->getSession()->setFlash('fail',['title' => 'Fail', 'text' => 'Redemption '.$redemption->redemption_invoice_no.' not updated']);
            }
            return $this->redirect(['view', 'id' => $redemption->pay_out_summary_id]);  
        } else {
            $model->status = $redemption->internel_status;
            $model->comment = $redemption->comment;
            return $this->renderAjax('_formpop_payment', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Deletes an existing BCPayOutSummary model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BCPayOutSummary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return BCPayOutSummary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BCPayOutSummary::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    /**
     * Finds the PayOutSummary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PayOutSummary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*protected function findModelredemption($id)
    {
        if (($model = BCRedemption::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }*/
    
    public function actionCrmnotfound()
    {
        $excelitems = BCRedemption::find()->where(['painterID' => 0])->all();
        if(!empty($excelitems)) {
            foreach ($excelitems as $myitem) {

                $check = \common\models\CRMList::find()->where(['crm_id' => $myitem->crm_id])->count();
                if($check > 0) {
                    $profile = PainterProfile::find()->where(['card_id' => $myitem->mastercrm->painter_id])->one();
                    $profile->crm_id = $myitem->mastercrm->crm_id;
                    $profile->save(false);

                    $bcredemption = BCRedemption::find()->where(['redemptionID' => $myitem->redemptionID])->one();
                    $bcredemption->painterID = $profile->user_id;
                    $bcredemption->internel_status = 'processing';
                    $bcredemption->save(false);

                    echo $myitem->redemptionID.'-'.$myitem->crm_id.'-'.$myitem->mastercrm->crm_id.'-'.$myitem->mastercrm->painter_id.'<br>';
                }
            }
        }else {
            echo 'Nothing Here';
        }

    }
    
    protected function mobibulk($id){
        $redemption = BCRedemption::find()->where(['redemptionID' => $id])->one();
        if(!empty($redemption)) {
            $mobile = $redemption->profile->mobile;
            $crm_id = $redemption->profile->crm_id;
            $paintername = $redemption->profile->full_name;
            $bankAccNo = substr($redemption->bank->account_number, 0, 20);
            $accountName = $redemption->bank->account_name;
            $bankName = $redemption->bank->bankName->bank_name;
            $bicCode = $redemption->bank->bankName->bic_code;
            $amount = number_format((float)$redemption->total_rm, 2, '.', '');
            //$amount = "1.00";

            if($amount > 100){
                $mobi_charge = (1 / 100) * $amount;
            }else {
                $mobi_charge = 1.00;
            }

            $jsonData = array(
                'service' => 'PAYOUT_TXN_REQ',
                'mobiApiKey' => Yii::$app->params['mobi.api.key'],
                'businessRegNo' => Yii::$app->params['mobi.api.RegNo'],
                'bankName' => $bankName,
                'bankAccNo' => trim($bankAccNo),
                'customerName' => $accountName,
                'amount' => $amount,
                'subMID' => Yii::$app->params['mobi.api.subMID'],
                'bicCode' => $bicCode,
            );

            // Convert the data to JSON format
            $jsonString = json_encode($jsonData);

            // Initialize cURL session
            $ch = curl_init(Yii::$app->params['mobi.api.url']);

            // Set cURL options
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonString);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($jsonString)
            ));

            // Execute cURL session and get the result
            $response = curl_exec($ch);

            // Check for cURL errors
            if (curl_errno($ch)) {
                echo 'Curl error: ' . curl_error($ch);
            }

            // Close cURL session
            curl_close($ch);

            $data = json_decode($response, true);

            if(!empty($data['responseData'])) {
                if($data['responseMessage'] == 'FAILURE') {
                    $code = 'M1';
                    $redemption->redemption_remarks = $data['failureReason'];
                    $redemption->internel_status = 'rejected';
                    $redemption->save(false);
                    $msg_status = "error";        
                }else {
                    $code = 'M2';
                    $redemption->mobi_trxId = $data['responseData']['trxId'];
                    $redemption->redemption_remarks = $data['responseDescription'];
                    $redemption->internel_status = 'paid';
                    $redemption->paid_date = date('Y-m-d');
                    $redemption->merchant = 'mobi';
                    $redemption->mobi_charge = $mobi_charge;
                    $redemption->save(false);
                    $msg_status = "success";
                }

                $emailTemplate = \common\models\EmailTemplate::find()->where(['code' => $code])->one();
                $emailBody = $emailTemplate->sms_text;

                $emailBody = str_replace('{name}', $paintername, $emailBody);
                $emailBody = str_replace('{crm_id}', $crm_id, $emailBody);
                $emailBody = str_replace('{paid_date}', date('d-m-Y'), $emailBody);
                $emailBody = str_replace('{amount}', $amount, $emailBody);
                $emailBody = str_replace('{account_name}', $accountName, $emailBody);

                $mobile = str_replace(' ','',$mobile); 
                $message = $emailBody;
                $message = html_entity_decode($message, ENT_QUOTES, 'utf-8'); 
                $message = urlencode($message);

                $username = urlencode(Yii::$app->params['sms.username']);
                $password = urlencode(Yii::$app->params['sms.password']);
                $sender_id = urlencode("66300");
                $type = '1';

                $fp = "https://www.isms.com.my/isms_send.php?un=$username&pwd=$password&dstno=$mobile&msg=$message&type=$type&sendid=$sender_id";
                $handle = @fopen($fp, "r");
                if ($handle) {
                  while (!feof($handle)) {
                      $buffer = fgets($handle, 10000);
                      $st = $buffer;
                  }
                }

                echo '<p>'.$crm_id.' ,'.$data['responseMessage'].','.$data['responseDescription'].'</p>';
                //\Yii::$app->getSession()->setFlash($msg_status,['title' => $data['responseMessage'], 'text' => $data['responseDescription']]);
            }else {
                echo '<p>'.$crm_id.' ,Error,'.$data['responseDescription'].'</p>';
                //\Yii::$app->getSession()->setFlash('error',['title' => 'API Error', 'text' => $data['responseDescription']]);
            }
            //return $this->redirect(['view', 'id' => $redemption->pay_out_summary_id]);
        }

    }
}
