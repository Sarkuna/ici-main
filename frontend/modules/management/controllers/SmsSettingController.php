<?php

namespace app\modules\management\controllers;

use Yii;
use app\models\SMSSetting;
//use common\models\SMSQueueSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SmsQueueController implements the CRUD actions for SMSQueue model.
 */
class SmsSettingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SMSQueue models.
     * @return mixed
     */
    public function actionIndex()
    {
        Yii::$app->cache->flush();
        $model = new \common\models\SMS_setting_Form();
        if ($model->load(Yii::$app->request->post())) {
            $rows = SMSSetting::updateAll(['del' => 0]);
            $selection = $model->name;
            foreach($selection as $valueid){
                $formFeatureModel = $this->findModelsms($valueid);
                $formFeatureModel->del = 1;
                $formFeatureModel->save();
            }
            \Yii::$app->getSession()->setFlash('success',['title' => 'Action', 'text' => 'Successfuly Saved']);
            return $this->redirect(['index']);          
        } else {
            return $this->render('_form_report', [
                        'model' => $model,
            ]);
        }
    }

    public function actionBulksms() {
        $selection=(array)Yii::$app->request->post('selection');
        if(!empty($selection)){
            foreach($selection as $valueid){
                $paintersid = $this->findModel($valueid);
                $painterID = $paintersid->painterID;
                $mobile = preg_replace('/\s+/', '', $paintersid->mobile);
                $templet = $paintersid->email_template_id;
                $sql = 'SELECT  `painterID` ,  `name` ,`mobile` , email_template_id, COUNT( painterID ) AS TotalTrn, GROUP_CONCAT( painterID ) AS User, SUM( r_points ) AS TotalPoints, GROUP_CONCAT( r_points ) AS Points, SUM( r_amount ) AS TotalAmounts, GROUP_CONCAT( r_amount ) AS Amount FROM sms_queue WHERE  `status` =  "P" AND painterID = '.$painterID.' GROUP BY painterID';
                $persms = \common\models\SMSQueue::findBySql($sql)
                    ->asArray()->one();
                
                //$mobile = $persms['mobile'];
                //$userID = $persms['painterID'];
                $data = \yii\helpers\Json::encode(array(
                    'total_approved_transactions' => $persms['TotalTrn'],
                    'redemption_point' => $persms['TotalPoints'],
                    'redemption_value' => $persms['TotalAmounts'],
                ));

                $result = Yii::$app->ici->sendSMSbulk($painterID, $mobile, $templet, $data);
                if($result == '2000 = SUCCESS' || $result == 'EMPTY/BLANK'){
                    $upd = \common\models\SMSQueue::updateAll(['status' => 'S'], 'painterID = '.$painterID.'');
                    \Yii::$app->getSession()->setFlash('success',['title' => 'SMS', 'text' => 'SMS send successful!']);
                    return $this->redirect(['index']);
                }else {
                    \Yii::$app->getSession()->setFlash('error',['title' => 'SMS', 'text' => 'SMS not send error code '.$result.'']);
                    return $this->redirect(['index']);
                }               
                
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Displays a single SMSQueue model.
     * @param integer $id
     * @return mixed
     */
    /*public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }*/

    /**
     * Creates a new SMSQueue model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*public function actionCreate()
    {
        $model = new SMSQueue();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->sms_queue_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }*/

    /**
     * Updates an existing SMSQueue model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->sms_queue_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }*/

    /**
     * Deletes an existing SMSQueue model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /*public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/
    protected function findModelsms($id)
    {
        if (($model = SMSSetting::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /**
     * Finds the SMSQueue model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SMSQueue the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SMSQueue::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
