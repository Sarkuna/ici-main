<?php

namespace app\modules\management\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

use common\models\Redemption;
use common\models\RedemptionSearch;
use common\models\RedemptionHistory;
use common\models\RedemptionRemark;
use common\models\PointOrder;


/**
 * RedemptionController implements the CRUD actions for Redemption model.
 */
class RedemptionStatementController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Redemption models.
     * @return mixed
     */
    public function actionIndex()
    {
        /*echo 'test';
        //$User
        die;*/
        set_time_limit(180);// seconds
        Yii::$app->cache->flush();
        $model = new \common\models\Report_Form();
        if ($model->load(Yii::$app->request->post())) {
            $mont = $model->reportdate;
            $membershipno = $model->membershipno;
            $date = explode('-',$mont);
            if(!empty($date[0]) && !empty($date[1])){
                $year = $date[0];
                $month = $date[1];
            }
            $model = Redemption::find()
                ->joinWith(['profile', 'bank'])
                //->where(['redemption_invoice_no' => 'RDM-016688'])
                ->where(['card_id' => $membershipno])
                ->andWhere(['like', 'redemption_created_datetime', $year.'-'.$month])
                //->groupBy('painterID')
                ->asArray()->one();
            $i = 1;
            $html = '';
            $html = '<html><body>';
            
            

            $len = count($model);
            if ($len > 0) {
                $i = 1;
                $html = '';
                $html = '<html><body>';
                $html .= '<h2 style="text-align: center; margin-top:0px;">Redemption Statement - ' . date("F Y", strtotime($mont)) . '</h2>
                    <div id="project">
                        <p><span>Full Name :</span> ' . $model['profile']['full_name'] . '</p>
                        <p><span>Membership ID# :</span> ' . $model['profile']['card_id'] . '</p>
                        <p><span>NRIC/PP :</span> ' . $model['profile']['ic_no'] . '</p>
                        <p><span>Mobile # :</span> ' . $model['profile']['mobile'] . '</p>
                    </div>
                    <div id="company">
                        <b>Bank Name:</b>'.$this->getBankName($model['bank']['bank_name']).'<br/>
                        <b>Account Name:</b> ' . $model['bank']['account_name'] . '<br/>
                        <b>Account #: ' . $model['bank']['account_number'] . '</b>
                    </div>
                    <div class="clear" style="margin-bottom:10px;"></div>';
                $html .= '<table border="0" style= "border-collapse:collapse; font-size:12px; font-family: Arial, sans-serif; margin-bottom: 20px; border:solid 1px #ccc;">
                    <thead>
                      <tr style= "background-color: #022a68; color:#ffffff;">
                        <th>#</th>  
                        <th class="service">Transaction#</th>
                        <th class="desc">Dealer Name</th>
                        <th>Area</th>
                        <th>Dealer Invoice</th>
                        <th>Items</th>
                        <th>Points Awarded</th>
                        <th>Value (RM)</th>
                      </tr>
                    </thead>
                    <tbody>';
                    $ids = explode(",", $model['orderID']);
                    $redemptionitems = \common\models\PointOrder::find()
                            ->joinWith([
                                'dealerOutlet' => function($que) {
                                    $que->select(['id','customer_name','area']);
                                },        
                            ])->where(['in', 'order_id', $ids])
                            ->asArray()->all();
                    $n = 1;
                    $totpoint1 = '';$totamount1 = ''; $totpoint = 0;$totamount=0;
                    $html .= '<tr style= "background-color: #022a68;">
                    <td colspan="8" style="text-align: left; background-color: #dddddd;">' . $model['redemption_invoice_no'] . '</td>  
                  </tr>';
                    foreach ($redemptionitems as $key => $redemptionitem) {
                        $order_id = $redemptionitem['order_id'];
                        //$order = \common\models\PointOrder::find()->select(['order_num','order_dealer_id'])->where(['order_id' => $order_id])->one();
                        $orderitem_tot_point = \common\models\PointOrderItem::find()->where(['point_order_id' => $order_id, 'Item_status' => 'G'])->sum('total_qty_point');
                        $orderitem_tot_amt = \common\models\PointOrderItem::find()->where(['point_order_id' => $order_id, 'Item_status' => 'G'])->sum('total_qty_value');
                        $orderitem_qty = \common\models\PointOrderItem::find()->where(['point_order_id' => $order_id, 'Item_status' => 'G'])->count();
                        //$Dealerid = $order->order_dealer_id;
                        //$dealername = \common\models\DealerList::find()->select(['id','customer_name','area'])->where(['id' => $Dealerid])->one();
                        $html .= '<tr>
                            <td>' . $n . '</td>
                            <td valign="top">' . $redemptionitem['order_num'] . '</td>
                            <td valign="top" style="text-align: left;">' . $redemptionitem['dealerOutlet'] ['customer_name'] . '</td>
                            <td valign="top" style="text-align: left;">' . $redemptionitem['dealerOutlet'] ['area'] . '</td>
                            <td valign="top" style="text-align: left;">' . $redemptionitem['dealer_invoice_no'] . '</td>
                            <td valign="top" class="text-center">' . $orderitem_qty . '</td>       
                            <td valign="top" class="text-center">' . $orderitem_tot_point . '</td>
                            <td valign="top" class="text-center">' . $orderitem_tot_amt . '</td>   
                          </tr>';
                        $totpoint1 += $orderitem_tot_point;
                        $totamount1 += $orderitem_tot_amt;
                        $n++;
                    }
                    $totpoint += $totpoint1;
                    $totamount += $totamount1;
                    $html .= '<tr style= "background-color: #f3f3f3;">
                        <td colspan="6" style="font-weight: bold;"> Sub Total :</td>
                        <td class="total" style="font-weight: bold;">' . $totpoint1 . '</td>
                        <td class="total" style="font-weight: bold;">' . Yii::$app->formatter->asDecimal($totamount1) . '</td> 
                    </tr></tbody></table>';
                $html .='</body></html>';
                //$html = 'Shihan';

                $pdf = new Pdf(); // or new Pdf();
                $mpdf = $pdf->api;
                $stylesheet = file_get_contents(Yii::$app->request->hostInfo.'/css/print.css'); // external css
                //$mpdf->SetHTMLHeader('<img src="http://dev.agnichakra.com/images/club_logo.png"/>');
                $mpdf->SetHTMLHeader('<div style="width: 100%; text-align:right;"><img src="'.Yii::$app->request->hostInfo.'/images/pdf_header_dulux.jpg"/></div>');
                $mpdf->AddPage('', // L - landscape, P - portrait 
                        '', '', '', '', 5, // margin_left
                        5, // margin right
                        55, // margin top
                        25, // margin bottom
                        5, // margin header
                        0); // margin footer
                $mpdf->WriteHTML($stylesheet, 1);
                $mpdf->WriteHtml($html);
                $mpdf->Output();
                //$mpdf->Output($membershipno.'.pdf', 'D');
                exit;
                //End pdf
            }else {
                \Yii::$app->getSession()->setFlash('warning',['title' => 'Action', 'text' => 'Sorry No Records Found...']);
                    return $this->redirect(['index']);
            }            
        } else {
            return $this->render('_form_report', [
                        'model' => $model,
            ]);
        }
    }
    
    private function getBankName($id) {
        $banks = \common\models\Banks::find()->select(['bank_name'])->where(['id' => $id])->asArray()->one();
        $bankname = $banks['bank_name'];
        return $bankname;
    }

    public function actionViewpdf($id)
    {
        //$this->layout = 'printlayout';
        //$stu_id = Yii::$app->miedz->url_decode($sid);
        //$model = $this->findModel($stu_id);
        $model = $this->findModel($id);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $this->renderPartial('_view_pdf', [
            'model' => $model,]),
            'cssFile' => '@frontend/web/css/print.css',
            'cssInline' => '.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
    border: 1px solid #ddd;
}', 
            'options' => [
                'title' => 'Akzo Nobel',
                'subject' => 'APPLICATION FORM'
            ],
            'methods' => [
                'SetHeader' => ['Generated By: Akzo Nobel||Generated On: ' . date("d-m-Y")],
                'SetFooter' => ['|Page {PAGENO}|'],
            ]
        ]);
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        return $pdf->render();
    }

    /**
     * Deletes an existing Redemption model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Redemption model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Redemption the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Redemption::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
