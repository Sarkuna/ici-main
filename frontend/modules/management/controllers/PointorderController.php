<?php

namespace app\modules\management\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use common\models\PointOrder;
use common\models\PointOrderItem;
use common\models\PointOrderSearch;
use common\models\Findpainter;
use common\models\BarCodes;
use common\models\ProductList;
use common\models\DealerList;
use common\models\Redemption;
use common\models\RedemptionItems;
use common\models\Campaign;
use common\models\CampaignAssignProducts;

use yii\helpers\Html;

use app\modules\painter\models\PainterProfile;
use app\modules\painter\models\BankingInformation;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * PointorderController implements the CRUD actions for PointOrder model.
 */
class PointorderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PointOrder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PointOrderSearch();
        $searchModel->order_status = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $count = PointOrder::find()->where(['order_status' => '1'])->count();
        $total_rm = PointOrder::find()->where(['order_status' => '1'])->sum('order_total_amount');
        $no_of_item = PointOrder::find()
                ->innerJoinWith(['orderItems'])
                ->where(['order_status' => '1'])
                ->count();
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'count' => $count,
            'no_of_item' => $no_of_item,
            'total_rm' => $total_rm,
        ]);
    }
    
    public function actionIndex2()
    {
        $this->layout = '/datatable';
        $count = PointOrder::find()->where(['order_status' => '1'])->count();
        $total_rm = PointOrder::find()->where(['order_status' => '1'])->sum('order_total_amount');
        $no_of_item = PointOrder::find()
                ->innerJoinWith(['orderItems'])
                ->where(['order_status' => '1'])
                ->count();
        
        
        $query = PointOrder::find()->where(['order_status' => 1])
        ->select('point_order.order_id, point_order.order_dealer_id, point_order.painter_login_id, order_num,point_order.dealer_invoice_no, point_order.dealer_invoice_date, point_order.order_total_point, point_order.order_total_amount, point_order.order_status')  
        ->innerJoinWith([
            'dealerOutlet' => function($que) {
                $que->select(['dealer_list.id','dealer_list.customer_name']);
            },
            'profile' => function($que) {
                $que->select(['painter_profile.user_id','card_id','full_name']);
            },
            'orderStatus' => function($que) {
                //$que->select(['id','username','mobile']);
            }
        ]);
   // get the total number of users
   $count = $query->count();
   //creating the pagination object
   $pagination = new Pagination(['totalCount' => $count, 'defaultPageSize' => 50]);
   //limit the query using the pagination and retrieve the users
   $models = $query->offset($pagination->offset)           
      ->limit($pagination->limit)
      ->all();
   return $this->render('index_u', [
      'models' => $models,
      'pagination' => $pagination,
       'count' => $count,
            'no_of_item' => $no_of_item,
            'total_rm' => $total_rm,
   ]);
        
    }
    public function actionCanceled()
    {
        $searchModel = new PointOrderSearch();
        $searchModel->order_status = 7;
        $dataProvider = $searchModel->searchcancel(Yii::$app->request->queryParams);
        $count = PointOrder::find()->where(['order_status' => '7'])->count();
        return $this->render('canceled', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'count' => $count
        ]);
    }
    
    public function actionSearch()
    {
        set_time_limit(0);
        Yii::$app->cache->flush();
        
        //->where(['id' => [1, 2, 3]])

        /*$query = (new \yii\db\Query())->select(['order_num'])->from('point_order')->limit(10);
$command = $query->createCommand();
$data = $command->queryAll();
echo '<pre>';
print_r($data);
die;*/
        $searchModel = new PointOrderSearch();
        //$dataProvider = $searchModel->search();
        if ($searchModel->load(Yii::$app->request->post())) {
            //$searchModel->clientID = $clientID;
        }else {
            //$searchModel->clientID = 0;
        }
        $dataProvider = $searchModel->searchall(Yii::$app->request->queryParams);
        return $this->render('all_index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    
    public function actionApproved()
    {
        set_time_limit(0);
        Yii::$app->cache->flush();
        $searchModel = new PointOrderSearch();
        $dataProvider = $searchModel->searchapproved(Yii::$app->request->queryParams);
        return $this->render('approved', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionMyindex()
    {
        $searchModel = new PointOrderSearch();
        $searchModel->painter_login_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('painterindex', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
        
    public function actionOnbhalfgroup()
    {
        $searchModel = new PointOrderSearch();
        //$searchModel->order_status = '17';        
        //$searchModel->redemption = 'N';
        $dataProvider = $searchModel->searchbyuser(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=20;
        
        return $this->render('painter_onbehalf_group', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionOnbhalfindex($id)
    {
        $searchModel = new PointOrderSearch();
        $searchModel->order_status = '17';
        $searchModel->redemption = 'N';
        $searchModel->painter_login_id = $id;
        $dataProvider = $searchModel->searchonbhalf(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=100;
        //$modelprofile = $this->findModel($id);
        $modelprofile = \common\models\PainterProfile::find()->where(['user_id' => $id])->one();
        
        return $this->render('painteronbehalf', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelprofile' => $modelprofile,
        ]);
    }
    
    public function actionBulkonbhalfredemption(){
        Yii::$app->cache->flush();
        $action=Yii::$app->request->post('action');
        $selection=(array)Yii::$app->request->post('selection');//typecasting

        foreach($selection as $valueid){
            $this->Bulkonbhalf1($valueid);
        }
        \Yii::$app->getSession()->setFlash('success',['title' => 'Redemption on-behalf', 'text' => 'Action successful!']);
        return $this->redirect(['pointorder/onbhalfgroup']);
        //exit();
    }
    
    protected function Bulkonbhalf1($valueid){
        $painterid = $valueid;
        $selection1 = PointOrder::find()
                ->where('order_status = :order_status', [':order_status' => 17])
                ->andWhere('redemption = :redemption', [':redemption' => 'N'])
                ->andWhere('painter_login_id = :painter_login_id', [':painter_login_id' => $painterid])
                ->asArray()->all();
        foreach($selection1 as $key => $order){
           $selection[] = $order['order_id'];
        }
        $selection_orderid = implode(',', $selection);

        if(count($selection) > 0){
            $msg = 1;
            $sumpoint = '0';
            $sumprice = '0.00';
            
            $verfication = BankingInformation::find()->where(['user_id' => $painterid, 'account_no_verification' => 'Y'])->count();
            
            $redemption = new Redemption();
            $redemption->painterID = $painterid;            
            $redemption->redemption_status = '1';
            $redemption->redemption_status_ray = '1';
            $redemption->orderID = $selection_orderid;
            $redemption->req_points = $sumpoint;
            $redemption->req_amount = $sumprice;            
            if($verfication == 0) {
               $redemption->internel_status = 'not_verified'; 
            }

            $chkinvoiceid = Redemption::find()->count();
            if($chkinvoiceid > 0){
                $newinvoiceid = Redemption::find()->orderBy(['redemption_invoice_no' => SORT_DESC,])->one();
                $incinvoice = str_replace(Yii::$app->params['invoice.prefix'],"",$newinvoiceid->redemption_invoice_no);
                $uniqueinvoice = Redemption::find()->where(['redemptionID' => $incinvoice])->count();
                if($uniqueinvoice > 0){
                    $incinvoice = str_pad(++$incinvoice,6,'0',STR_PAD_LEFT);
                }else {
                    $incinvoice = str_pad(++$incinvoice,6,'0',STR_PAD_LEFT); 
                }                
            }else{
                $incinvoice = Yii::$app->params['invoice.prefix.dft'];
            }

            $redemption->redemption_invoice_no = Yii::$app->params['invoice.prefix'].$incinvoice;
            $redemption->redemption_IP = $redemption->getRealIp();
            if($redemption->save()) {
                foreach($selection as $valueid){
                    $total_per_point = PointOrderItem::find()
                            ->where("painter_login_id = " . $painterid . " AND point_order_id = " . $valueid . " AND Item_status = 'G'")
                            ->sum('total_qty_point');
                    $sumpoint += $total_per_point;
                    
                    $total_per_price = PointOrderItem::find()
                            ->where("painter_login_id = " . $painterid . " AND point_order_id = " . $valueid . " AND Item_status = 'G'")
                            ->sum('total_qty_value');
                    $sumprice += $total_per_price;
                    
                    $redemptionitem = new RedemptionItems();
                    $redemptionitem->redemptionID = $redemption->redemptionID;
                    $redemptionitem->order_id = $valueid;
                    $redemptionitem->req_per_points = $total_per_point;
                    $redemptionitem->req_per_amount = $total_per_price;
                    $redemptionitem->save(false);

                    if($redemptionitem->save(false)){
                        $model = $this->findModel($valueid);
                        $model->redemption = 'Y';
                        $model->save(false);
                    }else{
                        print_r($redemptionitem->getErrors());
                    }
                    $msg++;
                }
                $updateredem = Redemption::find()->where(['redemptionID' => $redemption->redemptionID])->one();
                $updateredem->req_points = $sumpoint;
                $updateredem->req_amount = $sumprice;
                $updateredem->save(false);                
            }
            
            return true;

            /*if($msg > 1){
                \Yii::$app->getSession()->setFlash('success',['title' => 'Redemption on-behalf', 'text' => 'Action successful!']);
                return $this->redirect(['pointorder/onbhalfgroup']);
            }*/
        }
        //exit();
    }
    
    public function actionBulkonbhalf(){       
        $action=Yii::$app->request->post('action');
        $selection=(array)Yii::$app->request->post('selection');//typecasting
        $painterid = Yii::$app->request->post('painterid');
        $selection_orderid = implode(",", $selection);

        if(count($selection) > 0){
            $msg = 1;
            $sumpoint = '0';
            $sumprice = '0.00';
            
            $verfication = BankingInformation::find()->where(['user_id' => $painterid, 'account_no_verification' => 'Y'])->count();
            
            $redemption = new Redemption();
            $redemption->painterID = $painterid;            
            $redemption->redemption_status = '1';
            $redemption->redemption_status_ray = '1';
            $redemption->orderID = $selection_orderid;
            $redemption->req_points = $sumpoint;
            $redemption->req_amount = $sumprice;
            if($verfication == 0) {
               $redemption->internel_status = 'not_verified'; 
            }

            $chkinvoiceid = Redemption::find()->count();
            if($chkinvoiceid > 0){
                $newinvoiceid = Redemption::find()->orderBy(['redemption_invoice_no' => SORT_DESC,])->one();
                $incinvoice = str_replace(Yii::$app->params['invoice.prefix'],"",$newinvoiceid->redemption_invoice_no);
                //$incinvoice = $incinvoice + 1;
                $uniqueinvoice = Redemption::find()->where(['redemptionID' => $incinvoice])->count();
                if($uniqueinvoice > 0){
                    $incinvoice = str_pad(++$incinvoice,6,'0',STR_PAD_LEFT);
                }else {
                    $incinvoice = str_pad(++$incinvoice,6,'0',STR_PAD_LEFT);
                    
                }
                //$incinvoice = str_pad(++$incinvoice,6,'0',STR_PAD_LEFT);
                
            }else{
                $incinvoice = Yii::$app->params['invoice.prefix.dft'];
            }
            
            
            //echo $incinvoice;
            //echo Yii::$app->params['invoice.prefix'].$incinvoice;
            //die;
            $redemption->redemption_invoice_no = Yii::$app->params['invoice.prefix'].$incinvoice;
            $redemption->redemption_IP = $redemption->getRealIp();
            if($redemption->save()) {
                foreach($selection as $valueid){
                    $total_per_point = PointOrderItem::find()
                            ->where("painter_login_id = " . $painterid . " AND point_order_id = " . $valueid . " AND Item_status = 'G'")
                            ->sum('total_qty_point');
                    $sumpoint += $total_per_point;
                    
                    $total_per_price = PointOrderItem::find()
                            ->where("painter_login_id = " . $painterid . " AND point_order_id = " . $valueid . " AND Item_status = 'G'")
                            ->sum('total_qty_value');
                    $sumprice += $total_per_price;
                    
                    $redemptionitem = new RedemptionItems();
                    $redemptionitem->redemptionID = $redemption->redemptionID;
                    $redemptionitem->order_id = $valueid;
                    $redemptionitem->req_per_points = $total_per_point;
                    $redemptionitem->req_per_amount = $total_per_price;
                    $redemptionitem->save(false);

                    if($redemptionitem->save(false)){
                        $model = $this->findModel($valueid);
                        $model->redemption = 'Y';
                        $model->save(false);
                    }else{
                        print_r($redemptionitem->getErrors());
                    }
                    $msg++;
                }
                $updateredem = Redemption::find()->where(['redemptionID' => $redemption->redemptionID])->one();
                $updateredem->req_points = $sumpoint;
                $updateredem->req_amount = $sumprice;
                $updateredem->save(false);                
            }

            if($msg > 1){
                \Yii::$app->getSession()->setFlash('success',['title' => 'Redemption on-behalf', 'text' => 'Action successful!']);
                return $this->redirect(['pointorder/onbhalfgroup']);
            }
        }else{
           \Yii::$app->getSession()->setFlash('warning',['title' => 'Redemption on-behalf', 'text' => 'No items selected!']);
            return $this->redirect(['pointorder/onbhalfgroup']); 
        }
        
    }
    
    public function actionFindpainter()
    {
        $model = new Findpainter();

        if ($model->load(Yii::$app->request->post())) {            
            if ($model->type == 'mid') {
                $chkic = PainterProfile::find()->where(['card_id' => $model->membership_id])->one();
                $count = count($chkic);
                if ($count > 0) {
                    return $this->redirect(['newpoint', 'icno' => $chkic->ic_no]);
                } else {
                    return $this->redirect(['/painter/painterprofile/create', 'icno' => $model->ic_no]);
                }
            } else {
                $chkic = PainterProfile::find()->where(['ic_no' => $model->ic_no])->one();
                $count = count($chkic);

                if ($count > 0) {
                    $approveuser = PainterProfile::find()
                        ->andWhere(['profile_status' => 'A'])
                        ->andWhere(['or',
                            ['card_id' => $model->membership_id],
                            ['ic_no' => $model->ic_no]
                        ])->count();
                    if ($approveuser > 0) {
                        return $this->redirect(['newpoint', 'icno' => $model->ic_no]);
                    } else {
                        $model->showpopup = '2';
                        return $this->render('_form_find_painter', [
                                    'model' => $model,
                        ]);
                    }
                } else {
                    $model->showpopup = '1';
                    return $this->render('_form_find_painter', [
                                'model' => $model,
                    ]);
                }
            }
        } else {
            return $this->render('_form_find_painter', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionNewpoint($icno = null){        
        $chkic = PainterProfile::find()->where(['ic_no' => $icno])->one();
        $count = count($chkic);
        if($count > 0){
            
            $profile = PainterProfile::find()->where(['id' => $chkic->id])->one();
            $pointorder = new PointOrder();
            $pointorderitem = new PointOrderItem();
            $sql = 'SELECT * FROM campaign WHERE campaign_status="A" AND CURDATE() between campaign_start_date AND campaign_end_date';
            $campaign = Campaign::findBySql($sql)->one();
            $campaign_id = '';
            $campaign_rm_per_point = '';
            
            if ($pointorder->load(Yii::$app->request->post())){
                if($pointorder->validate()){
                    $data = Yii::$app->request->post();
                    $totalqty = $data['totalqty'];
                    $scanbarcodes = $data['barcodeid'];
                    $totqty = $data['totqty'];
                    $keys = array_combine($scanbarcodes, $totqty);

                    $total_point = 0;
                    $order_total_amount = 0.00;
                    $total_point_minus = 0;
                    $order_total_amount_minus = 0.00;
                    if(!empty($scanbarcodes)){
                        foreach($keys as $key => $value){
                            //echo $key.'-'.$value.'<br>';
                            //$id = $value;
                            //$barcode = BarCodes::find()->where(['bar_code_id' => $id, 'bar_code_status' => 'A'])->one();
                            //$bar_code_name = $barcode->bar_code_name;
                            //$product_list_id = $barcode->product_list_id;
                            $product_list_id = $key;
                            $productlist = ProductList::find()->where(['product_list_id' => $product_list_id, 'status' => 'A'])->one();
                            /*$chkcode = PointOrderItem::find()->where(['item_bar_code' => $id])->count();
                            if($chkcode == 0){
                                $total_point_minus = $total_point_minus + $productlist->total_points_awarded;
                                $order_total_amount_minus = $order_total_amount_minus + $productlist->total_value_price;
                            }*/
                            
                            $sql = 'SELECT * FROM campaign WHERE campaign_status="A" AND CURDATE() between campaign_start_date AND campaign_end_date';
                            $campaign = Campaign::findBySql($sql)->one();
                            $campaign_id = '';
                            $campaign_rm_per_point = '';
                            $Item_status = 'Good';

                            $pack_size = $productlist->pack_size;
                            $points_per_liter = $productlist->points_per_liter;
                            $per_value_price = $productlist->per_value_price;
                            $total_points_awarded = $productlist->total_points_awarded;
                            $total_value_price = $productlist->total_value_price;

                            if (count($campaign) > 0) {
                                $campaign_id = $campaign->campaign_id;
                                $per_value_price = $campaign->campaign_rm_per_point;
                                $CampaignAssignProductscount = CampaignAssignProducts::find()->where(['campaign_id' => $campaign_id, 'product_list_id' => $product_list_id])->count();
                                if ($CampaignAssignProductscount > 0) {
                                    $CampaignAssignProducts = CampaignAssignProducts::find()->where(['campaign_id' => $campaign_id, 'product_list_id' => $product_list_id])->one();
                                    //$campaign_status = 'Y';
                                    //$points_per_liter = $CampaignAssignProducts->new_points_per_l;
                                    $total_points_awarded = $pack_size * $CampaignAssignProducts->new_points_per_l;
                                    $total_value_price = $per_value_price * $total_points_awarded;

                                    //$qty_point = $total_points_awarded * $value2;
                                    //$qty_value = $total_value_price * $value2;
                                    //$Item_status = 'Campign';
                                }
                            }
                            
                            
                            $total_qty_point = $total_points_awarded * $value;
                            $total_point = $total_point + $total_qty_point;

                            $total_qty_amount = $total_value_price * $value;
                            $order_total_amount = $order_total_amount + $total_qty_amount;
                        }

                        //$numbers = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
                        /*$chkorder_num = PointOrder::find()->count();
                        if($chkorder_num > 0){
                            $order_num = PointOrder::find()
                            ->orderBy(['order_id' => SORT_DESC,])
                            ->one();
                            //$numbers = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
                            $numbers = $order_num->order_num + 1;
                        }else{
                            $numbers = '10000';
                        }*/

                        // Create the Order
                        //$pointorder->order_num = $numbers;
                        $pointorder->order_dealer_id = $pointorder->order_dealer_id;
                        $pointorder->painter_login_id = $profile->user_id;
                        $pointorder->painter_ic = $icno;
                        $pointorder->order_qty = $totalqty;
                        $pointorder->order_total_point = $total_point;
                        $pointorder->order_total_amount = $order_total_amount;
                        $pointorder->order_total_point_mines = $total_point_minus;
                        $pointorder->order_total_amount_mines = $order_total_amount_minus;
                        $pointorder->order_status = '1';
                        $pointorder->order_IP = $pointorder->getRealIp();
                        $pointorder->dealer_invoice_date = date('Y-m-d', strtotime($pointorder->dealer_invoice_date));
                        //$pointorder-> = 
                        $pointorder->save(false);
                        $lastinsid = $pointorder->order_id;
                        // Create the Order Item from the Order
                        //'point_order_id', 'painter_login_id', 'item_bar_code', 'item_bar_point', 'item_bar_total_point', 'item_bar_total_value', 'Item_status', 'Item_IP'
                        //echo $lastinsid;

                        foreach($keys as $key2 => $value2){
                            //$id = $scanbarcode;
                            //$barcode = BarCodes::find()->where(['bar_code_id' => $id, 'bar_code_status' => 'A'])->one();
                            //$sbar_code_name = $barcode->bar_code_name;

                            $product_list_id = $key2;
                            $productlist = ProductList::find()->where(['product_list_id' => $product_list_id, 'status' => 'A'])->one();

                            /*$chkcode = PointOrderItem::find()->where(['item_bar_code' => $id])->count();
                            if($chkcode == 0){
                                $item_bar_total_value = $productlist->total_value_price;
                                $Item_status = 'G';
                            }else{
                                $item_bar_total_value = '0.00';
                                $Item_status = 'U';                            
                            }*/
                            $total_point = $productlist->total_points_awarded * $value2;
                            $qty_point = $total_point;

                            $total_value = $productlist->total_value_price * $value2;
                            $qty_value = $total_value;

                            $Item_status = 'G';
                            $campaign_status = 'N';
                            
                            $pack_size = $productlist->pack_size;
                            $points_per_liter = $productlist->points_per_liter;
                            $per_value_price = $productlist->per_value_price;
                            $total_points_awarded = $productlist->total_points_awarded;
                            $total_value_price = $productlist->total_value_price;
                            if(count($campaign) > 0){
                                $campaign_id = $campaign->campaign_id;
                                $per_value_price = $campaign->campaign_rm_per_point;
                                $CampaignAssignProductscount = CampaignAssignProducts::find()->where(['campaign_id' => $campaign_id,'product_list_id' => $product_list_id])->count();
                                if($CampaignAssignProductscount > 0){
                                    $CampaignAssignProducts = CampaignAssignProducts::find()->where(['campaign_id' => $campaign_id,'product_list_id' => $product_list_id])->one();
                                    $campaign_status = 'Y';
                                    $points_per_liter = $CampaignAssignProducts->new_points_per_l;
                                    $total_points_awarded = $pack_size * $CampaignAssignProducts->new_points_per_l;
                                    $total_value_price = $per_value_price * $total_points_awarded;
                                    
                                    $qty_point = $total_points_awarded * $value2;
                                    $qty_value = $total_value_price * $value2;
                                }   
                            }


                            $pointorderitem = new PointOrderItem();
                            $pointorderitem->point_order_id = $lastinsid;
                            $pointorderitem->painter_login_id = $profile->user_id;
                            $pointorderitem->product_list_id = $key2;
                            $pointorderitem->total_qty = $value2;
                            $pointorderitem->item_pack_size = $pack_size;
                            $pointorderitem->item_bar_point = $points_per_liter;
                            $pointorderitem->item_bar_per_value = $per_value_price;                        
                            $pointorderitem->item_bar_total_point = $total_points_awarded;                        
                            $pointorderitem->item_bar_total_value = $total_value_price;
                            $pointorderitem->total_qty_point = $qty_point;
                            $pointorderitem->total_qty_value = $qty_value;
                            $pointorderitem->Item_status = $Item_status;
                            $pointorderitem->campaign_status = $campaign_status;
                            $pointorderitem->Item_IP = $pointorderitem->getRealIp();
                            $pointorderitem->save(false); 
                            unset($pointorderitem);
                        }
                        \Yii::$app->getSession()->setFlash('success',['title' => 'Point Awarding', 'text' => 'Point orders successfully created!']);
                        $session = Yii::$app->session;
                        if (($session['currentRole'] == Yii::$app->params['role.type.support'])) {
                            return $this->redirect(['/management/pointorder/findpainter']);
                        }else{
                           return $this->redirect(['index']); 
                        }
                    }
                    \Yii::$app->getSession()->setFlash('danger',['title' => 'Point Awarding', 'text' => 'Point orders not successfull!']);
                    return $this->redirect(['index']);
                }else{
                   //print_r($pointorder->getErrors());
                   return $this->render('_form_new_order', [
                        'profile' => $profile,
                        'pointorder' => $pointorder,
                        'pointorderitem' => $pointorderitem,
                    ]);
                }
            }else{            
                return $this->render('_form_new_order', [
                    'profile' => $profile,
                    'pointorder' => $pointorder,
                    'pointorderitem' => $pointorderitem,
                ]);
            }
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        /* return $this->render('_form_new_order', [
          'model' => $model,
          ]); */
    }
    
    public function actionEditpoint($id){
        $pointorder = $this->findModel($id);
        $pointorderitemlistsdel = PointOrderItem::find()->where(['point_order_id' => $id])->all();
        //$pointorderitem = new PointOrderItem();
        //$pointorderitem = PointOrderItem::find()->where(['point_order_id' => $id])->all();
        
        $profile = PainterProfile::find()->where(['user_id' => $pointorder->painter_login_id])->one();
        if ($pointorder->load(Yii::$app->request->post())){
                if($pointorder->validate()){
                    //$pointorder->order_num = $numbers;
                    $data = Yii::$app->request->post();
                    $totalqty = $data['totalqty'];
                    //$scanbarcodes = $data['barcodeid'];
                    //$totqty = $data['totqty'];
                    $total_qty_point = PointOrderItem::find()->where(['point_order_id' => $id])->sum('total_qty_point');
                    $total_qty_value = PointOrderItem::find()->where(['point_order_id' => $id])->sum('total_qty_value');
                    //echo $total_qty_point.'-'.$total_qty_value;
                    //$grandtotal = $data['grandtotal'];
     
                    $pointorder->order_dealer_id = $pointorder->order_dealer_id;
                    //$pointorder->painter_login_id = $profile->user_id;
                    //$pointorder->painter_ic = $icno;
                    $pointorder->order_qty = $totalqty;
                    $pointorder->order_total_amount = $total_qty_value;
                    $pointorder->order_total_point = $total_qty_point;
                    $pointorder->dealer_invoice_no = $pointorder->dealer_invoice_no;
                    $pointorder->dealer_invoice_price = $pointorder->dealer_invoice_price;
                    $pointorder->dealer_invoice_date = date('Y-m-d', strtotime($pointorder->dealer_invoice_date));
                    $pointorder->pay_out_percentage = $pointorder->pay_out_percentage;
                    $pointorder->order_remarks = $pointorder->order_remarks;
                    $pointorder->order_IP = $pointorder->getRealIp();
                    //$pointorder->save();
                    if($pointorder->save()) {
                       \Yii::$app->getSession()->setFlash('success',['title' => 'Point Awarding', 'text' => 'Point orders successfully created!']); 
                    }else {
                        print_r($pointorder->getErrors());
                    }
                    $session = Yii::$app->session;
                    if (($session['currentRole'] == Yii::$app->params['role.type.support'])) {
                        return $this->redirect(['/management/pointorder/findpainter']);
                    }else{
                       return $this->redirect(['index']); 
                    }
                }else{
                    
                   return $this->render('_form_edit_order', [
                        'profile' => $profile,
                        'pointorder' => $pointorder,
                        'pointorderitemlistsdel' => $pointorderitemlistsdel,
                    ]);
                }
                
            }else{ 
                
                return $this->render('_form_edit_order', [
                    'profile' => $profile,
                    'pointorder' => $pointorder,
                    //'pointorderitem' => $pointorderitem,
                    'pointorderitemlistsdel' => $pointorderitemlistsdel,
                ]);
            }
        //$pointorderitem = new PointOrderItem();
        
    }
    
    public function actionDeletepointitem($id){

        $item = $this->findModelorderitem($id);
        $orderid = $item->point_order_id;
        $qty = 1;
        $order_total_point = $item->total_qty_point;
        $order_total_amount = $item->total_qty_value;     
        $model = PointOrder::find()->where(['order_id' => $orderid])->one();

        $pointorder = $this->findModel($orderid);
        $pointorder->order_qty = $model->order_qty - $qty;
        $pointorder->order_total_point = $model->order_total_point - $order_total_point;
        $pointorder->order_total_amount = $model->order_total_amount - $order_total_amount;
        $pointorder->save();
        $this->findModelorderitem($id)->delete();
    }
    
    public function actionDstate($id){
        $dealerlists = DealerList::find()
                ->where(['LIKE', 'area', $id])
                ->andWhere(['=', 'status', 'A'])
                ->orderBy('customer_name ASC')
                ->all();
        if ($dealerlists) {
            //echo $sle;
            echo "<option value=''>-- Select --</option>"; 
            foreach ($dealerlists as $dealerlist) {
                echo "<option value='" . $dealerlist->id . "'>" . $dealerlist->customer_name . "</option>";
            }
        } else {
            echo "<option>-</option>";
        }
    }
    
    public function actionPdescription($id){
        $dealerlists = ProductList::find()
                ->where(['=', 'product_name', $id])
                ->andWhere(['status' => 'A'])
                ->all();
        if ($dealerlists) {
            //echo $sle;
            foreach ($dealerlists as $dealerlist) {
                echo "<option value='" . $dealerlist->product_list_id . "'>".$dealerlist->product_description . "</option>";
            }
        } else {
            echo "<option>-</option>";
        }
        die;
    }
    
    public function actionBrand($id){
        $dealerlists = ProductList::find()
                ->where(['=', 'product_type_id', $id])
                ->andWhere(['status' => 'A'])
                ->groupBy(['product_name'])
                ->all();
        if ($dealerlists) {
            echo "<option value=''>-- Select --</option>"; 
            foreach ($dealerlists as $dealerlist) {
                echo "<option value='" . $dealerlist->product_name . "'>".$dealerlist->product_name . "</option>";
            }
        } else {
            echo "<option>-</option>";
        }
        die;
    }
    
    public function actionGetitem($id=null,$qty=null,$campaignID=null)
    {
        //$response = null;
        if(empty($qty)){
           $qty = '1'; 
        }
        $chkbarcode = ProductList::find()->where(['product_list_id' => $id, 'status' => 'A'])->count();
        if($chkbarcode > 0){
            //$barcode = BarCodes::find()->where(['bar_code_name' => $id, 'bar_code_status' => 'A'])->one();
            //$bar_code_id = $barcode->bar_code_id;
            $product_list_id = $id;
            $productlist = ProductList::find()->where(['product_list_id' => $product_list_id, 'status' => 'A'])->one();
            //$chkcode = PointOrderItem::find()->where(['item_bar_code' => $bar_code_id])->count();
            if(!empty($campaignID)){
                $sql = 'SELECT * FROM campaign WHERE campaign_id = '.$campaignID.' AND campaign_status="A"';
                $campaign = Campaign::findBySql($sql)->one();
            }else {
                $campaign = 0;
            }
            
            $campaign_id = '';
            $campaign_rm_per_point = '';
            $Item_status = 'Good';
            
            $pack_size = $productlist->pack_size;
            $points_per_liter = $productlist->points_per_liter;
            $per_value_price = $productlist->per_value_price;
            $total_points_awarded = $productlist->total_points_awarded;
            $total_value_price = $productlist->total_value_price;
            
            if (!empty($campaignID) && count($campaign) > 0) {                
                $campaign_id = $campaign->campaign_id;
                $per_value_price = $campaign->campaign_rm_per_point;
                $CampaignAssignProductscount = CampaignAssignProducts::find()->where(['campaign_id' => $campaign_id, 'product_list_id' => $product_list_id])->count();
                if ($CampaignAssignProductscount > 0) {
                    $CampaignAssignProducts = CampaignAssignProducts::find()->where(['campaign_id' => $campaign_id, 'product_list_id' => $product_list_id])->one();
                    //$campaign_status = 'Y';
                    //$points_per_liter = $CampaignAssignProducts->new_points_per_l;
                    $total_points_awarded = $pack_size * $CampaignAssignProducts->new_points_per_l;
                    $total_value_price = $per_value_price * $total_points_awarded;

                    //$qty_point = $total_points_awarded * $value2;
                    //$qty_value = $total_value_price * $value2;
                    $Item_status = $campaign->campaign_short_code;
                }
            }

            $response['barcodename'] = $productlist->product_name;
            $response['barcodeid'] = $id;
            $response['product'] = $productlist->product_description;
            $response['qty'] = $qty;
            $response['point'] = $total_points_awarded * $qty;
            $response['value_price'] = $total_value_price * $qty;
            $response['status']       = $Item_status;            
        }else{
           $response = null; 
        }
        echo json_encode($response);
        /*echo Json::encode(array(
            'item_price'=>12, 
        ));*/
    }
    
    public function actionGetitemupdate($id=null,$qty=null,$orderid=null)
    {
        //$response = null;
        if(empty($qty)){
           $qty = '1'; 
        }
        $chkbarcode = ProductList::find()->where(['product_list_id' => $id, 'status' => 'A'])->count();
        if($chkbarcode > 0){
            //$barcode = BarCodes::find()->where(['bar_code_name' => $id, 'bar_code_status' => 'A'])->one();
            //$bar_code_id = $barcode->bar_code_id;
            $product_list_id = $id;
            $productlist = ProductList::find()->where(['product_list_id' => $product_list_id, 'status' => 'A'])->one();
            //$chkcode = PointOrderItem::find()->where(['item_bar_code' => $bar_code_id])->count();
            $model = PointOrder::find()->where(['order_id' => $orderid])->one();
            //echo '<pre>';
            
            $order_total_point = $productlist->total_points_awarded * $qty;
            $order_total_amount = $productlist->total_value_price * $qty;
            
            
            $pointorder = $this->findModel($orderid);
            $pointorder->order_qty = $model->order_qty + 1;
            $pointorder->order_total_point = $model->order_total_point + $order_total_point;
            $pointorder->order_total_amount = $model->order_total_amount + $order_total_amount;
            $pointorder->save();
            //print_r($pointorder->getErrors());
            
            $pointorderitem = new PointOrderItem();
            $pointorderitem->point_order_id = $orderid;
            $pointorderitem->painter_login_id = $model->painter_login_id;
            $pointorderitem->product_list_id = $product_list_id;
            $pointorderitem->total_qty = $qty;
            $pointorderitem->item_pack_size = $productlist->pack_size;
            $pointorderitem->item_bar_point = $productlist->points_per_liter;
            $pointorderitem->item_bar_per_value = $productlist->per_value_price;                        
            $pointorderitem->item_bar_total_point = $productlist->total_points_awarded;                        
            $pointorderitem->item_bar_total_value = $productlist->total_value_price;
            $pointorderitem->total_qty_point = $productlist->total_points_awarded * $qty;
            $pointorderitem->total_qty_value = $productlist->total_value_price * $qty;
            $pointorderitem->Item_status = 'G';
            $pointorderitem->Item_IP = $pointorderitem->getRealIp();
            $pointorderitem->save();
            //print_r($pointorderitem->getErrors());
            //die();
            $Item_status = 'Good';
            $response['barcodename'] = $productlist->product_name;
            $response['barcodeid'] = $id;
            $response['product'] = $productlist->product_description;
            $response['qty'] = $qty;
            $response['point'] = $productlist->total_points_awarded * $qty;
            $response['value_price'] = $productlist->total_value_price * $qty;
            $response['status']       = $Item_status;            
        }else{
           $response = null; 
        }
        echo json_encode($response);
        /*echo Json::encode(array(
            'item_price'=>12, 
        ));*/
    }
    
    public function actionNewpointold($icno = null){        
        $chkic = PainterProfile::find()->where(['ic_no' => $icno])->one();
        $count = count($chkic);
        if($count > 0){
            $profile = PainterProfile::find()->where(['id' => $chkic->id])->one();
            $pointorder = new PointOrder();
            $pointorderitem = new PointOrderItem();
            if ($pointorder->load(Yii::$app->request->post())){
                $data = Yii::$app->request->post();
                $totalqty = $data['totalqty'];
                $scanbarcodes = $data['barcodeid'];
                
                $total_point = 0;
                $order_total_amount = 0.00;
                $total_point_minus = 0;
                $order_total_amount_minus = 0.00;
                if(!empty($scanbarcodes)){
                    foreach($scanbarcodes as $value){
                        $id = $value;
                        $barcode = BarCodes::find()->where(['bar_code_id' => $id, 'bar_code_status' => 'A'])->one();
                        $bar_code_name = $barcode->bar_code_name;
                        $product_list_id = $barcode->product_list_id;
                        $productlist = ProductList::find()->where(['product_list_id' => $product_list_id, 'status' => 'A'])->one();
                        $chkcode = PointOrderItem::find()->where(['item_bar_code' => $id])->count();
                        if($chkcode == 0){
                            $total_point_minus = $total_point_minus + $productlist->total_points_awarded;
                            $order_total_amount_minus = $order_total_amount_minus + $productlist->total_value_price;
                        }
                        $total_point = $total_point + $productlist->total_points_awarded;
                        $order_total_amount = $order_total_amount + $productlist->total_value_price;
                    }

                    $numbers = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
                    $chkcardid = PointOrder::find()->where(['order_num' => $numbers,])->count();
                    if($chkcardid > 0){
                        $numbers = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
                    }
                    echo date('Y-m-d', strtotime($pointorder->dealer_invoice_date));
                    die;
                    // Create the Order
                    $pointorder->order_num = $numbers;
                    $pointorder->order_dealer_id = $pointorder->order_dealer_id;
                    $pointorder->painter_login_id = $profile->user_id;
                    $pointorder->painter_ic = $icno;
                    $pointorder->order_qty = $totalqty;
                    $pointorder->order_total_point = $total_point;
                    $pointorder->order_total_amount = $order_total_amount;
                    $pointorder->order_total_point_mines = $total_point_minus;
                    $pointorder->order_total_amount_mines = $order_total_amount_minus;
                    $pointorder->order_status = '1';
                    $pointorder->order_IP = $pointorder->getRealIp();
                    $pointorder->dealer_invoice_date = date('Y-m-d', strtotime($pointorder->dealer_invoice_date));
                    //$pointorder-> = 
                    $pointorder->save(false);
                    $lastinsid = $pointorder->order_id;
                    // Create the Order Item from the Order
                    //'point_order_id', 'painter_login_id', 'item_bar_code', 'item_bar_point', 'item_bar_total_point', 'item_bar_total_value', 'Item_status', 'Item_IP'
                    //echo $lastinsid;

                    foreach($scanbarcodes as $scanbarcode){
                        $id = $scanbarcode;
                        $barcode = BarCodes::find()->where(['bar_code_id' => $id, 'bar_code_status' => 'A'])->one();
                        $sbar_code_name = $barcode->bar_code_name;

                        $product_list_id = $barcode->product_list_id;
                        $productlist = ProductList::find()->where(['product_list_id' => $product_list_id, 'status' => 'A'])->one();
                        
                        $chkcode = PointOrderItem::find()->where(['item_bar_code' => $id])->count();
                        if($chkcode == 0){
                            $item_bar_total_value = $productlist->total_value_price;
                            $Item_status = 'G';
                        }else{
                            $item_bar_total_value = '0.00';
                            $Item_status = 'U';                            
                        }
                        
                        $pointorderitem = new PointOrderItem();
                        $pointorderitem->point_order_id = $lastinsid;
                        $pointorderitem->painter_login_id = $profile->user_id;
                        $pointorderitem->item_bar_code = $scanbarcode;
                        $pointorderitem->item_bar_code_name = $sbar_code_name;
                        $pointorderitem->item_pack_size = $productlist->pack_size;
                        $pointorderitem->item_bar_point = $productlist->points_per_liter;
                        $pointorderitem->item_bar_total_point = $productlist->total_points_awarded;
                        $pointorderitem->item_bar_per_value = $productlist->per_value_price;
                        $pointorderitem->item_bar_total_value = $item_bar_total_value;
                        $pointorderitem->Item_status = $Item_status;
                        $pointorderitem->Item_IP = $pointorderitem->getRealIp();
                        $pointorderitem->save(false); 
                        unset($pointorderitem);
                    }
                    \Yii::$app->getSession()->setFlash('success',['title' => 'Point Awarding', 'text' => 'Point orders successfully created!']);
                    return $this->redirect(['index']);
                }
                \Yii::$app->getSession()->setFlash('danger',['title' => 'Point Awarding', 'text' => 'Point orders not successfull!']);
                return $this->redirect(['index']);
            }else{            
                return $this->render('_form_new_order', [
                            'profile' => $profile,
                            'pointorder' => $pointorder,
                            'pointorderitem' => $pointorderitem,
                ]);
            }
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        /* return $this->render('_form_new_order', [
          'model' => $model,
          ]); */
    }
    
    public function actionGetitemold($id=null){
        //$response = null;
        $chkbarcode = $barcode = BarCodes::find()->where(['bar_code_name' => $id, 'bar_code_status' => 'A'])->count();
        if($chkbarcode > 0){
            $barcode = BarCodes::find()->where(['bar_code_name' => $id, 'bar_code_status' => 'A'])->one();
            $bar_code_id = $barcode->bar_code_id;
            $product_list_id = $barcode->product_list_id;
            $productlist = ProductList::find()->where(['product_list_id' => $product_list_id, 'status' => 'A'])->one();
            $chkcode = PointOrderItem::find()->where(['item_bar_code' => $bar_code_id])->count();
            if($chkcode == 0){
                $Item_status = 'Good';
            }else{
                $Item_status = 'Used';                            
            }

            $response['barcodename'] = $barcode->bar_code_name;
            $response['barcodeid'] = $bar_code_id;
            $response['product'] = $productlist->product_description;
            $response['point'] = $productlist->total_points_awarded;
            $response['status']       = $Item_status;            
        }else{
           $response = null; 
        }
        echo json_encode($response);
        /*echo Json::encode(array(
            'item_price'=>12, 
        ));*/
    }
    
    

    /**
     * Displays a single PointOrder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionViewviainvoice($id)
    {
        return $this->renderAjax('view_invoice', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionMyview($id)
    {
        //Yii::$app->user->id
        $model = PointOrder::find()->where(['order_id' => $id, 'painter_login_id' => Yii::$app->user->id])->one();
        $count = PointOrder::find()->where(['order_id' => $id, 'painter_login_id' => Yii::$app->user->id])->count();
        if ($count > 0) {
            return $this->render('painterview', [
                'model' => $model,
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        
    }

    /**
     * Creates a new PointOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PointOrder();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->order_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PointOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->order_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionOrderapprove($id=null)
    {
        
        $model = $this->findModel($id);       
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            $painterId = $model->painter_login_id;
            $user = \common\models\User::find()->where(['id' => $painterId])->one();
            $data = \yii\helpers\Json::encode(array(
                'transaction_ID' => $model->order_num,
                'transaction_point' => $model->order_total_point,
                'transaction_rm_value' => $model->order_total_amount,
            ));
            
            if ($model->order_status == '17') {
                $this->NewRedmption($model->order_id,$painterId);
            }

            /*if (!empty($user->email) && $model->order_status == '17') {
                $subject = '';                
                Yii::$app->ici->sendEmailpainter($user->id, $user->email, Yii::$app->params['email.template.code.successful.transactions.approve'], $data, $subject);
                
                $painterId, $mobile, $emailTemplateCode, $data = null
            }*/
            
            if ($model->order_status == '7') {
                $redmitioncheck = RedemptionItems::find()->where(['order_id' => $id])->count();
                if($redmitioncheck > 0){
                    $redmitioncheckItems = RedemptionItems::find()->where(['order_id' => $id])->one();                    
                    $redemption = Redemption::find()->where(['redemptionID' => $redmitioncheckItems->redemptionID])->one();
                    
                    $redemption->req_points = $redemption->req_points - $redmitioncheckItems->req_per_points;
                    $redemption->req_amount = $redemption->req_amount - $redmitioncheckItems->req_per_amount;
                    $redemption->save(false);
                    RedemptionItems::deleteAll('order_id = :order_id', [':order_id' => $id]);
                    
                    $balanceItems = RedemptionItems::find()->where(['redemptionID' => $redmitioncheckItems->redemptionID])->count();
                    if($balanceItems == 0) {
                       $redemption->redemption_status = 7;
                       $redemption->redemption_status_ray = 7;
                       $redemption->ticket = 'close';
                       $redemption->internel_status = null;
                       $redemption->save(false); 
                    }
                    
                    
                }
            }
            $painterinfo = \common\models\PainterProfile::find()
                ->where(['user_id' => $painterId])
                ->one();
            /*if(!empty($painterinfo->mobile) && $model->order_status == '17'){
                $smsqueue = new \common\models\SMSQueue();
                $smsqueue->painterID = $user->id;
                $smsqueue->name = $painterinfo->full_name;
                $smsqueue->mobile = $painterinfo->mobile;
                $smsqueue->redemption_invoice_no = $model->order_num;
                $smsqueue->r_points = $model->order_total_point;
                $smsqueue->r_amount = $model->order_total_amount;
                $smsqueue->data = $data;
                $smsqueue->email_template_id = Yii::$app->params['email.template.code.successful.transactions.approve'];
                $smsqueue->date_to_send = date('Y-m-d');
                $smsqueue->save(false);
                
                //$mobile = $painterinfo->mobile;
                //$result = Yii::$app->ici->sendSMSpainter($user->id, $mobile, Yii::$app->params['email.template.code.successful.transactions.approve'], $data);
            }*/

            if(!empty($painterinfo->mobile) && $model->order_status == '17'){
                $mobile = $painterinfo->mobile;
                $result = Yii::$app->ici->sendSMSpaintersetting($user->id, $mobile, Yii::$app->params['email.template.code.successful.transactions.approve'], $data);
            }
            \Yii::$app->getSession()->setFlash('success',['title' => 'Approval Action', 'text' => 'Action has been updated!']);
            return $this->redirect(['index']);  
        } else {
            return $this->renderAjax('_formpop', [
                'model' => $model,
            ]);
        }
    }
    
    
    public function actionAutoOrderApprove($limit=null)
    {
        /*$count = \common\models\ExcelTransactionItem::find()->where("status = 'pending'")->count();
        if($count > 0){
            return $this->redirect(['auto-order-approve', 'limit' => $limit]);
        }else {
            return $this->redirect(['index']);
        }*/

         foreach (\common\models\ExcelTransactionItem::find()->where("status = 'pending'")->limit($limit)->all() as $myitem) {
            $chekitem = PointOrder::find()->where(['order_num' => $myitem->transaction_num, 'redemption' => 'N'])->count();
            if($chekitem > 0) {
                $chekitem = PointOrder::find()->where(['order_num' => $myitem->transaction_num, 'redemption' => 'N'])->one();
                $id = $chekitem->order_id;
                
                $model = $this->findModel($id);
                $model->order_status = '17';
                
                if($model->save()) {
                    $painterId = $model->painter_login_id;
                    $this->NewRedmption($model->order_id,$painterId);
                    $status = 'success';
                    $comt = '';
                }else {
                    $status = 'fail';
                    $comt = 'DB Error';
                }
            }else {
                $status = 'fail';
                $comt = 'already exit/not found';
            }
            
            $atUpdate = \common\models\ExcelTransactionItem::findOne($myitem->id);
            $atUpdate->status = $status;
            $atUpdate->comment = $comt;
            $atUpdate->save();
            
            echo '<p>'.$myitem->transaction_num.' - '.$status.'</p>';
        }
    }

    /**
     * Deletes an existing PointOrder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionMycampaigns($id,$invdate){
        //echo $id.'-'.$id2;
        $mydate = date('Y-m-d', strtotime($invdate));
        $sql = 'SELECT * FROM campaign WHERE campaign_start_date <= "'.$mydate.'" AND campaign_end_date >= "'.$mydate.'" AND campaign_status="A"';
        //$sql = 'SELECT * FROM campaign WHERE campaign_start_date BETWEEN "'.$mydate.'" AND "'.$mydate.'" AND campaign_status="A"';
        $campaigns = Campaign::findBySql($sql)->all();
        foreach($campaigns as $campaign){
            //$camdate = date('d-m-Y', strtotime($campaign->))
            echo '<option value="'.$campaign->campaign_id.'">'.$campaign->campaign_description.'</option>';
        }
    }
    
    public function actionCampaignscheck($sel_date){
        $mydate = date('Y-m-d', strtotime($sel_date));
        //echo $mydate;
        $sql = 'SELECT * FROM campaign WHERE campaign_start_date <= "'.$mydate.'" AND campaign_end_date >= "'.$mydate.'" AND campaign_status="A"';
        $campaigns = Campaign::findBySql($sql)->count();
        echo $campaigns;
    }
    
    public function actionTestsms(){
        $username = urlencode(Yii::$app->params['sms.username']);
        $password = urlencode(Yii::$app->params['sms.password']);
        $sender_id = urlencode("66300");
        $type = '1';
        $mobile = '+601118889597';
        
        $message = "test abc";
        $message = html_entity_decode($message, ENT_QUOTES, 'utf-8'); 
        $message = urlencode($message);
        
        $fp = "https://www.isms.com.my/isms_send.php";
        $fp .= "?un=$username&pwd=$password&dstno=$mobile&msg=$message&type=$type&sendid=$sender_id";
        $result = Yii::$app->ici->ismscURL($fp);
        echo '<pre>';
        print_r($result);
    }
    
    protected function NewRedmption($valueid,$painterid){
        //New Code
        //$selection = PointOrderItem::find()->where("point_order_id = " . $valueid . " AND Item_status = 'G'")->all();
        $total_per_point = PointOrderItem::find()->where("point_order_id = " . $valueid . " AND Item_status = 'G'")->sum('total_qty_point');
        $sumpoint = $total_per_point;
                    
        $total_per_price = PointOrderItem::find()->where("point_order_id = " . $valueid . " AND Item_status = 'G'")->sum('total_qty_value');
        $sumprice = $total_per_price;
        
        $verfication = BankingInformation::find()->where(['user_id' => $painterid, 'account_no_verification' => 'Y'])->count();
            
        $chkinvoiceid = Redemption::find()->count();
        if($chkinvoiceid > 0){
            $newinvoiceid = Redemption::find()->orderBy(['redemption_invoice_no' => SORT_DESC,])->one();
            $incinvoice = str_replace(Yii::$app->params['invoice.prefix'],"",$newinvoiceid->redemption_invoice_no);
            $uniqueinvoice = Redemption::find()->where(['redemptionID' => $incinvoice])->count();
            if($uniqueinvoice > 0){
                $incinvoice = str_pad(++$incinvoice,6,'0',STR_PAD_LEFT);
            }else {
                $incinvoice = str_pad(++$incinvoice,6,'0',STR_PAD_LEFT); 
            }                
        }else{
            $incinvoice = Yii::$app->params['invoice.prefix.dft'];
        }
        
        $active_redemption = Redemption::find()->where(['painterID' => $painterid, 'ticket' => 'open'])->count();
        if($active_redemption > 0) {
            $redemption = Redemption::find()->where(['painterID' => $painterid, 'ticket' => 'open'])->one();
            $sumpoint = $sumpoint + $redemption->req_points;
            $sumprice = $sumprice + $redemption->req_amount;
            $redemption->orderID = $redemption->orderID.','.$valueid;
        }else {
            $redemption = new Redemption();
            $redemption->painterID = $painterid;            
            $redemption->redemption_status = '1';
            $redemption->redemption_status_ray = '1';
            $redemption->redemption_invoice_no = Yii::$app->params['invoice.prefix'].$incinvoice;
            $redemption->orderID = strval($valueid);
        }
        
        
        
        $redemption->req_points = $sumpoint;
        $redemption->req_amount = $sumprice;
        if($verfication == 0) {
           $redemption->internel_status = 'not_verified'; 
        }
        
        $redemption->redemption_IP = $redemption->getRealIp();
        
        if($redemption->save()) {
            //foreach($selection as $valueid){
            $redemptionitem = new RedemptionItems();
            $redemptionitem->redemptionID = $redemption->redemptionID;
            $redemptionitem->order_id = $valueid;
            $redemptionitem->req_per_points = $total_per_point;
            $redemptionitem->req_per_amount = $total_per_price;
            $redemptionitem->save(false); 
            //}
            
            $model = $this->findModel($valueid);
            $model->redemption = 'Y';
            $model->save(false);
            
            $redem = $this->findModelRedemption($redemption->redemptionID);
            $redem->ticket = 'open';
            $redem->save(false);
            return true;
        }else {
            print_r($redemption->getErrors());
            return false;
        }
    }
    
    /**
     * Finds the PointOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PointOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PointOrder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModelorderitem($id)
    {
        if (($model = PointOrderItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModelRedemption($id)
    {
        if (($model = Redemption::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
