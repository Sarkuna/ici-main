<?php

namespace app\modules\management\controllers;

use Yii;
use common\models\DealerList;
use common\models\DealerListSearch;
use common\models\Region;
use common\models\State;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * DealerlistController implements the CRUD actions for DealerList model.
 */
class DealerlistController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DealerList models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DealerListSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DealerList model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DealerList model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DealerList();

        if ($model->load(Yii::$app->request->post())) {
            $regionID = Region::find()
                ->where(['region_id' => $model->region_id])
                ->one();
            $model->region = $regionID->region_name;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionBulkImportDealer()
    {
        $model = new \common\models\ImportExcelForm();
        $msg = '';

        if ($model->load(Yii::$app->request->post())) {
            $model->excel = UploadedFile::getInstance($model, 'excel');
            if ($model->excel && $model->validate()) {
                $rpath = realpath(Yii::$app->basePath);
                $model->excel->saveAs($rpath.'/web/upload/excelfiles/' . $model->excel->baseName . '.' . $model->excel->extension);

                $inputFile = $rpath.'/web/upload/excelfiles/' . $model->excel->baseName . '.' . $model->excel->extension;
                try{
                    $inputFileType = \PHPExcel_IOFactory::identify($inputFile);
                    $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFile);
                } catch (Exception $e) {
                    die('Error');
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                
                //Bring the heading from excel which top of the row
                $headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,NULL,TRUE,FALSE);
                $myheadings = array_map('strtolower', $headings[0]);
                
                //pre-defined value
                $pre_defined = array( "customer_name", "region_id", "state", "area", "address", "code", "contact_no", "contact_person");
                //check value exit or not
                $checkmatch = count(array_intersect($myheadings, $pre_defined));

                if($checkmatch == 8){
                    $newmsg = '';
                    for ($row = 2; $row <= $highestRow; ++ $row) {
                        $myheadings = array_map('strtolower', $headings[0]);
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $rowData[0] = array_combine($myheadings, $rowData[0]);

                        $customer_name = $rowData[0]['customer_name'];
                        $region_id = trim($rowData[0]['region_id']);
                        //$region = $rowData[0]['region'];
                        $state = $rowData[0]['state'];
                        $area = $rowData[0]['area'];
                        $address = $rowData[0]['address'];
                        $code = trim($rowData[0]['code']);
                        $contact_no = $rowData[0]['contact_no'];
                        $contact_person = $rowData[0]['contact_person'];
                        
                        $count = DealerList::find()
                            ->where(['code' => $code])
                            ->count();
                        
                        if($count == 0) {
                            $model = new DealerList();
                            $newmsg = 'New dealer created';
                        }else {
                            $model = DealerList::find()->where(['code' => $code])->one();
                            $newmsg = 'Existing dealer updated';
                        }
                        
                        $regionID = Region::find()
                            ->where(['region_id' =>$region_id])
                            ->one();
                        
                        $model->customer_name = $customer_name;
                        $model->region_id = $region_id;
                        $model->region = $regionID->region_name;
                        $model->state = $state;
                        $model->area = $area;
                        $model->address = $address;
                        $model->code = $code;
                        $model->contact_no = $contact_no;
                        $model->contact_person = $contact_person;
                        $model->status = 'A';
                        
                        if($model->save()){
                            $msg .= '<li class="list-group-item"><span class="badge pull-right bg-green"><i class="fa fa-check" aria-hidden="true"></i></span>'.$code.' - '.$newmsg.'</li>';
                        }else{
                            $msg .= '<li class="list-group-item"><span class="badge pull-right bg-red"><i class="fa fa-exclamation" aria-hidden="true"></i></span> No action</li>';
                        }
                        

                        //$data[] = [$clientID,$model->point_upload_summary_id,$dealerID,$point1,$point2];
                    }
                }else {
                    echo 'Template format not match!';
                }
                
                unlink($inputFile);
                return $this->render('summary', [
                    'msg' => $msg,
                ]);
            }else{
                print_r($model->getErrors());
            }
        } else {
            //$session['referrer'] = Yii::$app->request->referrer;
            return $this->render('uploadexcel', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DealerList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $regionID = Region::find()
                ->where(['region_id' => $model->region_id])
                ->one();
            $model->region = $regionID->region_name;
            $model->save();
            \Yii::$app->getSession()->setFlash('success',['title' => 'Edit', 'text' => 'Record has been successfully updated.']);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DealerList model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
        $model = $this->findModel($id);
        $model->status = 'X';
        $model->save();
        \Yii::$app->getSession()->setFlash('success',['title' => 'Delete', 'text' => 'Record has been successfully deleted.']);
        return $this->redirect(['index']);
    }

    /**
     * Finds the DealerList model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DealerList the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DealerList::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionDregion($id){
        $states = State::find()
                ->where(['region_id' => $id])
                ->all();
        if ($states) {
            //echo $sle;
            foreach ($states as $state) {
                echo "<option value='" . $state->state_name . "'>" . $state->state_name . "</option>";
            }
        } else {
            echo "<option>-</option>";
        }
    }
}
