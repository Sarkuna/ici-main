<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\painter\models\PainterProfile */

$this->title = $model->full_name;
//$this->params['breadcrumbs'][] = ['label' => 'Painter Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-12">
        <section class="content-header">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">	
                        <i class="fa fa-street-view"></i>  Painter Profile
                        <div class="pull-right">

                        </div>
                    </h2>
                </div><!-- /.col -->
            </div>
        </section>
        

        <section class="content edusec-user-profile">
            <div class="col-lg-4 table-responsive edusec-pf-border no-padding" style="margin-bottom:15px">
                <div class="col-md-12 text-center">
                    <img class="img-circle edusec-img-disp" src="/upload/profile/icon-user-default.jpg" alt="No Image">		<div class="photo-edit">
                        <a class="photo-edit-icon" href="/edusec/index.php?r=student%2Fstu-master%2Fstu-photo&amp;sid=15" title="Change Profile Picture" data-toggle="modal" data-target="#photoup"><i class="fa fa-pencil"></i></a>
                    </div>
                </div>
                <table class="table table-striped">
                    <tbody><tr>
                            <th>Membership No</th>
                            <td><?= $model->card_id ?></td>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <td><?= Html::encode($model->full_name) ?></td>
                        </tr>
                        <tr>
                                <th>Bank Name</th>
                                <td><?= ($bankingInformation->bankName->bank_name) ? $bankingInformation->bankName->bank_name : Yii::$app->params['MsgNull'] ?></td>
                            </tr>
                        <tr>
                            <th>Account Name</th>
                            <td><?= ($bankingInformation->account_name) ? $bankingInformation->account_name : Yii::$app->params['MsgNull'] ?></td>
                        </tr>
                        <tr>
                            <th>Account Number</th>
                            <td><?= ($bankingInformation->account_number) ? $bankingInformation->account_number : Yii::$app->params['MsgNull'] ?></td>
                        </tr>
                        <tr>
                            <th>Email ID</th>
                            <td><?= $user->email ?></td>
                        </tr>
                        <tr>
                            <th>Mobile No</th>
                            <td><?= $model->mobile ?></td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>
                                <?php
                                if($user->status == 'P'){
                                    echo '<span class="label label-warning">Pending</span>';
                                }else if($user->status == 'A'){
                                    echo '<span class="label label-success">Active</span>';
                                }
                                ?>
                            </td>
                        </tr>
                    </tbody></table>
            </div>
            <div class="col-lg-8 profile-data">
                <ul class="nav nav-tabs responsive" id = "profileTab">
                    <li class="active" id = "personal-tab"><a href="#personal" data-toggle="tab"><i class="fa fa-street-view"></i> <?php echo Yii::t('app', 'Personal Details'); ?></a></li>
                    <li id = "family-tab"><a href="#company" data-toggle="tab"><i class="fa fa-bank"></i> <?php echo Yii::t('app', 'Company Information'); ?></a></li>
                    <li id = "fieldofstudy-tab"><a href="#bank" data-toggle="tab"><i class="fa fa-credit-card"></i> <?php echo Yii::t('app', 'Banking Information'); ?></a></li>
                </ul>
                <div id='content' class="tab-content responsive">
                    <div class="tab-pane active" id="personal">
                        <?= $this->render('_tab_personal', ['model' => $model, 'user' => $user]) ?>	
                    </div>
                    <div class="tab-pane" id="company">
                        <?= $this->render('_tab_company', ['model' => $model, 'companyInformation' => $companyInformation]) ?>
                    </div>
                    <div class="tab-pane" id="bank">
                        <?= $this->render('_tab_bank', ['model' => $model, 'bankingInformation' => $bankingInformation,]) ?>
                    </div>
                </div>
            </div>
        </section>        
    </div>
</div>
    <?php $this->registerJs("(function($) {
      fakewaffle.responsiveTabs(['xs', 'sm']);
  })(jQuery);", yii\web\View::POS_END, 'responsive-tab'); ?>
