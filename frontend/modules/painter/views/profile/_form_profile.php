<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\TitleOptions;
use common\models\Race;
use common\models\Banks;
use common\models\Country;
use common\models\DealerList;

/* @var $this yii\web\View */
/* @var $model app\modules\painter\models\PainterProfile */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Update Profile: ' . $model->full_name;
$this->params['breadcrumbs'][] = ['label' => $model->full_name, 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->full_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<style>
.box .box-solid {
     background-color: #F8F8F8;
}
</style>

<div class="col-xs-12">
    <div class="col-lg-10 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></h3></div>
    <div class="col-lg-2 col-sm-2 col-xs-12 no-padding" style="padding-top: 20px !important;">
        <div class="col-xs-12 no-padding">
            <?php //if(Yii::$app->user->can("/painter/painterprofile/create")) { ?>
            <?= Html::a('Back', ['index'], ['class' => 'btn btn-block btn btn-default']) ?>
	<?php //} ?>
        </div>
    </div>
</div>

<div class="col-xs-12 col-lg-12">
    <div class="painter-profile-form">
      <?php $form = ActiveForm::begin(); ?>

        <div class="box-success box view-item col-xs-12 col-lg-12">            
            <div class="box box-solid box-info col-xs-12 col-lg-12 no-padding">
                <div class="box-header with-border">
                    <h4 class="box-title"><i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Personal Details'); ?></h4>
                </div>
                
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                        <div class="col-xs-12 col-sm-4 col-lg-2">
                            <?=
                            $form->field($model, 'title')->dropDownList(
                                    ArrayHelper::map(TitleOptions::find()->all(), 'id', 'title'), ['prompt' => '-- Select --']
                            )
                            ?>                
                        </div>                        
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <?= $form->field($useremail, 'email')->textInput(['maxlength' => true]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>               
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                        <div class="col-xs-12 col-sm-3 col-lg-3">
                            
                            <?=
                                    $form->field($model, 'nationality')->radioList(['L'=>'Malaysian', 'F' =>'Non Malaysian'], [
                                        'prompt' => '-- Select --',
                                        'onchange' => '
                                            var ck = $("input:radio[name=\'PainterProfile[nationality]\']:checked").val()
                                            $.get( "' . Url::toRoute('/painter/profile/pickcountry') . '", {id: ck } )
                                                .done(function( data ) {
                                                    $("select#painterprofile-country").html(data);    
                                                }
                                            );'
                                        ]
                                    )
                                    ?>

                        </div>
                        <div class="col-xs-12 col-sm-3 col-lg-3">
                        <?=
                            $form->field($model, 'country')->dropDownList(
                                    ArrayHelper::map(Country::find()
                                            ->orderBy('name ASC')
                                            ->all(), 'id', 'name'), ['prompt' => '-- Select --']
                            )
                            ?>
                         </div>   
                        <div class="col-xs-12 col-sm-3 col-lg-3">
                            <?= $form->field($model, 'ic_no')->textInput(['maxlength' => true]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-3 col-lg-3">
                            <?=
                            $form->field($model, 'race')->dropDownList(
                                    ArrayHelper::map(Race::find()->all(), 'id', 'title'), ['prompt' => '-- Select --']
                            )
                            ?> 
                        </div>
                    </div>
                </div>
            </div>               
          
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div> 
        </div>
        
      <?php ActiveForm::end(); ?>  
    </div>    
</div>