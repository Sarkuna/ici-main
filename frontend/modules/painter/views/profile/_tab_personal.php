<?php
use yii\helpers\Html;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
  <div class="col-xs-12">
	<h2 class="page-header">	
	<i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Personal Details'); ?>
	<div class="pull-right">
	<?php //if((Yii::$app->user->can("/student/stu-master/update") && ($_REQUEST['id'] == Yii::$app->session->get('stu_id'))) || (in_array("SuperAdmin", $adminUser)) || Yii::$app->user->can("updateAllStuInfo")) { ?>
		<?= Html::a('<i class="fa fa-pencil-square-o"></i> '.Yii::t('app', 'Edit'), ['updateprofile', 'tab' => 'personal'], ['class' => 'btn btn-primary btn-sm', 'id' => 'update-data']) ?>
	<?php //} ?>
	</div>
	</h2>
  </div><!-- /.col -->
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('full_name') ?></div>
        <div class="col-md-9 col-sm-9 col-xs-6 edusec-profile-text"><?= $model->profileTitle->title ?>. <?= ($model->full_name) ? $model->full_name : Yii::t("app", "Not Set") ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-4 col-xs-6 edusec-profile-label"><?= $user->getAttributeLabel('email') ?></div>
        <div class="col-md-9 col-sm-8 col-xs-6 edusec-profile-text"><?=  $user->email ?></div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('dob') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= date('jS F Y', strtotime($model->dob)) ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('mobile') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->mobile ?></div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('ic_no') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->ic_no ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('race') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->profileRace->title ?></div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-4 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('address') ?></div>
        <div class="col-md-9 col-sm-8 col-xs-6 edusec-profile-text"><?= ($model->address) ? $model->address : Yii::t("app", "Not Set") ?></div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('nationality') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->nationality == 'L' ? 'Malaysian' : 'Non Malaysian' ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('country') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->profileCountry->name ?></div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('region_id') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->profileRegion->region_name ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('state_id') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->profileState->state_name ?></div>
        </div>
    </div>
</div>

