<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\date\DatePicker;
use borales\extensions\phoneInput\PhoneInput;

use common\models\TitleOptions;
use common\models\Race;
use common\models\Banks;
use common\models\Country;
use common\models\DealerList;
use common\models\PaintingJobs;
use common\models\Region;
use common\models\State;
use common\models\PicName;
$j_residential = ArrayHelper::map(PaintingJobs::find()->where(["painting_jobs_type" => 'R'])->all(), 'painting_jobs_id', 'painting_jobs_name');
$j_commercial = ArrayHelper::map(PaintingJobs::find()->where(["painting_jobs_type" => 'C'])->all(), 'painting_jobs_id', 'painting_jobs_name');
$d_region = ArrayHelper::map(Region::find()->orderBy('region_id ASC')->all(), 'region_id', 'region_name');
//$d_area = ArrayHelper::map(DealerList::find()->orderBy('area ASC')->groupBy(['area'])->all(), 'area', 'area');
$pic_names = ArrayHelper::map(PicName::find()->orderBy('name ASC')->all(), 'name', 'name');

/* @var $this yii\web\View */
/* @var $model app\modules\painter\models\PainterProfile */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Update Painter Profile: ' . $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Painter Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->full_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
if($model->profile_status == 'A'){
    $readonly = true;
}else{
    $readonly = false;
}
?>
<style>
.box .box-solid {
     background-color: #F8F8F8;
}
</style>

<div class="col-xs-12">
    <div class="col-lg-10 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></h3></div>
    <div class="col-lg-2 col-sm-2 col-xs-12 no-padding" style="padding-top: 20px !important;">
        <div class="col-xs-12 no-padding">
            <?php //if(Yii::$app->user->can("/painter/painterprofile/create")) { ?>
            <?= Html::a('Back', ['view', 'id' => $model->id], ['class' => 'btn btn-block btn btn-default']) ?>
	<?php //} ?>
        </div>
    </div>
</div>

<div class="col-xs-12 col-lg-12">
    <div class="painter-profile-form">
      <?php $form = ActiveForm::begin(); ?>

        <div class="box-success box view-item col-xs-12 col-lg-12">            
            <div class="box box-solid box-info col-xs-12 col-lg-12 no-padding">
                <div class="box-header with-border">
                    <h4 class="box-title"><i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Personal Details'); ?></h4>
                </div>
                
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-lg-7 no-padding">
                        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                            <div class="col-xs-12 col-sm-4 col-lg-4">
                                <?=
                                $form->field($model, 'title')->dropDownList(
                                        ArrayHelper::map(TitleOptions::find()->all(), 'id', 'title'), ['prompt' => '-- Select --']
                                )
                                ?>                
                            </div>
                            <div class="col-xs-12 col-sm-4 col-lg-8">
                                <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>               
                            </div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">                        
                            <div class="col-xs-12 col-sm-4 col-lg-7">
                                <?= $form->field($useremail, 'email', [
        'template' => '{label} <div class=""><div class="col-lg-12 input-group input-group">{input}
        <span class="input-group-addon" id="basic-addon2"></span></div><div class="help-block1 text-danger"></div>{error}{hint}</div>',
    ])->textInput(['autocomplete' => 'off']) ?>
                                
                                <?= 
                            $form->field($model, 'pic_name')->dropDownList($pic_names, ['prompt' => '-- Select --']
                            )
                            ?>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-lg-5">
                                
                                <?php
                                //$model->mobile = '+60'.$model->mobile;
                                echo $form->field($model, 'mobile')->widget(PhoneInput::className(), [
                                    'jsOptions' => [
                                        //'allowExtensions' => true,
                                        //'preferredCountries' => ['MY'],
                                        'onlyCountries' => ['MY'],
                                        'nationalMode' => false,
                                        
                                    ],
                                    'options' => ['placeholder' => '+60XXXXXXXXX', 'class' => 'form-control'],
                                ]);
                                ?>
                            </div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-lg-12">
                            <?= $form->field($model, 'address')->textarea(['rows' => 5, 'cols' => 5]); ?>
                        </div>  
                        
                        
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-lg-5 no-padding">
                        <div class="col-xs-12 col-sm-3 col-lg-12 no-padding">
                            <div class="col-xs-12 col-sm-3 col-lg-8">
                               <?php
                        echo $form->field($model, 'dob')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'Enter birth date ...'],
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'dd-M-yyyy',
                                'endDate' => '-20y',
                                'maxDate' => date('d-m-Y'),
                            ]
                        ]);
                        ?> 
                            </div>
                            
                        </div>
                        <div class="col-xs-12 col-sm-3 col-lg-6">
                            <?= $form->field($model, 'ic_no')->textInput(['maxlength' => true, 'disabled'=>$readonly]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-3 col-lg-6">
                            <?=
                            $form->field($model, 'race')->dropDownList(
                                    ArrayHelper::map(Race::find()->all(), 'id', 'title'), ['prompt' => '-- Select --']
                            )
                            ?> 
                        </div>
                        <div class="col-xs-12 col-sm-3 col-lg-6">
                            <?=
                        $form->field($model, 'region_id')->dropDownList($d_region, [
                            'disabled'=>$readonly,
                            'prompt' => '-- Select --',
                            'onchange' => '$.get( "' . Url::toRoute('/painter/painterprofile/dregion') . '", {id: $(this).val() } )
                                     .done(function( data ) {
                                        $("#painterprofile-state_id").html(data);
                                     }
                                 );'
                                ]
                        )
                        ?> 
                        </div>
                        <div class="col-xs-12 col-sm-3 col-lg-6">
                            <?=
                            //$j_residential = ArrayHelper::map(PaintingJobs::find()->where(["painting_jobs_type" => 'R'])->all(), 'painting_jobs_id', 'painting_jobs_name');    
                            $form->field($model, 'state_id')->dropDownList(
                                    ArrayHelper::map(State::find()
                                                //->where('=', 'region_id', $model->region_id)
                                                ->orderBy('state_id ASC')
                                                ->where(["region_id" => $model->region_id])->all(), 'state_id', 'state_name'), ['disabled'=>$readonly, 'prompt' => '-- Select --']
                            )
                            ?>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                        <div class="col-xs-12 col-sm-3 col-lg-6">
                            <?=
                            $form->field($model, 'nationality')->radioList(['L' => 'Malaysian', 'F' => 'Non Malaysian'], [
                                'prompt' => '-- Select --',
                                'onchange' => '
                                            var ck = $("input:radio[name=\'PainterProfile[nationality]\']:checked").val()
                                            $.get( "' . Url::toRoute('/painter/painterprofile/pickcountry') . '", {id: ck } )
                                                .done(function( data ) {
                                                    $("select#painterprofile-country").html(data);    
                                                }
                                            );'
                                    ]
                            )
                            ?>

                        </div>
                        <div class="col-xs-12 col-sm-3 col-lg-6">
                            <?=
                            $form->field($model, 'country')->dropDownList(
                                    ArrayHelper::map(Country::find()
                                                    ->orderBy('name ASC')
                                                    ->all(), 'id', 'name'), ['prompt' => '-- Select --']
                            )
                            ?>
                        </div>
                        </div>
                    </div>
                </div>
            </div>               
          
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div> 
        </div>
        
      <?php ActiveForm::end(); ?>  
    </div>    
</div>