<?php
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<h2 class="page-header"><i class="fa fa-ruble-sign"></i> Transaction</h2>
<div class="redemption-index table-responsive">
    <?=
    GridView::widget([
        'dataProvider' => $transaction,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'order_id',
            //'order_num',
            [
                'attribute' => 'order_num',
                'label' => 'Transaction #',
                'format' => 'html',
                //'headerOptions' => ['width' => '120', 'class' => 'text-center'],
                'value' => function ($model) {
                    return $model->order_num;
                },
            ],
            //'order_dealer_id',
            [
                'attribute' => 'customer_name',
                'label' => 'Dealer Name',
                'format' => 'html',
                //'headerOptions' => ['width' => '200'],
                'value' => function ($model) {
                    return $model->dealerOutlet->customer_name;
                },
            ],
            [
                'attribute' => 'dealer_invoice_no',
                'label' => 'Invoice#',
                'format' => 'html',
                //'headerOptions' => ['width' => '160'],
                'value' => function ($model) {
                    return $model->dealer_invoice_no;
                },
            ],
            [
                'attribute' => 'dealer_invoice_no',
                'label' => 'Invoice Date',
                'format' => 'html',
                //'headerOptions' => ['width' => '160'],
                'value' => function ($model) {
                    return date('d-M-y', strtotime($model->dealer_invoice_date));
                },
            ],            
            [
                'attribute' => 'total_items',
                'label' => 'Qty',
                'format' => 'html',
                'headerOptions' => ['width' => '60', 'class' => 'text-center'],
                'contentOptions' => ['class' => 'text-right'],
                'value' => function ($model) {
                    return $model->getTotalItems();
                },
            ],
            [
                'attribute' => 'order_total_point',
                'label' => 'Total Point',
                'format' => 'html',
                //'headerOptions' => ['width' => '130', 'class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'value' => function ($model) {
                    return $model->order_total_point;
                },
            ],
            [
                'attribute' => 'order_total_amount',
                'label' => 'Total RM',
                'format' => 'html',
                //'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                'contentOptions' => ['class' => 'text-right'],
                'value' => function ($model) {
                    return $model->order_total_amount;
                },
            ],
            [
                'attribute' => 'order_status',
                'label' => 'Status',
                'format' => 'html',
                //'headerOptions' => ['width' => '120'],
                'value' => function ($model) {
                    return $model->orderStatus->name;
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}', //{update} {delete}
                'buttons' => [
                    'view' => function ($url, $model) {
                        return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                    },
                ],
            ],
        ],
    ]);
    ?>
</div>
