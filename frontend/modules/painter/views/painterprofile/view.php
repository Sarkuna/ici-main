<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\modules\painter\models\PainterProfile */

$this->title = $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Painter Profiles', 'url' => Yii::$app->request->referrer];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>.nav>li>a {padding: 10px 10px;}</style>
<div class="row">
    <div class="col-lg-12">
        <section class="content-header">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header">	
                        <i class="fa fa-street-view"></i>  Painter Profile
                        <div class="pull-right">

                        </div>
                    </h2>
                </div><!-- /.col -->
            </div>
        </section>

        <section class="content edusec-user-profile">

            <div class="col-lg-12 profile-data">
                <ul class="nav nav-tabs responsive" id = "profileTab">
                    <li class="active" id = "personal-tab"><a href="#personal" data-toggle="tab"><i class="fa fa-street-view"></i> <?php echo Yii::t('app', 'Personal Info'); ?></a></li>
                    <li id = "family-tab"><a href="#company" data-toggle="tab"><i class="fas fa-university"></i> <?php echo Yii::t('app', 'Company Info'); ?></a></li>
                    <li id = "fieldofstudy-tab"><a href="#bank" data-toggle="tab"><i class="fas fa-credit-card"></i> <?php echo Yii::t('app', 'Banking Info'); ?></a></li>
                    <li id = "document-tab"><a href="#document" data-toggle="tab"><i class="fas fa-book"></i> <?php echo Yii::t('app', 'Documents'); ?></a></li>
                    <li id = "transaction-tab"><a href="#transaction" data-toggle="tab"><i class="fa fa-ruble-sign"></i> <?php echo Yii::t('app', 'Transaction'); ?></a></li>
                    <li id = "redemption-tab"><a href="#redemption" data-toggle="tab"><i class="fa fa-money-bill-alt"></i> <?php echo Yii::t('app', 'Redemption'); ?></a></li>
                    
                    
                </ul>
                <div id='content' class="tab-content responsive">
                    <div class="tab-pane active" id="personal">
                        <?= $this->render('_tab_personal', ['model' => $model, 'user' => $user, 'bankingInformation' => $bankingInformation]) ?>	
                    </div>
                    <div class="tab-pane" id="company">
                        <?= $this->render('_tab_company', ['model' => $model, 'companyInformation' => $companyInformation]) ?>
                    </div>
                    <div class="tab-pane" id="bank">
                        <?= $this->render('_tab_bank', ['model' => $model, 'bankingInformation' => $bankingInformation,]) ?>
                    </div>
                    <div class="tab-pane" id="document">
                        <?= $this->render('_tab_document_upload', ['model' => $model, 'prrofdocument' => $prrofdocument,]) ?>
                    </div>
                    <div class="tab-pane" id="transaction">
                        <?= $this->render('_tab_transaction', ['transaction' => $transaction,]) ?>
                    </div>
                    <div class="tab-pane" id="redemption">
                        <?= $this->render('_tab_redemption', ['model' => $model, 'searchModel' => $searchModel, 'dataProvider' => $dataProvider,]) ?>
                    </div>
                </div>
            </div>
        </section>        
    </div>
</div>
    <?php $this->registerJs("(function($) {
      fakewaffle.responsiveTabs(['xs', 'sm']);
  })(jQuery);", yii\web\View::POS_END, 'responsive-tab'); ?>
