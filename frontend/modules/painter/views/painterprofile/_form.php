<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\date\DatePicker;
use borales\extensions\phoneInput\PhoneInput;

use common\models\TitleOptions;
use common\models\Race;
use common\models\Banks;
use common\models\Country;
use common\models\DealerList;
use common\models\PaintingJobs;
use common\models\Region;
use common\models\State;
use common\models\PicName;
/* @var $this yii\web\View */
/* @var $model app\modules\painter\models\PainterProfile */
/* @var $form yii\widgets\ActiveForm */
$j_residential = ArrayHelper::map(PaintingJobs::find()->where(["painting_jobs_type" => 'R'])->all(), 'painting_jobs_id', 'painting_jobs_name');
$j_commercial = ArrayHelper::map(PaintingJobs::find()->where(["painting_jobs_type" => 'C'])->all(), 'painting_jobs_id', 'painting_jobs_name');
$d_region = ArrayHelper::map(Region::find()->orderBy('region_id ASC')->all(), 'region_id', 'region_name');
$pic_names = ArrayHelper::map(PicName::find()->orderBy('name ASC')->all(), 'name', 'name');
//$d_area = ArrayHelper::map(DealerList::find()->orderBy('area ASC')->groupBy(['area'])->all(), 'area', 'area');
?>

<script type="text/javascript" src="/themes/adminlte/webcamjs/webcam.js"></script>
<div class="col-xs-12 col-lg-12">
    <div class="painter-profile-form">
      <?php $form = ActiveForm::begin(); ?>

        <div class="box-success box view-item col-xs-12 col-lg-12">            
            <div class="box box-solid box-info col-xs-12 col-lg-12 no-padding">
                <div class="box-header with-border">
                    <h4 class="box-title"><i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Painter Profile Information'); ?></h4>
                </div>
                
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-lg-7 no-padding">
                        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                            <div class="col-xs-12 col-sm-4 col-lg-4">
                                <?=
                                $form->field($model, 'title')->dropDownList(
                                        ArrayHelper::map(TitleOptions::find()->all(), 'id', 'title'), ['prompt' => '-- Select --']
                                )
                                ?>                
                            </div>
                            <div class="col-xs-12 col-sm-4 col-lg-8">
                                <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>               
                            </div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">                        
                            <div class="col-xs-12 col-sm-4 col-lg-7">
                                <?= $form->field($useremail, 'email', [
        'template' => '{label} <div class=""><div class="col-lg-12 input-group input-group">{input}
        <span class="input-group-addon" id="basic-addon2"></span></div><div class="help-block1 text-danger"></div>{error}{hint}</div>',
    ])->textInput(['autocomplete' => 'off']) ?>
                                
                                <?= 
                            $form->field($model, 'pic_name')->dropDownList($pic_names, ['prompt' => '-- Select --']
                            )
                            ?>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-lg-5">
                                
                                <?php
                                echo $form->field($model, 'mobile')->widget(PhoneInput::className(), [
                                    'jsOptions' => [
                                        //'preferredCountries' => ['MY'],
                                        'onlyCountries' => ['MY'],
                                        'nationalMode' => false,
                                    ],
                                    'options' => ['placeholder' => '+60XXXXXXXXX', 'class' => 'form-control',],
                                ]);
                                ?>
                            </div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-lg-12">
                            <?= $form->field($model, 'address')->textarea(['rows' => 5, 'cols' => 5]); ?>
                        </div>  
                        
                        
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-lg-5 no-padding">
                        <div class="col-xs-12 col-sm-3 col-lg-12 no-padding">
                            <div class="col-xs-12 col-sm-3 col-lg-8">
                               <?php
                        echo $form->field($model, 'dob')->widget(DatePicker::classname(), [
                            'options' => ['placeholder' => 'Enter birth date ...'],
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'dd-M-yyyy',
                                'endDate' => '-18y',
                                'maxDate' => date('d-m-Y'),
                            ]
                        ]);
                        ?> 
                            </div>
                            
                        </div>
                        <div class="col-xs-12 col-sm-3 col-lg-6">
                            <?= $form->field($model, 'ic_no')->textInput(['maxlength' => true]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-3 col-lg-6">
                            <?=
                            $form->field($model, 'race')->dropDownList(
                                    ArrayHelper::map(Race::find()->all(), 'id', 'title'), ['prompt' => '-- Select --']
                            )
                            ?> 
                        </div>
                        <div class="col-xs-12 col-sm-3 col-lg-6">
                            <?=
                        $form->field($model, 'region_id')->dropDownList($d_region, [
                            'prompt' => '-- Select --',
                            'onchange' => '$.get( "' . Url::toRoute('/painter/painterprofile/dregion') . '", {id: $(this).val() } )
                                     .done(function( data ) {
                                        $("#painterprofile-state_id").html(data);
                                     }
                                 );'
                                ]
                        )
                        ?> 
                        </div>
                        <div class="col-xs-12 col-sm-3 col-lg-6">
                            <?=
                            $form->field($model, 'state_id')->dropDownList(
                                    ArrayHelper::map(State::find()
                                                    ->orderBy('state_id ASC')
                                                    ->all(), 'state_id', 'state_name'), ['prompt' => '-- Select --']
                            )
                            ?>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                        <div class="col-xs-12 col-sm-3 col-lg-6">
                            <?=
                            $form->field($model, 'nationality')->radioList(['L' => 'Malaysian', 'F' => 'Non Malaysian'], [
                                'prompt' => '-- Select --',
                                'onchange' => '
                                            var ck = $("input:radio[name=\'PainterProfile[nationality]\']:checked").val()
                                            $.get( "' . Url::toRoute('/painter/painterprofile/pickcountry') . '", {id: ck } )
                                                .done(function( data ) {
                                                    $("select#painterprofile-country").html(data);    
                                                }
                                            );'
                                    ]
                            )
                            ?>

                        </div>
                        <div class="col-xs-12 col-sm-3 col-lg-6">
                            <?=
                            $form->field($model, 'country')->dropDownList(
                                    ArrayHelper::map(Country::find()
                                                    ->orderBy('name ASC')
                                                    ->all(), 'id', 'name'), ['prompt' => '-- Select --']
                            )
                            ?>
                        </div>
                        </div>
                    </div>
                </div> <!-- End personal Div -->
            </div>               
          
            <div class="box box-solid box-info col-xs-12 col-lg-12 no-padding">
                <div class="box-header with-border">
                    <h4 class="box-title"><i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Painter Business Information'); ?></h4>
                </div>
                
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-lg-6 no-padding">                        
                        <div class="col-xs-12 col-sm-12 col-lg-12">
                            <?= $form->field($companyInformation, 'company_name')->textInput(['maxlength' => true]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                                <?= $form->field($companyInformation, 'tel')->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
                                    //'mask' => '9999999999999',
                                    'mask' => '9',
                                    'clientOptions' => ['repeat' => 15, 'greedy' => false],
                                    'options' => ['placeholder' => '60XXXXXXXXX', 'class' => 'form-control',],
                                ]) ?>               
                            </div>
                        <div class="col-xs-12 col-sm-12 col-lg-12">
                            <?= $form->field($companyInformation, 'mailing_address')->textarea(['rows' => 5, 'cols' => 5]); ?>
                        </div>                        
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-lg-3 no-padding">
                        <div class="col-xs-12 col-sm-4 col-lg-12">
                            <?= $form->field($companyInformation, 'no_painters')->textInput(['maxlength' => true]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-12">
                            
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-12">
                            <?php echo $form->field($companyInformation, 'residential[]')->checkboxList($j_residential); ?>              
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-lg-3 no-padding">
                        <div class="col-xs-12 col-sm-6 col-lg-12">
                            <?= $form->field($companyInformation, 'painter_sites')->textInput(['maxlength' => true]) ?>               
 
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-12">
                             <?php echo $form->field($companyInformation, 'commercial[]')->checkboxList($j_commercial); ?> 
                        </div>
                    </div>
                    
                </div>
            </div>
            
            <div class="box box-solid box-info col-xs-12 col-lg-12 no-padding">
                <div class="box-header with-border">
                    <h4 class="box-title"><i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Dealer Outlet Information'); ?></h4>
                </div>
                
                <div class="box-body">
                    <div class="col-xs-12 col-sm-6 col-lg-3">
                        <?php
                            $d_region = ArrayHelper::map(DealerList::find()->orderBy('region ASC')->groupBy(['region'])->all(), 'id', 'region');
                            $d_area = ArrayHelper::map(DealerList::find()->orderBy('area ASC')->groupBy(['area'])->all(), 'area', 'area');
                        ?>
                        <?=
                        $form->field($companyInformation, 'd_area')->dropDownList($d_area, [
                            'prompt' => '-- Select --',
                            'onchange' => '$.get( "' . Url::toRoute('/painter/painterprofile/dstate') . '", {id: $(this).val() } )
                                     .done(function( data ) {
                                        $("#companyinformation-dealer_outlet").html(data);
                                     }
                                 );'
                                ]
                        )
                        ?>             
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-3 no-padding">
                        <?php
                        $dealer_outlet = ArrayHelper::map(DealerList::find()->where(['status' => 'A'])->all(), 'id', 'customer_name')
                        ?>
                        <?=
                        $form->field($companyInformation, 'dealer_outlet')->dropDownList($dealer_outlet, [
                            'prompt' => '-- Select --',
                            'onchange' => '$.get( "' . Url::toRoute('/painter/painterprofile/dealeraddress') . '", {id: $(this).val() } )
                                     .done(function( data ) {
                                        $("#dealeraddress").html(data);
                                     }
                                 );'
                                ]
                        )
                        ?> 
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-6 no-padding">
                        <p id="dealeraddress"></p>
                    </div>
                    
                </div>
            </div>
            
            <div class="box box-solid box-info col-xs-12 col-lg-12 no-padding">
                <div class="box-header with-border">
                    <h4 class="box-title"><i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Painter Bank Information'); ?> <small>for redemption purposes</small></h4>
                </div>
                
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <?=
                            $form->field($bankingInformation, 'bank_name')->dropDownList(
                                    ArrayHelper::map(Banks::find()
                                            ->where(['status' => 'A'])
                                            ->all(), 'id', 'bank_name'), ['prompt' => '-- Select --']
                            )
                            ?>                
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <?= $form->field($bankingInformation, 'account_name')->textInput(['maxlength' => true]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-4">
                            <?= $form->field($bankingInformation, 'account_number')->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), [
                                    //'mask' => '9999999999999',
                                    'mask' => '9',
                                    'clientOptions' => ['repeat' => 17, 'greedy' => false],
                                    'options' => ['placeholder' => '123456789012', 'class' => 'form-control',],
                                ]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-3">
                            <?= $form->field($bankingInformation, 'account_ic_no')->textInput(['maxlength' => true]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-3">
                            <label class="control-label" for="account_ic_no"></label>
                            <p><input type="checkbox"> Same as above</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-solid box-info col-xs-12 col-lg-12 no-padding">
                <div class="box-header with-border">
                    <h4 class="box-title"><i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Proof of Document'); ?></h4>
                </div>
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                        <?= $form->field($model, 'files[]')->fileInput(['multiple' => true]) ?>
                        <a href="#myModal" class="btn btn-primary hidden" data-toggle="modal"><i class="fa fa-camera"></i> Camera</a>
                        <?= $form->field($model, 'snapdocument')->hiddenInput()->label(false) ?>
                        
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-12 no-padding multidoc" style="display:none;">
                        <!-- Your captured image will appear here... -->
                    </div>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div> 
        </div>
        
      <?php ActiveForm::end(); ?>  
    </div>    
</div>

<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Camera Snap</h4>
            </div>
            <div class="modal-body">
                <div id="my_camera"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onClick="save_photo()">Take picture</button>
            </div>
        </div>
    </div>
</div>

<script language="JavaScript">
    Webcam.set({
            // live preview size
            width: 700,
            height: 420,

            // device capture size
            dest_width: 640,
            dest_height: 480,

            // final cropped size
            crop_width: 480,
            crop_height: 480,

            // format and quality
            image_format: 'jpeg',
            jpeg_quality: 90,

            // flip horizontal (mirror mode)
            flip_horiz: true
    });

    $('#myModal').on('show.bs.modal', function () {				
      Webcam.attach( '#my_camera' );
    })

    $("#myModal").on("hidden.bs.modal", function () {
            Webcam.reset();
    });
    
    $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
                var acIC = $('#painterprofile-ic_no').val();
                $('#bankinginformation-account_ic_no').val(acIC);
                $('#bankinginformation-account_ic_no').prop('disabled',true);
            }
            else if($(this).prop("checked") == false){
                $('#bankinginformation-account_ic_no').val('');
                $('#bankinginformation-account_ic_no').prop('disabled',false);
            }
        });
</script>

<script language="JavaScript">
    // preload shutter audio clip
    var shutter = new Audio();
    shutter.autoplay = false;
    shutter.src = navigator.userAgent.match(/Firefox/) ? 'shutter.ogg' : 'shutter.mp3';

    function save_photo() {
        Webcam.snap( function(data_uri) {
            var raw_image_data = data_uri.replace(/^data\:image\/\w+\;base64\,/, '');
            var username = raw_image_data;

            $.ajax({
                url: '<?php echo \Yii::$app->getUrlManager()->createUrl('/painter/painterprofile/mycam') ?>',
                type: 'POST',
                dataType: 'json',                
                data: { username: username },
                success: function(data) {
                    //alert(data);
                    //$("#painterprofile-snapdocument").val(data.image_name);
                    var cur_val = $('#painterprofile-snapdocument').val();
                    if(cur_val)
                      $('#painterprofile-snapdocument').val(cur_val + "," + data.image_name);
                    else
                      $('#painterprofile-snapdocument').val(data.image_name);
                },
                error: function(error) {
                    alert('Sorry Somthing went wrong!');
                }        
             });
            //document.getElementById('results').innerHTML = '<img class="img-responsive" src="'+data_uri+'"/>';
            $('.multidoc').append('<div class="col-lg-3"><img class="img-responsive" src="'+data_uri+'"/></div>');
            $('.multidoc').show();
            //document.getElementById('results').style.display = 'block';
            //document.getElementById('results').show();
            $('#myModal').modal('toggle');
            Webcam.reset();

        });
        
    }
    var x_timer;
    $("#useremail-email").keyup(function (e){
        clearTimeout(x_timer);
        var useremail = $(this).val();
        x_timer = setTimeout(function(){
            check_username_ajax(useremail);
        }, 500);
    });
    function check_username_ajax(useremail){
        $("#basic-addon2").html('<i class="fa fa-circle-o-notch fa-spin"></i>');
        $.post('checkemail', {'useremail':useremail}, function(data) {
            if(data == 'no'){
                //$("#basic-addon2").html('<i class="fa fa-close text-danger" aria-hidden="true"></i>');
                //$(".field-useremail-email").addClass('required has-error');
                //This email has already been taken.
                $(".help-block1").html('This '+ useremail +' has already been taken.');
                $("#basic-addon2").html('<i class="fa fa-circle-o-notch fa-spin"></i>');
                $("#useremail-email").val('');
            }else{
                $(".help-block1").html('');
                $("#basic-addon2").html('<i class="fa fa-check text-success" aria-hidden="true"></i>');
                //$(".field-useremail-email").addClass('required has-success');
            }
            
        });
    }
</script>