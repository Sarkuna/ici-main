<?php
use yii\helpers\Html;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
  <div class="col-xs-12">
	<h2 class="page-header">	
	<i class="fas fa-university"></i> <?php echo Yii::t('app', 'Company Information'); ?>
	<div class="pull-right">
	<?php //if((Yii::$app->user->can("/student/stu-master/update") && ($_REQUEST['id'] == Yii::$app->session->get('stu_id'))) || (in_array("SuperAdmin", $adminUser)) || Yii::$app->user->can("updateAllStuInfo")) { ?>
		<?= Html::a('<i class="fa fa-pencil-square-o"></i> '.Yii::t('app', 'Edit'), ['updatecompany', 'id' => $model->id, 'tab' => 'company'], ['class' => 'btn btn-primary btn-sm', 'id' => 'update-data']) ?>
	<?php //} ?>
	</div>
	</h2>
  </div><!-- /.col -->
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-4 col-sm-3 col-xs-6 edusec-profile-label"><?= $companyInformation->getAttributeLabel('company_name') ?></div>
        <div class="col-md-8 col-sm-9 col-xs-6 edusec-profile-text"><?= ($companyInformation->company_name) ? $companyInformation->company_name : Yii::t("app", "Not Set") ?></div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-4 col-xs-6 edusec-profile-label"><?= $companyInformation->getAttributeLabel('tel') ?></div>
        <div class="col-lg-8 col-xs-6 edusec-profile-text"><?= $companyInformation->tel ?></div>
    </div>
    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-4 col-sm-4 col-xs-6 edusec-profile-label"><?= $companyInformation->getAttributeLabel('mailing_address') ?></div>
        <div class="col-md-8 col-sm-8 col-xs-6 edusec-profile-text"><?= ($companyInformation->mailing_address) ? $companyInformation->mailing_address : Yii::t("app", "Not Set") ?></div>
    </div>
    
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-4 col-xs-6 edusec-profile-label"><?= $companyInformation->getAttributeLabel('no_painters') ?></div>
        <div class="col-lg-8 col-xs-6 edusec-profile-text"><?= $companyInformation->no_painters ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-4 col-xs-6 edusec-profile-label"><?= $companyInformation->getAttributeLabel('painter_sites') ?></div>
        <div class="col-lg-8 col-xs-6 edusec-profile-text"><?= $companyInformation->painter_sites ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-4 col-xs-6 edusec-profile-label"><?= $companyInformation->getAttributeLabel('residential') ?></div>
        <div class="col-lg-8 col-xs-6 edusec-profile-text">
            <?php
            //echo $companyInformation->id;
            $residentials = common\models\PaintingJobsPainter::find()
            ->joinWith(['paintingJobs'])
            ->where([
                'painting_jobs_painter.company_information_id' => $companyInformation->id,
                'painting_jobs.painting_jobs_type' => 'R',
                //'condo_unit.condo_id' => $condoId,
            ])        
            //->orderBy('tbl_comments_id.id, tbl_user.id')
            ->all();
            if(count($residentials) > 0) {
                foreach ($residentials as $residential) {
                    echo '<i class="fa fa-check"></i>' . $residential->paintingJobs->painting_jobs_name . '&nbsp;&nbsp;&nbsp;';
                }
            }
            ?>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-4 col-xs-6 edusec-profile-label"><?= $companyInformation->getAttributeLabel('commercial') ?></div>
        <div class="col-lg-8 col-xs-6 edusec-profile-text">
            <?php
            //echo $companyInformation->id;
            $commercials = common\models\PaintingJobsPainter::find()
            ->joinWith(['paintingJobs'])
            ->where([
                'painting_jobs_painter.company_information_id' => $companyInformation->id,
                'painting_jobs.painting_jobs_type' => 'C',
                //'condo_unit.condo_id' => $condoId,
            ])        
            //->orderBy('tbl_comments_id.id, tbl_user.id')
            ->all();
            if(count($commercials) > 0) {
                foreach ($commercials as $commercial) {
                    echo '<i class="fa fa-check"></i>' . $commercial->paintingJobs->painting_jobs_name . '&nbsp;&nbsp;&nbsp;';
                }
            }
            ?>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-12 col-xs-12 edusec-profile-text"></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-4 col-sm-4 col-xs-6 edusec-profile-label"><?= $companyInformation->getAttributeLabel('dealer_outlet') ?></div>
        <div class="col-md-8 col-sm-8 col-xs-6 edusec-profile-text"><?= $companyInformation->dealerOutlet->customer_name ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-4 col-sm-4 col-xs-6 edusec-profile-label">Contact Person</div>
        <div class="col-md-8 col-sm-8 col-xs-6 edusec-profile-text"><?= $companyInformation->dealerOutlet->contact_person ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-4 col-sm-4 col-xs-6 edusec-profile-label">Contact No</div>
        <div class="col-md-8 col-sm-8 col-xs-6 edusec-profile-text"><?= $companyInformation->dealerOutlet->contact_no ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-4 col-sm-4 col-xs-6 edusec-profile-label">Address</div>
        <div class="col-md-8 col-sm-8 col-xs-6 edusec-profile-text"><?= $companyInformation->dealerOutlet->address ?></div>
    </div>

</div>

