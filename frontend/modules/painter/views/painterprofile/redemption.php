<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\RedemptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Redemptions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-12">
<div class="box">
    <div class="box-header with-border">
        <div class="col-lg-12 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?></h3></div>

    </div>
    <div class="box-body table-responsive">
        <div class="redemption-index">
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'redemptionID',
                    'redemption_invoice_no',
                    
                    [
                        'attribute' => 'total_transactions',
                        'label' => 'Total Transactions',
                        'format' => 'html',
                        'contentOptions' =>['class' => 'text-right'],
                        'value' => function ($model) {
                            return $model->getTotalTransactions();
                        },
                    ],  
                    [
                        'attribute' => 'req_points',
                        'label' => 'Total Points',
                        'format' => 'html',
                        'contentOptions' =>['class' => 'text-right'],
                        'value' => function ($model) {
                            return $model->req_points;
                        },
                    ],
                    [
                        'attribute' => 'req_amount',
                        'label' => 'Total RM Amount',
                        'format' => 'html',
                        'contentOptions' =>['class' => 'text-right'],
                        'value' => function ($model) {
                            return $model->req_amount;
                        },
                    ],
                    [
                        'attribute' => 'redemption_status',
                        'label' => 'Status',
                        'format' => 'html',
                        'value' => function ($model) {
                            //$ord =
                            return $model->orderStatus->name;
                        },
                    ],             
                    [
                        'attribute' => 'redemption_created_datetime',
                        //'format' => ['raw', 'Y-m-d H:i:s'],
                        'format' =>  ['date', 'php:d-m-Y h.i A'],
                        'options' => ['width' => '200']
                    ],            
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{redemptionview}', //{update} {delete}
                        'buttons' => [
                            'redemptionview' => function ($url, $model) {
                                return (Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('app', 'View'),]));
                            },
                        ],
                    ],               
                ],
            ]);
            ?>
        </div>
    </div>
</div>
</div>

