<?php
use yii\helpers\Html;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="row">
    <div class="col-xs-12">
        <h2 class="page-header">	
            <i class="fas fa-book"></i> Documents
            <div class="pull-right">
            <?php //if((Yii::$app->user->can("/student/stu-master/update") && ($_REQUEST['id'] == Yii::$app->session->get('stu_id'))) || (in_array("SuperAdmin", $adminUser)) || Yii::$app->user->can("updateAllStuInfo")) { ?>
                    <?= Html::a('<i class="fa fa-pencil-square-o"></i> '.Yii::t('app', 'Upload Documents'), ['uploaddocuments', 'id' => $model->id, 'tab' => 'document'], ['class' => 'btn btn-primary btn-sm', 'id' => 'update-data']) ?>
            <?php //} ?>
            </div>
        </h2>
    </div><!-- /.col -->
</div>

<?php
if (!empty($prrofdocument)) {
    echo '<ul class="mailbox-attachments clearfix">';
    foreach ($prrofdocument as $pdocument) {
        $extensionfile = explode(".", $pdocument->file_name);
        $extension = $extensionfile[1];
        //'pdf','doc','docs','jpg','jpeg','png','bmp'
        if($extension == 'pdf' || $extension == 'PDF'){
            $icon = 'fa-file-pdf';
        }elseif($extension == 'doc' || $extension == 'docx'){
            $icon = 'fa-file-word-o';
        }elseif($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'bmp'){
            $icon = 'fa-file-photo-o';
        }else{
            $icon = '';
        }
        
        $dbfilename = str_replace(" ", "%20", $pdocument->file_name);
        $filename = Yii::$app->request->hostInfo.'/upload/documents/' . $dbfilename;
        $head = get_headers($filename, 1);
        if ($head[0] == 'HTTP/1.1 200 OK') {
           $filename = Yii::$app->request->hostInfo.'/upload/documents/' . $dbfilename;
           $size = $pdocument->getFilesize($filename);
           $del = Html::a('<i class="fa fa-trash-o"></i> '.Yii::t('app', ''), ['deletedocuments', 'id' => $pdocument->upload_id, 'sid' => $model->id, 'tab' => 'document'], ['class' => 'btn btn-default btn-xs pull-left', 'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
            ],'id' => 'update-data']);
        echo '<li>';
        echo '<span class="mailbox-attachment-icon"><i class="fa '.$icon.'"></i></span>';
        echo '<div class="mailbox-attachment-info product-description1">
            <a href="/upload/documents/' . $pdocument->file_name . '" target="_blank" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> '.$pdocument->real_filename.'</a>
            <span class="mailbox-attachment-size text-center">
                '.$size.'
                '.$del.'
                <a href="/upload/documents/' . $pdocument->file_name . '" target="_blank" class="btn btn-default btn-xs pull-right"><i class="fa fa-download"></i></a>
            </span>
        </div>';
        echo '</li>';
           
        }else{
           $filename = '' ;
           $size = '';
        }

        
    }
    echo '</ul>';
    
}


?>