<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

use common\models\TitleOptions;
use common\models\Race;
use common\models\Banks;
use common\models\Country;
use common\models\DealerList;
use common\models\PaintingJobs;

/* @var $this yii\web\View */
/* @var $model app\modules\painter\models\PainterProfile */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Update Painter Profile: ' . $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Painter Profiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->full_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
$j_residential = ArrayHelper::map(PaintingJobs::find()->where(["painting_jobs_type" => 'R'])->all(), 'painting_jobs_id', 'painting_jobs_name');
$j_commercial = ArrayHelper::map(PaintingJobs::find()->where(["painting_jobs_type" => 'C'])->all(), 'painting_jobs_id', 'painting_jobs_name');

?>
<style>
.box .box-solid {
     background-color: #F8F8F8;
}
</style>

<div class="col-xs-12">
    <div class="col-lg-10 col-sm-10 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-edit"></i> <?= Html::encode($this->title) ?></h3></div>
    <div class="col-lg-2 col-sm-2 col-xs-12 no-padding" style="padding-top: 20px !important;">
        <div class="col-xs-12 no-padding">
            <?php //if(Yii::$app->user->can("/painter/painterprofile/create")) { ?>
            <?= Html::a('Back', ['view', 'id' => $model->id], ['class' => 'btn btn-block btn btn-default']) ?>
	<?php //} ?>
        </div>
    </div>
</div>

<div class="col-xs-12 col-lg-12">
    <div class="painter-profile-form">
      <?php $form = ActiveForm::begin(); ?>

        <div class="box-success box view-item col-xs-12 col-lg-12">                      
          
            <div class="box box-solid box-info col-xs-12 col-lg-12 no-padding">
                <div class="box-header with-border">
                    <h4 class="box-title"><i class="fa fa-info-circle"></i> <?php echo Yii::t('app', 'Company Information'); ?></h4>
                </div>
                
                <div class="box-body">
                    <div class="col-xs-12 col-sm-12 col-lg-6 no-padding">                        
                        <div class="col-xs-12 col-sm-12 col-lg-12">
                            <?= $form->field($companyInformation, 'company_name')->textInput(['maxlength' => true]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                                <?= $form->field($companyInformation, 'tel')->textInput(['maxlength' => true]) ?>               
                            </div>
                        <div class="col-xs-12 col-sm-12 col-lg-12">
                            <?= $form->field($companyInformation, 'mailing_address')->textarea(array('rows' => 5, 'cols' => 5, 'style' => 'text-transform:uppercase')); ?>
                        </div>                        
                    </div>

                    <div class="col-xs-12 col-sm-12 col-lg-3 no-padding">
                        <div class="col-xs-12 col-sm-4 col-lg-12">
                            <?= $form->field($companyInformation, 'no_painters')->textInput(['maxlength' => true]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-12">
                            <?= $form->field($companyInformation, 'painter_sites')->textInput(['maxlength' => true]) ?>               
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-12">
                            <?php 
                            $residentials = common\models\PaintingJobsPainter::find()
                            ->joinWith(['paintingJobs'])
                            ->where([
                                'painting_jobs_painter.company_information_id' => $companyInformation->id,
                                'painting_jobs.painting_jobs_type' => 'R',
                                //'condo_unit.condo_id' => $condoId,
                            ])        
                            //->orderBy('tbl_comments_id.id, tbl_user.id')
                            ->all();
                            if(count($residentials) > 0) {
                                foreach ($residentials as $residential) {
                                    $checkedList[] = $residential->paintingJobs->painting_jobs_id;
                                }
                                $companyInformation->residential = $checkedList;
                            }                           
                            echo $form->field($companyInformation, 'residential')->checkboxList($j_residential); ?>              
                        </div>
                        <div class="col-xs-12 col-sm-4 col-lg-12">
                            <?php 
                            //echo $companyInformation->id;
                            $commercials = common\models\PaintingJobsPainter::find()
                            ->joinWith(['paintingJobs'])
                            ->where([
                                'painting_jobs_painter.company_information_id' => $companyInformation->id,
                                'painting_jobs.painting_jobs_type' => 'C',
                            ])->all();
                            if(count($commercials) > 0) {
                                foreach ($commercials as $commercial) {                                    
                                    $checkedCommercial[] = $commercial->paintingJobs->painting_jobs_id;
                                }
                                $companyInformation->commercial = $checkedCommercial;                                
                            }
                            echo $form->field($companyInformation, 'commercial')->checkboxList($j_commercial); ?>              
                        </div>
                        
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-lg-3 no-padding">
                        <div class="col-xs-12 col-sm-6 col-lg-12 hidden">
                            <?php
                                $d_region = ArrayHelper::map(DealerList::find()->orderBy('region ASC')->groupBy(['region'])->all(), 'id', 'region');
                                $d_area = ArrayHelper::map(DealerList::find()->orderBy('area ASC')->groupBy(['area'])->all(), 'area', 'area');
                            ?>
                            <?=
                            $form->field($companyInformation, 'd_region')->dropDownList($d_region, [
                                'prompt' => '-- Select --',
                                'onchange' => ''
                                ]
                            )
                            ?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-12">
                            <?=
                            $form->field($companyInformation, 'd_area')->dropDownList($d_area, [
                                'prompt' => '-- Select --',
                                'onchange' => '$.get( "' . Url::toRoute('/painter/painterprofile/dstate') . '", {id: $(this).val() } )
                                     .done(function( data ) {
                                        $("#companyinformation-dealer_outlet").html(data);
                                     }
                                 );'
                                ]
                            )
                            ?>             
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-12">
                            <?=
                                $form->field($companyInformation, 'dealer_outlet')->dropDownList(
                                        ArrayHelper::map(DealerList::find()->all(), 'id', 'customer_name'), ['prompt' => '-- Select --']
                                )
                                ?>
                        </div>
                    </div>
                    
                </div>
            </div>
            

            <div class="form-group">
                <?= Html::submitButton($companyInformation->isNewRecord ? 'Create' : 'Update', ['class' => $companyInformation->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div> 
        </div>
        
      <?php ActiveForm::end(); ?>  
    </div>    
</div>