<?php
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<h2 class="page-header"><i class="fa fa-money-bill-alt"></i> Redemption</h2>


<div class="redemption-index">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //['class' => 'yii\grid\CheckboxColumn','headerOptions' => ['width' => '10'],],
            //'redemptionID',
            'redemption_invoice_no',
            [
                'attribute' => 'total_transactions',
                'label' => 'Total Transactions',
                'format' => 'html',
                //'headerOptions' => ['width' => '90', 'class' => 'text-center'],
                'contentOptions' => ['class' => 'text-right'],
                'value' => function ($model) {
                    return $model->getTotalTransactions();
                },
            ],
            [
                'attribute' => 'req_points',
                'label' => 'Total Points',
                'format' => 'html',
                //'headerOptions' => ['width' => '60', 'class' => 'text-center'],
                'contentOptions' => ['class' => 'text-right'],
                'value' => function ($model) {
                    return $model->req_points;
                },
            ],
            [
                'attribute' => 'req_amount',
                'label' => 'Total RM Amount',
                'format' => 'html',
                //'headerOptions' => ['width' => '100', 'class' => 'text-center'],
                'contentOptions' => ['class' => 'text-right'],
                'value' => function ($model) {
                    return $model->req_amount;
                },
            ],
            [
                'attribute' => 'redemption_created_datetime',
                'value' => 'redemption_created_datetime',
                //'format' => ['php:D, d-M-Y H:i:s A'],
                //'format' => 'datetime',
                'format' => ['date', 'php:d-m-Y h.i A'],
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'redemption_created_datetime',
                    'convertFormat' => true,
                    'readonly' => true,
                    //'type' => DatePicker::TYPE_BUTTON,
                    //'displayFormat' => 'php:D, d-M-Y H:i:s A',
                    'pluginOptions' => [
                        'format' => 'yyyy-M-dd'
                    //'format' => 'dd-M-yyyy'
                    ],
                ]),
                'options' => ['width' => '150']
            ],
            [
                'attribute' => 'redemption_status',
                'label' => 'Status',
                'value' => function ($model) {
                    return $model->orderStatus->name;
                },
                //'options' => ['width' => '50'],
            ],
            [
                'attribute' => 'internel_status',
                'label' => 'Payment Status',
                'value' => function ($model) {
                    if ($model->redemption_status == 17 && !empty($model->internel_status)) {
                        return $model->internel_status;
                    } else {
                        return 'N/A';
                    }
                },
                //'options' => ['width' => '50'],
            ],
            [
                'attribute' => 'action',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getActions();
                },
                //'options' => ['width' => '50'],
            ],
        ],
    ]);
    ?>
</div>