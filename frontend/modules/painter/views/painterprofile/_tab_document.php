<?php
use yii\helpers\Html;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<h2 class="page-header"><i class="fa fa-info-circle"></i> Uploaded Document</h2>
<?php
if (!empty($prrofdocument)) {
    echo '<ul class="mailbox-attachments clearfix">';
    foreach ($prrofdocument as $pdocument) {
        $extensionfile = explode(".", $pdocument->file_name);
        $extension = $extensionfile[1];
        //'pdf','doc','docs','jpg','jpeg','png','bmp'
        if($extension == 'pdf' || $extension == 'PDF'){
            $icon = 'fa-file-pdf';
        }elseif($extension == 'doc' || $extension == 'docx'){
            $icon = 'fa-file-word-o';
        }elseif($extension == 'jpg' || $extension == 'jpeg' || $extension == 'png' || $extension == 'bmp'){
            $icon = 'fa-image';
        }else{
            $icon = '';
        }
        
        $dbfilename = str_replace(" ", "%20", $pdocument->file_name);
        $filename = Yii::$app->request->hostInfo.'/upload/documents/' . $dbfilename;
        $head = get_headers($filename, 1);
        if ($head[0] == 'HTTP/1.1 200 OK') {
           $filename = Yii::$app->request->hostInfo.'/upload/documents/' . $dbfilename;
           $size = $pdocument->getFilesize($filename);
           $del = Html::a('<i class="fa fa-trash-o"></i> '.Yii::t('app', ''), ['deletedocuments', 'id' => $pdocument->upload_id, 'sid' => $model->id, 'tab' => 'document'], ['class' => 'btn btn-default btn-xs pull-left', 'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
            ],'id' => 'update-data']);
        echo '<li>';
        echo '<span class="mailbox-attachment-icon"><i class="fa '.$icon.'"></i></span>';
        echo '<div class="mailbox-attachment-info product-description1">
            <a href="/upload/documents/' . $pdocument->file_name . '" target="_blank" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> '.$pdocument->real_filename.'</a>
            <span class="mailbox-attachment-size text-center">
                '.$size.'
                '.$del.'
                <a href="/upload/documents/' . $pdocument->file_name . '" target="_blank" class="btn btn-default btn-xs pull-right"><i class="fa fa-download"></i></a>
            </span>
        </div>';
        echo '</li>';
           
        }else{
           $filename = '' ;
           $size = '';
        }

        
    }
    echo '</ul>';
    
}


?>

<?= $this->render('_form_approve', ['model' => $model, 'formapprove' => $formapprove,]) ?>

