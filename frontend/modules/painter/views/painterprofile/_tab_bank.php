<?php
use yii\helpers\Html;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
  <div class="col-xs-12">
	<h2 class="page-header">	
	<i class="fas fa-credit-card"></i> <?php echo Yii::t('app', 'Bank Information'); ?>
	<div class="pull-right">
	<?php //if((Yii::$app->user->can("/student/stu-master/update") && ($_REQUEST['id'] == Yii::$app->session->get('stu_id'))) || (in_array("SuperAdmin", $adminUser)) || Yii::$app->user->can("updateAllStuInfo")) { ?>
		<?= Html::a('<i class="fa fa-pencil-square-o"></i> '.Yii::t('app', 'Edit'), ['updatebank', 'id' => $model->id, 'tab' => 'bank'], ['class' => 'btn btn-primary btn-sm', 'id' => 'update-data']) ?>
	<?php //} ?>
	</div>
	</h2>
  </div><!-- /.col -->
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 edusec-profile-label"><?= $bankingInformation->getAttributeLabel('bank_name') ?></div>
        <div class="col-md-9 col-sm-9 col-xs-6 edusec-profile-text"><?= ($bankingInformation->bankName->bank_name) ? $bankingInformation->bankName->bank_name : Yii::$app->params['MsgNull'] ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 edusec-profile-label"><?= $bankingInformation->getAttributeLabel('account_name') ?></div>
        <div class="col-md-9 col-sm-9 col-xs-6 edusec-profile-text"><?= ($bankingInformation->account_name) ? $bankingInformation->account_name : Yii::$app->params['MsgNull'] ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 edusec-profile-label"><?= $bankingInformation->getAttributeLabel('account_number') ?></div>
        <div class="col-md-9 col-sm-9 col-xs-6 edusec-profile-text"><?= ($bankingInformation->account_number) ? $bankingInformation->account_number : Yii::$app->params['MsgNull'] ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 edusec-profile-label"><?= $bankingInformation->getAttributeLabel('account_ic_no') ?></div>
        <div class="col-md-9 col-sm-9 col-xs-6 edusec-profile-text"><?= ($bankingInformation->account_ic_no) ? $bankingInformation->account_ic_no : Yii::$app->params['MsgNull'] ?></div>
    </div> 
</div>

