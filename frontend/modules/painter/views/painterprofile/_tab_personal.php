<?php
use yii\helpers\Html;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
  <div class="col-xs-12">
	<h2 class="page-header">	
	<i class="fa fa-street-view"></i> <?php echo Yii::t('app', 'Painter Profile Information'); ?>
	<div class="pull-right">
	<?php //if((Yii::$app->user->can("/student/stu-master/update") && ($_REQUEST['id'] == Yii::$app->session->get('stu_id'))) || (in_array("SuperAdmin", $adminUser)) || Yii::$app->user->can("updateAllStuInfo")) { ?>
		<?= Html::a('<i class="fa fa-pencil-square-o"></i> '.Yii::t('app', 'Edit'), ['updateprofile', 'id' => $model->id, 'tab' => 'personal'], ['class' => 'btn btn-primary btn-sm', 'id' => 'update-data']) ?>
	<?php //} ?>
	</div>
	</h2>
  </div><!-- /.col -->
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    <table class="table table-striped">
        <tbody><tr>
                <th>Membership No</th>
                <td><?= $model->card_id ?></td>
            </tr>
            <tr>
                <th>Name</th>
                <td><?= Html::encode($model->full_name) ?></td>
            </tr>
            <tr>
                <th>PIC Name</th>
                <td><?= ($model->pic_name) ? $model->pic_name : Yii::$app->params['MsgNull'] ?></td>
            </tr>
            <tr>
                <th>Bank Name</th>
                <td><?= ($bankingInformation->bankName->bank_name) ? $bankingInformation->bankName->bank_name : Yii::$app->params['MsgNull'] ?></td>
            </tr>
            <tr>
                <th>Account Name</th>
                <td><?= ($bankingInformation->account_name) ? $bankingInformation->account_name : Yii::$app->params['MsgNull'] ?></td>
            </tr>
            <tr>
                <th>Account Number</th>
                <?php
                if ($bankingInformation->account_no_verification == 'Y') {
                    $verify = '<i class="fa fa-check text-green" aria-hidden="true"> Verified</i>';
                } else {
                    $verify = '<i class="fa fa-ban text-red" aria-hidden="true"> Not Verified</i>';
                }
                ?>
                <td><?= ($bankingInformation->account_number) ? $bankingInformation->account_number : Yii::$app->params['MsgNull'] ?> <?= $verify ?></td>
            </tr>
            <tr>
                <th>Email ID</th>
                <td><?= $user->email ?></td>
            </tr>
            <tr>
                <th>Mobile No</th>
                <td><?= $model->mobile ?></td>
            </tr>
            <tr>
                <th>Status</th>
                <td>
                    <?php
                    if ($model->profile_status == 'P') {
                        echo '<span class="label label-warning">Pending</span>';
                    } else if ($model->profile_status == 'R') {
                        echo '<span class="label label-info">Review</span>';
                    } else if ($model->profile_status == 'D') {
                        echo '<span class="label label-danger">Decline</span>';
                    } else if ($model->profile_status == 'A') {
                        echo '<span class="label label-success">Active</span>';
                    }
                    ?>
                </td>
            </tr>
        </tbody>
    </table>
</div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-3 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('full_name') ?></div>
        <div class="col-md-9 col-sm-9 col-xs-6 edusec-profile-text"><?= $model->profileTitle->title ?>. <?= ($model->full_name) ? $model->full_name : Yii::t("app", "Not Set") ?></div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-4 col-xs-6 edusec-profile-label"><?= $user->getAttributeLabel('email') ?></div>
        <div class="col-md-9 col-sm-8 col-xs-6 edusec-profile-text"><?=  $user->email ?></div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('dob') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= date('jS F Y', strtotime($model->dob)) ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('mobile') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->mobile ?></div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('ic_no') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->ic_no ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('race') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->profileRace->title ?></div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="col-md-3 col-sm-4 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('address') ?></div>
        <div class="col-md-9 col-sm-8 col-xs-6 edusec-profile-text"><?= ($model->address) ? $model->address : Yii::t("app", "Not Set") ?></div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('nationality') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->nationality == 'L' ? 'Malaysian' : 'Non Malaysian' ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('country') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->profileCountry->name ?></div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12 col-sm-12">
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding edusec-bg-row">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('region_id') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->profileRegion->region_name ?></div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 no-padding">
            <div class="col-lg-6 col-xs-6 edusec-profile-label"><?= $model->getAttributeLabel('state_id') ?></div>
            <div class="col-lg-6 col-xs-6 edusec-profile-text"><?= $model->profileState->state_name ?></div>
        </div>
    </div>
</div>

