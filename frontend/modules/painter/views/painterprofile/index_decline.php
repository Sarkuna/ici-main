<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\painter\models\PainterProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Painter Account Management';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-xs-12">
    <div class="box">
        <div class="box-header with-border">
            <div class="col-lg-4 col-sm-4 col-xs-12 no-padding"><h3 class="box-title"><i class="fa fa-th-list"></i> <?= Html::encode($this->title) ?> <span class="label label-danger"> Decline</span></h3></div>
            <div class="col-xs-6"></div>
            <div class="col-lg-2 col-sm-2 col-xs-12 no-padding">
                <div class="col-xs-12 no-padding">
                    <?php if (Yii::$app->user->can("/painter/painterprofile/create")) { ?>
                        <?= Html::a('Register New Painter', ['create'], ['class' => 'btn btn-block btn btn-success']) ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="box-body table-responsive">
            <div class="painter-profile-index">
                <?=
                GridView::widget([
                    'tableOptions' => ['class' => 'table table-bordered table-hover'],
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'card_id',
                        'full_name',
                        [
                            'attribute' => 'ic_no',
                            'label' => 'NRIC/PP Number',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->ic_no;
                            }
                        ],
                        
                        [
                            'attribute' => 'created_datetime',
                            'label' => 'Submitted Date',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if ($model->created_datetime != '0000-00-00 00:00:00') {
                                    $dateTime = date('d-m-Y h:i A', strtotime($model->created_datetime));
                                    return $dateTime;
                                } else {
                                    return 'N/A';
                                }
                            }
                        ],
                        [
                            'attribute' => 'action',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->getActions();
                            },
                            'options' => ['width' => '100']
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>

<?php
	yii\bootstrap\Modal::begin([
		'id' => 'guardModal',
		'header' => "<h4 class='modal-title'><i class='fa fa-edit'></i> Update </h4>",
                'id' => 'viewremarkModal',
		'header' => "<h4 class='modal-title'><i class='fa fa-file-o'></i> Remark </h4>",
	]);
 	yii\bootstrap\Modal::end(); 
?>

<script>
/***
  * Start Update Gardian Jquery
***/
function updateGuard(id) {
	$.ajax({
	  type:'GET',
	  url:'<?= Url::toRoute(["/support/membershippack/send-membership-pack"]) ?>',
	  data: { id : id},
	  success: function(data)
		   {
		       $(".modal-content").addClass("row");
		       $('.modal-body').html(data);
		       $('#guardModal').modal();

		   }
	});
}

/***
  * Start Update Gardian Jquery
***/
function viewremark(id) {
	$.ajax({
	  type:'GET',
	  url:'<?= Url::toRoute(["/painter/painterprofile/viewremark"]) ?>',
	  data: { id : id},
	  success: function(data)
		   {
		       $(".modal-content").addClass("row");
		       $('.modal-body').html(data);
		       $('#viewremarkModal').modal();

		   }
	});
}
</script>