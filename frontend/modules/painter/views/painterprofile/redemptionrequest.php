<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\painter\models\PainterProfile */

$this->title = 'Redemption Request';
$this->params['breadcrumbs'][] = ['label' => 'Redemption', 'url' => ['redemption']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="painter-profile-create">
    <?= $this->render('_form_redemption_request', [
        'model' => $model,
        'myorders' => $myorders,
]) ?>

</div>