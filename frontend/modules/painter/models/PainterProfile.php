<?php

namespace app\modules\painter\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use yii\helpers\Html;
use borales\extensions\phoneInput\PhoneInputValidator;
//use borales\extensions\phoneInput\PhoneInputBehavior;

/**
 * This is the model class for table "painter_profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $card_id
 * @property string $photo
 * @property integer $title
 * @property string $full_name
 * @property string $profile_name
 * @property string $mobile
 * @property integer $nationality
 * @property string $ic_no
 * @property integer $race
 * @property integer $pic_name
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class PainterProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 'X';
    const STATUS_ACTIVE = 'A';
    const STATUS_PENDING = 'P';
    public $status;
    public $snapdocument;
    public $files;
  
    
    public static function tableName()
    {
        return 'painter_profile';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),                
            ],
            BlameableBehavior::className(),
            //'phoneInput' => PhoneInputBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'full_name', 'nationality', 'country', 'race','address','region_id','state_id','dob'], 'required'],
            [['user_id', 'title', 'country', 'race', 'created_by', 'updated_by'], 'integer'],
            [['card_num', 'card_id','pic_name'], 'string'],            
            ['ic_no', 'required'],
            ['ic_no', 'unique', 'message' => 'This {attribute} has already been taken.'],
            [['mobile'], 'string', 'max' => 20],
            ['mobile', 'required'],
            //[['mobile'], PhoneInputValidator::className(), 'region' => 'MY'],
            //[['email'], 'email'],
            //['email', 'required'],
            //['email', 'unique', 'message' => 'This {attribute} has already been taken.'],
            //[['pic_name'], 'string', 'max' => 40],
            [['dob','snapdocument','created_datetime', 'updated_datetime','pic_name'], 'safe'],
            [['photo'], 'string', 'max' => 200],            
            //[['files'], 'file', 'maxFiles' => 4],
            [['files'], 'file', 'maxFiles' => 5, 'skipOnEmpty' => true, 'extensions'=>['pdf','doc','docx','jpg','jpeg','png','bmp'], 'checkExtensionByMimeType'=>false],
            [['nationality','profile_status'], 'string', 'max' => 2],
            [['full_name', 'email'], 'string', 'max' => 255],            
            [['profile_name', 'ic_no'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'card_id' => 'Membership No',
            'photo' => 'Photo',
            'title' => 'Title',
            'full_name' => 'Full Name',
            'profile_name' => 'Profile Name',
            'dob' => 'Date of Birth',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'address' => 'Address',
            'region_id' => 'Region',
            'state_id' => 'State',
            'nationality' => 'Nationality',
            'country' => 'Country',
            'ic_no' => 'NRIC/Passport Number',
            'race' => 'Race',
            'profile_status' => 'Profile Status',
            'pic_name' => 'PIC Name',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function afterFind() {
        parent::afterFind();
        $this->full_name = strtoupper($this->full_name);
    }
    
    public function getUser() {
        return $this->hasOne(\common\models\User::className(), ['id' => 'user_id'])->
       andWhere(['status' =>! self::STATUS_DELETED]);
    }
    
    public function getProfileTitle()
    {
        return $this->hasOne(\common\models\TitleOptions::className(), ['id' => 'title']);
    }
    
    public function getProfileCountry()
    {
        return $this->hasOne(\common\models\Country::className(), ['id' => 'country']);
    }
    
    public function getProfileRace()
    {
        return $this->hasOne(\common\models\Race::className(), ['id' => 'race']);
    }
    
    public function getProfileRegion()
    {
        return $this->hasOne(\common\models\Region::className(), ['region_id' => 'region_id']);
    }
    
    public function getProfileState()
    {
        return $this->hasOne(\common\models\State::className(), ['state_id' => 'state_id']);
    }
        
    public function getStatusDescription() {
        $returnValue = "";
        if ($this->status == "X") {
            $returnValue = "Deleted";
        } else if ($this->status == "P") {
            $returnValue = "<span class='label label-warning'>Pending</span>";
        } else if ($this->status == "A") {
            $returnValue = "Active";
        }
        return $returnValue;
    }
    
    public function getStatusProfile() {
        $returnValue = "";
        if ($this->profile_status == "P") {
            $returnValue = "<span class='label label-warning'>Pending</span>";
        } else if ($this->profile_status == "R") {
            $returnValue = '<a href="javascript:;" onclick="viewremark('. $this->id .');return false;"><span class="label label-info">Review</span></a>';
        } else if ($this->profile_status == "D") {
            $returnValue = "<span class='label label-danger'>Decline</span>";
        } else if ($this->profile_status == "A") {
            $returnValue = "<span class='label label-success'>Approve</span>";
        } 
        return $returnValue;
    }
    
    public function getActions() {
        $returnValue = "";
        $returnValue = $returnValue . '<a href="' . Url::to(['/painter/painterprofile/view/','id' => $this->id]) . '" title="View Profile" ><i class="fa fa-fw fa-eye"></i></a>';
        //if ($this->profile_status == "P" || $this->profile_status == "R") {
            $returnValue = $returnValue . '<a href="' . Url::to(['/painter/painterprofile/viewapprove/','id' => $this->id]) . '" title="Activate Profile" ><i class="fa fa-fw fa-check"></i></a>';
        //}
        if($this->profile_status == "A"){
            $returnValue = $returnValue . '<a href="' . Url::to(['/painter/painterprofile/sendpassword/','id' => $this->id]) . '" title="Resend password" ><i class="fa fa-fw fa-envelope"></i></a>';
        }
        //['class' => 'btn btn-success', 'data-toggle' => 'modal', 'data-target' => '#myModal']
        //$returnValue = $returnValue . '<a href="' . Url::to(['/support/membershippack/update/','id' => $this->user_id]) . '" title="Activate Profile" onclick = "updateGuard('.$this->user_id.');return false;"><i class="fa fa-fw fa-briefcase"></i></a>';
        return $returnValue;
    }
}