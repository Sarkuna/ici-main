<?php

namespace app\modules\painter\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\base\Model;

class RedeemForm extends Model
{
    public $amount;
    public $point;
    public $type;
    
    public function rules()
    {
        return [
            ['amount', 'required'],
            ['type', 'required']
            //['email', 'unique', 'message' => 'This {attribute} has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'amount' => 'Amount',
            'Point' => 'Total Points',
        ];
    }

}
