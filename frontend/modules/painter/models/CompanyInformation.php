<?php

namespace app\modules\painter\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "company_information".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $company_name
 * @property string $mailing_address
 * @property string $tel
 * @property integer $no_painters
 * @property integer $dealer_outlet
 * @property integer $painter_sites
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class CompanyInformation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $d_region;
    public $d_state;
    public $d_area;
    public $residential;
    public $commercial;
    public static function tableName()
    {
        return 'company_information';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dealer_outlet'], 'required'],
            //[['company_name', 'mailing_address', 'no_painters', 'dealer_outlet'], 'required'],
            [['user_id', 'no_painters', 'dealer_outlet', 'painter_sites', 'created_by', 'updated_by'], 'integer'],
            [['created_datetime', 'updated_datetime','d_region','d_state','d_area','residential','commercial'], 'safe'],
            [['company_name'], 'string', 'max' => 150],
            [['mailing_address'], 'string', 'max' => 250],
            [['tel'], 'string', 'max' => 20],
        ];
    }
    
    public function getDealerOutlet()
    {
        return $this->hasOne(\common\models\DealerList::className(), ['id' => 'dealer_outlet']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'company_name' => 'Company Name',
            'mailing_address' => 'Company Mailing Address',
            'tel' => 'Company Tel. No',
            'no_painters' => 'No. of Employees',
            'dealer_outlet' => 'Registered at Dealer Outlet',
            'painter_sites' => 'No. of painting jobs per month',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'd_region' => 'Region',
            'd_state' => 'State',
            'd_area' => 'Area',
            'residential' => 'Residential',
            'commercial' => 'Commercial',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
