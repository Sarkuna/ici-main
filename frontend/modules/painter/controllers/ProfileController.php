<?php

namespace app\modules\painter\controllers;

use Yii;
use app\modules\painter\models\PainterProfile;
use app\modules\painter\models\PainterProfileSearch;
use app\modules\painter\models\CompanyInformation;
use app\modules\painter\models\BankingInformation;
use \app\modules\painter\models\UserEmail;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\EmailForm;
use common\models\MembershipPack;
use common\models\PainterApprove;

/**
 * PainterprofileController implements the CRUD actions for PainterProfile model.
 */
class ProfileController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'request-password-reset', 'reset-password','about'],
                        'allow' => true,
                    ],
                    [
                        //'actions' => ['logout', 'index', 'changepassword', 'login', 'terms', 'viewapplication', 'viewapplication2', 'change-password'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PainterProfile models.
     * @return mixed
     */
    public function actionIndex()
    {
        /*$searchModel = new PainterProfileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);*/
        $userid = Yii::$app->user->id;
        $model = PainterProfile::find()->where(['user_id' => $userid])->one();
        $user = \common\models\User::find()->where(['id' => $userid])->one();
        $companyid = CompanyInformation::find()->where(['user_id' => $userid])->one();
        $bankid = BankingInformation::find()->where(['user_id' => $userid])->one();
        $companyInformation = CompanyInformation::findOne($companyid->id);
        $bankingInformation = BankingInformation::findOne($bankid->id);
        return $this->render('view', [
            'model' => $model,
            'companyInformation' => $companyInformation,
            'bankingInformation' => $bankingInformation,
            'user' => $user,
        ]);
    }

    
    public function actionUpdateprofile()
    {
        $userid = Yii::$app->user->id;
        $model = PainterProfile::find()->where(['user_id' => $userid])->one();
        $user = \common\models\User::find()->where(['id' => $userid])->one();
        $useremail = UserEmail::find()->where(['id' => $userid])->one();
        //echo '<pre>';
        //print_r($useremail);
        
        if ($model->load(Yii::$app->request->post()) && $useremail->load(Yii::$app->request->post())){
            if($model->validate() && $useremail->validate()){
                $userProfile = \common\models\User::find()->where(['id' => $userid])->one();
                $userProfile->email = $useremail->email;
                $userProfile->save(false);
                
                $model->email = $useremail->email;
                $model->save(false);
                //Yii::$app->session->setFlash('success', 'Profile has been updated!');
                \Yii::$app->getSession()->setFlash('success',['title' => 'Personal Details', 'text' => 'Profile has been updated!']);
                return $this->redirect(['index', '#' => 'personal']);                
            }else{
                return $this->render('_form_profile', [
                            'model' => $model,
                            'useremail' => $useremail,
                ]);
            }
        }else{
            return $this->render('_form_profile', [
                'model' => $model,
                'useremail' => $useremail,
            ]);
        }
    }
    
    public function actionUpdatecompany()
    {
        $userid = Yii::$app->user->id;
        $model = PainterProfile::find()->where(['user_id' => $userid])->one();
        $companyid = CompanyInformation::find()->where(['user_id' => $userid])->one();
        $companyInformation = CompanyInformation::findOne($companyid->id);
        if ($companyInformation->load(Yii::$app->request->post())){
            if($companyInformation->validate()){
                $companyInformation->save(false);
                //Yii::$app->session->setFlash('success', ' Company Information has been updated!');
                \Yii::$app->getSession()->setFlash('success',['title' => 'Company Information', 'text' => 'Company Information has been updated!']);
                return $this->redirect(['index', '#' => 'company']);                
            }else{
                return $this->render('_form_company', [
                    'model' => $model,
                    'companyInformation' => $companyInformation,
                ]);
            }
        }else{
            return $this->render('_form_company', [
                'model' => $model,
                'companyInformation' => $companyInformation,
            ]);
        }
    }
    
    public function actionUpdatebank()
    {
        $userid = Yii::$app->user->id;
        $model = PainterProfile::find()->where(['user_id' => $userid])->one();
        $bankid = BankingInformation::find()->where(['user_id' => $userid])->one();
        $bankingInformation = BankingInformation::findOne($bankid->id);
        
        if ($bankingInformation->load(Yii::$app->request->post())){
            if($bankingInformation->validate()){
                $bankingInformation->save(false);
                //Yii::$app->session->setFlash('success', ' Bank Information has been updated!');
                \Yii::$app->getSession()->setFlash('success',['title' => 'Bank Information', 'text' => 'Bank Information has been updated!']);
                return $this->redirect(['index', '#' => 'bank']);                
            }else{
                return $this->render('_form_bank', [
                    'model' => $model,
                    'bankingInformation' => $bankingInformation,
                ]);
            }
        }else{
            return $this->render('_form_bank', [
                'model' => $model,
                'bankingInformation' => $bankingInformation,
            ]);
        }
    }


    /**
     * Finds the PainterProfile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PainterProfile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PainterProfile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionPickcountry($id){
        if ($id == "L") {
            $sle = '';
            $countrys = \common\models\Country::find()
                ->where(['name' => 'Malaysia'])   
                ->orderBy('name ASC')
                ->all();
        } else {
            $sle = '<option value="">-- Select --</option>';
            $countrys = \common\models\Country::find()
                    ->where(['!=', 'name', 'Malaysia'])
                    ->orderBy('name ASC')
                    ->all();
        }
        if ($countrys) {
            echo $sle;
            foreach ($countrys as $country) {
                echo "<option value='" . $country->id . "'>" . $country->name . "</option>";
            }
        } else {
            echo "<option>-</option>";
        }
        
    }
}
