<?php

namespace app\modules\painter\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

use app\modules\painter\models\PainterProfile;
use app\modules\painter\models\PainterProfileSearch;
use app\modules\painter\models\CompanyInformation;
use app\modules\painter\models\BankingInformation;
use \app\modules\painter\models\UserEmail;

use common\models\EmailForm;
use common\models\MembershipPack;
use common\models\PainterApprove;
use common\models\PainterProofDocument;

use common\models\PointOrder;
use common\models\PointOrderSearch;
use common\models\PointOrderItem;
use common\models\RedemptionItems;

use common\models\Redemption;
use common\models\RedemptionSearch;
use common\models\DealerList;
use common\models\PaintingJobsPainter;
use common\models\Region;
use common\models\State;

/**
 * PainterprofileController implements the CRUD actions for PainterProfile model.
 */
class PainterprofileController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'request-password-reset', 'reset-password','about'],
                        'allow' => true,
                    ],
                    [
                        //'actions' => ['logout', 'index', 'changepassword', 'login', 'terms', 'viewapplication', 'viewapplication2', 'change-password'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PainterProfile models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PainterProfileSearch();
        $searchModel->profile_status = 'P';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionReview()
    {
        $searchModel = new PainterProfileSearch();
        $searchModel->profile_status = 'R';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index_review', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionApprove()
    {
        $searchModel = new PainterProfileSearch();
        $searchModel->profile_status = 'A';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index_approve', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionDecline()
    {
        $searchModel = new PainterProfileSearch();
        $searchModel->profile_status = 'D';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index_decline', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionIndex1()
    {
        $searchModel = new PainterProfileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $pending = PainterProfile::find()->where(['profile_status' => 'P'])->count();
        $review = PainterProfile::find()->where(['profile_status' => 'R'])->count();
        $approve = PainterProfile::find()->where(['profile_status' => 'A'])->count();
        $decline = PainterProfile::find()->where(['profile_status' => 'D'])->count();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'pending' => $pending,
            'review' => $review,
            'decline' => $decline,
            'approve' => $approve,
        ]);
    }
    /**
     * Displays a single PainterProfile model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $userid = $model->user_id;
        $user = \common\models\User::find()->where(['id' => $userid])->one();
        $companyid = CompanyInformation::find()->where(['user_id' => $userid])->one();
        $bankid = BankingInformation::find()->where(['user_id' => $userid])->one();
        $companyInformation = CompanyInformation::findOne($companyid->id);
        $bankingInformation = BankingInformation::findOne($bankid->id);
        $prrofdocument = PainterProofDocument::find()->where(['user_id' => $userid])->all();

        $searchModel = new RedemptionSearch();
        $searchModel->painterID = $userid;
        $dataProvider = $searchModel->searchbyuser(Yii::$app->request->queryParams);

        $transactionsearchModel = new PointOrderSearch();
        $transactionsearchModel->painter_login_id = $userid;
        $transaction = $transactionsearchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('view', [
            'model' => $model,
            'companyInformation' => $companyInformation,
            'bankingInformation' => $bankingInformation,
            'prrofdocument' => $prrofdocument,
            'user' => $user,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'transaction' => $transaction,
        ]);
    }
    
    public function actionUploaddocuments($id)
    {
        $model = $this->findModel($id);
        $userid = $model->user_id;
        
        $fileuploadform = new \common\models\UploadDocumentForm();  
        if ($fileuploadform->load(Yii::$app->request->post()) && $fileuploadform->validate()){
            $fileuploadform->files = UploadedFile::getInstances($fileuploadform, 'files');
            if ($fileuploadform->files) {
                foreach ($fileuploadform->files as $imageFile) {
                    $ref = substr(Yii::$app->getSecurity()->generateRandomString(),8);
                    $path = Yii::getAlias('@frontend') .'/web/upload/documents/';
                    //$baseName = str_replace(" ","",trim($gallery->title)) . '_image_' . str_replace(" ","",trim($imageFile->baseName));
                    $baseName = $userid.'_'.$ref;
                    $imageFile->saveAs($path . $baseName . '.' . $imageFile->extension);

                    $proofdocument = new \common\models\PainterProofDocument();
                    $proofdocument->user_id = $userid;
                    $proofdocument->ref = $baseName;
                    $proofdocument->file_name = $baseName . '.' . $imageFile->extension;
                    $proofdocument->real_filename = $imageFile->baseName. '.' . $imageFile->extension;
                    $proofdocument->save(false);
                    unset($proofdocument);
                }
                \Yii::$app->getSession()->setFlash('success',['title' => 'Documents Upload', 'text' => 'Action Successful.']);
                //return $this->redirect(['uploaddocuments', 'id' => $id, 'tab' => 'document']);
                //return $this->redirect(['view', 'id' => $id, 'tab' => '&#35;document']);
                return $this->redirect('view?id='.$id.'&tab=#document');
            }else{
                \Yii::$app->getSession()->setFlash('danger',['title' => 'Documents Upload', 'text' => 'Action Fail.']);
                return $this->redirect(['uploaddocuments', 'id' => $id, 'tab' => 'document']);
            }
        }else{
            return $this->render('uploaddocument', [
                'fileuploadform' => $fileuploadform,
                'model' => $model,
            ]);
        }
    }
    
    public function actionDeletedocuments($id,$sid)
    {
        $path = Yii::getAlias('@frontend') .'/web/upload/documents/';
        $image = PainterProofDocument::find()->where(['upload_id' => $id])->one();
        unlink($path. $image->file_name);
        $this->findModeldel($id)->delete();
        \Yii::$app->getSession()->setFlash('success',['title' => 'Documents Upload', 'text' => 'Action Successful.']);
        //return $this->redirect(['uploaddocuments', 'id' => $sid, 'tab' => 'document']); 
        //return $this->redirect(['view', 'id' => $sid, 'tab' => '#','document']); 
        return $this->redirect('view?id='.$sid.'&tab=#document');
    }
    
    protected function findModeldel($id)
    {
        if (($modeldel = PainterProofDocument::findOne($id)) !== null) {
            return $modeldel;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionMyprofile()
    {
        $userid = Yii::$app->user->id;
        $model = PainterProfile::find()->where(['user_id' => $userid])->one();
        $user = \common\models\User::find()->where(['id' => $userid])->one();
        $companyid = CompanyInformation::find()->where(['user_id' => $userid])->one();
        $bankid = BankingInformation::find()->where(['user_id' => $userid])->one();
        $companyInformation = CompanyInformation::findOne($companyid->id);
        $bankingInformation = BankingInformation::findOne($bankid->id);
        return $this->render('view', [
            'model' => $model,
            'companyInformation' => $companyInformation,
            'bankingInformation' => $bankingInformation,
            'user' => $user,
        ]);
    }
    
    public function actionViewapprove($id)
    {
        $model = $this->findModel($id);
        $userid = $model->user_id;
        $user = \common\models\User::find()->where(['id' => $userid])->one();
        $companyid = CompanyInformation::find()->where(['user_id' => $userid])->one();
        $bankid = BankingInformation::find()->where(['user_id' => $userid])->one();
        $companyInformation = CompanyInformation::findOne($companyid->id);
        $bankingInformation = BankingInformation::findOne($bankid->id);
        $prrofdocument = PainterProofDocument::find()->where(['user_id' => $userid])->all();
        $formapprove = new PainterApprove();
        return $this->render('approveview', [
            'model' => $model,
            'formapprove' => $formapprove,
            'companyInformation' => $companyInformation,
            'bankingInformation' => $bankingInformation,
            'user' => $user,
            'prrofdocument' => $prrofdocument,
        ]);
    }
    
    public function actionApprovel($id)
    {
        //$model = $this->findModel($id);
        $model = new PainterProfile();
        $formapprove = new PainterApprove();
        if ($formapprove->load(Yii::$app->request->post()) && $model->load(Yii::$app->request->post())){
            if ($formapprove->validate()) {
                //$model->load(Yii::$app->request->post());
                $profileid = $formapprove->pa_profileid;
                $painter = PainterProfile::findOne($profileid);
                //$profileid = '105';
                if ($painter !== null) {
                    $membership_ID = $painter->card_id;
                    $mobile = $painter->mobile;
                    $userid = $painter->user_id;
                    $fullname = $painter->full_name;
                    $user = \common\models\User::find()->where(['id' => $userid])->one();
                    $generatedPassword = str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);
                    $email = $user->email; 
                    //echo $model->profile_status;
                    //die();
                    //echo $email;
                    //die();
                    if($model->profile_status == 'A'){
                        $user->status = "A";
                        $user->setPassword($generatedPassword);
                        $user->generateAuthKey();
                        $checkassign = \common\models\AuthAssignment::find()->where(['item_name' => 'Painter' ,'user_id' => $userid])->count();
                        if($checkassign == 0){
                            $assignuser = new \common\models\AuthAssignment();
                            $assignuser->item_name = 'Painter';
                            $assignuser->user_id = $userid;
                            $assignuser->save(false);
                        }
                    }else{
                       $user->status = "I";
                    }
                    $user->save(false);
                    
                    $formapprove->pa_userid = $userid;
                    $formapprove->pa_status = $model->profile_status;
                    $formapprove->save(false);
                    
                    $painter->profile_status = $model->profile_status;
                    $painter->save();
                    // && $model->profile_status == 'A'
                    $data = \yii\helpers\Json::encode(array(
                        'name' => $fullname,
                        'password' => $generatedPassword,
                        'email' => $email,
                        'membership_ID' => $membership_ID,
                        'contact_no' => $mobile,
                    ));
                    if(!empty($email) && $model->profile_status == 'A'){
                        $subject = '';                        
                        Yii::$app->ici->sendEmailSetting($user->id, $user->email, Yii::$app->params['email.template.code.successful.painter.application'], $data, $subject);
                    }
                    
                    $painterinfo = \common\models\PainterProfile::find()
                        ->where(['user_id' => $user->id])
                        ->one();
                    

                        if(!empty($painterinfo->mobile) && $model->profile_status == 'A'){
                            $mobile = $painterinfo->mobile;
                            $result = Yii::$app->ici->sendSMSpaintersetting($user->id, $mobile, Yii::$app->params['email.template.code.successful.painter.application'], $data);
                        }
                    //\Yii::$app->getSession()->setFlash('success', 'Action Successful');
                    \Yii::$app->getSession()->setFlash('success',['title' => 'Approve', 'text' => 'Action Successful.']);
                    return $this->redirect(['/painter/painterprofile/index']);
                } else {
                    throw new NotFoundHttpException('The requested record does not exist.');
                }
            }else{
                print_r($formapprove->getErrors());
            }
        }
        return $this->redirect(['/painter/painterprofile/index']);
        //echo $id;
    }
    
    public function actionViewremark($id)
    {
        $remarks = PainterApprove::find()->where(['pa_profileid' => $id])->all();
        echo '<dl>';
        foreach($remarks as $remark){
            echo '<dt>'.date('d-m-Y', strtotime($remark->created_datetime)).'</dt>';
            echo '<dd>'.$remark->pa_remark.'</dd>';
        }
        echo '</dl>';
    }
    
    public function actionSendpassword($id) {
        $model = $this->findModel($id);
        if ($model->id != null) {
            $user = \common\models\User::find()->where(['id' => $model->user_id])->one();            
            $email = $user->email; 
            if(!empty($email)){
               $subject = '' ;
               $fullname = $model->full_name;
               $generatedPassword = str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT); 
               $user->setPassword($generatedPassword);
               $user->generateAuthKey();
               $user->save(false);
               
               $data = \yii\helpers\Json::encode(array(
                    'name' => $fullname,
                    'password' => $generatedPassword,
                    'email' => $email,
                ));
               //email.template.code.password.reset.request
               Yii::$app->ici->sendEmail($user->id, $user->email, Yii::$app->params['email.template.code.password.reset.request'], $data, $subject);
               //print_r($returnedValue);
               \Yii::$app->getSession()->setFlash('success',['title' => 'Email', 'text' => 'New password successfully send to '.$email.'.']);
               return $this->redirect(['/painter/painterprofile/index']);
            }else{
                \Yii::$app->getSession()->setFlash('error',['title' => 'Email', 'text' => 'Action Successful!']);
                return $this->redirect(['/painter/painterprofile/index']);
            }
        }
        return $this->redirect(['/painter/painterprofile/index']);
        //echo $id;
    }
    
    /**
     * Creates a new PainterProfile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($icno = null)
    {
        $model = new PainterProfile();
        $companyInformation = new CompanyInformation();
        $bankingInformation = new BankingInformation();
        $useremail = new UserEmail();
        $model->ic_no = $icno;
        $today = date('Y-m-d');

        if ($model->load(Yii::$app->request->post()) && $companyInformation->load(Yii::$app->request->post()) && $bankingInformation->load(Yii::$app->request->post()) && $useremail->load(Yii::$app->request->post())) {
            if($model->validate() && $companyInformation->validate() && $bankingInformation->validate() && $useremail->validate()){
                //echo $model->mobile;
                //die();
                
                $newemail = $useremail->email;
                $user = new \common\models\User();
                if(empty($newemail)){
                    $user->username = date('dmy_His');
                    $user->email = null;
                }else{
                    $user->username = substr(uniqid(rand(1,6)), 0, 4).'_'.strstr($newemail, '@', true);
                    $user->email = $newemail;
                }
                
                $user->status = "P";
                $user->user_type = "P";
                $user->save(false);

                $chkcardid = PainterProfile::find()->where(['LIKE', 'created_datetime', $today])   
                        ->orderBy(['card_num' => SORT_DESC,])
                        ->one();
                if(count($chkcardid) >= 1){ 
                    $str2 = $chkcardid->card_num;
                    $str3 = $str2 + 1;
                    $numbers = str_pad($str3, 3, '0', STR_PAD_LEFT);
                }else{
                    $numbers = '001';
                }

                $vregion = Region::find()->where(['region_id' => $model->region_id])->one();
                $vregioncode = $vregion->region_code;
                $vstate = State::find()->where(['state_id' => $model->state_id])->one();
                $vstatecode = $vstate->state_code;
                $membershipno = $vregioncode.$vstatecode.date('dmy').$numbers;
                
                $chkcardnum = PainterProfile::find()->where(['=', 'card_id', $membershipno])->one();
                if($chkcardnum >= 1){
                    $str2 = $chkcardnum->card_num;
                    $str3 = $str2 + 1;
                    $numbers = str_pad($str3, 3, '0', STR_PAD_LEFT);
                    $membershipno = $vregioncode.$vstatecode.date('dmy').$numbers;
                }
                
                //echo date('dmy');
                $model->user_id = $user->id;
                $model->card_id = $membershipno;
                $model->card_num = $numbers;
                $model->dob = date('Y-m-d', strtotime($model->dob));
                $model->full_name = strtoupper($model->full_name);
                $model->pic_name = $model->pic_name;
                $model->profile_status = "P";
                $model->save(false);

                $companyInformation->user_id = $user->id;
                $companyInformation->save(false);
                
                $residentials = $companyInformation->residential;
                if(!empty($residentials)){                    
                    foreach($residentials as $residential){
                        $painterjobsR = new PaintingJobsPainter();
                        $painterjobsR->company_information_id = $companyInformation->id;
                        $painterjobsR->painting_jobs_id = $residential;
                        $painterjobsR->painting_jobs_type = 'R';
                        $painterjobsR->save(false);
                        unset($painterjobsR);
                    }
                }
                
                $commercials = $companyInformation->commercial;
                if(!empty($commercials)){                    
                    foreach($commercials as $commercial){
                        $painterjobsC = new PaintingJobsPainter();
                        $painterjobsC->company_information_id = $companyInformation->id;
                        $painterjobsC->painting_jobs_id = $commercial;
                        $painterjobsC->painting_jobs_type = 'C';
                        $painterjobsC->save(false);
                        unset($painterjobsC);
                    }
                }

                $bankingInformation->user_id = $user->id;
                $bankingInformation->IP = $bankingInformation->getRealIp();
                $bankingInformation->save(false);
                
                /*if(!empty($model->snapdocument)){
                    $multidocments = explode(',', $model->snapdocument);
                    foreach($multidocments as $multidocment){
                        $proofdocument = new \common\models\PainterProofDocument();
                        $proofdocument->user_id = $user->id;
                        $proofdocument->ref = $model->id;
                        $proofdocument->file_name = $multidocment;
                        $proofdocument->save(false);
                        unset($proofdocument);
                    }                    
                }*/
                $model->files = UploadedFile::getInstances($model, 'files');
                if ($model->files) {
                    foreach ($model->files as $imageFile) {                        
                        $ref = substr(Yii::$app->getSecurity()->generateRandomString(),8);
                        $userid = $user->id;
                        $path = Yii::getAlias('@frontend') .'/web/upload/documents/';
                        //$baseName = str_replace(" ","",trim($gallery->title)) . '_image_' . str_replace(" ","",trim($imageFile->baseName));
                        $baseName = $userid.'_'.$ref;
                        $imageFile->saveAs($path . $baseName . '.' . $imageFile->extension);

                        $proofdocument = new \common\models\PainterProofDocument();
                        $proofdocument->user_id = $userid;
                        $proofdocument->ref = $baseName;
                        $proofdocument->file_name = $baseName . '.' . $imageFile->extension;
                        $proofdocument->real_filename = $imageFile->baseName. '.' . $imageFile->extension;
                        $proofdocument->save(false);
                        unset($proofdocument);
                    }
                }
                
                $mpack = new MembershipPack();
                $mpack->user_id = $user->id;
                $mpack->save(false);
                \Yii::$app->getSession()->setFlash('success',['title' => 'Painter', 'text' => 'profile created successfully!']);
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                //print_r($model->getErrors());
                return $this->render('create', [
                    'model' => $model,
                    'companyInformation' => $companyInformation,
                    'bankingInformation' => $bankingInformation,
                    'useremail' => $useremail,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'companyInformation' => $companyInformation,
                'bankingInformation' => $bankingInformation,
                'useremail' => $useremail,
            ]);
        }
    }

    /**
     * Updates an existing PainterProfile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $userid = $model->user_id;
        $companyid = CompanyInformation::find()->where(['user_id' => $userid])->one();
        $bankid = BankingInformation::find()->where(['user_id' => $userid])->one();
        $companyInformation = CompanyInformation::findOne($companyid->id);
        $bankingInformation = BankingInformation::findOne($bankid->id);

        if ($model->load(Yii::$app->request->post()) && $companyInformation->load(Yii::$app->request->post()) && $bankingInformation->load(Yii::$app->request->post())) {
            if($model->validate() && $companyInformation->validate() && $bankingInformation->validate()){
                echo 'validated';
                die();
                $user = new \common\models\User();
                //$user->username = strstr($model->email, '@', true);;
                $user->email = $model->email;
                //$user->status = "P";
                //$user->user_type = "P";
                $user->save(false);
                /*$numbers = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
                $chkcardid = PainterProfile::find()->where(['card_id' => $numbers,])->count();
                if($chkcardid > 0){
                    $numbers = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
                }*/
                //$model->user_id = $user->id;
                //$model->card_id = $numbers;
                $model->email = $model->email;
                $model->save(false);

                //$companyInformation->user_id = $user->id;
                $companyInformation->save(false);

                //$bankingInformation->user_id = $user->id;
                $bankingInformation->IP = $bankingInformation->getRealIp();
                $bankingInformation->save(false);
                
                /*$mpack = new MembershipPack();
                $mpack->user_id = $user->id;
                $mpack->save(false);*/

                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                return $this->render('update', [
                    'model' => $model,
                    'companyInformation' => $companyInformation,
                    'bankingInformation' => $bankingInformation,
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'companyInformation' => $companyInformation,
                'bankingInformation' => $bankingInformation,
            ]);
        }
    }
    
    public function actionUpdateprofile($id)
    {
        $model = $this->findModel($id);
        /*echo '<pre>';
        print_r($model);
        die();*/
        $userid = $model->user_id;
        $user = \common\models\User::find()->where(['id' => $userid])->one();
        $useremail = UserEmail::find()->where(['id' => $userid])->one();
        //echo '<pre>';
        //print_r($useremail);
        
        if ($model->load(Yii::$app->request->post()) && $useremail->load(Yii::$app->request->post())){
            if($model->validate() && $useremail->validate()){
                $userProfile = \common\models\User::find()->where(['id' => $userid])->one();
                if(!empty($useremail->email)){
                    $userProfile->email = $useremail->email;
                }
                $userProfile->save(false);
                
                $model->email = $useremail->email;
                $model->dob = date('Y-m-d', strtotime($model->dob));
                $model->save(false);
                if($model->profile_status != 'A'){
                    $str2 = substr($model->card_id, 4);
                    $vregion = Region::find()->where(['region_id' => $model->region_id])->one();
                    $vregioncode = $vregion->region_code;
                    $vstate = State::find()->where(['state_id' => $model->state_id])->one();
                    $vstatecode = $vstate->state_code;
                    $membershipno = $vregioncode.$vstatecode.$str2;

                    $model->card_id = $membershipno;
                    $model->ic_no = $model->ic_no;
                    $model->region_id = $model->region_id;
                    $model->state_id = $model->state_id;
                    $model->save(false);
                }
                
                //Yii::$app->session->setFlash('success', 'Profile has been updated!');
                \Yii::$app->getSession()->setFlash('success',['title' => 'Personal Details', 'text' => 'Profile has been updated!']);
                return $this->redirect(['view', 'id' => $model->id, 'tab' => 'personal']);                
            }else{
                return $this->render('_form_profile', [
                            'model' => $model,
                            'useremail' => $useremail,
                ]);
            }
        }else{
            return $this->render('_form_profile', [
                'model' => $model,
                'useremail' => $useremail,
            ]);
        }
    }
    
    public function actionUpdatecompany($id)
    {
        $model = $this->findModel($id);
        $userid = $model->user_id;
        $companyid = CompanyInformation::find()->where(['user_id' => $userid])->one();
        $companyInformation = CompanyInformation::findOne($companyid->id);
        if ($companyInformation->load(Yii::$app->request->post())){
            if($companyInformation->validate()){
                $companyInformation->save(false);
                $residentials = $companyInformation->residential;
                if (!empty($residentials)) {
                    $residentialsdel = PaintingJobsPainter::deleteAll(['company_information_id' => $companyInformation->id, 'painting_jobs_type' => 'R']);
                    foreach ($residentials as $residential) {
                        $painterjobsR = new PaintingJobsPainter();
                        $painterjobsR->company_information_id = $companyInformation->id;
                        $painterjobsR->painting_jobs_id = $residential;
                        $painterjobsR->painting_jobs_type = 'R';
                        $painterjobsR->save(false);
                        unset($painterjobsR);
                    }
                }
                $commercials = $companyInformation->commercial;
                if(!empty($commercials)){
                    $commercialsdel = PaintingJobsPainter::deleteAll(['company_information_id' => $companyInformation->id, 'painting_jobs_type' => 'C']);
                    foreach($commercials as $commercial){
                        $painterjobsC = new PaintingJobsPainter();
                        $painterjobsC->company_information_id = $companyInformation->id;
                        $painterjobsC->painting_jobs_id = $commercial;
                        $painterjobsC->painting_jobs_type = 'C';
                        $painterjobsC->save(false);
                        unset($painterjobsC);
                    }
                }
                //Yii::$app->session->setFlash('success', ' Company Information has been updated!');
                \Yii::$app->getSession()->setFlash('success',['title' => 'Company Information', 'text' => 'Company Information has been updated!']);
                return $this->redirect(['view', 'id' => $model->id, 'tab' => 'company']);                
            }else{
                return $this->render('_form_company', [
                    'model' => $model,
                    'companyInformation' => $companyInformation,
                ]);
            }
        }else{
            return $this->render('_form_company', [
                'model' => $model,
                'companyInformation' => $companyInformation,
            ]);
        }
    }
    
    public function actionUpdatebank($id)
    {
        $model = $this->findModel($id);
        $userid = $model->user_id;
        $bankid = BankingInformation::find()->where(['user_id' => $userid])->one();
        $profile = PainterProfile::find()->where(['user_id' => $userid])->one();
        $bankingInformation = BankingInformation::findOne($bankid->id);
        
        if ($bankingInformation->load(Yii::$app->request->post())){
            if($bankingInformation->validate()){
                /*echo $bankingInformation->account_no_verification;
                die;
                if(empty($bankingInformation->account_ic_no)){
                    $bankingInformation->account_ic_no = $profile->ic_no;
                }
                $old = $bankingInformation->OldAttributes['account_number'];
                $new = $bankingInformation->account_number;

                if ($old != $new) {
                    $bankingInformation->account_no_verification = 'N';
                }*/
                $bankingInformation->remark = null;
                $bankingInformation->save(false);
                //Yii::$app->session->setFlash('success', ' Bank Information has been updated!');
                \Yii::$app->getSession()->setFlash('success',['title' => 'Bank Information', 'text' => 'Bank Information has been updated!']);
                return $this->redirect(['view', 'id' => $model->id, 'tab' => 'bank']);                
            }else{
                return $this->render('_form_bank', [
                    'model' => $model,
                    'profile' => $profile,
                    'bankingInformation' => $bankingInformation,
                ]);
            }
        }else{
            return $this->render('_form_bank', [
                'model' => $model,
                'profile' => $profile,
                'bankingInformation' => $bankingInformation,
            ]);
        }
    }

    /**
     * Deletes an existing PainterProfile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionRedemption()
    {
        $searchModel = new RedemptionSearch();
        $searchModel->painterID = Yii::$app->user->id;
        $dataProvider = $searchModel->searchpainter(Yii::$app->request->queryParams);

        return $this->render('redemption', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionPayout()
    {
        $searchModel = new RedemptionSearch();
        $searchModel->painterID = Yii::$app->user->id;
        $searchModel->redemption_status_ray = ['2','10','19'];
        $dataProvider = $searchModel->searchpainter(Yii::$app->request->queryParams);

        return $this->render('payout', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionRedemptionview($id)
    {
        if (($model = Redemption::findOne($id)) !== null) {
            return $this->render('redemption_view', [
                'model' => $model,
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionPayoutview($id)
    {
        if (($model = Redemption::findOne($id)) !== null) {
            return $this->render('pay_out_view', [
                'model' => $model,
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionRedemptionRequest()
    {

        $model = new \common\models\Redemption();
        //'=','painter_login_id', Yii::$app->user->id
        if ($model->load(Yii::$app->request->post())) {
            $model->painterID = Yii::$app->user->id;            
            $model->redemption_status = '1';
            $model->redemption_status_ray = '1';
            $chkinvoiceid = \common\models\Redemption::find()->count();
            if($chkinvoiceid > 0){
                $newinvoiceid = \common\models\Redemption::find()->orderBy(['redemption_invoice_no' => SORT_DESC,])->one();
                $incinvoice = str_replace(Yii::$app->params['invoice.prefix'],"",$newinvoiceid->redemption_invoice_no);
                //$incinvoice = $incinvoice + 1;
                $incinvoice = str_pad(++$incinvoice,6,'0',STR_PAD_LEFT);
            }else{
                $incinvoice = Yii::$app->params['invoice.prefix.dft'];
            }
            $model->redemption_invoice_no = Yii::$app->params['invoice.prefix'].$incinvoice;
            $model->redemption_IP = $model->getRealIp();
            if($model->save()) {
                $orderids = explode(',', $model->orderID);
                foreach($orderids as $value){
                    $total_per_point = PointOrderItem::find()
                            ->where("painter_login_id = " . Yii::$app->user->id . " AND point_order_id = " . $value . " AND Item_status = 'G'")
                            ->sum('total_qty_point');

                    $total_per_price = PointOrderItem::find()
                            ->where("painter_login_id = " . Yii::$app->user->id . " AND point_order_id = " . $value . " AND Item_status = 'G'")
                            ->sum('total_qty_value');
                    $redemptionitem = new RedemptionItems();
                    $redemptionitem->redemptionID = $model->redemptionID;
                    $redemptionitem->order_id = $value;
                    $redemptionitem->req_per_points = $total_per_point;
                    $redemptionitem->req_per_amount = $total_per_price;
                    $redemptionitem->save(false); 
                    unset($redemptionitem);
                    //if($redemptionitem->save(false)){
                        //unset($redemptionitem);
                    $updateorder = PointOrder::find()
                            ->where("order_id = " . $value . "")
                            ->one();
                    $updateorder->redemption = 'Y';
                    $updateorder->save(false);

                }
                \Yii::$app->getSession()->setFlash('success',['title' => 'Redemption Request', 'text' => 'Action successful!']);
                return $this->redirect(['redemption']);
            }else{
                print_r($model->getErrors());
            }
        }else{            
            /*$myorders = (new \yii\db\Query())->from('point_order po')
            ->join('JOIN', 'redemption_items ri', 'po.order_id = ri.order_id')
            ->where(['po.painter_login_id' => Yii::$app->user->getId(), 'po.order_status' => '17'])
            ->all();*/

            $myorders = PointOrder::find()
                 ->where("painter_login_id = " . Yii::$app->user->id. " AND order_status = 17 ")
                 ->orderBy('order_status ASC')    
                 ->all(); 

            return $this->render('redemptionrequest', [
                'model' => $model,
                'myorders' => $myorders,
            ]);
        }
    }

    /**
     * Finds the PainterProfile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PainterProfile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PainterProfile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionPickcountry($id){
        if ($id == "L") {
            $sle = '';
            $countrys = \common\models\Country::find()
                ->where(['name' => 'Malaysia'])   
                ->orderBy('name ASC')
                ->all();
        } else {
            $sle = '<option value="">-- Select --</option>';
            $countrys = \common\models\Country::find()
                    ->where(['!=', 'name', 'Malaysia'])
                    ->orderBy('name ASC')
                    ->all();
        }
        if ($countrys) {
            echo $sle;
            foreach ($countrys as $country) {
                echo "<option value='" . $country->id . "'>" . $country->name . "</option>";
            }
        } else {
            echo "<option>-</option>";
        }
        
    }
    
    public function actionDstate($id){
        $dealerlists = DealerList::find()
                ->where(['LIKE', 'area', $id])
                ->andwhere(['=', 'status', 'A'])
                ->orderBy('customer_name ASC')
                ->all();
        if ($dealerlists) {
            //echo $sle;
            foreach ($dealerlists as $dealerlist) {
                echo "<option value='" . $dealerlist->id . "'>" . $dealerlist->customer_name . "</option>";
            }
        } else {
            echo "<option>-</option>";
        }
    }
    
    public function actionDregion($id){
        $states = State::find()
                ->where(['region_id' => $id])
                ->all();
        if ($states) {
            //echo $sle;
            foreach ($states as $state) {
                echo "<option value='" . $state->state_id . "'>" . $state->state_name . "</option>";
            }
        } else {
            echo "<option>-</option>";
        }
    }
    
    public function actionDealeraddress($id){
        $address = DealerList::find()
                ->where(['id' => $id])
                ->one();
        echo $address->address;
    }

    public function actionCheckemail(){
        $useremail = filter_var($_POST["useremail"], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH);
        $userer = \common\models\User::find()->where(['email' => $useremail])->count();
        if($userer > 0){
            //die('<i class="fa fa-close text-danger" aria-hidden="true"></i>');
            echo 'no';
        }else{
            echo 'yes';
            //die('<i class="fa fa-check text-success" aria-hidden="true"></i>');
        }
    }
    
    public function actionMycam(){
        $encoded_data = $_POST['username'];        
        $image_data = base64_decode( $encoded_data );
        $path = Yii::getAlias('@frontend') .'/web/upload/documents/';
        $file_name = $path;
        $image_name = substr(uniqid(rand(1,6)), 0, 8).'.jpg';
        $file = $file_name . $image_name;

        $success = file_put_contents($file, $image_data);
        
        if ($success) {
            //echo "Successfully uploaded"; 
            $response['image_name'] = $image_name;
        } else {
            //echo "Not uploaded";
            $response = null;
        }
        echo json_encode($response);
    }
    
    public function actionTest(){
        $yy = '01021711161000';
        $str2 = substr($yy, 10);
        //echo $invID;
        $str3 = $str2 + 1;
        $ab = str_pad($str3, 3, '0', STR_PAD_LEFT);
        echo $ab;
    }
    
}