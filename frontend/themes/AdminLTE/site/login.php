<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-login">
    <div class="login-box">
        <div class="login-logo col-lg-12 hide" style="padding: 0px; background-color: #012169;">
            <div class="col-lg-6 col-xs-6 text-left">
                <img src="/images/logo.png" class="img-responsive" style="max-width: 150px;">
            </div>
            <div class="col-lg-6 col-xs-6 text-right" style="display: inline-block;vertical-align: middle;float: none;">
            <img src="/images/akzonobel-white.png" class="img-responsive" style="float: right;max-width: 100px;">
            </div>            
        </div><!-- /.login-logo -->
        <div class="clearfix"></div>
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <?= $form->field($model, 'username', ['options' => [
                    'tag' => 'div',
                    'class' => 'form-group field-loginform-username has-feedback required',
                ],
                'template' => '{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>{error}{hint}'
            ])->textInput(['placeholder' => 'Username/Email'])
            ?>
            
            <?= $form->field($model, 'password', ['options' => [
                    'tag' => 'div',
                    'class' => 'form-group field-loginform-password has-feedback required',
                ],
                'template' => '{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>{error}{hint}'
            ])->passwordInput(['placeholder' => 'Password'])
            ?>
            
                
                
            
            <div class="row">
                <div class="col-xs-8" style="padding: 0;">
                    <?= $form->field($model,'rememberMe',['options'=>
                            [
                               'tag'=>'div',
                               'class'=>'checkbox icheck'
                            ]
                        ])->checkbox();
                    ?>
                </div><!-- /.col -->
                <div class="col-xs-4">
                    <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
                </div>
                </div><!-- /.col -->                
            </div>
            <?php ActiveForm::end(); ?>
            <a href="request-password-reset">Forgot your password</a><br>
            <a href="#" data-toggle="modal" data-target="#myModal">How to Apply?</a><br>            
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">How to Apply?</h4>
      </div>
      <div class="modal-body">
          <ol>
              <li> Painter is required to complete the
                  application form available at Dulux
                  Painter's Club Participating Dealer
                  and/or obtain it from Dulux Painter’s
                  Club representatives
              </li>
              <li> Upon completion of application form,
                  Painter can submit the form by
                  putting it in the drop-box at
                  Participating Dealer's premises or
                  hand it over to our Dulux Painter’s
                  Club representatives</li>
          </ol>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->