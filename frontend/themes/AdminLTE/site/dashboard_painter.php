<?php
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\PointOrder;
use common\models\Redemption;
    
use app\components\ProductOverviewPainterWidget;
use app\components\RedemptionRequestPainterWidget;
use app\components\PayOutPainterWidget;

?>
<?php
$trTotal = (new \yii\db\Query())->from('point_order po')
        ->join('JOIN', 'point_order_item poi', 'po.order_id = poi.point_order_id')
        ->where(['po.painter_login_id' => Yii::$app->user->getId(), 'po.order_status' => '17', 'poi.Item_status' => 'G'])
        //->count();
        ->sum('poi.total_qty_point');
if($trTotal > 0){
   $pointawerd = $trTotal;
}else{
   $pointawerd = '0'; 
}
$points_redemption = '0';
$payout = '0.00';
$points_redemption = \common\models\Redemption::find()->where(['painterID' => Yii::$app->user->getId(),'redemption_status' => '19'])->sum('req_points');
$payout = \common\models\Redemption::find()->where(['painterID' => Yii::$app->user->getId(),'redemption_status' => '19'])->sum('req_amount');
$points_balance = $pointawerd - $points_redemption;

//$allorder =
$allorder = PointOrder::find()->where(['painter_login_id' => Yii::$app->user->getId()])->count();
$opending = PointOrder::find()->where(['order_status' => '1', 'painter_login_id' => Yii::$app->user->getId()])->count();
$ocancel = PointOrder::find()->where(['order_status' => '7', 'painter_login_id' => Yii::$app->user->getId()])->count();
$oapprove = PointOrder::find()->where(['order_status' => '17', 'painter_login_id' => Yii::$app->user->getId()])->count();

//Redemption
$allredemption = Redemption::find()->where(['painterID' => Yii::$app->user->getId()])->count();
$rpending = Redemption::find()->where(['redemption_status' => '1', 'painterID' => Yii::$app->user->getId()])->count();
$rcancel = Redemption::find()->where(['redemption_status' => '7', 'painterID' => Yii::$app->user->getId()])->count();
$rapprove = Redemption::find()->where(['redemption_status' => '17', 'painterID' => Yii::$app->user->getId()])->count();

//Payout
$allpayout = Redemption::find()->where(['redemption_status' => '17', 'painterID' => Yii::$app->user->getId()])->count();
$p_processing = Redemption::find()->where(['redemption_status_ray' => '2', 'painterID' => Yii::$app->user->getId()])->count();
$p_paid = Redemption::find()->where(['redemption_status_ray' => '19', 'painterID' => Yii::$app->user->getId()])->count();
$p_failed = Redemption::find()->where(['redemption_status_ray' => '10', 'painterID' => Yii::$app->user->getId()])->count();

$po_total = Redemption::find()->where(['redemption_status_ray' => '19', 'painterID' => Yii::$app->user->getId()])->count();
$po_rm = Redemption::find()->where(['redemption_status_ray' => '19', 'painterID' => Yii::$app->user->getId()])->sum('req_amount');
$po_point = Redemption::find()->where(['redemption_status_ray' => '19', 'painterID' => Yii::$app->user->getId()])->sum('req_points');

$totalawpoint = PointOrder::find()->where(['order_status' => '17', 'painter_login_id' => Yii::$app->user->getId()])->sum('order_total_point');
$totalawrm = PointOrder::find()->where(['order_status' => '17', 'painter_login_id' => Yii::$app->user->getId()])->sum('order_total_amount');

$balancerm = $totalawrm - $po_rm;
$balancepoint = $totalawpoint - $po_point;
?>

<section class="content">
    <div class="row">
        <div class="col-lg-6 col-sm-8 col-xs-12">
            <div class="callout callout-info msg-of-day" style="padding: 5px;">
                <h4><i class="fa fa-bullhorn"></i> <?php echo Yii::t('app', 'Message of day box') ?></h4>
                <p><marquee onmouseout="this.setAttribute('scrollamount', 6, 0);" onmouseover="this.setAttribute('scrollamount', 0, 0);" scrollamount="6" behavior="scroll" direction="left"><?= Yii::$app->params['message.marque'] ?></marquee></p>
            </div>
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-12">
            <div class="info-box cbt">
                <span class="info-box-icon bg-aqua cbt1"><i class="fa fa-gift"></i></span>
                <div class="info-box-content">
                    <h4 style="margin-top: 0px; margin-bottom: 0px;">Awarded</h4>
                    <span class="progress-description">Total RM <span class="pull-right text-bold">RM<?= $totalawrm ?></span></span>
                    <span class="progress-description">Total Points<span class="pull-right text-bold"><?= $totalawpoint ?></span></span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div>
        <div class="col-lg-3 col-sm-3 col-xs-12">
            <div class="info-box cbt">
                <span class="info-box-icon bg-aqua cbt1"><i class="fa fa-gift"></i></span>
                <div class="info-box-content">
                    <h4 style="margin-top: 0px; margin-bottom: 0px;">Balance</h4>
                    <span class="progress-description">RM Balance <span class="pull-right text-bold">RM<?= $balancerm ?></span></span>
                    <span class="progress-description">Points Balance <span class="pull-right text-bold"><?= $balancepoint ?></span> </span>
                </div><!-- /.info-box-content -->
            </div><!-- /.info-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?= $allorder ?></h3>
                    <h4>Total Transaction</h4>
                    <span class="progress-description">Approved <span class="pull-right"><?= $oapprove ?></span></span>
                    <span class="progress-description">Pending <span class="pull-right"><?= $opending ?></span></span>
                    <span class="progress-description">Cancel <span class="pull-right"><?= $ocancel ?></span></span>
                </div>
                <div class="icon">
                    <i class="fa fa-rub"></i>
                </div>                
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= $allredemption ?></h3>
                    <h4>Total Redemptions</h4>
                    <span class="progress-description">Approved <span class="pull-right"><?= $rapprove ?></span></span>
                    <span class="progress-description">Pending <span class="pull-right"><?= $rpending ?></span></span>
                    <span class="progress-description">Cancel <span class="pull-right"><?= $rcancel ?></span></span>
                </div>
                <div class="icon">
                    <i class="fa fa-mail-forward"></i>
                </div>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?= $allpayout ?></h3>
                    <h4>Total Pay-out</h4>
                    <span class="progress-description">Processing <span class="pull-right"><?= $p_processing ?></span></span>
                    <span class="progress-description">Paid <span class="pull-right"><?= $p_paid ?></span></span>
                    <span class="progress-description">Failed <span class="pull-right"><?= $p_failed ?></span></span>
                </div>
                <div class="icon">
                    <i class="fa fa-mail-reply"></i>
                </div>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-light-blue">
                <div class="inner">
                    <h3><?= $po_total ?></h3>
                    <h4>Paid</h4>
                    <span class="progress-description">Total RM Pay out <span class="pull-right">RM<?= $po_rm ?></span></span>
                    <span class="progress-description">Total Points Pay out <span class="pull-right"><?= $po_point ?></span></span>
                    <br>
                </div>
                <div class="icon">
                    <i class="fa fa-money"></i>
                </div>
            </div>
        </div><!-- ./col -->
    </div>
    
    
    <div class="row">
       <div class="col-lg-6 col-xs-12">
           <?= ProductOverviewPainterWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        </div> <!---/.col-sm-3----> 
        
        <div class="col-lg-6 col-xs-12">
            <?= RedemptionRequestPainterWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        </div> <!---/.col-sm-3---->
        
        <div class="col-lg-6 col-xs-12">
            <?= PayOutPainterWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        </div> <!---/.col-sm-3---->              
    </div>
</section>

