<?php
use frontend\assets\DataTableAsset;

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\components\TopMenuWidget;
use app\components\LeftMenuWidget;
use app\components\AlertWidget;
//use frontend\widgets\Alert;
use common\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */
$baseURL = Yii::$app->request->url;

DataTableAsset::register($this);
//AppAsset::register($this);
$controller = Yii::$app->controller;
$default_controller = Yii::$app->defaultRoute;
$isHome = (($controller->id === $default_controller) && ($controller->action->id === $controller->defaultAction)) ? true : false;
if($isHome == true){
    $dash = '';
}else {
    $dash = '-';
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    
    <title>Dulux Painter's Club <?= $dash ?> <?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body class="skin-black  pace-done">
<?php $this->beginBody() ?>
    <div class="wrapper">
        <!-- Topmenu Widget --->
        <?= TopMenuWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        <!-- End Top Menu ---->
        <!-- Left side column. contains the logo and sidebar -->
        <?= LeftMenuWidget::widget(['path' => Yii::$app->request->getPathInfo()]) ?>
        <div class="content-wrapper">
            <?=
            Breadcrumbs::widget(
                    [
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        'homeLink' => ['label' => '<i class="fa fa-dashboard"></i> ' . Yii::t('app', 'Home'), 'encode' => false, 'url' => Yii::$app->homeUrl],
                    ]
            )
            ?> 
            <section class="content-header">
                 
            </section>
            
            <section class="content">
                 <?= Alert::widget() ?>
                <div class="row">
                    <?= $content ?> 
                </div>
               
            </section>
        </div>
        
        <footer class="main-footer">
            <strong>Copyright &copy; 2014-<?= date('Y') ?>.</strong> All rights reserved.
        </footer>
        

            <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>        
    </div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
