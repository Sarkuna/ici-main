<?php
use frontend\assets\LoginAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

LoginAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<link rel="icon" type="image/png" sizes="64x64" href="/images/favicon.ico">
    Apple/Safari icon 
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon.ico">-->
    <?= Html::csrfMetaTags() ?>
    <title>Dulux Painter's Club</title>
    <?php $this->head() ?>

</head>
<style>
    .headerbg{background-color: #012169;border-bottom: 1px solid #DFE1E3;}
    .major-logo img {
        display: block;
        height: 70px;
        margin: 10px 0;
    }
    .minor-logo{
        height: 15px;
        padding-top: 30px;
    }
    .minor-logo img{
        height: 15px;
    }
    
    .login-box-body, .register-box-body {
        border-radius: 10px;
    }
</style>
<body class="login-page">
    <?php $this->beginBody() ?>
    
    <div class="wrap">
        <div class="row">
            <div class="col-lg-12 headerbg">
                <div class="col-lg-offset-2 col-lg-4 major-logo">
                    <img src="/images/club_logo.png" alt="Dulux Malaysia" title="Click to go to home page">
                </div>
                <div class="col-lg-push-2 col-lg-4 col-xs-6 minor-logo" style="display: inline-block;vertical-align: middle;float: none;">
                    <img src="/images/akzonobel-white.png" alt="Akzonobel Malaysia" title="Click to go to home page">
                </div>
            </div>
        </div>
        <div class="container">
            <?= Alert::widget() ?>
        <?= $content ?>
        </div>
    </div>
    <?php $this->endBody() ?>
    
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
</body>
</html>
<?php $this->endPage() ?>
