<?php
use frontend\assets\DashbordAsset;

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\components\TopMenuWidget;
use app\components\LeftMenuWidget;
use app\components\AlertWidget;
//use frontend\widgets\Alert;
use common\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */
$baseURL = Yii::$app->request->url;

DashbordAsset::register($this);
//AppAsset::register($this);
?>

<?= $content ?> 

