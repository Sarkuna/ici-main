<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;

class ShihanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                    ],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        $phone="+601118889597";  // Enter your phone number here
        $apikey="yU5nmPZyoHBP";     // Enter your personal apikey received in step 3 above
        $message = "Dear Mr John Doe from Wonderful Paint Shop, on behalf of AkzoNobel, here's your Dulux In-Store Incentive Rewards for May-Jun '23 Campaign.

TnG E-Wallet Reload Pin: xxxxxxxxxx (RM100), xxxxxxxxxx (RM50) 

Total: RM150.00. Expiry Date: 23-08-2024. 

Kindly acknowledge the receipt of rewards by replying *RECEIVED* to this message.

Any queries please contact 0374909139 or reply message to this number. Thank You.";

        $url='https://api.whatsapp.com/send/?phone='.$phone.'&text='.urlencode($message);
echo $url;
die;
        if($ch = curl_init($url))
        {
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $html = curl_exec($ch);
            $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            // echo "Output:".$html;  // you can print the output for troubleshooting
            curl_close($ch);
            return $html;
        }
        else
        {
            return false;
        }
    }

}
