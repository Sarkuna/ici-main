<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;

use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'request-password-reset', 'reset-password','about','webcam','mycam','contactus','termsandconditions','test','testrbc','testsms'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'changepassword', 'login', 'terms', 'viewapplication', 'viewapplication2', 'change-password','contactus','termsandconditions','testrbc','testsms','redeem'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    /*public function actionIndex()
    {
        return $this->render('index');
    }*/
    
    public function actionIndex() {

        $session = Yii::$app->session;

        if (!Yii::$app->user->isGuest) {

            //if (($session['currentPersona'] == "owner") || ($session['currentPersona'] == "tenant")) {
            if (($session['currentRole'] == Yii::$app->params['role.type.administrator'])) {
                return $this->render('dashboard_administrator');
            } else if (($session['currentRole'] == Yii::$app->params['role.type.management'])) {
                return $this->render('dashboard_management');
            } else if (($session['currentRole'] == Yii::$app->params['role.type.support'])) {
                return $this->render('dashboard_support');
            } else if (($session['currentRole'] == Yii::$app->params['role.type.painter'])) {
                return $this->render('dashboard_painter');
            }
        } else {
            return $this->redirect(['home']);
        }
        //return $this->render('index');
    }
    
    

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = 'loginlayout';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            Yii::$app->ici->addtolog('success', Yii::$app->user->id);
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionContactus()
    {
        //echo 'test';
        return $this->render('contactus');
    }
    
    public function actionTermsandconditions()
    {
        //echo 'test';
        return $this->render('termsandconditions');
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        $user = 'PG%200346595-V';
        $ADRegNo = htmlentities(trim($user), ENT_QUOTES);
        $new = str_replace(' ', '%20', 'AG%200346595-V');
        $url = 'https://michelin.rewardssolution.com/site/webapi?user=PG0346595-V&point=2805&pointexpiring=2805&pointexpireddate=31-Jan-2022';
        $url = str_replace("%20", " ", $url);
        echo $url;
        //echo phpinfo();
        //return $this->render('test');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    /*public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }*/

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = 'loginlayout';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = 'loginlayout';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            \Yii::$app->getSession()->setFlash('success', ['title' => 'Reset Password', 'text' => 'New password saved.']);

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
    public function actionWebcam(){
        //echo 'test';
        $img = '<img src="/upload/documents/camera757.jpg" class="user-image" alt="User Image">';
        return $this->render('webcam', [
                //'model' => $model,
                'img' => $img,
            ]);
    }
    
    public function actionTest($mydate){
        $date = Yii::$app->request->get('mydate');

        foreach (\common\models\EmailQueue::find()->where("email_template_global_id = '5' AND sms_status = 'P' AND (mobile IS NOT NULL) AND (created_datetime LIKE '%$date%')")->limit(300)->all() as $emailQueue) {
            //Check date_to_send 
            $today = new \DateTime('now');

            $emailTemplate = \common\models\EmailTemplate::findOne($emailQueue->email_template_global_id);

            $emailBody = $emailTemplate->sms_text;

            $painterinfo = \common\models\PainterProfile::find()
            ->where(['user_id' => $emailQueue->user_id])
            ->one();

            $name = $painterinfo->full_name;
            $membership_ID = $painterinfo->card_id;
            $mobile = str_replace(' ','',$emailQueue->mobile);  

            $data2 = \yii\helpers\Json::decode($emailQueue['data']);
            
            $redemption_ID = isset($data2['redemption_ID']) ? $data2['redemption_ID'] : null;
            $intake_month = isset($data2['intake_month']) ? $data2['intake_month'] : null;
            $paid_date = isset($data2['re_paid_date']) ? $data2['re_paid_date'] : null;
            $redemption_value  = isset($data2['redemption_value']) ? $data2['redemption_value'] : null;
            $redemption_point = isset($data2['redemption_point']) ? $data2['redemption_point'] : null;
            $total_approved_transactions = isset($data2['total_approved_transactions']) ? $data2['total_approved_transactions'] : null;
            $account_holder_name = isset($data2['account_holder_name']) ? $data2['account_holder_name'] : null;
            $bank_account_no = isset($data2['bank_account_no']) ? $data2['bank_account_no'] : null;


            $emailBody = str_replace('{name}', $name, $emailBody);
            $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
            $emailBody = str_replace('{redemption_ID}', $redemption_ID, $emailBody);
            $emailBody = str_replace('{intake_month}', $intake_month, $emailBody);
            $emailBody = str_replace('{paid_date}', $paid_date, $emailBody);
            $emailBody = str_replace('{redemption_value}', $redemption_value, $emailBody);
            $emailBody = str_replace('{redemption_point}', $redemption_point, $emailBody);
            $emailBody = str_replace('{total_approved_transactions}', $total_approved_transactions, $emailBody);
            $emailBody = str_replace('{account_holder_name}', $account_holder_name, $emailBody);
            $emailBody = str_replace('{bank_account_no}', $bank_account_no, $emailBody);

            $message = $emailBody;
            $message = html_entity_decode($message, ENT_QUOTES, 'utf-8'); 
            $message = urlencode($message);

            $username = urlencode(Yii::$app->params['sms.username']);
            $password = urlencode(Yii::$app->params['sms.password']);
            $sender_id = urlencode("66300");
            $type = '1';
            

                  
            $fp = "https://www.isms.com.my/isms_send.php?un=$username&pwd=$password&dstno=$mobile&msg=$message&type=$type&sendid=$sender_id";
            $handle = @fopen($fp, "r");
            if ($handle) {
              while (!feof($handle)) {
                  $buffer = fgets($handle, 10000);
                  $st = $buffer;
              }
            }
            
            //if($st == '2000 = SUCCESS' || $st == 'EMPTY/BLANK'){
                $emailQueueToUpdate = \common\models\EmailQueue::findOne($emailQueue->id);
                $emailQueueToUpdate->sms_status = "S";
                $emailQueueToUpdate->sms_result = $st;
                $emailQueueToUpdate->date_to_send = date('Y-m-d');
                $emailQueueToUpdate->save();
            //}
            echo $mobile.','.$st.'<br>';
        }
        echo 'Report - '.$date;
        exit();
    }
    
    public function actionTestrbc(){
        $vdate = date('d-m-Y H:i:s');
        Yii::$app->mailer->compose()
                    ->setTo('shihanagni@gmail.com')
                    ->setFrom([Yii::$app->params['supportEmail'] =>  'Dulux Painter\'s Club'])
                    ->setSubject('Cron Testing DEv')
                    ->setHtmlBody('Cron Detail Message')
                    ->send();
    }
    
    public function actionMycam(){
        //echo 'test';
        $encoded_data = $_POST['username'];        
        $image_data = base64_decode( $encoded_data );
        $path = Yii::getAlias('@frontend') .'/web/upload/documents/';
        $file_name = $path;
        $image_name = substr(uniqid(rand(1,6)), 0, 8).'.jpg';
        $file = $file_name . $image_name;

        $success = file_put_contents($file, $image_data);
        
        if ($success) {
            //echo "Successfully uploaded"; 
            $response['image_name'] = $image_name;
        } else {
            //echo "Not uploaded";
            $response = null;
        }
        echo json_encode($response);
    }
    
    public function actionTestsms(){
        $emailBody = 'test';
        $mobile = "601118889597";
        
        $message = $emailBody;
        $message = html_entity_decode($message, ENT_QUOTES, 'utf-8'); 
        $message = urlencode($message);
        //$destination = $myuser->cust_mobile;
        //$new = substr($myuser->mobile, 0, -3) . 'xxx';
        //echo $short.'<br>';


                    
        $username = urlencode(Yii::$app->params['sms.username']);
        $password = urlencode(Yii::$app->params['sms.password']);
        $sender_id = urlencode("66300");
        $type = '1';
        
        $link = "https://www.isms.com.my/isms_send.php";
        $link .= "?un=$username&pwd=$password&dstno=$mobile&msg=$message&type=$type&sendid=$sender_id";
        
        //print_r($link);
        //die();
        //$result = Yii::$app->residenz->ismscURL($fp);
        $http = curl_init($link);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);
        echo '<pre>';
        print_r($http_result);
        //return $http_result;
    }
    
    
}