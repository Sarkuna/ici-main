<?php

namespace frontend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use common\models\ProfileUser;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\painter\models\RedeemForm;
use app\modules\painter\models\BankingInformation;
/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionChange()
    {
	$model=$this->findModel(Yii::$app->user->id);
	$model->scenario = 'change';

	if(isset($_POST['User']))
	{
		$model->attributes = $_POST['User'];
		$user = User::findOne(Yii::$app->user->id);
                $user->setPassword($model->new_pass);
                $user->auth_key = Yii::$app->security->generateRandomString();
		//$model->password_hash = md5($model->new_pass.$model->new_pass);
		if($user->save()){
                    //Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');                    
                    \Yii::$app->getSession()->setFlash('success',['title' => 'Change Password', 'text' => 'Your password has been changed successfully']);
                    return $this->goHome();
                    //return $this->redirect(['/site/index']);
                }else{
                    print_r($user->getErrors());
                }
	}

	return $this->render('password_changeform',[
		'model'=>$model,
	]);
     }
     
     public function actionRedeem() {
         
	$model = new RedeemForm();

	if ($model->load(Yii::$app->request->post())){
		$userid = Yii::$app->user->id;
                $bankinfo = BankingInformation::find()->where(['user_id' => $userid, 'account_no_verification' => 'Y'])->one();
                $profile = \common\models\PainterProfile::find()->where(['user_id' => $userid])->one();

                $bankAccNo = $bankinfo->account_number;
                $customerName = $bankinfo->account_name;
                $bankName = $bankinfo->bankName->bank_name;
                $bicCode = $bankinfo->bankName->bic_code;
                $amount = number_format((float)$model->amount, 2, '.', ''); 
                
                if($model->type == 'S'){
                    $url = 'https://san.gomobi.io/externalapi/payoutservice';
                    $mobiApiKey = 'b07ad9f31df158edb188a41f725899bc';
                    $businessRegNo = '12345';
                    $subMID = '201100000012450';
                    
                    $jsonData = array(
                        'service' => 'PAYOUT_TXN_REQ',
                        'mobiApiKey' => 'b07ad9f31df158edb188a41f725899bc',
                        'businessRegNo' => '12345',
                        'bankName' => 'Ambank Malaysia Berhad',
                        'bankAccNo' => '8881048358878',
                        'customerName' => 'mobitest',
                        'amount' => $amount,
                        'subMID' => '201100000012450',
                        'bicCode' => 'ARBKMYKL',
                    );
                }else {
                    $url='https://trustpayouts.gomobi.io/externalapi/payoutservice';
                    $mobiApiKey = 'a2d59377c23cc149a0c2206fd51e8416';
                    $businessRegNo = 'REWA61411';
                    $subMID = '201100000000138';
                    
                    $jsonData = array(
                        'service' => 'PAYOUT_TXN_REQ',
                        'mobiApiKey' => $mobiApiKey,
                        'businessRegNo' => $businessRegNo,
                        'bankName' => $bankName,
                        'bankAccNo' => $bankAccNo,
                        'customerName' => $customerName,
                        'amount' => $amount,
                        'subMID' => $subMID,
                        'bicCode' => $bicCode,
                    );
                }

                // Convert the data to JSON format
                $jsonString = json_encode($jsonData);

                // Initialize cURL session
                $ch = curl_init($url);

                // Set cURL options
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonString);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($jsonString)
                ));

                // Execute cURL session and get the result
                $response = curl_exec($ch);

                // Check for cURL errors
                if (curl_errno($ch)) {
                    echo 'Curl error: ' . curl_error($ch);
                }

                // Close cURL session
                curl_close($ch);

                $data = json_decode($response, true);

                if($data['responseMessage'] == 'FAILURE') {
                    $msg = 'Dear '.$profile->full_name.', Your payment request was Unsuccessful due to Account Number Invalid. Please update your bank details to AkzoNobel Sales PIC. Your current registered bank details are as follows: '.$bankName.' '.$bankAccNo.' '.$customerName;
                }else {
                    $msg = 'Dear '.$profile->full_name.', Your payment request has been successful. Paid Date: '.date('d-m-Y').' Value: RM'.$amount.' Paid to : '.$customerName.' Bank Name: '.$bankName.' Bank Account No:'.preg_replace('/\d(?=\d{4})/m', '*', $bankAccNo);
                }
                $emailBody = $msg;
                $mobile = str_replace(' ','',$profile->mobile); 
                $message = $emailBody;
                $message = html_entity_decode($message, ENT_QUOTES, 'utf-8'); 
                $message = urlencode($message);

                $username = urlencode(Yii::$app->params['sms.username']);
                $password = urlencode(Yii::$app->params['sms.password']);
                $sender_id = urlencode("66300");
                $type = '2';

                $fp = "https://www.isms.com.my/isms_send.php?un=$username&pwd=$password&dstno=$mobile&msg=$message&type=$type&sendid=$sender_id";
                $handle = @fopen($fp, "r");
                if ($handle) {
                  while (!feof($handle)) {
                      $buffer = fgets($handle, 10000);
                      $st = $buffer;
                  }
                }

                return $this->render('redeem_result',[
                        'response'=>$data,
                ]);
	}else {
            return $this->render('redeem',[
                    'model'=>$model,
            ]);
        }
    }
     
     public function actionProfile(){
        $model=$this->findProfile(Yii::$app->user->id);
        $userid = $model->user_id;
        $user = \common\models\User::find()->where(['id' => $userid])->one();
        return $this->render('profile_user',[
            'model' => $model,
            'user' => $user,
	]);
     }
     
     public function actionProfileupdate($id){
        $model=$this->findProfile($id);
        $userid = $model->user_id;
        $user = \common\models\User::find()->where(['id' => $userid])->one();
        return $this->render('update_profile',[
            'model' => $model,
            'user' => $user,
	]);
     }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findProfile($id)
    {
        $id = '1';
        if (($model = ProfileUser::findOne(['profile_user_id' => $id, 'user_id' => Yii::$app->user->id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
