<?php
    $session = Yii::$app->session;

    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\grid\GridView;
?>

<?php
$copunt = 0;
$productoverviews = $dataProvider->getModels();
//echo '<pre>';
//print_r($announcements);
//die();
$count = count($productoverviews);

?>

<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">Transactions Overview</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table no-margin">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Transaction #</th>
                        <th>Dealer Name</th>
                        <th>Total Items</th>
                        <th class="text-center">Total Points</th>
                        <th class="text-center">Total RM</th>
                        <th>Status</th>
                        <th></th> 
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($count > 0) {
                        foreach ($productoverviews as $productoverview) {
                            if($productoverview->order_status == '1'){
                               $lbg = 'warning';
                            }else if($productoverview->order_status == '17'){
                               $lbg = 'success';
                            }else if($productoverview->order_status == '7'){
                               $lbg = 'danger';
                            }else{
                               $lbg = 'default'; 
                            }
                            $total_points = \common\models\PointOrderItem::find()->where(['point_order_id' => $productoverview->order_id,'Item_status' => 'G'])->sum('item_bar_total_point');
                            $mytotal_points = $total_points ? $total_points : '0';
                            $status = '<span class="label label-'.$lbg.'">'.$productoverview->orderStatus->name.'</span>';
                            $ahrf = '<a href="/management/pointorder/myview?id='.$productoverview->order_id.'"><span class="glyphicon glyphicon-search"></span></a>';
                            echo '<tr>';
                            echo '<td width="100">' . date('d-m-Y', strtotime($productoverview->created_datetime)) . '</td>';
                            echo '<td width="100">' . $productoverview->order_num . '</td>';
                            echo '<td>' . $productoverview->dealerOutlet->customer_name . '</td>';
                            echo '<td>'.$productoverview->getTotalItems().'</td>';
                            echo '<td class="text-right">' . $productoverview->order_total_point . '</td>';
                            echo '<td class="text-right">' . $productoverview->order_total_amount . '</td>';
                            echo '<td>' . $status . '</td>';
                            echo '<td>' . $ahrf . '</td>';
                            echo '</tr>';
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div><!-- /.table-responsive -->
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        <a href="/management/pointorder/myindex" class="btn btn-sm btn-default btn-flat pull-right">View All</a>
    </div><!-- /.box-footer -->
</div><!-- /.box -->