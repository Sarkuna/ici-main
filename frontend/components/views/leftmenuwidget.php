<?php
$session = Yii::$app->session;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <?php
        $role = Yii::$app->session->get('currentRole');
        $myurl = Yii::$app->request->url;
        $menuItems[] = '';
        
        $home = ['label' => '<i class="fa fa-home"></i> <span>Home</span></i>', 'url' => ['/site']];
        if (($session['currentRole'] == Yii::$app->params['role.type.administrator'])) {
            //$adminlink1 = ['label' => '<i class="fa fa-money"></i> <span>Redemption Payment</span></i>', 'url' => ['/management/redemption/payment'], 'active' => $myurl == '/management/redemption/payment'];
            $adminlink1 = ['label' => '<i class="fa fa-money-bill-alt"></i> <span>Redemption Payment</span>',
                    'url' => ['#'],
                    'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                    'items' => [
                        ['label' => '<i class="fa fa-exclamation-circle"></i> Processing', 'url' => ['/management/redemption/processing'], 'active' => $myurl == '/management/redemption/processing'],
                        ['label' => '<i class="fa fa-exclamation-circle"></i> Pay out Summary', 'url' => ['/management/redemption-payment/index'], 'active' => $myurl == '/management/redemption-payment/index'],
                        ['label' => '<i class="fa fa-ban"></i> Denied', 'url' => ['/management/redemption/denied'], 'active' => $myurl == '/management/redemption/denied'],
                        ['label' => '<i class="fa fa-times"></i> Failed', 'url' => ['/management/redemption/failed'], 'active' => $myurl == '/management/redemption/failed'],
                        ['label' => '<i class="fa fa-check"></i> Paid', 'url' => ['/management/redemption/paid'], 'active' => $myurl == '/management/redemption/paid'],
                        //['label' => '<i class="fa fa-envelope-open"></i> <span>SMS Queue</span>', 'url' => ['/management/sms-queue/index'], 'active' => $myurl == '/management/sms-queue/index'],
                        //['label' => 'Another action', 'url' => '#'],
                        //['label' => 'Something else here', 'url' => '#'],
                    ],
                ];
                
            //$adminlink2 = ['label' => '<i class="fa fa-id-card-o"></i> <span>Manage Dealer Lists</span></i>', 'url' => ['/management/dealerlist'], 'active' => $myurl == '/management/dealerlist'];
            $adminlink2 = ['label' => '<i class="fa fa-folder text-green"></i> <span>RAY Settings</span>',
                    'url' => ['#'],
                    'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                    'items' => [
                        ['label' => '<i class="fa fa-circle text-blue"></i> Redemption Summary', 'url' => ['/management/report/redemptionsummery'], 'active' => $myurl == '/management/report/redemptionsummery'],
                        ['label' => '<i class="fa fa-address-card"></i> <span>Manage Dealer Lists</span></i>', 'url' => ['/management/dealerlist'], 'active' => $myurl == '/management/dealerlist'],
                        //['label' => '<i class="fa fa-file-alt text-yellow"></i> MAS Text', 'url' => ['/management/report/bulkpayment'], 'active' => $myurl == '/management/report/bulkpayment'],
                        ['label' => '<i class="fa fa-file-alt text-yellow"></i> Public Bank', 'url' => ['/management/report/bulk-payment-excel'], 'active' => $myurl == '/management/report/bulk-payment-excel'],
                        ['label' => '<i class="fa fa-file-alt text-red"></i> Bar Code Public Bank', 'url' => ['/management/report/bar-code-public-bank'], 'active' => $myurl == '/management/report/bar-code-public-bank'],
                        ['label' => '<i class="fa fa-landmark"></i> <span>Bank Account Verification</span></i>', 'url' => ['/management/import/bulk-account-verified'], 'active' => $myurl == '/management/import/bulk-account-verified'],
                        ['label' => '<i class="fa fa-envelope"></i> <span>Email Templates</span></i>', 'url' => ['/admin/emailtemplate'], 'active' => $myurl == '/admin/emailtemplate'],
                        //['label' => 'Another action', 'url' => '#'],
                        //['label' => 'Something else here', 'url' => '#'],
                    ],
                ];
            
        }else{
            $adminlink1 = '';
            $adminlink2 = '';
        }
        
        if ($session['currentRole'] == Yii::$app->params['role.type.management']) {
            $payout = ['label' => '<i class="fa fa-exclamation-circle"></i> Pay out Summary', 'url' => ['/management/redemption-payment/index'], 'active' => $myurl == '/management/redemption-payment/index'];
        }else {
            $payout = '';
        }

        if ($session['currentRole'] == Yii::$app->params['role.type.administrator'] || $session['currentRole'] == Yii::$app->params['role.type.management']) {
            $menuItems = [
                $home,
                ['label' => '<i class="fa fa-shopping-cart"></i> <span>Register Painter / <br>   Record Transaction</span>', 'url' => ['/management/pointorder/findpainter'], 'active' => $myurl == '/management/pointorder/findpainter'],
                //['label' => '<i class="fa fa-users"></i> <span>Painter Account Management</span>', 'url' => ['/painter/painterprofile'], 'active' => $myurl == '/painter/painterprofile'],
                ['label' => '<i class="fa fa-users"></i> <span>Painter Account <br>Management</span>',
                    'url' => ['#'],
                    'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                    'items' => [
                        ['label' => '<i class="fa fa-exclamation-circle"></i> Pending', 'url' => ['/painter/painterprofile'], 'active' => $myurl == '/painter/painterprofile'],
                        ['label' => '<i class="fa fa-check"></i> <span>Approve</span></i>', 'url' => ['/painter/painterprofile/approve'], 'active' => $myurl == '/painter/painterprofile/approve'],
                        ['label' => '<i class="fa fa-question-circle"></i> Review', 'url' => ['/painter/painterprofile/review'], 'active' => $myurl == '/painter/painterprofile/review'],
                        ['label' => '<i class="fa fa-times"></i> <span>Decline</span>', 'url' => ['/painter/painterprofile/decline'], 'active' => $myurl == '/painter/painterprofile/decline'],
                        //['label' => 'Another action', 'url' => '#'],
                        //['label' => 'Something else here', 'url' => '#'],
                    ],
                ],
                
                //['label' => '<i class="fa fa-suitcase"></i> <span>Membership Pack</span>', 'url' => ['/support/membershippack'], 'active' => $myurl == '/support/membershippack'],
                //['label' => '<i class="fa fa-rub"></i> <span>Painter Transaction<br>Management</span>', 'url' => ['/management/pointorder'], 'active' => $myurl == '/management/pointorder'],
                
                ['label' => '<i class="fa fa-ruble-sign"></i> <span>Painter Transaction<br>Management</span>',
                    'url' => ['#'],
                    'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                    'items' => [
                        //['label' => '<i class="fa fa-search"></i> Search', 'url' => ['/management/pointorder/search'], 'active' => $myurl == '/management/pointorder/search'],
                        ['label' => '<i class="fa fa-exclamation-circle"></i> Pending', 'url' => ['/management/pointorder'], 'active' => $myurl == '/management/pointorder'],
                        ['label' => '<i class="fa fa-check"></i> <span>Approve</span></i>', 'url' => ['/management/pointorder/approved'], 'active' => $myurl == '/management/pointorder/approved'],
                        ['label' => '<i class="fa fa-times"></i> Cancel', 'url' => ['/management/pointorder/canceled'], 'active' => $myurl == '/management/pointorder/canceled'],
                        ['label' => '<i class="fa fa-upload"></i> Bulk Transaction', 'url' => ['/management/excel-transaction/index'], 'active' => $myurl == '/management/excel-transaction/index'],
                        //['label' => '<i class="fa fa-envelope-open"></i> <span>SMS Queue</span>', 'url' => ['/management/sms-queue/index'], 'active' => $myurl == '/management/sms-queue/index'],
                        //['label' => 'Another action', 'url' => '#'],
                        //['label' => 'Something else here', 'url' => '#'],
                    ],
                ],                
                
                //['label' => '<i class="fa fa-handshake"></i> <span>Redemption on-behalf</span>', 'url' => ['/management/pointorder/onbhalfgroup'], 'active' => $myurl == '/management/pointorder/onbhalfgroup'],
                //['label' => '<i class="fa fa-mail-forward"></i> <span>Points Redemption <br>Management</span>', 'url' => ['/management/redemption'], 'active' => $myurl == '/management/redemption'],
                ['label' => '<i class="fa fa-star"></i> <span>Points Redemption<br>Management</span>',
                    'url' => ['#'],
                    'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                    'items' => [
                        ['label' => '<i class="fa fa-exclamation-circle"></i> Pending', 'url' => ['/management/redemption'], 'active' => $myurl == '/management/redemption'],
                        ['label' => '<i class="fa fa-check"></i> <span>Approve</span></i>', 'url' => ['/management/redemption/approved'], 'active' => $myurl == '/management/redemption/approved'],
                        ['label' => '<i class="fa fa-ban"></i> <span>Canceled</span></i>', 'url' => ['/management/redemption/canceled'], 'active' => $myurl == '/management/redemption/canceled'],
                    ],
                ],
                //['label' => '<i class="fa fa fa-gift"></i> <span>Campaign Management</span>', 'url' => ['/management/campaign'], 'active' => $myurl == '/management/campaign'],
                $payout,
                $adminlink1,
                ['label' => '<i class="fa fa-money-bill-alt"></i> <span>Bar Code Payment</span>',
                    'url' => ['#'],
                    'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                    'items' => [
                        ['label' => '<i class="fa fa-upload"></i> Upload Excel', 'url' => ['/management/bar-code/create'], 'active' => $myurl == '/management/bar-code/create'],
                        ['label' => '<i class="fa fa-exclamation-circle"></i> Pay out Summary', 'url' => ['/management/bar-code/index'], 'active' => $myurl == '/management/bar-code/index'],
                        ['label' => '<i class="fa fa-upload"></i> Bulk Payment Upload', 'url' => ['/management/bc-redemption/import-excel-redemption'], 'active' => $myurl == '/management/bc-redemption/import-excel-redemption'],                        
                        /*['label' => '<i class="fa fa-ban"></i> Denied', 'url' => ['/management/redemption/denied'], 'active' => $myurl == '/management/redemption/denied'],
                        ['label' => '<i class="fa fa-times"></i> Failed', 'url' => ['/management/redemption/failed'], 'active' => $myurl == '/management/redemption/failed'],
                        ['label' => '<i class="fa fa-check"></i> Paid', 'url' => ['/management/redemption/paid'], 'active' => $myurl == '/management/redemption/paid'],*/
                    ],
                ],
                $adminlink2,
                ['label' => '<i class="fa fa-folder"></i> <span>Report Center</span>',
                    'url' => ['#'],
                    'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                    'items' => [
                        //['label' => '<i class="fa fa-circle"></i> Summary View', 'url' => ['/management/report'], 'active' => $myurl == '/management/report'],
                        ['label' => '<i class="fa fa-circle text-red"></i> Painter Account Management', 'url' => ['/management/report/painterprofilesummery'], 'active' => $myurl == '/management/report/painterprofilesummery'],
                        ['label' => '<i class="fa fa-circle text-yellow"></i> Transaction Supporting<br>Document', 'url' => ['/management/report/transactionitem'], 'active' => $myurl == '/management/report/transactionitem'],
                        //['label' => '<i class="fa fa-circle text-green"></i> Redemption Supporting<br>Document', 'url' => ['/management/report/redemptionsd'], 'active' => $myurl == '/management/report/redemptionsd'],
                        ['label' => '<i class="fa fa-circle text-green"></i> Redemption Supporting<br>Excel', 'url' => ['/management/report/redemption-supporting-document-excel'], 'active' => $myurl == '/management/report/redemption-supporting-document-excel'],
                        ['label' => '<i class="fa fa-circle text-teal"></i> Redemption Statement', 'url' => ['/management/redemption-statement'], 'active' => $myurl == '/management/redemption-statement'],
                        ['label' => '<i class="fa fa-circle text-teal"></i> Dealer Database', 'url' => ['/management/report/dealerlist-excel'], 'active' => $myurl == '/management/report/dealerlist-excel'],
                        ['label' => '<i class="fa fa-circle text-teal"></i> Transaction- Management', 'url' => ['/management/report/painter-transaction-management'], 'active' => $myurl == '/management/report/painter-transaction-management'],
                        
                        
                        //['label' => 'Another action', 'url' => '#'],
                        //['label' => 'Something else here', 'url' => '#'],
                    ],
                ],
                //['label' => '<i class="fa fa fa-envelope"></i> <span>SMS Setting</span>', 'url' => ['/management/sms-setting'], 'active' => $myurl == '/management/sms-setting'],
                    //['label' => 'Menu 2', 'url' => ['/link2/index']],
            ];
        } else if (($session['currentRole'] == Yii::$app->params['role.type.support'])) {
            $menuItems = [
                $home,
                ['label' => '<i class="fa fa-shopping-cart"></i> <span>Register Painter / <br>   Record Transaction</span>', 'url' => ['/management/pointorder/findpainter'], 'active' => $myurl == '/management/pointorder/findpainter'],
                //['label' => '<i class="fa fa-users"></i> <span>Painter Account Management</span>', 'url' => ['/painter/painterprofile'], 'active' => $myurl == '/painter/painterprofile'],
                ['label' => '<i class="fa fa-users"></i> <span>Painter Account <br>Management</span>',
                    'url' => ['#'],
                    'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                    'items' => [
                        ['label' => '<i class="fa fa-exclamation-circle"></i> Pending', 'url' => ['/painter/painterprofile'], 'active' => $myurl == '/painter/painterprofile'],
                        ['label' => '<i class="fa fa-check"></i> <span>Approve</span></i>', 'url' => ['/painter/painterprofile/approve'], 'active' => $myurl == '/painter/painterprofile/approve'],
                        //['label' => '<i class="fa fa-question-circle"></i> Review', 'url' => ['/painter/painterprofile/review'], 'active' => $myurl == '/painter/painterprofile/review'],
                        //['label' => '<i class="fa fa-times"></i> <span>Decline</span>', 'url' => ['/painter/painterprofile/decline'], 'active' => $myurl == '/painter/painterprofile/decline'],
                        //['label' => 'Another action', 'url' => '#'],
                        //['label' => 'Something else here', 'url' => '#'],
                    ],
                ],
                ['label' => '<i class="fa fa-suitcase"></i> <span>Membership Pack</span>', 'url' => ['/support/membershippack'], 'active' => $myurl == '/support/membershippack'],
                ['label' => '<i class="fa fa-ruble-sign"></i> <span>Painter Transaction<br>Management</span>',
                    'url' => ['#'],
                    'template' => '<a href="#" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                    'items' => [
                        ['label' => '<i class="fa fa-exclamation-circle"></i> Pending', 'url' => ['/management/pointorder'], 'active' => $myurl == '/management/pointorder'],
                    ],
                ], 
            ];
        } else if (($session['currentRole'] == Yii::$app->params['role.type.painter'])) {
            $menuItems = [
                $home,
                ['label' => '<i class="fa fa fa-rub"></i> <span>Transactions</span>', 'url' => ['/management/pointorder/myindex'], 'active' => $myurl == '/management/pointorder/myindex'],
                ['label' => '<i class="fa fa-mail-forward"></i> <span>Redemptions</span>', 'url' => ['/painter/painterprofile/redemption'], 'active' => $myurl == '/painter/painterprofile/redemption'],
                ['label' => '<i class="fa fa-mail-reply"></i> <span>Pay-out Amount</span>', 'url' => ['/painter/painterprofile/payout'], 'active' => $myurl == '/painter/painterprofile/payout'],
                ['label' => '<i class="fa fa-user"></i> <span>My Profile</span>', 'url' => ['/painter/profile'], 'active' => $myurl == '/painter/profile'],
                ['label' => '<i class="fa fa-life-ring"></i> <span>Contact Us</span>', 'url' => ['/site/contactus'], 'active' => $myurl == '/site/contactus'],
                ['label' => '<i class="fa fa-handshake-o"></i> <span>Terms and conditions</span>', 'url' => ['/site/termsandconditions'], 'active' => $myurl == '/site/termsandconditions'],
            ];
        }
        ?>
        
        
        <?php
            echo Menu::widget([
                'options' => ['class' => 'sidebar-menu treeview'],
                'items' => $menuItems,
                'submenuTemplate' => "\n<ul class='treeview-menu'>\n{items}\n</ul>\n",
                'encodeLabels' => false, //allows you to use html in labels
                'activateParents' => true,]);
        ?>
        
    </section>
    <!-- /.sidebar -->
</aside>