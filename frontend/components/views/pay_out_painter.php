<?php
    $session = Yii::$app->session;

    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\grid\GridView;
?>

<?php
$copunt = 0;
$productoverviews = $dataProvider->getModels();
//echo '<pre>';
//print_r($announcements);
//die();
$count = count($productoverviews);

?>

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-rub"></i><sup><i class="fa fa-arrow-circle-o-right"></i></sup>Pay-out Overview </h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table no-margin">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Redemption #</th>
                        <!--<th>Dealer</th>-->
                        <th class="text-center">Total Points</th>
                        <th class="text-center">Total RM Amount</th>
                        <th>Status</th> 
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($count > 0) {
                        foreach ($productoverviews as $productoverview) {
                            if($productoverview->redemption_status_ray == '2'){
                               $lbg = 'warning';
                            }else if($productoverview->redemption_status_ray == '19'){
                               $lbg = 'success';
                            }else if($productoverview->redemption_status_ray == '10'){
                               $lbg = 'danger';
                            }else{
                               $lbg = 'default'; 
                            }
                            $status = '<span class="label label-'.$lbg.'">'.$productoverview->orderStatus2->name.'</span>';
                            $ahrf = '<a href="/painter/painterprofile/redemptionview?id='.$productoverview->redemptionID.'"><span class="glyphicon glyphicon-search"></span></a>';
                            echo '<tr>';
                            echo '<td width="100">' . date('d-m-Y', strtotime($productoverview->redemption_created_datetime)) . '</td>';
                            echo '<td>' . $productoverview->redemption_invoice_no . '</td>';
                            //echo '<td>' . $productoverview->dealerOutlet->customer_name . '</td>';
                            echo '<td class="text-right">' . $productoverview->req_points . '</td>';
                            echo '<td class="text-right">' . $productoverview->req_amount . '</td>';
                            echo '<td>' . $status . '</td>';
                            //echo '<td>' . $ahrf . '</td>';
                            echo '</tr>';
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div><!-- /.table-responsive -->
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        <a href="/painter/painterprofile/payout" class="btn btn-sm btn-default btn-flat pull-right">View All</a>
    </div><!-- /.box-footer -->
</div><!-- /.box -->