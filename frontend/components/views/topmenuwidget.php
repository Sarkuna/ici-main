<?php
$session = Yii::$app->session;
use yii\helpers\Url;
$role = Yii::$app->session->get('currentRole');
?>
<style>
    .main-header .logo {line-height: normal;}
</style>

<header class="main-header">
    <!-- Logo -->
    <a href="/site" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>Dulux Painter's</b>Club Program</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img src="/images/small_logo.jpg"></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                
                <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="/upload/profile/icon-user-default.jpg" class="user-image" alt="User Image" />
                  
                  <span class="hidden-xs">
                      <?php 
                      if (($session['currentRole'] == Yii::$app->params['role.type.painter'])) {
                          echo strtoupper($painterprofile->full_name);
                      }else{
                          echo strtoupper($userinfo->username);
                      }
                      
                      ?>
                  </span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header bg-light-blue">
                    <img src="/upload/profile/icon-user-default.jpg" class="img-circle" alt="User Image" />
                    <p>
                      <?php 
                      if (($session['currentRole'] == Yii::$app->params['role.type.painter'])) {
                          echo strtoupper($painterprofile->full_name);
                      }else{
                          echo strtoupper($userinfo->username);
                      }
                      
                      ?>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                      <div class="col-xs-6 no-padding">
                          <?php
                            
                            if (($session['currentRole'] == Yii::$app->params['role.type.administrator'])) {
                                
                            } else if (($session['currentRole'] == Yii::$app->params['role.type.management'])) {
                                //return $this->render('dashboard_management');
                            } else if (($session['currentRole'] == Yii::$app->params['role.type.support'])) {
                                //return $this->render('dashboard_support');
                            } else if (($session['currentRole'] == Yii::$app->params['role.type.painter'])) {
                                //return $this->render('dashboard_painter');
                                echo '<a style="font-size:13px" href="/painter/profile/" class="btn btn-default btn-flat">My Profile</a>';
                            }
                          ?>
                          
                      </div>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="/user/change" class="btn btn-default btn-flat">Change Password</a>
                    </div>
                    <div class="pull-right">
                        
                      <a href="/logout" class="btn btn-default btn-flat" data-method="post"><i class="fa fa-sign-out"></i> Log Out</a> 
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
        </div>
    </nav>
</header>