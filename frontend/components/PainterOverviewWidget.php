<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use app\modules\painter\models\PainterProfile;

class PainterOverviewWidget extends Widget
{
    public $path;

    /*public function init()
    {
        parent::init();
    }

    public function run()
    {        
        return $this->render('painteroverview');
        
    }*/
    public function init()
    {
        
        parent::init();

    }

    public function run()
    {
        $session = Yii::$app->session;
        $query = PainterProfile::find();
        //$query->joinWith(['user']);
        //status
        $query->andWhere(['!=','profile_status', 'A']);
        /*$query = \common\models\User::find();

        $query->select([
                    '*',
                ])
                ->where(['user_type' => 'P'])
                ->all();*/

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
        ]);
        
        return $this->render('painteroverview',array('dataProvider'=>$dataProvider));
        
    }
}