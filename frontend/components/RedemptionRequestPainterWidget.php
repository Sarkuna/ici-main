<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use common\models\PointOrder;
use common\models\Redemption;

class RedemptionRequestPainterWidget extends Widget
{
    public $path;

    /*public function init()
    {
        parent::init();
    }

    public function run()
    {        
        return $this->render('painteroverview');
        
    }*/
    public function init()
    {
        
        parent::init();

    }

    public function run()
    {
        $session = Yii::$app->session;
        $query = Redemption::find();
        //$query->joinWith(['user']);
        //status
        $query->andWhere(['=','painterID', Yii::$app->user->id]);
        $query->andWhere(['IN','redemption_status', ['1','7']]);
        /*$query = \common\models\User::find();

        $query->select([
                    '*',
                ])
                ->where(['user_type' => 'P'])
                ->all();*/

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'redemption_status' => SORT_ASC
                    //'id' => SORT_DESC
                ]
            ],
        ]);
        
        return $this->render('redemption_request_painter',array('dataProvider'=>$dataProvider));
        
    }
}