<?php
namespace app\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\db\Query;
use yii\data\ActiveDataProvider;
use common\models\PointOrder;

class ProductOverviewWidget extends Widget
{
    public $path;

    /*public function init()
    {
        parent::init();
    }

    public function run()
    {        
        return $this->render('painteroverview');
        
    }*/
    public function init()
    {
        
        parent::init();

    }

    public function run()
    {
        $session = Yii::$app->session;
        $query = PointOrder::find();
        //$query->joinWith(['user']);
        //status
        $query->andWhere(['=','order_status', '1']);
        /*$query = \common\models\User::find();

        $query->select([
                    '*',
                ])
                ->where(['user_type' => 'P'])
                ->all();*/

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'order_status' => SORT_ASC
                    //'id' => SORT_DESC
                ]
            ],
        ]);
        
        return $this->render('productoverview',array('dataProvider'=>$dataProvider));
        
    }
}