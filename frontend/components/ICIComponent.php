<?php

namespace app\components;

use Yii;
use yii\helpers\Html;
use yii\base\Component;
use yii\base\Exception;
use mPDF;
use kartik\mpdf\Pdf;

class ICIComponent extends Component {
    
    public function getManagementUsers() {
        $users = 'I am from component';

        return $users;
    }
    
    public function passwordResetEmail($email,$username,$resetLink) {
        //$emailBody = 'This test Email';
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => 'RP05'])
                ->one();

        $emailSubjectTemp = $emailTemplate->subject;
        $emailSubject = $emailSubjectTemp;
        $emailSubject = str_replace('{name}', $username, $emailSubject);
        
        $emailBodyTemp = $emailTemplate->template;
        $emailBody = $emailBodyTemp;
        $emailBody = str_replace('{name}', $username, $emailBody);
        $emailBody = str_replace('{resetLink}', $resetLink, $emailBody);
        //echo $emailBody;
        //die();
        Yii::$app->mailer->compose()
                ->setTo($email)
                //->setCc($data["owneremail"])
                //->setBcc($data["managementemails"])
                //->setFrom([\Yii::$app->params['posterMasterEmail'] => \Yii::$app->name])
                //->setReplyTo(\Yii::$app->params['replyTo'])
                ->setFrom([Yii::$app->params['supportEmail'] => 'Dulux Painter\'s Club'])
                //->setReplyTo(\Yii::$app->params['paymentIresidenzMail'])
                ->setSubject($emailSubject)
                //->setSubject($emailSubject)
                ->setHtmlBody($emailBody)
                //->attach($path)
                ->send();
        
        /*return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Password reset for ' . Yii::$app->name)
            ->send();*/
    }
    
    public function sendEmail($userId, $emailTo, $emailTemplateCode, $data = null, $subject = null)
    {
        $siteUrl = Yii::$app->request->hostInfo;
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => $emailTemplateCode])
                ->one();
        
        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }
        
        $name = isset($data2['name']) ? $data2['name'] : null;
        $email = isset($data2['email']) ? $data2['email'] : null;
        $password = isset($data2['password']) ? $data2['password'] : null;
        $membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        $contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;
        
        if ($subject) {
            $emailSubject = $subject;
        } else {
            $emailSubject = $emailTemplate->subject;
            //$emailSubject = $emailTemplate->subject;
            $emailSubject = str_replace('{membership_ID}', $membership_ID, $emailSubject);
        }
        
        $emailBody = $emailTemplate->template;

        $emailBody = str_replace('{name}', $name, $emailBody);
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{password}', $password, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);
        //membership_ID
        //contact_no
        //$emailBody = str_replace('{verification_id}', $verification_id, $emailBody);
        //$emailBody = str_replace('{vccode}', $vccode, $emailBody);
        $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);
        
        //print_r($emailBody);
        //die();

        Yii::$app->mailer->compose()
            ->setTo($emailTo)
            //->setCc($data["owneremail"])
            //->setBcc($data["managementemails"])
            //->setFrom([\Yii::$app->params['posterMasterEmail'] => \Yii::$app->name])
            //->setReplyTo(\Yii::$app->params['replyTo'])
            ->setFrom([Yii::$app->params['supportEmail'] =>  'Dulux Painter\'s Club'])
            //->setReplyTo(\Yii::$app->params['paymentIresidenzMail'])
            ->setSubject($emailSubject)
            //->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            //->attach($path)
            ->send();
        //return true;
    }
    
    public function sendEmailSetting($userId, $emailTo, $emailTemplateCode, $data = null, $subject = null)
    {
        $siteUrl = Yii::$app->request->hostInfo;
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => $emailTemplateCode])
                ->one();
        if ($emailTemplate->email == 1) {

            if ($data != null) {
                $data2 = \yii\helpers\Json::decode($data);
            }

            $name = isset($data2['name']) ? $data2['name'] : null;
            $email = isset($data2['email']) ? $data2['email'] : null;
            $password = isset($data2['password']) ? $data2['password'] : null;
            $membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
            $contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;

            if ($subject) {
                $emailSubject = $subject;
            } else {
                $emailSubject = $emailTemplate->subject;
                //$emailSubject = $emailTemplate->subject;
                $emailSubject = str_replace('{membership_ID}', $membership_ID, $emailSubject);
            }

            $emailBody = $emailTemplate->template;

            $emailBody = str_replace('{name}', $name, $emailBody);
            $emailBody = str_replace('{email}', $email, $emailBody);
            $emailBody = str_replace('{password}', $password, $emailBody);
            $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
            $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);
            //membership_ID
            //contact_no
            //$emailBody = str_replace('{verification_id}', $verification_id, $emailBody);
            //$emailBody = str_replace('{vccode}', $vccode, $emailBody);
            $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);

            //print_r($emailBody);
            //die();

            Yii::$app->mailer->compose()
                    ->setTo($emailTo)
                    //->setCc($data["owneremail"])
                    //->setBcc($data["managementemails"])
                    //->setFrom([\Yii::$app->params['posterMasterEmail'] => \Yii::$app->name])
                    //->setReplyTo(\Yii::$app->params['replyTo'])
                    ->setFrom([Yii::$app->params['supportEmail'] => 'Dulux Painter\'s Club'])
                    //->setReplyTo(\Yii::$app->params['paymentIresidenzMail'])
                    ->setSubject($emailSubject)
                    //->setSubject($emailSubject)
                    ->setHtmlBody($emailBody)
                    //->attach($path)
                    ->send();
            //return true;
        }else {
            return true;
        }
    }
    
    public function sendEmailpainter($painterId, $emailTo, $emailTemplateCode, $data = null, $subject = null)
    {
        $siteUrl = Yii::$app->request->hostInfo;
        
        $painterinfo = \common\models\PainterProfile::find()
                ->where(['user_id' => $painterId])
                ->one();
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => $emailTemplateCode])
                ->one();
        
        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }

        $name = $painterinfo->full_name;
        $membership_ID = $painterinfo->card_id;
        $contact_no = $painterinfo->mobile;        
        
        //Successful Transactions created for Member
        $tr_ID = isset($data2['transaction_ID']) ? $data2['transaction_ID'] : null;
        $tr_point = isset($data2['transaction_point']) ? $data2['transaction_point'] : null;
        $tr_rm_value = isset($data2['transaction_rm_value']) ? $data2['transaction_rm_value'] : null;
        
        $re_ID = isset($data2['redemption_ID']) ? $data2['redemption_ID'] : null;
        $re_point = isset($data2['redemption_point']) ? $data2['redemption_point'] : null;
        $re_rm_value = isset($data2['redemption_value']) ? $data2['redemption_value'] : null;
        
        $re_paid_date = isset($data2['re_paid_date']) ? $data2['re_paid_date'] : null;
        $bank = isset($data2['bank']) ? $data2['bank'] : null;
        $bank_account_no = isset($data2['bank_account_no']) ? $data2['bank_account_no'] : null;
        $account_holder_name = isset($data2['account_holder_name']) ? $data2['account_holder_name'] : null;
        
        $balance_total_points = isset($data2['balance_total_points']) ? $data2['balance_total_points'] : null;
        $balance_rm_value = isset($data2['balance_rm_value']) ? $data2['balance_rm_value'] : null;
        
        if ($subject) {
            $emailSubject = $subject;
        } else {
            $emailSubject = $emailTemplate->subject;
            //$emailSubject = $emailTemplate->subject;
            $emailSubject = str_replace('{membership_ID}', $membership_ID, $emailSubject);
        }
        
        $emailBody = $emailTemplate->template;

        $emailBody = str_replace('{name}', $name, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);
        //Successful Transactions created for Member
        $emailBody = str_replace('{transaction_ID}', $tr_ID, $emailBody);
        $emailBody = str_replace('{transaction_point}', $tr_point, $emailBody);
        $emailBody = str_replace('{transaction_rm_value}', $tr_rm_value, $emailBody);
        //
        $emailBody = str_replace('{redemption_ID}', $re_ID, $emailBody);
        $emailBody = str_replace('{redemption_point}', $re_point, $emailBody);
        $emailBody = str_replace('{redemption_value}', $re_rm_value, $emailBody);
        
        $emailBody = str_replace('{paid_date}', $re_paid_date, $emailBody);
        $emailBody = str_replace('{bank}', $bank, $emailBody);
        $emailBody = str_replace('{bank_account_no}', $bank_account_no, $emailBody);
        $emailBody = str_replace('{account_holder_name}', $account_holder_name, $emailBody);
        //Paid Date
        //membership_ID
        //contact_no
        //$emailBody = str_replace('{verification_id}', $verification_id, $emailBody);
        //$emailBody = str_replace('{vccode}', $vccode, $emailBody);
        $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);
        
        //print_r($emailBody);
        //die();

        Yii::$app->mailer->compose()
            ->setTo($emailTo)
            //->setCc($data["owneremail"])
            //->setBcc($data["managementemails"])
            //->setFrom([\Yii::$app->params['posterMasterEmail'] => \Yii::$app->name])
            //->setReplyTo(\Yii::$app->params['replyTo'])
            ->setFrom([Yii::$app->params['supportEmail'] =>  'Dulux Painter\'s Club'])
            //->setReplyTo(\Yii::$app->params['paymentIresidenzMail'])
            ->setSubject($emailSubject)
            //->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            //->attach($path)
            ->send();
        //return true;
    }
    
    public function sendEmailpainterSetting($painterId, $emailTo, $emailTemplateCode, $data = null, $subject = null)
    {
        $siteUrl = Yii::$app->request->hostInfo;
        
        $painterinfo = \common\models\PainterProfile::find()
                ->where(['user_id' => $painterId])
                ->one();
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => $emailTemplateCode])
                ->one();
        if ($emailTemplate->email == 1) {
        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }

        $name = $painterinfo->full_name;
        $membership_ID = $painterinfo->card_id;
        $contact_no = $painterinfo->mobile;        
        
        //Successful Transactions created for Member
        $tr_ID = isset($data2['transaction_ID']) ? $data2['transaction_ID'] : null;
        $tr_point = isset($data2['transaction_point']) ? $data2['transaction_point'] : null;
        $tr_rm_value = isset($data2['transaction_rm_value']) ? $data2['transaction_rm_value'] : null;
        
        $re_ID = isset($data2['redemption_ID']) ? $data2['redemption_ID'] : null;
        $re_point = isset($data2['redemption_point']) ? $data2['redemption_point'] : null;
        $re_rm_value = isset($data2['redemption_value']) ? $data2['redemption_value'] : null;
        
        $re_paid_date = isset($data2['re_paid_date']) ? $data2['re_paid_date'] : null;
        $bank = isset($data2['bank']) ? $data2['bank'] : null;
        $bank_account_no = isset($data2['bank_account_no']) ? $data2['bank_account_no'] : null;
        $account_holder_name = isset($data2['account_holder_name']) ? $data2['account_holder_name'] : null;
        
        $balance_total_points = isset($data2['balance_total_points']) ? $data2['balance_total_points'] : null;
        $balance_rm_value = isset($data2['balance_rm_value']) ? $data2['balance_rm_value'] : null;
        
        if ($subject) {
            $emailSubject = $subject;
        } else {
            $emailSubject = $emailTemplate->subject;
            //$emailSubject = $emailTemplate->subject;
            $emailSubject = str_replace('{membership_ID}', $membership_ID, $emailSubject);
        }
        
        $emailBody = $emailTemplate->template;

        $emailBody = str_replace('{name}', $name, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);
        //Successful Transactions created for Member
        $emailBody = str_replace('{transaction_ID}', $tr_ID, $emailBody);
        $emailBody = str_replace('{transaction_point}', $tr_point, $emailBody);
        $emailBody = str_replace('{transaction_rm_value}', $tr_rm_value, $emailBody);
        //
        $emailBody = str_replace('{redemption_ID}', $re_ID, $emailBody);
        $emailBody = str_replace('{redemption_point}', $re_point, $emailBody);
        $emailBody = str_replace('{redemption_value}', $re_rm_value, $emailBody);
        
        $emailBody = str_replace('{paid_date}', $re_paid_date, $emailBody);
        $emailBody = str_replace('{bank}', $bank, $emailBody);
        $emailBody = str_replace('{bank_account_no}', $bank_account_no, $emailBody);
        $emailBody = str_replace('{account_holder_name}', $account_holder_name, $emailBody);
        $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);

        Yii::$app->mailer->compose()
            ->setTo($emailTo)
            ->setFrom([Yii::$app->params['supportEmail'] =>  'Dulux Painter\'s Club'])
            ->setSubject($emailSubject)
            ->setHtmlBody($emailBody)
            ->send();
        }else {
            return true;
        }
    }
    
    public function sendSMSpainter($painterId, $mobile, $emailTemplateCode, $data = null)
    {
    //function ismscURL($link) {
        $siteUrl = Yii::$app->request->hostInfo;
        
        $painterinfo = \common\models\PainterProfile::find()
                ->where(['user_id' => $painterId])
                ->one();
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => $emailTemplateCode])
                ->one();
        
        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }

        $name = $painterinfo->full_name;
        $membership_ID = $painterinfo->card_id;
        $contact_no = $painterinfo->mobile;

        $email = isset($data2['email']) ? $data2['email'] : null;
        $password = isset($data2['password']) ? $data2['password'] : null;
        //$membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
        //$contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;
        
        //Successful Transactions created for Member
        $tr_ID = isset($data2['transaction_ID']) ? $data2['transaction_ID'] : null;
        $tr_point = isset($data2['transaction_point']) ? $data2['transaction_point'] : null;
        $tr_rm_value = isset($data2['transaction_rm_value']) ? $data2['transaction_rm_value'] : null;
        
        $re_ID = isset($data2['redemption_ID']) ? $data2['redemption_ID'] : null;
        $re_point = isset($data2['redemption_point']) ? $data2['redemption_point'] : null;
        $re_rm_value = isset($data2['redemption_value']) ? $data2['redemption_value'] : null;
        
        $re_paid_date = isset($data2['re_paid_date']) ? $data2['re_paid_date'] : null;
        $bank = isset($data2['bank']) ? $data2['bank'] : null;
        $bank_account_no = isset($data2['bank_account_no']) ? $data2['bank_account_no'] : null;
        $account_holder_name = isset($data2['account_holder_name']) ? $data2['account_holder_name'] : null;
        
        $balance_total_points = isset($data2['balance_total_points']) ? $data2['balance_total_points'] : null;
        $balance_rm_value = isset($data2['balance_rm_value']) ? $data2['balance_rm_value'] : null;
        $total_approved_transactions = isset($data2['total_approved_transactions']) ? $data2['total_approved_transactions'] : null;
        
        $emailBody = $emailTemplate->sms_text;

        $emailBody = str_replace('{name}', $name, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);
        
        $emailBody = str_replace('{email}', $email, $emailBody);
        $emailBody = str_replace('{password}', $password, $emailBody);

        //Successful Transactions created for Member
        $emailBody = str_replace('{transaction_ID}', $tr_ID, $emailBody);
        $emailBody = str_replace('{transaction_point}', $tr_point, $emailBody);
        $emailBody = str_replace('{transaction_rm_value}', $tr_rm_value, $emailBody);
        //
        $emailBody = str_replace('{redemption_ID}', $re_ID, $emailBody);
        $emailBody = str_replace('{redemption_point}', $re_point, $emailBody);
        $emailBody = str_replace('{redemption_value}', $re_rm_value, $emailBody);
        
        $emailBody = str_replace('{paid_date}', $re_paid_date, $emailBody);
        $emailBody = str_replace('{bank}', $bank, $emailBody);
        $emailBody = str_replace('{bank_account_no}', $bank_account_no, $emailBody);
        $emailBody = str_replace('{account_holder_name}', $account_holder_name, $emailBody);
        
        $emailBody = str_replace('{total_approved_transactions}', $total_approved_transactions, $emailBody);

        
        $message = $emailBody;
        $message = html_entity_decode($message, ENT_QUOTES, 'utf-8'); 
        $message = urlencode($message);
        //$destination = $myuser->cust_mobile;
        //$new = substr($myuser->mobile, 0, -3) . 'xxx';
        //echo $short.'<br>';


                    
        $username = urlencode(Yii::$app->params['sms.username']);
        $password = urlencode(Yii::$app->params['sms.password']);
        $sender_id = urlencode("66300");
        $type = '1';
        
        $link = "https://www.isms.com.my/isms_send.php";
        $link .= "?un=$username&pwd=$password&dstno=$mobile&msg=$message&type=$type&sendid=$sender_id";
        
        //print_r($link);
        //die();
        //$result = Yii::$app->residenz->ismscURL($fp);
        $http = curl_init($link);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        return $http_result;
    }
    
    public function sendSMSpaintersetting($painterId, $mobile, $emailTemplateCode, $data = null)
    {
    //function ismscURL($link) {
        $siteUrl = Yii::$app->request->hostInfo;
        
        $painterinfo = \common\models\PainterProfile::find()
                ->where(['user_id' => $painterId])
                ->one();
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => $emailTemplateCode])
                ->one();

        if($emailTemplate->sms == 1) {

            if ($data != null) {
                $data2 = \yii\helpers\Json::decode($data);
            }

            $name = $painterinfo->full_name;
            $membership_ID = $painterinfo->card_id;
            $contact_no = $painterinfo->mobile;

            $email = isset($data2['email']) ? $data2['email'] : null;
            $password = isset($data2['password']) ? $data2['password'] : null;
            //$membership_ID = isset($data2['membership_ID']) ? $data2['membership_ID'] : null;
            //$contact_no = isset($data2['contact_no']) ? $data2['contact_no'] : null;
            //Successful Transactions created for Member
            $tr_ID = isset($data2['transaction_ID']) ? $data2['transaction_ID'] : null;
            $tr_point = isset($data2['transaction_point']) ? $data2['transaction_point'] : null;
            $tr_rm_value = isset($data2['transaction_rm_value']) ? $data2['transaction_rm_value'] : null;

            $re_ID = isset($data2['redemption_ID']) ? $data2['redemption_ID'] : null;
            $re_point = isset($data2['redemption_point']) ? $data2['redemption_point'] : null;
            $re_rm_value = isset($data2['redemption_value']) ? $data2['redemption_value'] : null;

            $re_paid_date = isset($data2['re_paid_date']) ? $data2['re_paid_date'] : null;
            $bank = isset($data2['bank']) ? $data2['bank'] : null;
            $bank_account_no = isset($data2['bank_account_no']) ? $data2['bank_account_no'] : null;
            $account_holder_name = isset($data2['account_holder_name']) ? $data2['account_holder_name'] : null;

            $balance_total_points = isset($data2['balance_total_points']) ? $data2['balance_total_points'] : null;
            $balance_rm_value = isset($data2['balance_rm_value']) ? $data2['balance_rm_value'] : null;
            $total_approved_transactions = isset($data2['total_approved_transactions']) ? $data2['total_approved_transactions'] : null;

            $emailBody = $emailTemplate->sms_text;

            $emailBody = str_replace('{name}', $name, $emailBody);
            $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
            $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);

            $emailBody = str_replace('{email}', $email, $emailBody);
            $emailBody = str_replace('{password}', $password, $emailBody);

            //Successful Transactions created for Member
            $emailBody = str_replace('{transaction_ID}', $tr_ID, $emailBody);
            $emailBody = str_replace('{transaction_point}', $tr_point, $emailBody);
            $emailBody = str_replace('{transaction_rm_value}', $tr_rm_value, $emailBody);
            //
            $emailBody = str_replace('{redemption_ID}', $re_ID, $emailBody);
            $emailBody = str_replace('{redemption_point}', $re_point, $emailBody);
            $emailBody = str_replace('{redemption_value}', $re_rm_value, $emailBody);

            $emailBody = str_replace('{paid_date}', $re_paid_date, $emailBody);
            $emailBody = str_replace('{bank}', $bank, $emailBody);
            $emailBody = str_replace('{bank_account_no}', $bank_account_no, $emailBody);
            $emailBody = str_replace('{account_holder_name}', $account_holder_name, $emailBody);

            $emailBody = str_replace('{total_approved_transactions}', $total_approved_transactions, $emailBody);


            $message = $emailBody;
            $message = html_entity_decode($message, ENT_QUOTES, 'utf-8');
            $message = urlencode($message);
            //$destination = $myuser->cust_mobile;
            //$new = substr($myuser->mobile, 0, -3) . 'xxx';
            //echo $short.'<br>';



            $username = urlencode(Yii::$app->params['sms.username']);
            $password = urlencode(Yii::$app->params['sms.password']);
            $sender_id = urlencode("66300");
            $type = '1';

            $link = "https://www.isms.com.my/isms_send.php";
            $link .= "?un=$username&pwd=$password&dstno=$mobile&msg=$message&type=$type&sendid=$sender_id";

            //print_r($link);
            //die();
            //$result = Yii::$app->residenz->ismscURL($fp);
            $http = curl_init($link);
            curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
            $http_result = curl_exec($http);
            $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
            curl_close($http);

            return $http_result;
        } else {
            return true;
        }
    }
    
    public function sendSMSbulk($painterId, $mobile, $emailTemplateCode, $data = null)
    {
    //function ismscURL($link) {
        $siteUrl = Yii::$app->request->hostInfo;
        
        $painterinfo = \common\models\PainterProfile::find()
                ->where(['user_id' => $painterId])
                ->one();
        
        $emailTemplate = \common\models\EmailTemplate::find()
                ->where(['code' => $emailTemplateCode])
                ->one();
        
        if ($data != null) {
            $data2 = \yii\helpers\Json::decode($data);
        }

        $name = $painterinfo->full_name;
        $membership_ID = $painterinfo->card_id;
        $contact_no = $painterinfo->mobile;


        
        //Successful Transactions created for Member        
        $re_ID = isset($data2['total_approved_transactions']) ? $data2['total_approved_transactions'] : null;
        $re_point = isset($data2['redemption_point']) ? $data2['redemption_point'] : null;
        $re_rm_value = isset($data2['redemption_value']) ? $data2['redemption_value'] : null;

        
        $emailBody = $emailTemplate->sms_text;

        $emailBody = str_replace('{name}', $name, $emailBody);
        $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
        $emailBody = str_replace('{contact_no}', $contact_no, $emailBody);

        $emailBody = str_replace('{total_approved_transactions}', $re_ID, $emailBody);
        $emailBody = str_replace('{redemption_point}', $re_point, $emailBody);
        $emailBody = str_replace('{redemption_value}', $re_rm_value, $emailBody);

        
        $message = $emailBody;
        $message = html_entity_decode($message, ENT_QUOTES, 'utf-8'); 
        $message = urlencode($message);

                    
        $username = urlencode(Yii::$app->params['sms.username']);
        $password = urlencode(Yii::$app->params['sms.password']);
        $sender_id = urlencode("66300");
        $type = '1';
        
        $link = "https://www.isms.com.my/isms_send.php";
        $link .= "?un=$username&pwd=$password&dstno=$mobile&msg=$message&type=$type&sendid=$sender_id";
        
        

        $http = curl_init($link);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);
        return $http_result;
    }
    
    public function sendSMStest($mobile)
    {
    //function ismscURL($link) {
        $siteUrl = Yii::$app->request->hostInfo;
        $emailBody = "test";
        $message = $emailBody;
        $message = html_entity_decode($message, ENT_QUOTES, 'utf-8'); 
        $message = urlencode($message);
  
        $username = urlencode(Yii::$app->params['sms.username']);
        $password = urlencode(Yii::$app->params['sms.password']);
        $sender_id = urlencode("66300");
        $type = '1';
        
        $link = "https://www.isms.com.my/isms_send.php";
        $link .= "?un=$username&pwd=$password&dstno=$mobile&msg=$message&type=$type&sendid=$sender_id";
        
        //print_r($link);
        //die();
        $http = curl_init($link);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);
print_r($http_result);
        die();
        //return $http_result;
    }
    
    function ismscURL($link) {
        $http = curl_init($link);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);

        return $http_result;
    }
    
    public static function addtolog($status = null, $message = null, $uID = 0)
    {
        \common\models\Actionlog::add($status, $message, $uID);
    }
    
    public static function mobiBalance(){
        $url='https://portal.gomobi.io/externalapi/checkDepositValue';

        $jsonData = array(
            'service' => 'DEPOSIT_BALANCE_API',
            'mobikey' => Yii::$app->params['mobi.api.key'],
        );
        // Convert the data to JSON format
        $jsonString = json_encode($jsonData);      
        $ch = curl_init($url);

        // Set cURL options
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonString);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($jsonString)
        ));

        // Execute cURL session and get the result
        $response = curl_exec($ch);

        // Check for cURL errors
        if (curl_errno($ch)) {
            echo 'Curl error: ' . curl_error($ch);
        }

        // Close cURL session
        curl_close($ch);

        $data = json_decode($response, true);
        return $data['responseData']['depositAmount'];
    }
}
