CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P',
  `user_type` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `created_datetime` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_datetime` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `del` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=49 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `user_type`, `created_datetime`, `created_by`, `updated_datetime`, `updated_by`, `created_at`, `updated_at`, `del`) VALUES
(1, 'myrsadmin', 'SGpfgCyFpA20jdFGtV57BgGLCv92sFFL', '$2y$13$Do.i6c.FCAXpFDsfSHnPMOaVKw71wIGu4HdG1ztJ7VucqdCbOp6Wi', NULL, 'iciadmin@agnichakra.com', 'A', 'A', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 1471666302, 1471666302, ''),
(3, 'support', '3nPtU9hCKwEnKJ0w4O_iF4uUTxuJmb2V', '$2y$13$8L.2AF10tDrRWIpD.Fiij.uhFOHrOBftCV5CKeWYHNJNnENHJM/G2', NULL, 'support@yahoo.com', 'A', 'S', '0000-00-00 00:00:00', 0, '2016-08-27 12:06:53', 3, 1471666359, 1471666359, ''),
(4, 'management', 'pxwQY-Cewd1Zooloz9bM63NNJO-cvauk', '$2y$13$YLCxLE87rh0uXU3ZcyLDze27beg7h1W8jIQAkTC29mzwVeZIZRiyW', NULL, 'management@yahoo.com', 'A', 'M', '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 1471666424, 1471666424, ''),
(38, '657f_shihanagni', 'GiQYWxaEGUv8yON0B4H79GCjErnKZa-F', '$2y$13$fuTFbviI9lb.Q8EuktFCluqYGLExzNjc0dTfZAb.omTeJ5JS6a0NW', '8tRqhtEr2nO1RQ8dPzlFN-PcDKKDhmZf_1479800409', 'shihanagni@gmail.com', 'A', 'P', '2016-10-09 16:17:43', 1, '2016-11-22 07:40:09', 0, 0, 0, ''),
(39, '257f_sarkuna', 'yXLcEtykHgSe0Vdh93f_E3RvqgAKGaXl', '$2y$13$rsZNKmBEb7Vj5/YP/UwwMOmKI6Ng21khL7gnS68QplTmDn119Nrte', NULL, 'sarkuna@businessboosters.com.my', 'A', 'P', '2016-10-12 02:58:49', 1, '2016-10-12 06:37:33', 0, 0, 0, ''),
(40, '557f_meiching.yow', 'pOgNUzJVbdEKVf6-3EdNZacPB6bPlQs_', '$2y$13$M8dKZ6DLWMPmIMpQsX7qkub5pbjWopSlhrCFuKuAKW55aUmSPdUte', NULL, 'meiching.yow@akzonobel.com', 'A', 'P', '2016-10-13 01:46:55', 1, '2016-10-13 01:47:32', 1, 0, 0, ''),
(43, '4581_shihan', 'MSEOHb6cifPyg-abnC448EGXYSNgGL8f', '$2y$13$e09zvY04Bg9CIfRYF9iaF.0VUmWfYoLiaEFGjPPB3Veor0kfrNpvq', NULL, 'shihan@yahoo.com', 'A', 'P', '2016-11-02 07:42:37', 1, '2016-11-21 15:30:03', 1, 0, 0, ''),
(44, '6581_sarkunamail', 'JVPURAMEHyuUEIIjLGkLdfrwETUwrGZS', '$2y$13$RkxYtBGxfY7atwMN1oONDeE0KgPupqb1w4vHYo7mtDMcX9DyHflG2', '2B_1ag8unALwVAQgPtuZCGGV-F_DcwXy_1479799529', 'sarkunamail@gmail.com', 'A', 'P', '2016-11-02 07:55:46', 1, '2016-11-22 07:25:29', 0, 0, 0, ''),
(45, '5582_shmshihan', '2DhAfNtUk2bBxVBTK4NDH5Y1a-CkbY6q', '$2y$13$P9moYq4Qjm24iKHxGlY4YeZG9PM6IYxrg/Ysq7O8X3OEDrjkeDWay', NULL, 'shmshihan@yahoo.com', 'A', 'P', '2016-11-13 12:36:53', 1, '2016-11-13 12:52:54', 1, 0, 0, ''),
(46, '3583_shilgate', 'V7vHh3SQKm6Xbx1q4bLfbUioVAcsuIa3', '$2y$13$KnpMYG/ZTIDG8GZOn5MowOagZGEkFxsMvOn.V9qYBjZXvuymE51l2', NULL, 'shilgate@hotmail.com', 'A', 'P', '2016-11-21 07:54:24', 1, '2016-11-21 15:42:14', 1, 0, 0, ''),
(47, '5583_sarkuna', 'TTOZf8n1CikCcMinO4bBM-j0Z-kFD58H', '$2y$13$601QHelnKs2ZJ/RgJLbqZOUUhcGPRCV7fL8QXhgeukrNcyK.16HE.', NULL, 'sarkuna@masterzmyind.com', 'A', 'P', '2016-11-21 16:13:30', 1, '2016-11-21 16:14:28', 1, 0, 0, ''),
(48, '6583_craig.tham', 'UW_h62t3EYrZEYOFhKZQU7XN4JTS2CbA', '$2y$13$UK3AK6kJ4M7hkekude0Tb.I7H1zDypjEdw6a4LwoVxqrOt/08lYkK', NULL, 'craig.tham@akzonobel.com', 'A', 'P', '2016-11-22 05:02:17', 1, '2016-11-22 05:02:41', 1, 0, 0, '');

CREATE TABLE IF NOT EXISTS `profile_user` (
  `profile_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `profile_photo` varchar(300) DEFAULT NULL,
  `profile_title` int(11) DEFAULT NULL,
  `profile_full_name` varchar(250) DEFAULT NULL,
  `profile_nickname` varchar(100) DEFAULT NULL,
  `profile_mobile` varchar(20) DEFAULT NULL,
  `profile_address` text,
  `profile_nationality` varchar(2) DEFAULT NULL,
  `profile_country` int(11) DEFAULT NULL,
  `profile_ic_no` varchar(20) DEFAULT NULL,
  `profile_race` int(11) DEFAULT NULL,
  `created_datetime` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_datetime` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`profile_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `profile_user`
--

INSERT INTO `profile_user` (`profile_user_id`, `user_id`, `profile_photo`, `profile_title`, `profile_full_name`, `profile_nickname`, `profile_mobile`, `profile_address`, `profile_nationality`, `profile_country`, `profile_ic_no`, `profile_race`, `created_datetime`, `created_by`, `updated_datetime`, `updated_by`) VALUES
(1, 1, NULL, 1, 'RAY Admin', 'rayadmin', '12345456787', NULL, NULL, 2, NULL, 2, '2016-10-01 00:00:00', 1, '2016-10-03 00:00:00', 1);