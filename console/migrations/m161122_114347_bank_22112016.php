<?php

use yii\db\Migration;

class m161122_114347_bank_22112016 extends Migration
{
    public function up()
    {
        $mysqlHostName = Yii::$app->params['mysql.host.name'];
        $mysqlUserName = Yii::$app->params['mysql.user.name'];
        $mysqlPassword = Yii::$app->params['mysql.password'];
        $mysqlDatabaseName = Yii::$app->params['mysql.database.name'];
        $mysqlImportFilename = Yii::$app->params['root.path'] . 'console/migrations/sql_shihan_bank_22112016.sql';
        
        $command = 'mysql -h' . $mysqlHostName . ' -u' . $mysqlUserName . ' -p' . $mysqlPassword . ' ' . $mysqlDatabaseName . ' < ' . $mysqlImportFilename;
        exec($command, $output = array(), $worked);
        switch ($worked) {
            case 0:
                echo 'Successfully imported to database.';
                break;
            case 1:
                echo 'There was an error during import.';
                break;
        }
    }

    public function down()
    {
        echo "m161122_114347_bank_22112016 cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
