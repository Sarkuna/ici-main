CREATE TABLE IF NOT EXISTS `banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(100) NOT NULL,
  `status` char(2) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `bank_name` (`bank_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `bank_name`, `status`) VALUES
(1, 'Affin Bank Berhad', 'A'),
(2, 'Agrobank', 'A'),
(3, 'Al Rajhi Banking & Investment Corporation (Malaysia) Berhad', 'A'),
(4, 'Alliance Bank Malaysia Berhad', 'A'),
(5, 'AmBank (M) Berhad', 'A'),
(6, 'AmIslamic Bank Berhad', 'A'),
(7, 'Bank Islam Malaysia Berhad', 'A'),
(8, 'Bank Kerjasama Rakyat Malaysia', 'A'),
(9, 'Bank Muamalat Malaysia Berhad', 'A'),
(10, 'Bank of America Malaysia Berhad', 'A'),
(11, 'Bank of China (M) Berhad', 'A'),
(12, 'Bank of Tokyo-Mitsubishi UFJ (Malaysia) Bhd', 'A'),
(13, 'Bank Simpanan Nasional', 'A'),
(14, 'BNP Paribas Malaysia Berhad', 'A'),
(15, 'CIMB Bank Berhad', 'A'),
(16, 'Citibank Berhad', 'A'),
(17, 'Deutsche Bank (Malaysia) Berhad', 'A'),
(18, 'Hong Leong Bank Berhad', 'A'),
(19, 'HSBC Amanah Malaysia Berhad', 'A'),
(20, 'HSBC Bank Malaysia Berhad', 'A'),
(21, 'Industrial and Commercial Bank of China (Malaysia) Berhad', 'A'),
(22, 'J.P. Morgan Chase Bank Berhad', 'A'),
(23, 'Kuwait Finance House (M) Berhad', 'A'),
(24, 'Maybank Islamic Berhad', 'A'),
(25, 'Mizuho Bank (Malaysia) Berhad', 'A'),
(26, 'OCBC Bank (Malaysia) Berhad', 'A'),
(27, 'Public Bank Berhad', 'A'),
(28, 'RHB Bank Berhad', 'A'),
(29, 'RHB Islamic Bank Berhad', 'A'),
(30, 'Standard Chartered Bank Malaysia Berhad', 'A'),
(31, 'Sumitomo Mitsui Banking Corporation Malaysia Berhad', 'A'),
(32, 'The Royal Bank of Scotland Berhad', 'A'),
(33, 'United Overseas Bank (Malaysia) Berhad', 'A');

CREATE TABLE IF NOT EXISTS `banking_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `bank_name` int(11) NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `account_number` varchar(100) NOT NULL,
  `IP` varchar(20) NOT NULL,
  `created_datetime` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_datetime` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

CREATE TABLE IF NOT EXISTS `company_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `company_name` varchar(150) NOT NULL,
  `mailing_address` varchar(300) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `no_painters` int(11) DEFAULT NULL,
  `dealer_outlet` int(11) NOT NULL,
  `painter_sites` int(11) DEFAULT NULL,
  `created_datetime` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_datetime` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;
