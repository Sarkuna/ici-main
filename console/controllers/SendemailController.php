<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

/**
 * 
 */
class SendemailController extends Controller {

    public function actionIndex() {

        $siteUrl = Yii::$app->params['siteUrl'];
        
        foreach (\common\models\EmailQueue::find()->where("status = 'P' AND email_template_global_id = '5' AND (email IS NOT NULL) AND (date_to_send IS NULL OR date_to_send <= CURDATE())")->limit(100)->all() as $emailQueue) {
            //Check date_to_send 
            $today = new \DateTime('now');

            $emailTemplate = \common\models\EmailTemplate::findOne($emailQueue->email_template_global_id);

            $emailBody = $emailTemplate->template;

            $painterinfo = \common\models\PainterProfile::find()
            ->where(['user_id' => $emailQueue->user_id])
            ->one();

            $name = $painterinfo->full_name;
            $membership_ID = $painterinfo->card_id;
            $contact_no = $painterinfo->mobile;  

            $data2 = \yii\helpers\Json::decode($emailQueue['data']);

            $redemption_ID = isset($data2['redemption_ID']) ? $data2['redemption_ID'] : null;
            $intake_month = isset($data2['intake_month']) ? $data2['intake_month'] : null;
            $paid_date = isset($data2['re_paid_date']) ? $data2['re_paid_date'] : null;
            $redemption_point  = isset($data2['redemption_point']) ? $data2['redemption_point'] : null;
            $redemption_value  = isset($data2['redemption_value']) ? $data2['redemption_value'] : null;
            $account_holder_name = isset($data2['account_holder_name']) ? $data2['account_holder_name'] : null;
            $bank_account_no = isset($data2['bank_account_no']) ? $data2['bank_account_no'] : null;
            $bank = isset($data2['bank']) ? $data2['bank'] : null;
            
            $emailSubject = $emailTemplate->subject;
            $emailSubject = str_replace('{membership_ID}', $membership_ID, $emailSubject);
                
            $emailBody = str_replace('{name}', $name, $emailBody);
            $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
            $emailBody = str_replace('{redemption_ID}', $redemption_ID, $emailBody);
            $emailBody = str_replace('{intake_month}', $intake_month, $emailBody);
            $emailBody = str_replace('{paid_date}', $paid_date, $emailBody);
            $emailBody = str_replace('{redemption_point}', $redemption_point, $emailBody);
            $emailBody = str_replace('{redemption_value}', $redemption_value, $emailBody);
            $emailBody = str_replace('{bank}', $bank, $emailBody);
            $emailBody = str_replace('{account_holder_name}', $account_holder_name, $emailBody);
            $emailBody = str_replace('{bank_account_no}', $bank_account_no, $emailBody);     
            $emailBody = str_replace('{site_url}', $siteUrl, $emailBody);




            if (!empty($emailQueue->email)) {
                Yii::$app->mailer->compose()
                    ->setTo($emailQueue->email)
                    //->setFrom([\Yii::$app->params['replyTo'] => \Yii::$app->name])
                    //->setReplyTo(\Yii::$app->params['replyTo'])
                   //->setFrom([\Yii::$app->params['adminIresidenzMail'] => \Yii::$app->name])
                    ->setFrom([Yii::$app->params['supportEmail'] =>  'Dulux Painter\'s Club'])
                    //->setReplyTo(\Yii::$app->params['replyTo'])
                    ->setSubject($emailSubject)
                    ->setHtmlBody($emailBody)
                    ->send();
                
                $emailQueueToUpdate = \common\models\EmailQueue::findOne($emailQueue->id);
                $emailQueueToUpdate->status = "C";
                $emailQueueToUpdate->date_to_send = date('Y-m-d');
                $emailQueueToUpdate->save();
            }

            
        }
    }

}
