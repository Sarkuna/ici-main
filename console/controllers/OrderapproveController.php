<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\web\NotFoundHttpException;

use common\models\PointOrder;
use common\models\Redemption;
use common\models\RedemptionItems;
use common\models\PointOrderItem;
use app\modules\painter\models\PainterProfile;
use common\models\BankingInformation;
/**
 * 
 */
class OrderapproveController extends Controller {

    public function actionIndex() {
        $count = \common\models\ExcelTransactionItem::find()->where("status = 'pending'")->count();
        if($count > 0) {
            foreach (\common\models\ExcelTransactionItem::find()->where("status = 'pending'")->limit(25)->all() as $myitem) {
                $chekitem = PointOrder::find()->where(['order_num' => $myitem->transaction_num, 'redemption' => 'N'])->count();
                if ($chekitem > 0) {
                    $chekitem = PointOrder::find()->where(['order_num' => $myitem->transaction_num, 'redemption' => 'N'])->one();
                    $id = $chekitem->order_id;
                    if($myitem->transaction_status == 'Approve' || $myitem->transaction_status == 'approve') {
                        $ostatus = 17;
                    }else {
                        $ostatus = 7;
                    }

                    $model = $this->findModel($id);
                    $model->order_status = $ostatus;

                    if ($model->save()) {
                        $painterId = $model->painter_login_id;
                        if($model->order_status == 17) {
                            $this->NewRedmption($model->order_id, $painterId);
                        }else {
                            $redmitioncheck = RedemptionItems::find()->where(['order_id' => $id])->count();
                            if($redmitioncheck > 0){
                                $redmitioncheckItems = RedemptionItems::find()->where(['order_id' => $id])->one();                    
                                $redemption = Redemption::find()->where(['redemptionID' => $redmitioncheckItems->redemptionID])->one();

                                $redemption->req_points = $redemption->req_points - $redmitioncheckItems->req_per_points;
                                $redemption->req_amount = $redemption->req_amount - $redmitioncheckItems->req_per_amount;
                                $redemption->save(false);
                                RedemptionItems::deleteAll('order_id = :order_id', [':order_id' => $id]);

                                $balanceItems = RedemptionItems::find()->where(['redemptionID' => $redmitioncheckItems->redemptionID])->count();
                                if($balanceItems == 0) {
                                   $redemption->redemption_status = 7;
                                   $redemption->redemption_status_ray = 7;
                                   $redemption->ticket = 'close';
                                   $redemption->internel_status = null;
                                   $redemption->save(false); 
                                }


                            }
                        }
                        $status = 'success';
                        $comt = '';
                    } else {
                        $status = 'fail';
                        $comt = 'DB Error';
                    }
                } else {
                    $status = 'fail';
                    $comt = 'already exit/not found';
                }

                $atUpdate = \common\models\ExcelTransactionItem::findOne($myitem->id);
                $atUpdate->status = $status;
                $atUpdate->comment = $comt;
                $atUpdate->save();

                //echo '<p>' . $myitem->transaction_num . ' - ' . $status . '</p>';
            }
        }
        
    }
    
    protected function NewRedmption($valueid,$painterid){
        //New Code
        //$selection = PointOrderItem::find()->where("point_order_id = " . $valueid . " AND Item_status = 'G'")->all();
        $total_per_point = PointOrderItem::find()->where("point_order_id = " . $valueid . " AND Item_status = 'G'")->sum('total_qty_point');
        $sumpoint = $total_per_point;
                    
        $total_per_price = PointOrderItem::find()->where("point_order_id = " . $valueid . " AND Item_status = 'G'")->sum('total_qty_value');
        $sumprice = $total_per_price;
        
        $verfication = BankingInformation::find()->where(['user_id' => $painterid, 'account_no_verification' => 'Y'])->count();
            
        $chkinvoiceid = Redemption::find()->count();
        if($chkinvoiceid > 0){
            $newinvoiceid = Redemption::find()->orderBy(['redemption_invoice_no' => SORT_DESC,])->one();
            $incinvoice = str_replace(Yii::$app->params['invoice.prefix'],"",$newinvoiceid->redemption_invoice_no);
            $uniqueinvoice = Redemption::find()->where(['redemptionID' => $incinvoice])->count();
            if($uniqueinvoice > 0){
                $incinvoice = str_pad(++$incinvoice,6,'0',STR_PAD_LEFT);
            }else {
                $incinvoice = str_pad(++$incinvoice,6,'0',STR_PAD_LEFT); 
            }                
        }else{
            $incinvoice = Yii::$app->params['invoice.prefix.dft'];
        }
        
        $active_redemption = Redemption::find()->where(['painterID' => $painterid, 'ticket' => 'open'])->count();
        if($active_redemption > 0) {
            $redemption = Redemption::find()->where(['painterID' => $painterid, 'ticket' => 'open'])->one();
            $sumpoint = $sumpoint + $redemption->req_points;
            $sumprice = $sumprice + $redemption->req_amount;
            $redemption->orderID = $redemption->orderID.','.$valueid;
        }else {
            $redemption = new Redemption();
            $redemption->painterID = $painterid;            
            $redemption->redemption_status = '1';
            $redemption->redemption_status_ray = '1';
            $redemption->redemption_invoice_no = Yii::$app->params['invoice.prefix'].$incinvoice;
            $redemption->orderID = strval($valueid);
            $redemption->created_by = 1;
        }
        
        
        
        $redemption->req_points = $sumpoint;
        $redemption->req_amount = $sumprice;
        
        if($verfication == 0) {
           $redemption->internel_status = 'not_verified'; 
        }
        
        $redemption->redemption_IP = isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : '127.0.0.1';
        
        if($redemption->save()) {
            $redemptionitem = new RedemptionItems();
            $redemptionitem->redemptionID = $redemption->redemptionID;
            $redemptionitem->order_id = $valueid;
            $redemptionitem->req_per_points = $total_per_point;
            $redemptionitem->req_per_amount = $total_per_price;
            $redemptionitem->save(false); 
            
            $model = $this->findModel($valueid);
            $model->redemption = 'Y';
            $model->save(false);
            
            $redem = $this->findModelRedemption($redemption->redemptionID);
            $redem->ticket = 'open';
            $redem->save(false);
            return true;
        }else {
            //print_r($redemption->getErrors());
            return false;
        }
    }
    
    protected function findModel($id)
    {
        if (($model = PointOrder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModelorderitem($id)
    {
        if (($model = PointOrderItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModelRedemption($id)
    {
        if (($model = Redemption::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}