<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

/**
 * 
 */
class RedemptionapprovesmsController extends Controller {

    public function actionIndex() {
        foreach (\common\models\EmailQueue::find()->where("type='S' AND sms_status = 'P' AND email_template_global_id = '4' AND (mobile IS NOT NULL)")->limit(100)->all() as $emailQueue) {
            //Check date_to_send 
            $today = new \DateTime('now');

            $emailTemplate = \common\models\EmailTemplate::findOne($emailQueue->email_template_global_id);

            $emailBody = $emailTemplate->sms_text;

            $painterinfo = \common\models\PainterProfile::find()
            ->where(['user_id' => $emailQueue->user_id])
            ->one();

            $name = $painterinfo->full_name;
            $membership_ID = $painterinfo->card_id;
            $mobile = str_replace(' ','',$emailQueue->mobile);  

            $data2 = \yii\helpers\Json::decode($emailQueue['data']);
            
            $redemption_ID = isset($data2['redemption_ID']) ? $data2['redemption_ID'] : null;
            $intake_month = isset($data2['intake_month']) ? $data2['intake_month'] : null;
            $paid_date = isset($data2['re_paid_date']) ? $data2['re_paid_date'] : null;
            $redemption_value  = isset($data2['redemption_value']) ? $data2['redemption_value'] : null;
            $redemption_point  = isset($data2['redemption_point']) ? $data2['redemption_point'] : null;
            $account_holder_name = isset($data2['account_holder_name']) ? $data2['account_holder_name'] : null;
            $bank_account_no = isset($data2['bank_account_no']) ? $data2['bank_account_no'] : null;
            $total_approved_transactions = isset($data2['total_approved_transactions']) ? $data2['total_approved_transactions'] : null;


            $emailBody = str_replace('{name}', $name, $emailBody);
            $emailBody = str_replace('{membership_ID}', $membership_ID, $emailBody);
            $emailBody = str_replace('{redemption_ID}', $redemption_ID, $emailBody);
            $emailBody = str_replace('{intake_month}', $intake_month, $emailBody);
            $emailBody = str_replace('{paid_date}', $paid_date, $emailBody);
            $emailBody = str_replace('{redemption_value}', $redemption_value, $emailBody);
            $emailBody = str_replace('{redemption_point}', $redemption_point, $emailBody);
            $emailBody = str_replace('{account_holder_name}', $account_holder_name, $emailBody);
            $emailBody = str_replace('{bank_account_no}', $bank_account_no, $emailBody);
            $emailBody = str_replace('{total_approved_transactions}', $total_approved_transactions, $emailBody);

            $message = $emailBody;
            $message = html_entity_decode($message, ENT_QUOTES, 'utf-8'); 
            $message = urlencode($message);

            $username = urlencode(Yii::$app->params['sms.username']);
            $password = urlencode(Yii::$app->params['sms.password']);
            $sender_id = urlencode("66300");
            $type = '1';
            

                  
            $fp = "https://www.isms.com.my/isms_send.php?un=$username&pwd=$password&dstno=$mobile&msg=$message&type=$type&sendid=$sender_id";
            $handle = @fopen($fp, "r");
            if ($handle) {
              while (!feof($handle)) {
                  $buffer = fgets($handle, 10000);
                  $st = $buffer;
              }
            }
            
            if($st == '2000 = SUCCESS' || $st == 'EMPTY/BLANK'){
               $status = 'S';
            }else {
               $status = 'F'; 
            }
                $emailQueueToUpdate = \common\models\EmailQueue::findOne($emailQueue->id);
                $emailQueueToUpdate->sms_status = $status;
                $emailQueueToUpdate->sms_result = $st;
                $emailQueueToUpdate->date_to_send = date('Y-m-d');
                $emailQueueToUpdate->save();
            //}
        }
    }
}
