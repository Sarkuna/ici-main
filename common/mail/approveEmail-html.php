<?php
$fullname = $fullname;
$email = $email;
$password = $generatedPassword;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
   <meta name="format-detection" content="telephone=yes">
   <title>Welcome</title>
   <style type="text/css">
.ReadMsgBody { width: 100%; background-color: #ffffff; }
.ExternalClass { width: 100%; background-color: #ffffff; }
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
html { width: 100%; }
body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; }
body { margin: 0; padding: 0; }
table { border-spacing: 0; }
img { display: block !important; }
table td { border-collapse: collapse; }
.yshortcuts a { border-bottom: none !important; }
a { color: #58c3f0; text-decoration: none; }
 @media only screen and (max-width: 640px) {
body { width: auto !important; }
table[class="table600"] { width: 450px !important; text-align: center !important; }
table[class="table-inner"] { width: 86% !important; }
table[class="table2-2"] { width: 47% !important; }
table[class="table3-3"] { width: 100% !important; text-align: center !important; }
table[class="table1-3"] { width: 29.9% !important; }
table[class="table3-1"] { width: 64% !important; text-align: center !important; }
/* Image */
img[class="img1"] { width: 100% !important; height: auto !important; }
}
 @media only screen and (max-width: 479px) {
body { width: auto !important; }
table[class="table600"] { width: 290px !important; }
table[class="table-inner"] { width: 80% !important; }
table[class="table2-2"] { width: 100% !important; }
table[class="table3-3"] { width: 100% !important; text-align: center !important; }
table[class="table1-3"] { width: 100% !important; }
table[class="table3-1"] { width: 100% !important; text-align: center !important; }
table[class="middle-line"] { display: none !important; }
/* image */
img[class="img1"] { width: 100% !important; }
}
</style>
</head>

<body>
   <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
      <tr>
         <td>
            <!--Header-->

            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#58c3f0">

               <!-- webversion -->
               <!-- end webversion -->
               <tr>
                  <td align="center">
                     <table  class="table600"  height="175" bgcolor="#69c9f2" width="600" border="0" cellspacing="0" cellpadding="0">

                        <!-- logo -->
                        <tr>
                           <td align="center" height="100">
                              <img src="<?= Yii::$app->params['emailUrl'] ?>/images/email_img/akzonobel-logo-white.png" width="250" alt="logo" />
                           </td>
                            <!--<td align="right" height="200">
                              <img src="<?= Yii::$app->params['emailUrl'] ?>/images/email_img/dulux_logo.jpg" width="100" alt="logo" />
                           </td>-->
                        </tr>
                        <!-- end logo --> </table>
                  </td>
               </tr>
               <tr>
                  <td bgcolor="#c1e4fa">
                     <table class="table600" width="600" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#c7e7fb">
                        <tr>
                           <td height="20"></td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>

            <!--End Header--> </td>
      </tr>


      <!-- 1/1 content -->
      <tr>
         <td>
            <table class="table600" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
               <tr>
                  <td height="30"></td>
               </tr>
               <tr>
                  <td>
                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                           <td align="left" style="font-family: Arial, Helvetica, sans-serif; color:#353333; font-size:24px;">
                               Dear <?= $fullname ?>,
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
               <tr>
                  <td height="15"></td>
               </tr>

               <!-- content -->
               <tr>
                  <td align="left" valign="top" style="font-family: Arial, Helvetica, sans-serif; color:#adadad; font-size:18px; line-height:24px;">
                      <h3 style="color: #736363;font-weight: normal;">Welcome to Akzonobel loyalty programs. We have created your Akzonobel loyalty account.</h3>
                      
                  </td>
               </tr>
               
               <tr>
                  <td align="left" valign="top" style="font-family: Arial, Helvetica, sans-serif; color:#adadad; font-size:13px; line-height:24px;">
                      <h3 style="color: #736363;font-weight: normal; margin: 0px 0px 10px 0px;font-size: 18px;">Your login details are</h3>
                      <h4 style="margin: 0px; padding: 0; color:#3e3535;"><label style="width: 90px;display: inline-block;">Email: </label><?= $email ?></h4>
                      <h4 style="margin: 0px 0px 10px 0px; padding: 0; color:#3e3535;"><label style="width: 85px;display: inline-block;">Password: </label> <?= $password ?></h4>
                  </td>
               </tr>
               <tr>
                   <td align="left" valign="top" style="font-family: Arial, Helvetica, sans-serif; color:#adadad; font-size:13px; line-height:24px;">
                   <table style="border-radius:4px" bgcolor="#bad576" width="100" border="0" align="left" cellpadding="0" cellspacing="0">
                                 <tr>
                                     <td height="40" align="center" style="font-family: Arial, Helvetica, sans-serif; font-size:14px; color:#ffffff;"><a href="<?= Yii::$app->params['emailUrl'] ?>" style="color: #ffffff;">Login</a></td>
                                 </tr>
                              </table>
                       </td>
               </tr>
               <!-- end content --> </table>
         </td>
      </tr>


      <!--footer info-->
      <tr>
         <td>
            <table width="100%   " border="0" align="center" cellpadding="0" cellspacing="0">
               <tr>
                  <td height="30"></td>
               </tr>
               <tr>
                  <td height="30" align="center">
                     <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="table600">
                        <tr>
                           <td>
                              <table class="table-inner" width="280" border="0" align="center" cellpadding="0" cellspacing="0">
                                 <tr>
                                    <td width="78" valign="middle" style="border-collapse: collapse;">
                                       <table width="100%" height="1" border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                                          <tr>
                                             <td height="1" width="100%" bgcolor="#eae9e9" style="border-collapse: collapse;"></td>
                                          </tr>
                                       </table>
                                    </td>
                                    <td width="65" height="8"align="center" bgcolor="#58c3f0" style="max-width:65px;"></td>
                                    <td width="78" valign="middle" style="border-collapse: collapse;">
                                       <table border="0" cellpadding="0" cellspacing="0" width="100%" height="1" style="border-collapse: collapse;">
                                          <tr>
                                             <td height="1" width="100%" bgcolor="#eae9e9" style="border-collapse: collapse;"></td>
                                          </tr>
                                       </table>
                                    </td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
               <tr>
                  <td height="15"></td>
               </tr>
               <tr>
                  <td align="center" bgcolor="#e2e2e2">
                     <table class="table600" width="600" border="0" cellspacing="0" cellpadding="0" bgcolor="#e8e8e8">
                        <tr>
                           <td height="20"></td>
                        </tr>
                        <tr>
                           <td align="center" valign="top">
                              <table class="table-inner" width="600" border="0" cellspacing="0" cellpadding="0">

                                 <!-- notification -->
                                 <tr>
                                    <td align="center" style="font-family: Helvetica, Arial, sans-serif; font-size:12px ; color:#999999; padding:0px 35px; line-height:20px;">
                                       Join our community to get even more ideas, tips and inspiration.
                                    </td>
                                 </tr>
                                 <!-- end notification -->
                                 <tr>
                                    <td height="20"></td>
                                 </tr>

                                 <!--social-->
                                 <tr>
                                    <td align="center">
                                       <table width="172" border="0" cellpadding="0" cellspacing="0" class="social">
                                          <tr>
                                             <td align="center">
                                                <a href="#">
                                                   <img src="<?= Yii::$app->params['emailUrl'] ?>/images/email_img/social_facebook.png" width="25" height="25" alt="facebook" />
                                                </a>
                                             </td>
                                             
                                          </tr>
                                       </table>
                                    </td>
                                 </tr>
                                 <!--end social--> </table>
                           </td>
                        </tr>
                        <tr>
                           <td height="20"></td>
                        </tr>
                     </table>
                  </td>
               </tr>
               <tr>
                  <td align="center" bgcolor="#d9d9d9">
                     <table class="table600" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                           <td height="30" align="center" bgcolor="#e1e1e1" style="font-family: Helvetica, Arial, sans-serif; font-size:11px; color:#6b6b6b;">
                              <!--Copyright ©
                              <a href="#" style="color:#009ce7; text-decoration:none;">www.yourdomain.com</a>
                              , All rights reserved-->
                              Akzo Nobel Paints (Malaysia) Sdn. Bhd. (3393-V). Registered office: A-10-01, Level 10, Block A, PJ8, No.23, Jalan Barat, Seksyen 8, 46050 Petaling Jaya, Selangor, Malaysia 
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>

            <!--End footer info--> </td>
      </tr>
   </table>
</body>
</html>