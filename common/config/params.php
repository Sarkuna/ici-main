<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'info@agnichakra.com',
    'user.passwordResetTokenExpire' => 3600,
    
    'role.name.administrator' => 'Administrator',
    'role.name.management' => 'Management',
    'role.name.support' => 'Support',    
    'role.name.painter' => 'Painter',
    
    'role.type.administrator' => 'A', //Administrator
    'role.type.management' => 'M', //Management
    'role.type.support' => 'S', //Support
    'role.type.painter' => 'P', //Painter
    
    'MsgNull' => 'No data',
    'nomsg' => 'No record found',
    
    'email.template.code.successful.painter.application' => 'SPA01',
    'email.template.code.successful.transactions.approve' => 'STCM02',
    'email.template.code.successful.redemption.approve' => 'SRRCM03',
    'email.template.code.successful.redemption.payout' => 'SPM04',       
    'email.template.code.password.reset.request' => 'PRR06',
    
    'sms.username' => 'Sarkuna',
    'sms.password' => 'dulux2016',
    
    'message.marque' => 'Dulux Painter\'s Club proudly brings to you, the latest exciting campaign, "PAINT YOUR WAY TO FANTASTIC PRIZES!". Contact your Painter Marketing Officer for more details.'
];
