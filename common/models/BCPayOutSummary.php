<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\Url;

use common\models\BCRedemption;

/**
 * This is the model class for table "bc_pay_out_summary".
 *
 * @property int $id
 * @property int $batch
 * @property string $start_date
 * @property string $end_date
 * @property float $rm_value
 * @property string|null $remark
 * @property int $created_by
 * @property string $created_datetime
 * @property int $updated_by
 * @property string $updated_datetime
 */
class BCPayOutSummary extends \yii\db\ActiveRecord
{
    public $excel;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bc_pay_out_summary';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['start_date', 'end_date'], 'required'],
            [['batch', 'created_by', 'updated_by'], 'integer'],
            [['start_date', 'end_date', 'created_datetime', 'updated_datetime'], 'safe'],
            [['rm_value'], 'number'],
            [['remark'], 'string'],
            ['excel', 'required'],
            [['excel'], 'file', 'extensions'=>['xls', 'xlsx'], 'checkExtensionByMimeType'=>false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'batch' => 'Batch',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'rm_value' => 'RM Value',
            'remark' => 'Remark',
            'created_by' => 'Created By',
            'created_datetime' => 'Created Datetime',
            'updated_by' => 'Updated By',
            'updated_datetime' => 'Updated Datetime',
        ];
    }
    
    public function getStartredemption(){
        $return = BCRedemption::find()
                //->select('redemption_invoice_no')
                ->where(['pay_out_summary_id' => $this->id])
                ->orderBy(['redemption_invoice_no'=>SORT_ASC])
                ->one();
        
        return $return->redemption_invoice_no;
    }
    
    public function getEndredemption(){
        $return = BCRedemption::find()
                ->where(['pay_out_summary_id' => $this->id])
                ->orderBy(['redemption_invoice_no' => SORT_DESC])
                ->one();
        
        return $return->redemption_invoice_no;
    }
    
    public function getTotalUsers(){
        return BCRedemption::find()
                ->where(['pay_out_summary_id' => $this->id])
                ->groupBy('crm_id')
                ->count();
    }
    
    public function getCountofSk(){
        return BCRedemption::find()
                ->where(['pay_out_summary_id' => $this->id])
                //->groupBy('crm_id')
                ->sum('count_of_sku');
    }
    
    public function getSumofPoints(){
        return BCRedemption::find()
                ->where(['pay_out_summary_id' => $this->id])
                //->groupBy('crm_id')
                ->sum('sum_of_points');
    }
    
    public function getTotalAmount(){
        return BCRedemption::find()
                ->where(['pay_out_summary_id' => $this->id])
                ->sum('total_rm');
    }
    
    public function getBankAccVerified(){
        return BCRedemption::find()
                ->where(['pay_out_summary_id' => $this->id])
                ->andWhere(['!=', 'painterID', 0])
                ->andWhere(['!=', 'internel_status', 'not_verified'])
                ->groupBy('crm_id')
                ->count();
    }
    
    public function getBankAccUnverified(){
        return BCRedemption::find()
                ->where(['pay_out_summary_id' => $this->id])
                ->andWhere(['!=', 'painterID', 0])
                ->andWhere(['=', 'internel_status', 'not_verified'])
                ->groupBy('crm_id')
                ->count();
    }
    
    public function getReVerified(){
        return BCRedemption::find()
                ->where(['pay_out_summary_id' => $this->id])
                ->andWhere(['!=', 'painterID', 0])
                ->andWhere(['=', 'internel_status', 're_verified'])
                ->groupBy('crm_id')
                ->count();
    }
    
    public function getPaidAcc(){
        return BCRedemption::find()
                ->where(['pay_out_summary_id' => $this->id])
                ->andWhere(['=', 'internel_status', 'paid'])
                ->groupBy('crm_id')
                ->count();
    }
    
    public function getUnpaidAcc(){
        return BCRedemption::find()
                ->where(['pay_out_summary_id' => $this->id])
                ->andWhere(['!=', 'internel_status', 'paid'])
                ->groupBy('crm_id')
                ->count();
    }
    
    public function getUnpaidAccRm(){
        return BCRedemption::find()
                ->where(['pay_out_summary_id' => $this->id])
                ->andWhere(['!=', 'internel_status', 'paid'])
                ->sum('total_rm');
    }
    
    public function getPaidAccRm(){
        
        $return = BCRedemption::find()
                ->where(['pay_out_summary_id' => $this->id])
                ->andWhere(['=', 'internel_status', 'paid'])
                ->sum('total_rm');
        if(empty($return)){
            $return = 0;
        }
        return $return;
    }
}
