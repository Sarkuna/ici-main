<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class SMS_setting_Form extends Model
{
    public $name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            //[['name'], 'required'],
            [['name'], 'safe']

        ];
    }
    
    public function attributeLabels()
    {
        return [
            'name' => 'Module Name',
            //'reportdateto' => 'Date To',
            //membershipno' => 'Membership No'
        ];
    }

    
}
