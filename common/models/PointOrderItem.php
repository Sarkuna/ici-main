<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use yii\helpers\Html;

use common\models\PointOrder;
use common\models\ProductList;
use common\models\Redemption;
/**
 * This is the model class for table "point_order_item".
 *
 * @property integer $item_id
 * @property integer $point_order_id
 * @property integer $painter_login_id
 * @property integer $item_bar_code
 * @property integer $item_bar_point
 * @property integer $item_bar_total_point
 * @property string $item_bar_total_value
 * @property string $Item_status
 * @property string $Item_IP
 * @property string $created_datetime
 * @property integer $created_by
 * @property string $updated_datetime
 * @property integer $updated_by
 */
class PointOrderItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'point_order_item';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['point_order_id', 'painter_login_id', 'item_bar_point', 'item_bar_total_point', 'item_bar_total_value', 'Item_status', 'Item_IP'], 'required'],
            [['point_order_id', 'painter_login_id', 'item_bar_code', 'item_pack_size', 'created_by', 'updated_by'], 'integer'],
            [['item_bar_total_point','item_bar_per_value','item_bar_total_value'], 'number'],
            [['created_datetime', 'created_by', 'updated_datetime', 'updated_by','item_bar_code', 'item_bar_code_name','total_qty_point','total_qty_value','item_bar_point'], 'safe'],
            [['Item_status'], 'string', 'max' => 2],
            [['item_bar_code_name'], 'string', 'max' => 100],
            [['Item_IP'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Item ID',
            'point_order_id' => 'Point Order ID',
            'painter_login_id' => 'Painter Login ID',
            'item_bar_code' => 'Item Bar Code',
            'item_bar_point' => 'Item Bar Point',
            'item_bar_total_point' => 'Item Bar Total Point',
            'item_bar_total_value' => 'Item Bar Total Value',
            'Item_status' => 'Item Status',
            'Item_IP' => 'Item  Ip',
            'created_datetime' => 'Created Datetime',
            'created_by' => 'Created By',
            'updated_datetime' => 'Updated Datetime',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getOrder()
    {
        return $this->hasOne(PointOrder::className(), ['order_id' => 'point_order_id']);
    }
    
    public function getProductListItem()
    {
        return $this->hasOne(ProductList::className(), ['product_list_id' => 'product_list_id']);
    }
    
    public function getProfile()
    {
        return $this->hasOne(\app\modules\painter\models\PainterProfile::className(), ['user_id' => 'painter_login_id']);
    }
    
    public function getDealer()
    {
        return $this->hasOne(\common\models\DealerList::className(), ['id' => 'Order.order_dealer_id']);
    }
    public function getRedemption()
    {
        //return 'test'.$this->point_order_id;
        //'userid' => [1001,1002,1003,1004,1005],

        $rd = Redemption::find()->where(['LIKE', 'orderID', $this->point_order_id])->all();
        //echo count($rd);
        $no = '';
        foreach($rd as $r){
            $no .= $r->redemption_invoice_no;
        }
        return $no;
        //echo '<pre>';
        //print_r($rd);
        //return 'test'. $rd->redemption_invoice_no;
        //die();
        //return $this->hasOne(\app\modules\painter\models\PainterProfile::className(), ['user_id' => 'painter_login_id']);
    }
    public function getStatusDescription() {
        $returnValue = "";
        if ($this->Item_status == "D") {
            $returnValue = "<span class='label label-warning'>Damaged</span>";
        } else if ($this->Item_status == "U") {
            $returnValue = "<span class='label label-danger'>Used</span>";
        } else if ($this->Item_status == "G") {
            $returnValue = "<span class='label label-success'>Good</span>";
        }
        return $returnValue;
    }
    
    public function getRealIp() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    
    public function getTotalItemPackSize($itm,$newmonth){

        $query = \common\models\PointOrderItem::find()->joinWith([
            'order' => function($query) {
                $query->select(['point_order.created_datetime'])
                        ->where(['point_order.redemption' => 'Y']);
            }])
            ->select(['item_pack_size'])
            ->where(['item_pack_size'=> $itm])
           ->andWhere(['like','point_order.created_datetime',$newmonth])         
            ->sum('total_qty_value');   
        return $query; 
        exit;
    }
    
    public function getTotalItemPackSize2(){
        return 0;
    }
}
