<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "email_template".
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $subject
 * @property string $template
 * @property string|null $sms_text
 * @property int $email
 * @property int $sms
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property int $created_by
 * @property int $updated_by
 */
class EmailTemplate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'email_template';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['code', 'name', 'subject', 'template', 'email', 'sms', 'created_datetime', 'updated_datetime', 'created_by', 'updated_by'], 'required'],
            [['code', 'name', 'subject', 'template'], 'required'],
            [['template'], 'string'],
            [['email', 'sms', 'created_by', 'updated_by'], 'integer'],
            [['created_datetime', 'updated_datetime'], 'safe'],
            [['code'], 'string', 'max' => 15],
            [['name'], 'string', 'max' => 150],
            [['subject'], 'string', 'max' => 300],
            [['sms_text'], 'string', 'max' => 500],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
            'subject' => 'Subject',
            'template' => 'Template',
            'sms_text' => 'Sms Text',
            'email' => 'Email',
            'sms' => 'Sms',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
