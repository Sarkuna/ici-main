<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Tng;

/**
 * TngSearch represents the model behind the search form of `common\models\Tng`.
 */
class TngSearch extends Tng
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ref_id', 'rm50', 'rm100'], 'integer'],
            [['customer_sap_id', 'company_name', 'staff_name', 'staff_mobile', 'tpin', 'status'], 'safe'],
            [['total_rewards'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tng::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ref_id' => $this->ref_id,
            'total_rewards' => $this->total_rewards,
            'rm50' => $this->rm50,
            'rm100' => $this->rm100,
        ]);

        $query->andFilterWhere(['like', 'customer_sap_id', $this->customer_sap_id])
            ->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'staff_name', $this->staff_name])
            ->andFilterWhere(['like', 'staff_mobile', $this->staff_mobile])
            ->andFilterWhere(['like', 'tpin', $this->tpin])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
