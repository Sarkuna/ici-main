<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * This is the model class for table "membership_pack".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $status
 * @property string $IP
 * @property string $created_datetime
 * @property integer $created_by
 * @property string $mp_updated_datetime
 * @property integer $updated_by
 */
class MembershipPack extends \yii\db\ActiveRecord
{
    const STATUS_DELETED = 'X';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'membership_pack';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'mp_updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['user_id', 'IP', 'created_datetime', 'created_by', 'mp_updated_datetime', 'updated_by'], 'required'],
            [['pack_status'], 'required'],
            [['user_id', 'created_by', 'updated_by'], 'integer'],
            [['created_datetime', 'mp_updated_datetime'], 'safe'],
            [['pack_status'], 'string', 'max' => 2],
            [['remark'], 'string', 'max' => 300],
            [['IP'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'pack_status' => 'Status',
            'remark' => 'Remark',
            'IP' => 'Ip',
            'created_datetime' => 'Created Datetime',
            'created_by' => 'Created By',
            'mp_updated_datetime' => 'Updated Datetime',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getUser() {
        return $this->hasOne(\common\models\User::className(), ['id' => 'user_id']);
    }

    public function getActions() {
        $returnValue = "";
        //$returnValue = $returnValue . '<a href="' . Url::to(['/painter/painterprofile/update/','id' => $this->id]) . '" title="Edit Profile"><span class="icon fa fa-edit"></span></a>' . '&nbsp;&nbsp;&nbsp;';
        //$returnValue = $returnValue . '<a href="' . Url::to(['/painter/painterprofile/view/','id' => $this->id]) . '" title="View Profile"><span class="icon fa fa-eye"></span></a>' . '&nbsp;&nbsp;&nbsp;';
        //$returnValue = $returnValue . '<a href="' . Url::to(['/painter/painterprofile/view/','id' => $this->id]) . '" title="Delete Profile"><span class="icon fa fa-remove"></span></a>' . '&nbsp;&nbsp;&nbsp;';
        $returnValue = Html::a('<i class="fa fa-pencil-square-o"></i> '.Yii::t('app', 'Edit'), '#', ['class' => 'btn btn-primary btn-sm', 'onclick' => "updateGuard(".$this->id.");return false;"]);
        return $returnValue;
    }
    
    public function getStatusDescription() {
        $returnValue = "";
        if ($this->pack_status == "N") {
            $returnValue = "No";
        } else if ($this->pack_status == "Y") {
            $returnValue = "Yes";
        }
        return $returnValue;
    }
    
    public function getUserPainter() {
        return $this->hasOne(\common\models\PainterProfile::className(), ['user_id' => 'id']);
    }
}
