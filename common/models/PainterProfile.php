<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\Url;

/**
 * This is the model class for table "painter_profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $card_id
 * @property string $photo
 * @property integer $title
 * @property string $full_name
 * @property string $profile_name
 * @property string $mobile
 * @property integer $nationality
 * @property string $ic_no
 * @property integer $race
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class PainterProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const STATUS_DELETED = 'X';
    const STATUS_ACTIVE = 'A';
    const STATUS_PENDING = 'P';
    public static function tableName()
    {
        return 'painter_profile';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'full_name', 'mobile', 'nationality', 'country', 'race'], 'required'],
            [['user_id', 'card_id', 'title', 'country', 'race', 'created_by', 'updated_by'], 'integer'],
            ['ic_no', 'required'],
            ['ic_no', 'unique', 'message' => 'This {attribute} has already been taken.'],
            [['email'], 'email'],
            ['email', 'required'],
            ['email', 'unique', 'message' => 'This {attribute} has already been taken.'],
            [['created_datetime', 'updated_datetime'], 'safe'],
            [['photo'], 'string', 'max' => 200],
            [['nationality'], 'string', 'max' => 2],
            [['full_name', 'email'], 'string', 'max' => 255],            
            [['profile_name', 'mobile', 'ic_no'], 'string', 'max' => 20],
            [['pic_name'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'card_id' => 'Membership No',
            'photo' => 'Photo',
            'title' => 'Title',
            'full_name' => 'Full Name',
            'profile_name' => 'Profile Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'nationality' => 'Nationality',
            'country' => 'Country',
            'ic_no' => 'NRIC/Passport Number',
            'race' => 'Race',
            'pic_name' => 'PIC Name',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getUser() {
        return $this->hasOne(\common\models\User::className(), ['id' => 'user_id'])->
       andWhere(['status' =>! self::STATUS_DELETED]);
    }
    
    public function getProfileTitle()
    {
        return $this->hasOne(\common\models\TitleOptions::className(), ['id' => 'title']);
    }
    
    public function getProfileCountry()
    {
        return $this->hasOne(\common\models\Country::className(), ['id' => 'country']);
    }
    
    public function getProfileRace()
    {
        return $this->hasOne(\common\models\Race::className(), ['id' => 'race']);
    }
    
     public function getProfileRegion()
    {
        return $this->hasOne(\common\models\Region::className(), ['region_id' => 'region_id']);
    }
    
    public function getProfileState()
    {
        return $this->hasOne(\common\models\State::className(), ['state_id' => 'state_id']);
    }
    
    
    
    public function getBank()
    {
        return $this->hasOne(\app\modules\painter\models\BankingInformation::className(), ['user_id' => 'user_id']);
    }
    
    public function getCompany()
    {
        return $this->hasOne(\app\modules\painter\models\CompanyInformation::className(), ['user_id' => 'user_id']);
    }
    
    public function getStatusDescription() {
        $returnValue = "";
        if ($this->status == "X") {
            $returnValue = "Deleted";
        } else if ($this->status == "P") {
            $returnValue = "Pending";
        } else if ($this->status == "A") {
            $returnValue = "Active";
        }
        return $returnValue;
    }
    
    public function getActions() {
        $returnValue = "";

        $returnValue = $returnValue . '<a href="' . Url::to(['/painter/painterprofile/view/','id' => $this->id]) . '" title="View Profile"><span class="icon fa fa-eye"></span></a>' . '&nbsp;&nbsp;&nbsp;';
        $returnValue = $returnValue . '<a href="' . Url::to(['/painter/painterprofile/view/','id' => $this->id]) . '" title="Delete Profile"><span class="icon fa fa-remove"></span></a>' . '&nbsp;&nbsp;&nbsp;';
        return $returnValue;
    }
}
