<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "excel_transaction_item".
 *
 * @property int $id
 * @property int $auto_transaction_id
 * @property string $transaction_num
 * @property string $transaction_status
 * @property string $status
 * @property string|null $comment
 */
class ExcelTransactionItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'excel_transaction_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['auto_transaction_id', 'transaction_num', 'transaction_status'], 'required'],
            [['auto_transaction_id'], 'integer'],
            [['status', 'comment'], 'string'],
            [['transaction_num'], 'string', 'max' => 50],
            [['transaction_status'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'auto_transaction_id' => 'Auto Transaction ID',
            'transaction_num' => 'Transaction Num',
            'transaction_status' => 'Transaction Status',
            'status' => 'Status',
            'comment' => 'Comment',
        ];
    }
}
