<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class EmailForm extends Model
{
    public $email;
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            ['email', 'email'],
            ['email', 'required'],
            ['email', 'unique', 'message' => 'This email has already been taken.'],
        ];
    }

    
}
