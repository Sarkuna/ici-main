<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "redemption_history".
 *
 * @property integer $redemption_history_id
 * @property integer $redemptionID
 * @property integer $redemption_status
 * @property integer $notify
 * @property string $comment
 * @property string $date_added
 */
class RedemptionRemark extends \yii\db\ActiveRecord
{
    public $comment;
    public $intake_month;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment'], 'required'],
            [['comment', 'intake_month'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'comment' => 'Comment',
            'intake_month' => 'Intake Month'
        ];
    }
}
