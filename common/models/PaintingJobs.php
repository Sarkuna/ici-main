<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "painting_jobs".
 *
 * @property integer $painting_jobs_id
 * @property string $painting_jobs_type
 * @property string $painting_jobs_name
 */
class PaintingJobs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'painting_jobs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['painting_jobs_type', 'painting_jobs_name'], 'required'],
            [['painting_jobs_type'], 'string', 'max' => 1],
            [['painting_jobs_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'painting_jobs_id' => 'Painting Jobs ID',
            'painting_jobs_type' => 'Painting Jobs Type',
            'painting_jobs_name' => 'Painting Jobs Name',
        ];
    }
}
