<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_list".
 *
 * @property integer $product_list_id
 * @property integer $product_type_id
 * @property integer $product_category_id
 * @property integer $product_price_category_id
 * @property string $product_name
 * @property string $product_description
 * @property integer $pack_size
 * @property integer $points_per_liter
 * @property integer $total_points_awarded
 * @property string $per_value_price
 * @property string $total_value_price
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class ProductList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_type_id', 'product_category_id', 'product_price_category_id', 'product_name', 'product_description', 'pack_size', 'points_per_liter', 'total_points_awarded', 'total_value_price'], 'required'],
            [['product_type_id', 'product_category_id', 'product_price_category_id', 'pack_size', 'points_per_liter', 'total_points_awarded', 'created_by', 'updated_by'], 'integer'],
            [['product_description'], 'string'],
            [['per_value_price', 'total_value_price'], 'number'],
            [['created_datetime', 'updated_datetime'], 'safe'],
            [['product_name'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_list_id' => 'Product List ID',
            'product_type_id' => 'Product Type ID',
            'product_category_id' => 'Product Category ID',
            'product_price_category_id' => 'Product Price Category ID',
            'product_name' => 'Product Name',
            'product_description' => 'Product Description',
            'pack_size' => 'Pack Size',
            'points_per_liter' => 'Points Per Liter',
            'total_points_awarded' => 'Total Points Awarded',
            'per_value_price' => 'Per Value Price',
            'total_value_price' => 'Total Value Price',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
