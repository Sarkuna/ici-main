<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "redemption_items".
 *
 * @property integer $redemption_items_ID
 * @property integer $redemptionID
 * @property integer $req_per_points
 * @property string $req_per_amount
 */
class RedemptionItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'redemption_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['redemptionID', 'req_per_points', 'req_per_amount'], 'required'],
            [['redemptionID', 'req_per_points'], 'integer'],
            [['req_per_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'redemption_items_ID' => 'Redemption Items  ID',
            'redemptionID' => 'Redemption ID',
            'req_per_points' => 'Req Per Points',
            'req_per_amount' => 'Req Per Amount',
        ];
    }
    
    public function getRedemption()
    {
        return $this->hasOne(Redemption::className(), ['redemptionID' => 'redemptionID']);
    }
}
