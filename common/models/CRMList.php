<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "crm_list".
 *
 * @property int $id
 * @property string $crm_id
 * @property string $painter_id
 * @property string $painter_name
 */
class CRMList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['crm_id', 'painter_id', 'painter_name'], 'required'],
            [['crm_id', 'painter_id'], 'string', 'max' => 100],
            [['painter_name'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'crm_id' => 'Crm ID',
            'painter_id' => 'Painter ID',
            'painter_name' => 'Painter Name',
        ];
    }
}
