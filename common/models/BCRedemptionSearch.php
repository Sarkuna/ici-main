<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BCRedemption;

/**
 * BCRedemptionSearch represents the model behind the search form of `common\models\BCRedemption`.
 */
class BCRedemptionSearch extends BCRedemption
{
    public $account_number;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['redemptionID', 'painterID', 'crm_id', 'count_of_sku', 'created_by', 'updated_by', 'pay_out_summary_id'], 'integer'],
            [['redemption_invoice_no', 'redemption_remarks', 'redemption_IP', 'created_datetime', 'updated_datetime', 'paid_date', 'internel_status', 'comment', 'review_comment', 'ticket', 'account_number'], 'safe'],
            [['sum_of_points', 'per_rm', 'total_rm'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BCRedemption::find();
        $query->joinWith(['bank']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'redemptionID' => $this->redemptionID,
            'painterID' => $this->painterID,
            'crm_id' => $this->crm_id,
            'count_of_sku' => $this->count_of_sku,
            'sum_of_points' => $this->sum_of_points,
            'per_rm' => $this->per_rm,
            'total_rm' => $this->total_rm,
            'internel_status' => $this->internel_status,
            'redemption_invoice_no' => $this->redemption_invoice_no,
            //'created_by' => $this->created_by,
            //'created_datetime' => $this->created_datetime,
            //'updated_by' => $this->updated_by,
            //'updated_datetime' => $this->updated_datetime,
            'pay_out_summary_id' => $this->pay_out_summary_id,
            //'paid_date' => $this->paid_date,
        ]);

        $query->andFilterWhere(['=','redemption_invoice_no', $this->redemption_invoice_no]);
        $query->andFilterWhere(['=','banking_information.account_number', $this->account_number]);

        return $dataProvider;
    }
}
