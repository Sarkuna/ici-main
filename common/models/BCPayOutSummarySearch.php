<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BCPayOutSummary;

/**
 * BCPayOutSummarySearch represents the model behind the search form of `common\models\BCPayOutSummary`.
 */
class BCPayOutSummarySearch extends BCPayOutSummary
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'batch', 'created_by', 'updated_by'], 'integer'],
            [['start_date', 'end_date', 'remark', 'created_datetime', 'updated_datetime'], 'safe'],
            [['rm_value'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BCPayOutSummary::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'batch' => $this->batch,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'rm_value' => $this->rm_value,
            'created_by' => $this->created_by,
            'created_datetime' => $this->created_datetime,
            'updated_by' => $this->updated_by,
            'updated_datetime' => $this->updated_datetime,
        ]);

        $query->andFilterWhere(['like', 'remark', $this->remark]);

        return $dataProvider;
    }
}
