<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "redemption_history".
 *
 * @property integer $redemption_history_id
 * @property integer $redemptionID
 * @property integer $redemption_status
 * @property integer $notify
 * @property string $comment
 * @property string $date_added
 */
class RedemptionBulk extends \yii\db\ActiveRecord
{
    public $comment;
    public $redemption_status_ray;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['redemption_status_ray','comment'], 'required'],
            [['comment'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'redemption_status_ray' => 'Status',
            'comment' => 'Comment',
        ];
    }
}
