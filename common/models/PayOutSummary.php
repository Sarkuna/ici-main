<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pay_out_summary".
 *
 * @property integer $pay_out_summary_id
 * @property string $date_created
 * @property string $description
 */
class PayOutSummary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pay_out_summary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created', 'description'], 'required'],
            [['date_created','start_date','end_date'], 'safe'],
            [['description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pay_out_summary_id' => 'Pay Out Summary ID',
            'date_created' => 'Date Created',
            'description' => 'Description',
        ];
    }
    
    public function getTotalUsers(){
        //return Comment::find()->where(['post' => $this->id])->count();
        return Redemption::find()
                //->joinWith(['order'])
                ->where(['pay_out_summary_id' => $this->pay_out_summary_id])
                //->andWhere(['=', 'redemption_status_ray', 19])
                ->groupBy('painterID')
                ->count();
    }
    
    public function getTotalAmount(){
        //return Comment::find()->where(['post' => $this->id])->count();
        return Redemption::find()
                //->joinWith(['order'])
                ->where(['pay_out_summary_id' => $this->pay_out_summary_id])
                ->andWhere(['=', 'redemption_status', 17])
                //->groupBy('painterID')
                ->sum('req_amount');
    }
    
    public function getBankAccVerified(){
        //return Comment::find()->where(['post' => $this->id])->count();
        return Redemption::find()
                ->joinWith(['bank'])
                ->where(['pay_out_summary_id' => $this->pay_out_summary_id])
                ->andWhere(['=', 'account_no_verification', 'Y'])
                ->groupBy('painterID')
                ->count();
    }
    
    public function getBankAccUnverified(){
        //return Comment::find()->where(['post' => $this->id])->count();
        return Redemption::find()
                ->joinWith(['bank'])
                ->where(['pay_out_summary_id' => $this->pay_out_summary_id])
                ->andWhere(['!=', 'account_no_verification', 'Y'])
                ->groupBy('painterID')
                ->count();
    }
    
    public function getReVerified(){
        return Redemption::find()
                ->where(['pay_out_summary_id' => $this->pay_out_summary_id])
                ->andWhere(['=', 'internel_status', 're-verified'])
                ->groupBy('painterID')
                ->count();
    }

    
    public function getPaidAcc(){
        //return Comment::find()->where(['post' => $this->id])->count();
        return Redemption::find()
                ->where(['pay_out_summary_id' => $this->pay_out_summary_id])
                ->andWhere(['=', 'redemption_status_ray', 19])
                ->groupBy('painterID')
                ->count();
    }
    
    public function getUnpaidAcc(){
        //return Comment::find()->where(['post' => $this->id])->count();
        return Redemption::find()
                ->where(['pay_out_summary_id' => $this->pay_out_summary_id])
                ->andWhere(['!=', 'redemption_status_ray', 19])
                //->groupBy('painterID')
                ->count();
    }
    
    public function getUnpaidAccRm(){
        //return Comment::find()->where(['post' => $this->id])->count();
        return Redemption::find()
                ->where(['pay_out_summary_id' => $this->pay_out_summary_id])
                ->andWhere(['is', 'paid_date', null])
                ->sum('req_amount');
    }
    
    public function getStartredemption(){
        //return Comment::find()->where(['post' => $this->id])->count();
        $return = Redemption::find()
                //->select('redemption_invoice_no')
                ->where(['pay_out_summary_id' => $this->pay_out_summary_id])
                //->andWhere(['=', 'redemption_status_ray', 19])
                //->groupBy('painterID')
                ->orderBy(['redemption_invoice_no'=>SORT_ASC])
                ->one();
        
        return $return->redemption_invoice_no;
    }
    
    public function getEndredemption(){
        //return Comment::find()->where(['post' => $this->id])->count();
        $return = Redemption::find()
                //->select('redemption_invoice_no')
                ->where(['pay_out_summary_id' => $this->pay_out_summary_id])
                //->andWhere(['=', 'redemption_status_ray', 19])
                //->groupBy('painterID')
                ->orderBy(['redemption_invoice_no' => SORT_DESC])
                ->one();
        
        return $return->redemption_invoice_no;
    }

}
