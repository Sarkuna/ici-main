<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * This is the model class for table "redemption".
 *
 * @property integer $redemptionID
 * @property integer $painterID
 * @property string $redemption_invoice_no
 * @property string $orderID
 * @property integer $req_points
 * @property string $req_amount
 * @property integer $redemption_status
 * @property integer $redemption_remarks
 * @property string $redemption_IP
 * @property string $redemption_created_datetime
 * @property integer $redemption_created_by
 * @property string $redemption_updated_datetime
 * @property integer $redemption_updated_by
 */
class Redemption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $card_id;
    public $total_transactions;
    public $painter_user;
    public $v_orders;
    public $v_invoice;
    
    public static function tableName()
    {
        return 'redemption';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'redemption_created_datetime',
                'updatedAtAttribute' => 'redemption_updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['painterID', 'redemption_invoice_no', 'orderID', 'req_points', 'req_amount', 'redemption_status','redemption_status_ray'], 'required'],
            [['painterID', 'req_points', 'redemption_status','redemption_status_ray', 'created_by', 'updated_by'], 'integer'],
            [['orderID','redemption_remarks'], 'string'],
            [['req_amount'], 'number'],
            [['redemption_status_ray','total_transactions','redemption_created_datetime', 'redemption_updated_datetime','ticket'], 'safe'],
            [['redemption_invoice_no'], 'string', 'max' => 100],
            //[['redemption_invoice_no'], 'string', 'max' => 100],
            //[['redemption_invoice_no'], 'unique'],
            [['redemption_IP'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'redemptionID' => 'Redemption ID',
            'painterID' => 'Painter ID',
            'redemption_invoice_no' => 'Redemption #',
            'orderID' => 'Order ID',
            'req_points' => 'Points',
            'req_amount' => 'Amount',
            'total_transactions' => 'Total Transactions',
            'redemption_status' => 'Status',
            'redemption_status_ray' => 'Status',
            'redemption_remarks' => 'Redemption Remarks',
            'redemption_IP' => 'Redemption  Ip',
            'redemption_created_datetime' => 'Requested Date',
            'created_by' => 'Redemption Created By',
            'redemption_updated_datetime' => 'Updated Datetime',
            'updated_by' => 'Redemption Updated By',
        ];
    }
    
    public function getUser()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'painterID']);
    }
    
    public function getRedemptionitems()
    {
        return $this->hasOne(RedemptionItems::className(), ['redemptionID' => 'redemptionID']);
    }
    
    public function getProfile()
    {
        return $this->hasOne(\app\modules\painter\models\PainterProfile::className(), ['user_id' => 'painterID']);
    }
    
    public function getBank()
    {
        return $this->hasOne(\app\modules\painter\models\BankingInformation::className(), ['user_id' => 'painterID']);
    }
    
    public function getOrderStatus() {
        return $this->hasOne(\common\models\OrderStatus::className(), ['order_status_id' => 'redemption_status']);
    }
    
    public function getOrderStatus2() {
        return $this->hasOne(\common\models\OrderStatus::className(), ['order_status_id' => 'redemption_status_ray']);
    }
    
    
    public function getTotalTransactions(){
        //return Comment::find()->where(['post' => $this->id])->count();
        return RedemptionItems::find()->where(['redemptionID' => $this->redemptionID])->count();
    }
    
    public function getTotalAmount(){
        //return Comment::find()->where(['post' => $this->id])->count();
        return RedemptionItems::find()->where(['redemptionID' => $this->redemptionID])->count();
    }

    public function getActions() {
        $returnValue = "";


        $returnValue = $returnValue . '<a href="' . Url::to(['/management/redemption/view/', 'id' => $this->redemptionID]) . '" title="View" ><i class="fa fa-fw fa-eye"></i></a>';
        //$returnValue = $returnValue . '<a href="' . Url::to(['/management/redemption/invoiceapprove/','id' => $this->redemptionID]) . '" title="Approve" ><i class="fa fa-fw fa-check"></i></a>';
        if ($this->redemption_status == "1" || $this->redemption_status_ray == "2") {
            $returnValue = $returnValue . '<a href="' . Url::to(['/management/redemption/invoiceapprove/', 'id' => $this->redemptionID]) . '" title="Approve" ><i class="fa fa-fw fa-check"></i></a>';
        }
        return $returnValue;
    }
    
    public function getActions2() {
        $returnValue = "";


        $returnValue = $returnValue . '<a href="' . Url::to(['/management/redemption/view/', 'id' => $this->redemptionID]) . '" title="View" ><i class="fa fa-fw fa-eye"></i></a>';
        //$returnValue = $returnValue . '<a href="' . Url::to(['/management/redemption/invoiceapprove/','id' => $this->redemptionID]) . '" title="Approve" ><i class="fa fa-fw fa-check"></i></a>';
        /*if ($this->redemption_status == "1") {
            $returnValue = $returnValue . '<a href="' . Url::to(['/management/redemption/invoiceapprove/', 'id' => $this->redemptionID]) . '" title="Approve" ><i class="fa fa-fw fa-check"></i></a>';
        }*/
        if ($this->redemption_status == "17") {
            if ($this->redemption_status_ray == "2"){
                $returnValue = $returnValue . '<a href="' . Url::to(['/management/redemption/makepayment/', 'id' => $this->redemptionID]) . '" title="Make Payment" ><i class="fa fa-fw fa-money"></i></a>';
            }else if ($this->redemption_status_ray == "19"){
                $returnValue = $returnValue . '<a href="' . Url::to(['/management/redemption/viewpdf/', 'id' => $this->redemptionID]) . '" title="View PDF" target="_blank"><i class="fa fa-file-pdf-o"></i></a>';
            }
        }
        return $returnValue;
    }

    public function getRealIp() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
}
