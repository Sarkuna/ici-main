<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bar_codes".
 *
 * @property integer $bar_code_id
 * @property integer $product_list_id
 * @property string $bar_code_name
 * @property string $bar_code_status
 * @property string $bar_code_batch
 * @property string $bar_code_expiry
 * @property string $created_datetime
 * @property string $updated_datetime
 * @property integer $created_by
 * @property integer $updated_by
 */
class BarCodes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bar_codes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_list_id', 'bar_code_name'], 'required'],
            [['product_list_id', 'created_by', 'updated_by'], 'integer'],
            [['bar_code_expiry', 'created_datetime', 'updated_datetime'], 'safe'],
            [['bar_code_name', 'bar_code_batch'], 'string', 'max' => 100],
            [['bar_code_status'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bar_code_id' => 'Bar Code ID',
            'product_list_id' => 'Product List ID',
            'bar_code_name' => 'Bar Code Name',
            'bar_code_status' => 'Bar Code Status',
            'bar_code_batch' => 'Bar Code Batch',
            'bar_code_expiry' => 'Bar Code Expiry',
            'created_datetime' => 'Created Datetime',
            'updated_datetime' => 'Updated Datetime',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
