<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Redemption;

/**
 * RedemptionSearch represents the model behind the search form about `common\models\Redemption`.
 */
class RedemptionSearch extends Redemption
{
    /**
     * @inheritdoc
     */
    public $card_id;
    public $ic_no;
    public $full_name;
    public $account_no_verification;
    public $account_number;
    public $bank_name;
    public $account_name;
    public $limit;
    public $pagination = true;


    public function rules()
    {
        return [
            [['redemptionID', 'painterID', 'req_points', 'redemption_status', 'redemption_remarks', 'created_by', 'updated_by'], 'integer'],
            [['redemption_status_ray','full_name','ic_no','card_id','redemption_invoice_no', 'orderID', 'redemption_IP', 'redemption_created_datetime', 'redemption_updated_datetime', 'account_no_verification', 'account_number', 'bank_name', 'internel_status', 'account_name'], 'safe'],
            [['req_amount'], 'number'],
            [['limit'], 'integer', 'min' => 7, 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchpainter($params)
    {
        //,'painter_profile.mobile'
        $query = Redemption::find()
            ->select(['redemptionID','painterID','redemption_invoice_no','req_points','req_amount','redemption_created_datetime', 'redemption_status', 'redemption_status_ray'])
            /*->andWhere(['BETWEEN', 'redemption_created_datetime',
                new \yii\db\Expression('(NOW() - INTERVAL 12 MONTH)'),
                new \yii\db\Expression('NOW()')])*/
            ->limit(30);

 
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort'=> ['defaultOrder' => ['redemption_status'=>SORT_DESC]],
            'sort'=> ['defaultOrder' => ['redemptionID'=>SORT_DESC]],
            //'totalCount'=> Redemption::find()->count()
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'redemptionID' => $this->redemptionID,
            'req_points' => $this->req_points,
            'req_amount' => $this->req_amount,
            'redemption_status' => $this->redemption_status,
            'painterID' => $this->painterID,
            //'redemption_status_ray' => $this->redemption_status_ray
        ]);
        return $dataProvider;
    }
    
    public function search($params)
    {
        //,'painter_profile.mobile'
        $query = Redemption::find()
            ->select(['redemptionID','painterID','redemption_invoice_no','req_points','req_amount','redemption_created_datetime', 'redemption_status'])
            ->andWhere(['BETWEEN', 'redemption_created_datetime',
                new \yii\db\Expression('(NOW() - INTERVAL 3 MONTH)'),
                new \yii\db\Expression('NOW()')])
            ->limit(30);

 
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort'=> ['defaultOrder' => ['redemption_status'=>SORT_DESC]],
            'sort'=> ['defaultOrder' => ['redemptionID'=>SORT_DESC]],
            //'totalCount'=> Redemption::find()->count()
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'redemptionID' => $this->redemptionID,
            'req_points' => $this->req_points,
            'req_amount' => $this->req_amount,
            'redemption_status' => $this->redemption_status,
            'painterID' => $this->painterID,
            //'redemption_status_ray' => $this->redemption_status_ray
        ]);
        return $dataProvider;
    }
    
    public function searchray($params)
    {
        //$query = Redemption::find();
        //$query->joinWith(['profile']);
        
        $query = Redemption::find()->joinWith([
            'profile' => function($query) {
                $query->select(['painter_profile.id','painter_profile.user_id','painter_profile.card_id','painter_profile.full_name','painter_profile.ic_no','painter_profile.mobile']);
                //$query->select(['profile.id','profile.username'])->where(['user.status' => 'A']);
            },            
            'bank',
            ])
            ->select(['*']);
            /*->where(['point_order.order_status'=> 17])
            ->andWhere(['BETWEEN', 'point_order.created_datetime',
                new \yii\db\Expression('(NOW() - INTERVAL 1 MONTH)'),
                new \yii\db\Expression('NOW()')])*/
            //->limit(20);

        // add conditions that should always apply here
        
        if($this->pagination){
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'sort'=> ['defaultOrder' => ['redemption_status_ray'=>SORT_ASC]],
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);
        }else{
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'sort'=> ['defaultOrder' => ['redemption_status_ray'=>SORT_ASC]],
                'pagination' => false,
            ]);
        }

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'redemptionID' => $this->redemptionID,
            'painterID' => $this->painterID,
            'card_id' => $this->card_id,
            'ic_no' => $this->ic_no,
            'req_points' => $this->req_points,
            'req_amount' => $this->req_amount,
            'redemption_status' => $this->redemption_status,
            'redemption_status_ray' => $this->redemption_status_ray,
            'redemption_remarks' => $this->redemption_remarks,
            //'redemption_created_datetime' => $this->redemption_created_datetime,
            'redemption_created_by' => $this->created_by,
            'redemption_updated_datetime' => $this->redemption_updated_datetime,
            'redemption_updated_by' => $this->updated_by,
            'pay_out_summary_id' => $this->pay_out_summary_id
        ]);
        $query->andFilterWhere(['like', 'redemption_created_datetime', $this->redemption_created_datetime]);
        $query->andFilterWhere(['like', 'redemption_invoice_no', $this->redemption_invoice_no])
            ->andFilterWhere(['like', 'orderID', $this->orderID])
            ->andFilterWhere(['like', 'card_id', $this->card_id]) 
            ->andFilterWhere(['like', 'full_name', $this->full_name]) 
            ->andFilterWhere(['like', 'internel_status', $this->internel_status])
            ->andFilterWhere(['like', 'redemption_IP', $this->redemption_IP]);
        $query->andFilterWhere(['like', 'card_id', $this->card_id]);
        
        $query->andFilterWhere(['like', 'banking_information.account_no_verification', $this->account_no_verification]);
        $query->andFilterWhere(['like', 'banking_information.account_number', $this->account_number]);
        $query->andFilterWhere(['like', 'banking_information.bank_name', $this->bank_name]);
        $query->andFilterWhere(['like', 'banking_information.account_name', $this->account_name]);
        
        $query->andFilterWhere(['IN', 'redemption_status_ray', ['2','8','10','19']]);
            //->andFilterWhere(['like', 'roles', $this->roles]);
        //echo $this->redemption_created_datetime;
        //die();
        //$query->limit($this->limit);
        return $dataProvider;
    }
    
    public function searchraypaid($params)
    {
        //$query = Redemption::find();
        //$query->joinWith(['profile']);
        
        $query = Redemption::find()->joinWith([
            'profile' => function($query) {
                $query->select(['painter_profile.id','painter_profile.user_id','painter_profile.card_id','painter_profile.full_name','painter_profile.ic_no','painter_profile.mobile']);
                //$query->select(['profile.id','profile.username'])->where(['user.status' => 'A']);
            },            
            //'bank',
            ])
            ->select(['redemptionID','painterID','redemption_invoice_no','req_points','req_amount','redemption_created_datetime'])
            /*->where(['point_order.order_status'=> 17])
            ->andWhere(['BETWEEN', 'point_order.created_datetime',
                new \yii\db\Expression('(NOW() - INTERVAL 1 MONTH)'),
                new \yii\db\Expression('NOW()')])*/
            ->limit(30);

        // add conditions that should always apply here
        
        if($this->pagination){
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'sort'=> ['defaultOrder' => ['redemption_status_ray'=>SORT_ASC]],
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);
        }else{
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'sort'=> ['defaultOrder' => ['redemption_status_ray'=>SORT_ASC]],
                'pagination' => false,
            ]);
        }

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'redemptionID' => $this->redemptionID,
            'painterID' => $this->painterID,
            'card_id' => $this->card_id,
            'ic_no' => $this->ic_no,
            'req_points' => $this->req_points,
            'req_amount' => $this->req_amount,
            'redemption_status' => $this->redemption_status,
            'redemption_status_ray' => $this->redemption_status_ray,
            'redemption_remarks' => $this->redemption_remarks,
            //'redemption_created_datetime' => $this->redemption_created_datetime,
            'redemption_created_by' => $this->created_by,
            'redemption_updated_datetime' => $this->redemption_updated_datetime,
            'redemption_updated_by' => $this->updated_by,
        ]);
        $query->andFilterWhere(['like', 'redemption_created_datetime', $this->redemption_created_datetime]);
        $query->andFilterWhere(['like', 'redemption_invoice_no', $this->redemption_invoice_no])
            //->andFilterWhere(['like', 'orderID', $this->orderID])
            ->andFilterWhere(['like', 'card_id', $this->card_id]) 
            ->andFilterWhere(['like', 'full_name', $this->full_name]); 
            //->andFilterWhere(['like', 'internel_status', $this->internel_status]);
            //->andFilterWhere(['like', 'redemption_IP', $this->redemption_IP]);
        $query->andFilterWhere(['like', 'card_id', $this->card_id]);
        
        //$query->andFilterWhere(['like', 'banking_information.account_no_verification', $this->account_no_verification]);
        //$query->andFilterWhere(['like', 'banking_information.account_number', $this->account_number]);
        //$query->andFilterWhere(['like', 'banking_information.bank_name', $this->bank_name]);
        //$query->andFilterWhere(['like', 'banking_information.account_name', $this->account_name]);
        
        //$query->andFilterWhere(['IN', 'redemption_status_ray', ['2','8','10','19']]);
            //->andFilterWhere(['like', 'roles', $this->roles]);
        //echo $this->redemption_created_datetime;
        //die();
        self::getDb()->cache(function ($db) use ($dataProvider) {
            $dataProvider->prepare();

        }, 3600);
        $query->limit($this->limit);
        return $dataProvider;
    }
    
    
    public function searchbyuser($params)
    {
        //,'painter_profile.mobile'
        $query = Redemption::find()->joinWith([
            'profile' => function($query) {
                $query->select(['painter_profile.id','painter_profile.user_id','painter_profile.card_id','painter_profile.full_name','painter_profile.ic_no']);
                //$query->select(['profile.id','profile.username'])->where(['user.status' => 'A']);
            },            
            //'bank',
            ])
            ->select(['redemptionID','painterID','redemption_invoice_no','req_points','req_amount','redemption_created_datetime', 'redemption_status'])
            /*->where(['point_order.order_status'=> 17])*/

            ->limit(30);
            


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort'=> ['defaultOrder' => ['redemption_status'=>SORT_DESC]],
            'sort'=> ['defaultOrder' => ['redemptionID'=>SORT_DESC]],
            //'totalCount'=> Redemption::find()->count()
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'redemptionID' => $this->redemptionID,
            'painterID' => $this->painterID,
            'card_id' => $this->card_id,
            'ic_no' => $this->ic_no,
            //'full_name' => $this->full_name,
            'req_points' => $this->req_points,
            'req_amount' => $this->req_amount,
            'redemption_status' => $this->redemption_status,
            'redemption_status_ray' => $this->redemption_status_ray,
            'redemption_remarks' => $this->redemption_remarks,
            //'redemption_created_datetime' => $this->redemption_created_datetime,
            'redemption_created_by' => $this->created_by,
            'redemption_updated_datetime' => $this->redemption_updated_datetime,
            'redemption_updated_by' => $this->updated_by,
        ]);
        $query->andFilterWhere(['like', 'redemption_created_datetime', $this->redemption_created_datetime]);
        $query->andFilterWhere(['like', 'redemption_invoice_no', $this->redemption_invoice_no])
            ->andFilterWhere(['like', 'orderID', $this->orderID])
            ->andFilterWhere(['like', 'card_id', $this->card_id])
            ->andFilterWhere(['like', 'full_name', $this->full_name])      
            ->andFilterWhere(['like', 'redemption_IP', $this->redemption_IP]);
        $query->andFilterWhere(['like', 'card_id', $this->card_id]);
        $query->andFilterWhere(['IN', 'redemption_status', ['1','7','17']]);
            //->andFilterWhere(['like', 'roles', $this->roles]);
        //echo $this->redemption_created_datetime;
        //die();
        return $dataProvider;
    }
    
    
    public function searchcanceled($params)
    {
        //,'painter_profile.mobile'
        $query = Redemption::find()
            ->select(['redemptionID','painterID','redemption_invoice_no','req_points','req_amount','redemption_created_datetime', 'redemption_status'])
            /*->andWhere(['BETWEEN', 'redemption_created_datetime',
                new \yii\db\Expression('(NOW() - INTERVAL 3 MONTH)'),
                new \yii\db\Expression('NOW()')])*/
            ->limit(30);

 
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort'=> ['defaultOrder' => ['redemption_status'=>SORT_DESC]],
            'sort'=> ['defaultOrder' => ['redemptionID'=>SORT_DESC]],
            //'totalCount'=> Redemption::find()->count()
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'redemptionID' => $this->redemptionID,
            'req_points' => $this->req_points,
            'req_amount' => $this->req_amount,
            'redemption_status' => $this->redemption_status,
            'painterID' => $this->painterID,
            //'redemption_status_ray' => $this->redemption_status_ray
        ]);
        return $dataProvider;
    }
}
