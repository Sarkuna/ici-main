<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "painting_jobs_painter".
 *
 * @property integer $painting_jobs_painter_id
 * @property integer $company_information_id
 * @property integer $painting_jobs_id
 * @property string $painting_jobs_type
 */
class PaintingJobsPainter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'painting_jobs_painter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_information_id', 'painting_jobs_id', 'painting_jobs_type'], 'required'],
            [['company_information_id', 'painting_jobs_id'], 'integer'],
            [['painting_jobs_type'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'painting_jobs_painter_id' => 'Painting Jobs Painter ID',
            'company_information_id' => 'Company Information ID',
            'painting_jobs_id' => 'Painting Jobs ID',
            'painting_jobs_type' => 'Painting Jobs Type',
        ];
    }
    
    public function getPaintingJobs()
    {
        return $this->hasOne(\common\models\PaintingJobs::className(), ['painting_jobs_id' => 'painting_jobs_id']);
    }
}
