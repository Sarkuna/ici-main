<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "email_queue".
 *
 * @property integer $id
 * @property integer $email_template_id
 * @property integer $email_template_global_id
 * @property integer $user_id
 * @property string $name
 * @property string $email
 * @property string $data
 * @property string $datetime
 * @property string $status
 */
class EmailQueue extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'email_queue';
    }

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['email_template_id', 'email_template_global_id', 'user_id', 'created_by', 'updated_by', 'condo_id'], 'integer'],
            [['name'], 'required'],
            [['data'], 'string'],
            [['created_datetime', 'updated_datetime','date_to_send','type'], 'safe'],
            [['mobile'], 'string', 'max' => 20],
            [['name', 'email'], 'string', 'max' => 150],
            [['status', 'type'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'email_template_id' => Yii::t('app', 'Email Template ID'),
            'email_template_global_id' => Yii::t('app', 'Global Email Template ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'data' => Yii::t('app', 'Data'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    //Not used anymore
    public function addIntoQueue($user_id, $email_template_code, $data) {
        $session = Yii::$app->session;

        $user = \common\models\User::findOne($user_id);

        $emailTemplate = \common\models\EmailTemplate::find()
            ->where(['condo_id' => $session['currentCondoId'], 'code' => $email_template_code])
            ->one();

        $this->email_template_id = $emailTemplate->id;
        $this->user_id = $user_id;
        $this->name = $user->username;
        $this->email = $user->email;
        if ($data != null)
            $this->data = $data;

        $this->status = 'P';

        $this->save();
    }

    public function insertIntoQueue($user_id, $email_template_code, $data) {
        $session = Yii::$app->session;

        $user = \common\models\User::findOne($user_id);

        $emailTemplate = \common\models\EmailTemplateGlobal::find()
            ->where(['code' => $email_template_code])
            ->one();

        $this->email_template_global_id = $emailTemplate->id;
        $this->user_id = $user_id;
        $this->name = $user->username;
        $this->email = $user->email;
        if ($data != null)
            $this->data = $data;

        $this->status = 'P';

        $this->save(false);
    }
}
