<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\AutoTransactionItem;

/**
 * AutoTransactionItemSearch represents the model behind the search form of `common\models\AutoTransactionItem`.
 */
class AutoTransactionItemSearch extends AutoTransactionItem
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'auto_transaction_id'], 'integer'],
            [['transaction_num', 'transaction_status', 'status', 'comment'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AutoTransactionItem::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'auto_transaction_id' => $this->auto_transaction_id,
        ]);

        $query->andFilterWhere(['like', 'transaction_num', $this->transaction_num])
            ->andFilterWhere(['like', 'transaction_status', $this->transaction_status])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
