<?php
namespace common\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Login form
 */
class CampaignAssignProductsForm extends Model
{
    public $product_name;
    public $product_description;
    public $new_points;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['product_name', 'new_points'], 'required'],
            [['files'], 'file', 'maxFiles' => 5, 'skipOnEmpty' => true, 'extensions'=>['pdf','doc','docx','jpg','jpeg','png','bmp'], 'checkExtensionByMimeType'=>false],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'files' => 'Proof Documents',
            'new_points' => 'New Points per (L)',
        ];
    }

    
}
