<?php
namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;


class FileForm extends Model
{
    public $file;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['file', 'required'],
            //['excel', 'file'],
            [['file'], 'file', 'extensions' => 'csv'],
        ];
    }

    
}
