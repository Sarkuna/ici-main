<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "bc_redemption".
 *
 * @property int $redemptionID
 * @property int $painterID
 * @property int $crm_id
 * @property string $redemption_invoice_no
 * @property int $count_of_sku
 * @property float $sum_of_points
 * @property float $per_rm
 * @property float $total_rm
 * @property string $redemption_remarks
 * @property string $redemption_IP
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $pay_out_summary_id
 * @property string|null $paid_date
 * @property string|null $internel_status
 * @property string|null $comment
 * @property string|null $review_comment
 * @property string|null $ticket
 */
class BCRedemption extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bc_redemption';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => 'mdm\autonumber\Behavior',
                'attribute' => 'redemption_invoice_no', // required
                    'group' => 23, // optional
                    'value' => 'BCR?' , // format auto number. '?' will be replaced with generated number
                    'digit' => 6 // optional, default to null. 
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['painterID', 'crm_id', 'count_of_sku', 'created_by', 'updated_by', 'pay_out_summary_id'], 'integer'],
            [['crm_id', 'count_of_sku', 'sum_of_points', 'per_rm', 'total_rm', 'redemption_remarks', 'redemption_IP'], 'required'],
            [['sum_of_points', 'per_rm', 'total_rm','total_rm_crm'], 'number'],
            [['redemption_remarks', 'comment', 'review_comment', 'ticket'], 'string'],
            [['paid_date','total_rm_crm'], 'safe'],
            [['redemption_invoice_no'], 'string', 'max' => 100],
            [['redemption_IP'], 'string', 'max' => 20],
            [['internel_status'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'redemptionID' => 'Redemption ID',
            'painterID' => 'Painter ID',
            'crm_id' => 'Crm ID',
            'redemption_invoice_no' => 'Redemption Invoice No',
            'count_of_sku' => 'Count Of Sku',
            'sum_of_points' => 'Sum Of Points',
            'per_rm' => 'Per Rm',
            'total_rm' => 'Total Rm',
            'redemption_remarks' => 'Redemption Remarks',
            'redemption_IP' => 'Redemption Ip',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'pay_out_summary_id' => 'Pay Out Summary ID',
            'paid_date' => 'Paid Date',
            'internel_status' => 'Internel Status',
            'comment' => 'Comment',
            'review_comment' => 'Review Comment',
            'ticket' => 'Ticket',
        ];
    }
    
    public function getProfile()
    {
        return $this->hasOne(PainterProfile::className(), ['user_id' => 'painterID']);
    }
    
    public function getBank()
    {
        return $this->hasOne(BankingInformation::className(), ['user_id' => 'painterID']);
    }
    
    public function getMastercrm()
    {
        return $this->hasOne(CRMList::className(), ['crm_id' => 'crm_id']);
    }
}
