<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_status".
 *
 * @property integer $order_status_id
 * @property integer $language_id
 * @property string $name
 */
class PicName extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pic_name';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pic_id' => 'Order Status ID',
            'name' => 'Name',
        ];
    }
}
