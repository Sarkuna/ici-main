<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bc_excel_summery".
 *
 * @property int $id
 * @property int $bc_pay_out_id
 * @property int|null $bc_redemption_id
 * @property string $points
 * @property string $timestamps
 * @property string $product_name
 * @property string $product_id
 * @property string $magnitude
 * @property string $sku
 * @property int $serial_no
 * @property string $type
 * @property int|null $crm_id
 * @property string|null $status
 */
class BCExcelSummery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bc_excel_summery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bc_pay_out_id', 'points', 'timestamps', 'product_name', 'product_id', 'magnitude', 'sku', 'serial_no', 'type'], 'required'],
            [['bc_pay_out_id', 'bc_redemption_id', 'serial_no', 'crm_id'], 'integer'],
            [['timestamps'], 'string'],
            [['points', 'sku', 'status'], 'string', 'max' => 10],
            [['product_name'], 'string', 'max' => 255],
            [['product_id'], 'string', 'max' => 20],
            [['magnitude'], 'string', 'max' => 6],
            [['type'], 'string', 'max' => 30],
            [['serial_no'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bc_pay_out_id' => 'Bc Pay Out ID',
            'bc_redemption_id' => 'Bc Redemption ID',
            'points' => 'Points',
            'timestamps' => 'Timestamps',
            'product_name' => 'Product Name',
            'product_id' => 'Product ID',
            'magnitude' => 'Magnitude',
            'sku' => 'Sku',
            'serial_no' => 'Serial No',
            'type' => 'Type',
            'crm_id' => 'CRM ID',
            'status' => 'Status',
        ];
    }
    
    public function getPayout()
    {
        return $this->hasOne(BCPayOutSummary::className(), ['id' => 'bc_pay_out_id']);
    }
    
    public function getPainter()
    {
        return $this->hasOne(PainterProfile::className(), ['crm_id' => 'crm_id']);
    }
}
