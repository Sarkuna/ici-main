<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ReportPainterProfile;

/**
 * PainterProfileSearch represents the model behind the search form about `app\modules\painter\models\PainterProfile`.
 */
class PainterProfileSearch extends PainterProfile
{
    public $user;
    public $status;
    public $email;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'card_id', 'title','nationality', 'race', 'created_by', 'updated_by'], 'integer'],
            [['email','profile_status','photo', 'full_name', 'profile_name', 'mobile', 'ic_no', 'created_datetime', 'updated_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PainterProfile::find();
        $query->joinWith(['user']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['profile_status'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'card_id' => $this->card_id,
            'title' => $this->title,
            //'email' => $this->email,
            'nationality' => $this->nationality,
            'race' => $this->race,
            'profile_status' => $this->profile_status,
            'created_datetime' => $this->created_datetime,
            'updated_datetime' => $this->updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            //'status' => $this->status,
        ]);
        

        $query->andFilterWhere(['like', 'user.email', $this->email])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'profile_name', $this->profile_name])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'ic_no', $this->ic_no])
            ->andFilterWhere(['like', 'profile_status', $this->profile_status]);    
            //->andFilterWhere(['like', 'status', $this->status]);
        
        if ($this->user != null) {
            $query->andFilterWhere(['like', 'user.id', $this->user]);
        }

        /*echo '<pre>';
        print_r($dataProvider);
        die();*/

        return $dataProvider;
    }
    
    public function searchemails($params)
    {
        $query = User::find()->where(['user_type' => 'P'])->andWhere(['not', ['email'=>'']]);
        //$query = PainterProfile::find();
        //$query->joinWith(['userPainter']);
        //$query->andFilterWhere(['<>', 'email', $this->email]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort'=> ['defaultOrder' => ['profile_status'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'card_id' => $this->card_id,
            'title' => $this->title,
            //'email' => $this->email,
            'nationality' => $this->nationality,
            'race' => $this->race,
            'profile_status' => $this->profile_status,
            'created_datetime' => $this->created_datetime,
            'updated_datetime' => $this->updated_datetime,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            //'status' => $this->status,
        ]);
        

        /*$query->andFilterWhere(['like', 'user.email', $this->email])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'profile_name', $this->profile_name])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'ic_no', $this->ic_no])
            ->andFilterWhere(['like', 'profile_status', $this->profile_status]);    
        
        if ($this->user != null) {
            $query->andFilterWhere(['like', 'user.id', $this->user]);
        }*/

        /*echo '<pre>';
        print_r($dataProvider);
        die();*/

        return $dataProvider;
    }
}
