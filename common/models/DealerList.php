<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use yii\helpers\Html;

/**
 * This is the model class for table "dealer_list".
 *
 * @property integer $id
 * @property string $customer_name
 * @property string $account_type
 * @property string $region
 * @property string $state
 * @property string $area
 * @property string $address
 * @property string $contact_no
 * @property string $contact_person
 * @property string $status
 * @property string $created_datetime
 * @property integer $created_by
 * @property string $updated_datetime
 * @property integer $updated_by
 */
class DealerList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dealer_list';
    }
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_name', 'region', 'state', 'area', 'address', 'contact_no', 'contact_person','code'], 'required'],
            [['address'], 'string'],
            [['region_id','created_datetime', 'updated_datetime'], 'safe'],
            [['region_id','created_by', 'updated_by'], 'integer'],
            [['code'], 'string', 'max' => 100],
            [['customer_name'], 'string', 'max' => 300],
            [['account_type', 'region'], 'string', 'max' => 100],
            [['state', 'area'], 'string', 'max' => 50],
            [['contact_no', 'contact_person'], 'string', 'max' => 200],
            [['status'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_name' => 'Dealer',
            'account_type' => 'Account Type',
            'region_id' => 'Region',
            'region' => 'Region',
            'state' => 'State',
            'area' => 'Area',
            'address' => 'Address',
            'contact_no' => 'Contact No',
            'contact_person' => 'Contact Person',
            'status' => 'Status',
            'created_datetime' => 'Created Datetime',
            'created_by' => 'Created By',
            'updated_datetime' => 'Updated Datetime',
            'updated_by' => 'Updated By',
        ];
    }
    
    public function getStatus() {
        $returnValue = "";
        if ($this->status == "A") {
            $returnValue = "<span class='label label-success'>Active</span>";
        } else if ($this->status == "X") {
            $returnValue = "<span class='label label-danger'>Deleted</span>";
        }
        return $returnValue;
    }
}