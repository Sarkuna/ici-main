<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PointOrder;

/**
 * PointOrderSearch represents the model behind the search form about `common\models\PointOrder`.
 */
class PointOrderSearch extends PointOrder
{
    /**
     * @inheritdoc
     */
    public $card_id;
    public $full_name;
    public $customer_name;
    public $dealer_invoice_no;
    public $item_id;
    public $globalSearch;
    
    public function rules()
    {
        return [
            [['order_id', 'order_qty', 'order_total_point', 'created_by', 'updated_by'], 'integer'],
            [['item_id','customer_name','full_name','card_id','painter_login_id', 'order_num','painter_ic', 'order_status', 'redemption','order_remarks', 'order_IP', 'created_datetime', 'globalSearch', 'updated_datetime', 'dealer_invoice_no', 'dealer_invoice_date'], 'safe'],
            [['order_total_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        set_time_limit(0);
        Yii::$app->cache->flush();
        $query = PointOrder::find()
        ->select('point_order.order_id, point_order.order_dealer_id, point_order.painter_login_id, order_num,point_order.dealer_invoice_no, point_order.dealer_invoice_date, point_order.order_total_point, point_order.order_total_amount, point_order.order_status')  
        /*->innerJoinWith([
            'dealerOutlet' => function($que) {
                $que->select(['dealer_list.id','dealer_list.customer_name']);
            },
            'profile' => function($que) {
                $que->select(['painter_profile.user_id','card_id','full_name']);
            },
            'orderStatus' => function($que) {
                //$que->select(['id','username','mobile']);
            }
        ])*/
                //->cache(7200)
            ->where(['point_order.order_status' => '1'])
               ->limit(20);
        
        //$query = PointOrder::find()->limit(20);
        //$query->innerJoinWith(['dealerOutlet','profile','orderStatus']);
        // add conditions that should always apply here
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder'=>'point_order.order_num ASC'],
            'sort'=> ['defaultOrder' => ['order_num'=>SORT_DESC]],
            //'totalCount'=> PointOrder::find()->count(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $this->load($params);


        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'order_num' => $this->order_num,
            //'order_dealer_id' => $this->order_dealer_id,
            //'profile.card_id' => $this->card_id,
            'dealer_invoice_no' => $this->dealer_invoice_no,
            'order_status' => $this->order_status,
            //'painter_login_id' => $this->painter_login_id,
            //'updated_datetime' => $this->updated_datetime,
        ]);
        /*$query->andFilterWhere(['like', 'point_order.created_datetime', $this->created_datetime]);
        $query->andFilterWhere(['like', 'painter_ic', $this->painter_ic])
            ->andFilterWhere(['like', 'card_id', $this->card_id])
            ->andFilterWhere(['like', 'full_name', $this->full_name])    
            ->andFilterWhere(['like', 'customer_name', $this->customer_name]);*/            

        return $dataProvider;
    }
    
    public function searchcancel($params)
    {
        set_time_limit(0);
        Yii::$app->cache->flush();
        $query = PointOrder::find()
        ->select('point_order.order_id, point_order.order_dealer_id, point_order.painter_login_id, order_num,point_order.dealer_invoice_no, point_order.dealer_invoice_date, point_order.order_total_point, point_order.order_total_amount, point_order.order_status')  
        /*->innerJoinWith([
            'dealerOutlet' => function($que) {
                $que->select(['dealer_list.id','dealer_list.customer_name']);
            },
            'profile' => function($que) {
                $que->select(['painter_profile.user_id','card_id','full_name']);
            },
            'orderStatus' => function($que) {
                //$que->select(['id','username','mobile']);
            }
        ])*/
                //->cache(7200)
            ->where(['point_order.order_status' => '7'])
               ->limit(20);
        
        //$query = PointOrder::find()->limit(20);
        //$query->innerJoinWith(['dealerOutlet','profile','orderStatus']);
        // add conditions that should always apply here
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder'=>'point_order.order_num ASC'],
            'sort'=> ['defaultOrder' => ['order_num'=>SORT_DESC]],
            //'totalCount'=> PointOrder::find()->count(),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $this->load($params);


        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'order_num' => $this->order_num,
            //'order_dealer_id' => $this->order_dealer_id,
            //'profile.card_id' => $this->card_id,
            'dealer_invoice_no' => $this->dealer_invoice_no,
            'order_status' => $this->order_status,
            //'painter_login_id' => $this->painter_login_id,
            //'updated_datetime' => $this->updated_datetime,
        ]);
        /*$query->andFilterWhere(['like', 'point_order.created_datetime', $this->created_datetime]);
        $query->andFilterWhere(['like', 'painter_ic', $this->painter_ic])
            ->andFilterWhere(['like', 'card_id', $this->card_id])
            ->andFilterWhere(['like', 'full_name', $this->full_name])    
            ->andFilterWhere(['like', 'customer_name', $this->customer_name]);*/            

        return $dataProvider;
    }
    
    public function searchapproved($params)
    {            
        $query = PointOrder::find()
            /*->joinWith([
            'profile' => function($query) {
                $query->select(['painter_profile.id','painter_profile.user_id','painter_profile.card_id','painter_profile.full_name']);
                //$query->select(['profile.id','profile.username'])->where(['user.status' => 'A']);
            }])*/
            ->select(['point_order.order_id','point_order.order_num','point_order.painter_login_id','point_order.order_total_point','point_order.order_total_amount','point_order.order_status','point_order.created_datetime', 'point_order.dealer_invoice_no', 'point_order.dealer_invoice_date'])
            ->where(['point_order.order_status'=> 17])       
            /*->andWhere(['BETWEEN', 'point_order.created_datetime',
                '2022-02-16 00:00:00',
                '2022-03-02 00:00:00'
            ])*/
            ->andWhere(['BETWEEN', 'point_order.created_datetime',
                new \yii\db\Expression('(NOW() - INTERVAL 1 MONTH)'),
                new \yii\db\Expression('NOW()')
            ])      
            ->cache(7200)
            ->limit(20);
            //->andWhere(['condo_unit.status' => 'A'])
            //->asArray()->all();    

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder'=>'point_order.order_num ASC'],
            'sort'=> ['defaultOrder' => ['order_num'=>SORT_ASC]],
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $this->load($params);


        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'order_num' => $this->order_num,
            //'card_id' => $this->card_id,
            //'full_name' => $this->full_name,
            //'order_status_id' => $this->order_status,
            //'painter_login_id' => $this->painter_login_id,
            'created_datetime' => $this->created_datetime
        ]);
        /*$query->andFilterWhere(['like', 'created_datetime', $this->created_datetime]);
        $query->andFilterWhere(['like', 'painter_ic', $this->painter_ic])
            ->andFilterWhere(['like', 'card_id', $this->card_id])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'dealer_invoice_no', $this->dealer_invoice_no])
            ->andFilterWhere(['like', 'dealer_invoice_date', $this->dealer_invoice_date])     
            ->andFilterWhere(['like', 'customer_name', $this->customer_name]);*/

        return $dataProvider;
    }
    
    public function searchall($params)
    {
        set_time_limit(0);
        Yii::$app->cache->flush();
        $query = PointOrder::find()
        ->select(['order_id','order_num','painter_login_id'])
        ->limit(1);
        
        //$query = PointOrder::find();
        //$query->joinWith(['profile']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            /*'pagination' => [
                'pageSize' => 20, 
            ],*/
            //'sort'=> ['defaultOrder' => ['order_num'=>SORT_ASC]]
            'sort'=> ['defaultOrder' => ['order_num'=>SORT_DESC]],
            //'sort'=> ['defaultOrder' => ['order_id'=>SORT_DESC]]
        ]);
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
             //$query->where('0=1');
            return $dataProvider;
        }

        if(empty($this->globalSearch)) {
            $query->where('0=1');
        }
        

            
        $query->orFilterWhere(['=', 'order_num', $this->globalSearch])    
            ->orFilterWhere(['like', 'card_id', $this->globalSearch])
            ->orFilterWhere(['like', 'full_name', $this->globalSearch]);
          
        
        
        return $dataProvider;
    }
    
    public function searchonbhalf($params)
    {
        $query = PointOrder::find();
        $query->joinWith(['dealerOutlet','profile','orderStatus']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20, 
            ],
            'sort'=> ['defaultOrder' => ['order_status'=>SORT_DESC]]
            //'sort'=> ['defaultOrder' => ['order_id'=>SORT_DESC]]
        ]);
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'order_num' => $this->order_num,
            'order_dealer_id' => $this->order_dealer_id,
            'card_id' => $this->card_id,
            'full_name' => $this->full_name,
            //'customer_name' => $this->customer_name, 
            'order_status_id' => $this->order_status,
            'painter_login_id' => $this->painter_login_id,
            'order_qty' => $this->order_qty,
            'order_total_point' => $this->order_total_point,
            'order_total_amount' => $this->order_total_amount,
            'redemption' => $this->redemption,
            //'created_datetime' => $this->created_datetime,
            'created_by' => $this->created_by,
            'updated_datetime' => $this->updated_datetime,
            'updated_by' => $this->updated_by,
        ]);
        $query->andFilterWhere(['like', 'point_order.created_datetime', $this->created_datetime]);
        $query->andFilterWhere(['like', 'painter_ic', $this->painter_ic])
            ->andFilterWhere(['like', 'order_status_id', $this->order_status])
            ->andFilterWhere(['like', 'order_remarks', $this->order_remarks])
            ->andFilterWhere(['like', 'card_id', $this->card_id])
            ->andFilterWhere(['like', 'full_name', $this->full_name])    
            ->andFilterWhere(['like', 'customer_name', $this->customer_name])            
            ->andFilterWhere(['like', 'order_IP', $this->order_IP]);

       /* echo '<pre>';
        print_r($query);
        die();*/
        //$dataProvider->setTotalCount(1000);
        return $dataProvider;
    }
    
    public function searchtra($params)
    {
        $query = PointOrder::find();
        $query->joinWith(['dealerOutlet','profile','orderStatus','orderItems']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['order_status'=>SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'order_num' => $this->order_num,
            'order_dealer_id' => $this->order_dealer_id,
            'card_id' => $this->card_id,
            'full_name' => $this->full_name,
            //'customer_name' => $this->customer_name, 
            'order_status_id' => $this->order_status,
            'painter_login_id' => $this->painter_login_id,
            'order_qty' => $this->order_qty,
            'order_total_point' => $this->order_total_point,
            'order_total_amount' => $this->order_total_amount,
            'redemption' => $this->redemption,
            //'created_datetime' => $this->created_datetime,
            'created_by' => $this->created_by,
            'updated_datetime' => $this->updated_datetime,
            'updated_by' => $this->updated_by,
        ]);
        $query->andFilterWhere(['like', 'point_order.created_datetime', $this->created_datetime]);
        $query->andFilterWhere(['like', 'painter_ic', $this->painter_ic])
            ->andFilterWhere(['like', 'order_status_id', $this->order_status])
            ->andFilterWhere(['like', 'order_remarks', $this->order_remarks])
            ->andFilterWhere(['like', 'card_id', $this->card_id])
            ->andFilterWhere(['like', 'full_name', $this->full_name])    
            ->andFilterWhere(['like', 'customer_name', $this->customer_name])            
            ->andFilterWhere(['like', 'order_IP', $this->order_IP]);

       /* echo '<pre>';
        print_r($query);
        die();*/
        return $dataProvider;
    }
    
    public function searchbyuser($params)
    {
        //$query = PointOrder::find()->select('painter_login_id')->distinct();
        //$query = PointOrder::find()->groupBy('painter_login_id');
        //$query->joinWith(['dealerOutlet','profile','orderStatus']);
        // add conditions that should always apply here
        
        $query = PointOrder::find()->joinWith([
            'profile' => function($query) {
                $query->select(['painter_profile.id','painter_profile.user_id','painter_profile.card_id','painter_profile.full_name']);
                //$query->select(['profile.id','profile.username'])->where(['user.status' => 'A']);
            }])
            ->select(['point_order.order_id','point_order.order_num','point_order.painter_login_id','point_order.order_total_point','point_order.order_total_amount','point_order.order_status','point_order.created_datetime'])
            ->where(['point_order.order_status'=> 17])            
            ->andWhere(['point_order.redemption' => 'N'])
            ->groupBy('painter_login_id');
            
        //$query = PointOrder::find()->groupBy('painter_login_id');
        //$query->joinWith(['dealerOutlet','profile','orderStatus']);    

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['order_status'=>SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_id' => $this->order_id,
            'order_num' => $this->order_num,
            'order_dealer_id' => $this->order_dealer_id,
            'card_id' => $this->card_id,
            'full_name' => $this->full_name,
            //'customer_name' => $this->customer_name, 
            'order_status_id' => $this->order_status,
            'painter_login_id' => $this->painter_login_id,
            'order_qty' => $this->order_qty,
            'order_total_point' => $this->order_total_point,
            'order_total_amount' => $this->order_total_amount,
            'redemption' => $this->redemption,
            //'created_datetime' => $this->created_datetime,
            'created_by' => $this->created_by,
            'updated_datetime' => $this->updated_datetime,
            'updated_by' => $this->updated_by,
        ]);
        $query->andFilterWhere(['like', 'point_order.created_datetime', $this->created_datetime]);
        $query->andFilterWhere(['like', 'painter_ic', $this->painter_ic])
            ->andFilterWhere(['like', 'order_status_id', $this->order_status])
            ->andFilterWhere(['like', 'order_remarks', $this->order_remarks])
            ->andFilterWhere(['like', 'card_id', $this->card_id])
            ->andFilterWhere(['like', 'full_name', $this->full_name])    
            ->andFilterWhere(['like', 'customer_name', $this->customer_name])            
            ->andFilterWhere(['like', 'order_IP', $this->order_IP]);

       /* echo '<pre>';
        print_r($query);
        die();*/
        return $dataProvider;
    }
    
}
