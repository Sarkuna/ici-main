<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SMSQueue;

/**
 * SMSQueueSearch represents the model behind the search form about `common\models\SMSQueue`.
 */
class SMSQueueSearch extends SMSQueue
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sms_queue_id', 'painterID', 'r_points'], 'integer'],
            [['name', 'mobile', 'redemption_invoice_no', 'data', 'email_template_id', 'date_to_send', 'created_datetime', 'status'], 'safe'],
            [['r_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SMSQueue::find()->groupBy('painterID');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            //'sms_queue_id' => $this->sms_queue_id,
            //'painterID' => $this->painterID,
            //'r_points' => $this->r_points,
            //'r_amount' => $this->r_amount,
            //'date_to_send' => $this->date_to_send,
            //'created_datetime' => $this->created_datetime,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'redemption_invoice_no', $this->redemption_invoice_no])
            ->andFilterWhere(['like', 'data', $this->data])
            ->andFilterWhere(['like', 'email_template_id', $this->email_template_id])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
