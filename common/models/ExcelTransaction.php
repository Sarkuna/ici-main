<?php

namespace common\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

/**
 * This is the model class for table "excel_transaction".
 *
 * @property int $id
 * @property string $upload_date
 * @property string $desceription
 * @property int $upload_by
 */
class ExcelTransaction extends \yii\db\ActiveRecord
{
    public $excel;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'excel_transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['approval_month'], 'required'],
            [['upload_date'], 'safe'],
            [['desceription'], 'string'],
            [['upload_by'], 'integer'],
            ['excel', 'required'],
            [['excel'], 'file', 'extensions'=>['xls', 'xlsx'], 'checkExtensionByMimeType'=>false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'approval_month' => 'Redemption Payment Month',
            'upload_date' => 'Upload Date',
            'desceription' => 'Desceription',
            'upload_by' => 'Upload By',
        ];
    }
    
    public function getTotalTransection(){
        //return Comment::find()->where(['post' => $this->id])->count();
        return ExcelTransactionItem::find()->where(['auto_transaction_id' => $this->id])->count();
    }
    
    public function getTotalPending(){
        //return Comment::find()->where(['post' => $this->id])->count();
        return ExcelTransactionItem::find()->where(['auto_transaction_id' => $this->id, 'transaction_status' => 'Pending', 'status' => 'success'])->count();
    }
    public function getTotalApprove(){
        //return Comment::find()->where(['post' => $this->id])->count();
        return ExcelTransactionItem::find()->where(['auto_transaction_id' => $this->id, 'transaction_status' => 'Approve', 'status' => 'success'])->count();
    }
    public function getTotalCancel(){
        //return Comment::find()->where(['post' => $this->id])->count();
        return ExcelTransactionItem::find()->where(['auto_transaction_id' => $this->id, 'transaction_status' => 'Cancel', 'status' => 'success'])->count();
    }
    
    public function getTotalFail(){
        //return Comment::find()->where(['post' => $this->id])->count();
        return ExcelTransactionItem::find()->where(['auto_transaction_id' => $this->id, 'status' => 'fail'])->count();
    }
    
    public function getBy(){
        //return Comment::find()->where(['post' => $this->id])->count();
        $user = ProfileUser::find()->where(['user_id' => $this->upload_by])->one();
        return $user->profile_full_name;
    }
}
