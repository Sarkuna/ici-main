<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "campaign_assign_products".
 *
 * @property integer $cap_id
 * @property integer $campaign_id
 * @property integer $product_list_id
 * @property string $cap_datetime
 */
class CampaignAssignProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'campaign_assign_products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['campaign_id', 'product_list_id'], 'required'],
            [['campaign_id', 'product_list_id'], 'integer'],
            [['cap_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cap_id' => 'Cap ID',
            'campaign_id' => 'Campaign ID',
            'product_list_id' => 'Product List ID',
            'cap_datetime' => 'Cap Datetime',
        ];
    }
}
