<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tng".
 *
 * @property int $id
 * @property int $ref_id
 * @property string $customer_sap_id
 * @property string $company_name
 * @property string $staff_name
 * @property string $staff_mobile
 * @property float $total_rewards
 * @property int|null $rm50
 * @property int|null $rm100
 * @property string $tpin
 * @property string $status
 */
class Tng extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tng';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ref_id', 'customer_sap_id', 'company_name', 'staff_name', 'staff_mobile', 'total_rewards', 'tpin'], 'required'],
            [['ref_id', 'rm50', 'rm100'], 'integer'],
            [['total_rewards'], 'number'],
            [['tpin', 'status'], 'string'],
            [['customer_sap_id'], 'string', 'max' => 50],
            [['company_name'], 'string', 'max' => 255],
            [['staff_name'], 'string', 'max' => 150],
            [['staff_mobile'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ref_id' => 'Ref ID',
            'customer_sap_id' => 'Customer Sap ID',
            'company_name' => 'Company Name',
            'staff_name' => 'Staff Name',
            'staff_mobile' => 'Staff Mobile',
            'total_rewards' => 'Total Rewards',
            'rm50' => 'Rm 50',
            'rm100' => 'Rm 100',
            'tpin' => 'Tpin',
            'status' => 'Status',
        ];
    }
}
