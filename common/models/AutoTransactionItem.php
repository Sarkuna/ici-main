<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "painter_approve".
 *
 * @property integer $pa_id
 * @property integer $pa_profileid
 * @property integer $pa_userid
 * @property string $pa_status
 * @property string $pa_remark
 * @property string $created_datetime
 * @property integer $created_by
 * @property string $updated_datetime
 * @property integer $updated_by
 * @property string $pa_del
 */
class AutoTransactionItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auto_transaction_item';
    }
    
    /*public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_datetime',
                'updatedAtAttribute' => 'updated_datetime',
                'value' => new Expression('NOW()'),
            ],
            BlameableBehavior::className(),
        ];
    }*/

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['auto_transaction_id',], 'required'],
            [['auto_transaction_id'], 'integer'],
            [['comment'], 'string'],
            [['status'], 'safe'],
            [['transaction_num'], 'string', 'max' => 50],
            [['transaction_status'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    /*public function attributeLabels()
    {
        return [
            'pa_id' => 'Pa ID',
            'pa_profileid' => 'Pa Profileid',
            'pa_userid' => 'Pa Userid',
            'pa_status' => 'Status',
            'pa_remark' => 'Remark',
            'created_datetime' => 'Created Datetime',
            'created_by' => 'Created By',
            'updated_datetime' => 'Updated Datetime',
            'updated_by' => 'Updated By',
            'pa_del' => 'Pa Del',
        ];
    }*/
}
