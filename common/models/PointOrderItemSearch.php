<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PointOrderItem;

/**
 * PointOrderItemSearch represents the model behind the search form about `common\models\PointOrderItem`.
 */
class PointOrderItemSearch extends PointOrderItem
{
    public $card_id;
    public $pagination = true;
    public $limit;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'point_order_id', 'painter_login_id', 'item_bar_code', 'item_bar_point', 'item_bar_total_point', 'created_by', 'updated_by'], 'integer'],
            [['item_bar_total_value'], 'number'],
            [['limit'], 'integer', 'min' => 7, 'max' => 30],
            [['card_id','Item_status', 'Item_IP', 'created_datetime', 'updated_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PointOrderItem::find();
        $query->joinWith(['order']);
        //$query->joinWith(['dealerOutlet','profile','orderStatus']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'pagination' => [ 'pageSize' => 10 ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'item_id' => $this->item_id,
            'point_order_id' => $this->point_order_id,
            'painter_login_id' => $this->painter_login_id,
            'item_bar_code' => $this->item_bar_code,
            'item_bar_point' => $this->item_bar_point,
            'item_bar_total_point' => $this->item_bar_total_point,
            'item_bar_total_value' => $this->item_bar_total_value,
            'created_datetime' => $this->created_datetime,
            'created_by' => $this->created_by,
            'updated_datetime' => $this->updated_datetime,
            'card_id' => $this->card_id,
            'updated_by' => $this->updated_by,
        ]);
        $query->andWhere(['=','order_status', '17']);
        $query->andFilterWhere(['like', 'Item_status', $this->Item_status])
            ->andFilterWhere(['like', 'Item_IP', $this->Item_IP]);

        return $dataProvider;
    }
    
    public function search2($params)
    {
        $query = PointOrderItem::find();
        $query->joinWith(['order']);
        //$query->joinWith(['dealerOutlet','profile','orderStatus']);
        // add conditions that should always apply here
        
        if($this->pagination){
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'sort' => ['defaultOrder' =>[ 'created_datetime' => SORT_DESC ] ],
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);
        }else{
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'sort' => ['defaultOrder' =>[ 'created_datetime' => SORT_DESC ] ],
                'pagination' => false,
            ]);
        }

        

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'item_id' => $this->item_id,
            'point_order_id' => $this->point_order_id,
            'painter_login_id' => $this->painter_login_id,
            'item_bar_code' => $this->item_bar_code,
            'item_bar_point' => $this->item_bar_point,
            'item_bar_total_point' => $this->item_bar_total_point,
            'item_bar_total_value' => $this->item_bar_total_value,
            'created_datetime' => $this->created_datetime,
            'created_by' => $this->created_by,
            'updated_datetime' => $this->updated_datetime,
            'card_id' => $this->card_id,
            'updated_by' => $this->updated_by,
        ]);
        $query->andWhere(['=','order_status', '17']);
        $query->andFilterWhere(['like', 'Item_status', $this->Item_status])
            ->andFilterWhere(['like', 'Item_IP', $this->Item_IP]);
        
        //$dataProvider->setTotalCount(1000);
        // Set query limit.
        $query->limit($this->limit);
        return $dataProvider;
    }
    
    public function search3($params)
    {
        //$connection = Yii::$app->getDb();
        $sql = "SELECT pp.card_id,pp.full_name,pp.ic_no,PO.order_num, dl.customer_name, PO.dealer_invoice_no, PO.dealer_invoice_price, poi.total_qty, poi.total_qty_point, pp.created_datetime, PO.redemption FROM `point_order_item` AS poi INNER JOIN `point_order` AS PO ON poi.`point_order_id` = PO.`order_id` INNER JOIN `painter_profile` AS pp ON poi.`painter_login_id` = pp.`user_id` INNER JOIN `product_list` ON poi.`product_list_id` = `product_list`.`product_list_id` INNER JOIN `dealer_list` AS dl ON PO.`order_dealer_id` = dl.`id` WHERE (`order_status` = '17')";
        //$command = $connection->createCommand($sql);

        //$query = $command->queryAll();
        $query  =  PointOrderItem::findBySql($sql);
        if($this->pagination){
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                //'sort' => ['defaultOrder' =>[ 'order' => SORT_ASC ] ],
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);
        }else{
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                //'sort' => ['defaultOrder' =>[ 'order' => SORT_ASC ] ],
                'pagination' => false,
            ]);
        }
        
        return $dataProvider;
    }
}
